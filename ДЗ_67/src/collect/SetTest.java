package collect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

public class SetTest {

	public static void main(String[] args) {


		ArrayList<Client> innerClients = new ArrayList<Client>();
		innerClients.add(new Client(1, "������ ���� ��������"));
		innerClients.add(new Client(2, "������ ���� ��������"));
		innerClients.add(new Client(3, "������� ����� ��������"));
		innerClients.add(new Client(4, "������� ����� ���������"));
		innerClients.add(new Client(5, "������ ����� ���������"));
		innerClients.add(new Client(5, "������ ����� ���������"));
		innerClients.add(new Client(7, "������ ����� ���������"));
		
		TreeSet<Client> treeSetClients = new TreeSet<Client>(innerClients);
		
		HashSet<Client> hashSetClients = new HashSet<Client>(innerClients);
		System.out.println(hashSetClients);
		
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		
		
		
		
	}

}


class Client implements Comparable<Client>{
	
	Integer iD;
	String fio;
	
	public Client(Integer iD, String fio) {
		super();
		this.iD = iD;
		this.fio = fio;
	}

	@Override
	public String toString() {
		return "ClientInner [iD=" + iD + ", ���=" + fio + "]";
	}

	@Override
	public int compareTo(Client Client_2) {
		return this.iD.compareTo(Client_2.iD);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fio == null) ? 0 : fio.hashCode());
		result = prime * result + ((iD == null) ? 0 : iD.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (fio == null) {
			if (other.fio != null)
				return false;
		} else if (!fio.equals(other.fio))
			return false;
		if (iD == null) {
			if (other.iD != null)
				return false;
		} else if (!iD.equals(other.iD))
			return false;
		return true;
	}

	
	
}