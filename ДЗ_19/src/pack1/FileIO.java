package pack1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import arrays.Mass;

public class FileIO {

	public static void main(String[] args) throws FileNotFoundException, IOException {

		/*
		String StringFromFile = Mass.readStringFromDisk("C:\\dz_19_3.txt");
		System.out.println("����������� ������:");
		System.out.println(StringFromFile);
		
		String[] decomposedString = Mass.decomposeStringIntoArrayOfSubstrings(StringFromFile, " ");
		System.out.println("����������� ������������ ������:");
		Mass.showStringArray(decomposedString);
		
		String[] reversedArrayOfString = Mass.reverseArrayOfString(decomposedString);
		System.out.println("���������������:");
		Mass.showStringArray(reversedArrayOfString);
		
		Mass.writeFileToDisk("C:\\��_19������.txt", reversedArrayOfString, true);
		*/
		
		/*
		BufferedReader bufferedReader = null;
		BufferedWriter bufferedWriter = null;
		
		bufferedReader = new BufferedReader(new FileReader("src/forum.log"));
		bufferedWriter = new BufferedWriter(new FileWriter("src/dz_19_1.txt"));
		
		String[] strBuffer = new String[10];
		for (int i = 0; (i < 10) && ((strBuffer[i] = bufferedReader.readLine()) != null); i++) {}
		
		for (int i = 0; i < strBuffer.length; i++) {
			try {
				bufferedWriter.append(strBuffer[i]);
				bufferedWriter.newLine();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		bufferedReader.close();
		bufferedWriter.close();
		*/
		
		FileIO fileIO = new FileIO();
		fileIO.dz_19_task_2_1("C:\\dz_19_3.txt", "C:\\��_19������.txt");
		
	}
	
	private void dz_19_task_2_1(String pathToRead, String pathToWrite) throws FileNotFoundException, IOException {
		
		String StringFromFile = Mass.readStringFromDisk(pathToRead);
		System.out.println("����������� ������:");
		System.out.println(StringFromFile);
		
		String[] decomposedString = Mass.decomposeStringIntoArrayOfSubstrings(StringFromFile, " ");
		System.out.println("����������� ������������ ������:");
		Mass.showStringArray(decomposedString);
		
		String[] reversedArrayOfString = Mass.reverseArrayOfString(decomposedString);
		System.out.println("���������������:");
		Mass.showStringArray(reversedArrayOfString);
		
		Mass.writeFileToDisk(pathToWrite, reversedArrayOfString, true);
		
	}

}
