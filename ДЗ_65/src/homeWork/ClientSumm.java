package homeWork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

import bankSystem.Client;

public class ClientSumm {

	public static void main(String[] args) throws IOException {

		ArrayList<Client> arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
		
		ClientColl treeSetOfClients = new ClientColl(new TreeSet<Client>(arrayListOfClients));
		
		System.out.println(treeSetOfClients.myGetSummFromAllClients());
		
	}

}
