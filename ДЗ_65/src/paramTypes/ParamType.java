package paramTypes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import collect.Collect;
import bankSystem.BankCard;
import bankSystem.BankMain;
import bankSystem.Bankomat;
import bankSystem.Client;
import bankSystem.Person;
import bankSystem.Staff;
import bankSystem.Summ;

public class ParamType <T1, K1> {

	T1 fld1;
	K1 fld2;
		
	public ParamType(T1 fld1, K1 fld2) {
		super();
		this.fld1 = fld1;
		this.fld2 = fld2;
	}
	
	public ParamType() {
		super();
	}
	
	@Override
	public String toString() {
		return "ParamType [fld1=" + fld1 + ", fld2=" + fld2.toString() + "]";
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws FileNotFoundException, IOException {

		BankMain bankMain = new BankMain();
		
		Long summ = 5000L;
		
		for (Iterator iterator = bankMain.arrayListOfClients.iterator(); iterator.hasNext();) {
			
			Client client = (Client) iterator.next();
			
			BankCard bk = client.getBankCard();
			if (bk == null) {
				iterator.remove();
			} else {
				bk.getSumm().setSumm(summ);
			}
			
			summ = summ + 1000;
			
		}
		
		Vector<ParamType<Integer, Client>> vectorClient = new Vector<ParamType<Integer,Client>>();
		Vector<ParamType<Integer, Staff>> vectorStaff = new Vector<ParamType<Integer,Staff>>();
		Vector<ParamType<Integer, BankCard>> vectorBankCard = new Vector<ParamType<Integer,BankCard>>();
		Vector<ParamType<Integer, Bankomat>> vectorBankomat = new Vector<ParamType<Integer,Bankomat>>();
		
//		ArrayList<Bankomat> arrOfBancomats = Bankomat.makeCollBankomat("src/bancomats.in");
//		ArrayList<Staff> arrOfStaffs = Staff.makeCollStaff("src/staff.in");
//		ArrayList<Client> arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
//		ArrayList<BankCard> arrayOfBankCards = BankCard.makeCollOfBankCards("src/master-visa-2014-05-14---01.in");
		
		ParamType pt = new ParamType();
		
		pt.addObjectsToVector(vectorClient, bankMain.arrayListOfClients);
		pt.addObjectsToVector(vectorStaff, bankMain.arrOfStaffs);
		pt.addObjectsToVector(vectorBankCard, bankMain.arrayOfBankCards);
		pt.addObjectsToVector(vectorBankomat, bankMain.arrOfBancomats);
		
//		System.out.println(vectorClient);
//		System.out.println(vectorStaff);
//		System.out.println(vectorBankCard);
		System.out.println(vectorBankomat);
		
//		Client etalon = new Client(null, null, null);
//		etalon.setBankCard(new BankCard(null));
//		etalon.getBankCard().setSumm(new Summ(5000L, "UAH"));
//		
//		Vector<Client> resAllMore5000 = pt.findAllMoreThanEtalon(vectorClient, etalon, new Comparator<Client>() {
//			@Override
//			public int compare(Client client1, Client client2) {
//				return client1.getBankCard().getSumm().getSumm().compareTo(
//						client2.getBankCard().getSumm().getSumm());
//			}
//		});
		
		//System.out.println(resAllMore5000);
		
		Bankomat bankomatEtalon = new Bankomat(null, null, 7000L, null);
		
//		Vector<Bankomat> resAllLess20000 = pt.findAllMoreThanEtalon(vectorBankomat, bankomatEtalon, new Comparator<Bankomat>() {
//			@Override
//			public int compare(Bankomat bnmt1, Bankomat bnmt2) {
//				return bnmt1.getSumm().getSumm().compareTo(
//							bnmt2.getSumm().getSumm());
//			}
//		});
//		
//		System.out.println(resAllLess20000);
		
		Collection<Bankomat> bankomats = pt.findElementsByEtalon(bankMain.arrOfBancomats,
				bankomatEtalon,
				new Comparator<Bankomat>() {
					@Override
					public int compare(Bankomat bnmt1, Bankomat bnmt2) {
						return bnmt1.getSumm().getSumm().compareTo(
								bnmt2.getSumm().getSumm());
					}
				},
				"<");
		
		bankomatEtalon = new Bankomat(null, "�������� Fozzi", 7000L, null);
		Collection<Bankomat> bankomats2 = pt.findElementsByEtalon(bankMain.arrOfBancomats,
				bankomatEtalon,
				new Comparator<Bankomat>() {
					@Override
					public int compare(Bankomat bnmt1, Bankomat bnmt2) {
						return bnmt1.getAddress().compareTo(
								bnmt2.getAddress());
					}
				},
				"==");
		
		System.out.println(bankomats2);
		
//		Client etalon = new Client("������", null, null);
//		etalon.setBankCard(new BankCard(null));
//		etalon.getBankCard().setSumm(new Summ(5000L, "UAH"));
//		
//		Collection<Client> clients = pt.findElementsByEtalon(bankMain.arrayListOfClients,
//				etalon,
//				new Comparator<Client>() {
//					@Override
//					public int compare(Client client1, Client client2) {
//						return client1.getSurename().compareTo(client2.getSurename());
//					}
//				},
//				"==");
//		
//		System.out.println(clients);
		
		Client etalon = new Client("������", null, null);
		etalon.setBankCard(new BankCard(null));
		etalon.getBankCard().setSumm(new Summ(5000L, "UAH"));
		
		Collection<Client> clients = pt.findElementsByEtalon(bankMain.arrayListOfClients,
				etalon,
				new Comparator<Client>() {
					@Override
					public int compare(Client client1, Client client2) {
						return client1.getBankCard().getSumm().getSumm().compareTo(
								client2.getBankCard().getSumm().getSumm());
					}
				},
				">");
		
		System.out.println(clients);
		
//		2.1.2. ��������� ���� ����� � �����() � ������� �� ������ ���� ������� (� equals() 
//				�������� ���������� ������ �������, ��� ����� �������� �� �������� �������� ��� 
//				�������� � ��������� �������, ������� � ������ ������ ����� ������ �������������)

	
//		TreeSet<Client> treeClients = new TreeSet<Client>(new Comparator<Client>() {
//			@Override
//			public int compare(Client client1, Client client2) {
//				return client1.getSurename().compareTo(client2.getSurename());
//			}
//		});
//		
//		treeClients.addAll(bankMain.arrayListOfClients);
//		
//		System.out.println(treeClients);
//		
//		ParamType<Object, Object> paramType = new ParamType<Object, Object>();
//		paramType.filterByEtalon(treeClients, new Client("�������", "�����", "�����������"));
//		
//		System.out.println(treeClients);
		
//		2.1.3. ��������� ���� �� ����� � �����() � ������� �� ������ ��� ��������� � 16-��������
//		������� 1 (� equals() �������� ���������� ������ 16-�������� �����, ��� ����� �������� 
//		�� �������� �������� ��� �������� � ��������� ��������, ������� � ������ ������ ����� 
//		������ �������������)
		
//		TreeSet<BankCard> treeBankCards = new TreeSet<BankCard>(new Comparator<BankCard>() {
//			@Override
//			public int compare(BankCard card1, BankCard card2) {
//				return card1.getCardNumber().compareTo(card2.getCardNumber());
//			}
//		});
//		
//		treeBankCards.addAll(bankMain.arrayOfBankCards);
//		
//		System.out.println(treeBankCards);
//		
//		ParamType<Object, Object> paramType2 = new ParamType<Object, Object>();
//		paramType2.filterByEtalon(treeBankCards, new BankCard(1111111111111111L));
//		
//		System.out.println(treeBankCards);
		

//		2.1.4. ��������� ���� ����� � �����() � ������� �� ������ ��� ��������� ������� 1 
//		( � equals() �������� ���������� ������ ����� ����������, ��� ����� �������� �� ��������
//		�������� ��� �������� � ��������� ����������, ������� � ������ ������ ����� ������ �������������))
		
//		TreeSet<Bankomat> treeBankomats = new TreeSet<Bankomat>(
//				new Comparator<Bankomat>() {
//					@Override
//					public int compare(Bankomat card1, Bankomat card2) {
//						return card1.getNumInvent().compareTo(
//								card2.getNumInvent());
//					}
//				});
//
//		treeBankomats.addAll(bankMain.arrOfBancomats);
//
//		System.out.println(treeBankomats);
//
//		ParamType<Object, Object> paramType2 = new ParamType<Object, Object>();
//		paramType2.filterByEtalon(treeBankomats, new Bankomat("1", ""));
//
//		System.out.println(treeBankomats);
		
//		Vector<Client> clientsSourse = new Vector<Client>(bankMain.arrayListOfClients);
//		
//		Vector<Person> personsDest = new Vector<Person>();
//		
//		//pt.metTest3(clientsSourse, clientsDest);
//		pt.metTest3(clientsSourse, personsDest);
//		
//		
//		
//		System.out.println(personsDest);
		
					
	}
	
	private void addObjectsToVector(Vector<ParamType<Integer, K1>> vector, Collection<K1> array) {
		
		int i = 1;
		for (K1 elem : array) {
			ParamType<Integer, K1> paramType = new ParamType<Integer, K1>(i++, elem);
			vector.add(paramType);
		}
									
	}
	
	private Double createRandomCollArrayList(Collection<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < 1000000; i++) {
			arr.add(new Random().nextInt(1000));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
	
//	�������� ����� � ����������� � ����������, ������� � ������� �� ������ ������� �� ���������,
//	������� �������������� ������ ������� � ��������� ���������� � ������ ��� ��������, 
//	������ ������� ������ ���� �. 1� �������� � ���������, �� ������� ������. 2� �������� � 2������.
//	��� ������� ������ ������ ������������� . � �� �������� ������()
	
	<T> void filterByEtalon(TreeSet<T> inputCollection, T etalon) {
		
		ArrayList<T> listEtalon = new ArrayList<T>();
		listEtalon.add(etalon);
		
		inputCollection.removeAll(listEtalon);
					
	}
	
//	�������� ����� metTest3, ������� �� ��������� ������ <People ��� ���������> ������� ��������
//	� ��������� ������ <������ ��� ��������>, ����� 2 ��������� : �������� ��������� <People ���
//	���������> � ��������� ���������� <������ ��� ��������>, (�.�. ����� ������������ ������ 
//	������ � �������� � ���������������� (=������) �������� ���������, �� ����� ������������ �����.
//	����).
	
	<T> void metTest3(Collection<?extends T> sourseList, Collection<?super T> destList){
		
		for (T object : sourseList) {
			destList.add(object);
		}
				
		//destList.addAll(sourseList);
							
	}
	
//	3.1.1. ������� �����, ������� ����� ����� ���� ��������, � ������� �� ����� ������ ���2 
//	�������� ������ � ������� �� � ��������� ����������. ������ ����� ����� �������� � �����
//	5000 �� �����.
	
	Vector<K1> findAllMoreThanEtalon(Vector<ParamType<T1, K1>> vectorClients, K1 etalon, Comparator<K1> COMP){
		
		Vector<K1> resultClients = new Vector<K1>();
		
		for (ParamType<T1, K1> element : vectorClients) {
		
			if (COMP.compare(etalon, element.fld2) >= 0) {
				
				resultClients.add(element.fld2);
				
			}
			
		}
						
		return resultClients;
		
	}
	
//	������� ��� �������� ����� ������������� �����-������ ���� ������-������-����� � ����������������.
//	����� ����� ��� ��������� �� ������ � ����� ����� � � ��������� ���������� �������� ��� ������� 
//	�� ������-�� �������� ���������. � ������ 4 ���������:
//		 ������ �������������
//		 ������-������� � ������������ ���������� ������ ��� ���������
//		 ����������
//		 String-���� ��� ���� : >, <, >=, <=,==
//		 ��������� �������� ������� �������������� (�.�. � �������������� �����������). � �������������
//		 � ���� ��� ���, �� ������ �� �������� ������ ������
	
	<T> Collection<T> findElementsByEtalon(Collection<T> collection, T etalon, Comparator<T> COMPARATOR, String ... mark){
		
		Vector<T> resultCollection = new Vector<T>();
		String resMark = ">";
		
		if (mark.length > 0) {
			resMark = mark[0];
		} 
		
		switch (resMark) {
		case ">":
			for (T element : collection) {
				if (COMPARATOR.compare(etalon, element) < 0) {
					resultCollection.add(element);
				}
			}
			break;
		case "<":
			for (T element : collection) {
				if (COMPARATOR.compare(etalon, element) > 0) {
					resultCollection.add(element);
				}
			}
			break;
		case ">=":
			for (T element : collection) {
				if (COMPARATOR.compare(etalon, element) <= 0) {
					resultCollection.add(element);
				}
			}
			break;
		case "<=":
			for (T element : collection) {
				if (COMPARATOR.compare(etalon, element) >= 0) {
					resultCollection.add(element);
				}
			}
			break;	
		case "==":
			for (T element : collection) {
				if (COMPARATOR.compare(etalon, element) == 0) {
					resultCollection.add(element);
				}
			}
			break;
		default:
			for (T element : collection) {
				if (COMPARATOR.compare(etalon, element) < 0) {
					resultCollection.add(element);
				}
			}
			break;
		}
						
		return (Collection<T>)resultCollection;
		
	}
	
		
		
}

