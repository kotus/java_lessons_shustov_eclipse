package pack1;

import java.io.IOException;
import java.io.RandomAccessFile;

import arrays.Mass;

public class FileIO {

	
	public static void main(String[] args) throws IOException {
		
		/*
		long position = 0;
		position = lines10_25("src/forum.log", "src/������10-25.out", 10, 25);

		RandomAccessFile reader = new RandomAccessFile("src/forum.log", "r");
		reader.skipBytes((int)position);
		String str = reader.readLine();
		
		RandomAccessFile writer = new RandomAccessFile("src/������10-25.out", "rw");
		writer.write((" ------ " + str + "\n").getBytes("ISO-8859-1"));
		
		//String str2 = new String(  str.getBytes( "ISO-8859-1"),  "CP1251" );
		//System.out.println(str2);
		*/
		
		/*
		String[][] matrix = loadMatrixFromFile("src/input.txt", ",");

		Long[][] longMatrix = Mass.convertStringMatrixToLongMatrix(matrix);
		
		writeMatrixOfNumbersToDisk("src/mass2digit.in", longMatrix);
		*/
		
		/*
		Long[][] mass2 = readMatrixOfNumbersToDisk("src/mass2digit.in");
		
		Mass.showMatixOfNumbers(mass2);
		*/
		
		System.out.println("������ ����� ��� � ������: " + Integer.SIZE / 8);  //���������� ��� / 8 = ���������� ����
		System.out.println("������ ����� ����  � ������: " + Long.SIZE / 8);  //���������� ��� / 8 = ���������� ����
		
		/*
		String[][] matrix = loadMatrixFromFile("src/input.txt", ",");

		Long[][] longMatrix = Mass.convertStringMatrixToLongMatrix(matrix);
		
		writeMatrixOfIntegersToDisk("src/mass2integer.in", longMatrix);
		*/
		
		/*
		Integer intValue = readIntegerFromFile("src/mass2integer.in", 5);
		System.out.println(intValue);
		*/
		
		/*
		RandomAccessFile writer = new RandomAccessFile("src/DZ_24bytes.txt", "rw");
		//writer.skipBytes((int) writer.length());
		writer.write("������� �������� ������� � ����� �����".getBytes("CP1251"));
		*/
				
	}
	
	public static long lines10_25(String inputFile, String outputFile, int beginLine, int lineCount) 
			throws IOException{
		
		RandomAccessFile reader = new RandomAccessFile(inputFile, "r");
		RandomAccessFile writer = new RandomAccessFile(outputFile, "rw");
		
		long position = 0;
		
		String[] strArray = new String[lineCount-beginLine];
		int arrayIndexCounter = 0;
		
		for (int i = 1; i < lineCount; i++) {
						
			if (i < beginLine) {
				reader.readLine();
				continue;
			} 
			
			strArray[arrayIndexCounter] = reader.readLine();
			arrayIndexCounter++;
			
			if (i == lineCount - 1){
				
				position = reader.getFilePointer();
				
			}
			
		}
				
		
		for (int i = 0; i < strArray.length; i++) {
			writer.write((strArray[i]  + "\n").getBytes("ISO-8859-1"));
		}
		
		return position;
		
	}

	public static String[][] loadMatrixFromFile(String path, String delimetr) throws IOException {
		
		RandomAccessFile reader = new RandomAccessFile(path, "r");
		
		if (reader.length() == 0) {
			return null;
		}
		
		int rowCount = 0;
		int colCount = 0;
		String strTemp = "";
		
		while((strTemp = reader.readLine()) != null){
			if (colCount < strTemp.split(delimetr).length){
				colCount = strTemp.split(delimetr).length;
			}
			rowCount++;
		}
				
		reader.seek(0);
		
		String[][] matrix = new String[rowCount][colCount];
		
		for (int i = 0; (strTemp = reader.readLine()) != null; i++) {
			String[] arrOfString = strTemp.trim().split(delimetr);
			for (int j = 0; j < arrOfString.length; j++) {
				matrix[i][j] = new String(arrOfString[j].getBytes("ISO-8859-1"), "CP1251");
			}
		}
				
		return matrix;
		
	}
	
	public static void writeMatrixOfNumbersToDisk(String path, Long[][] inputMatrix) throws IOException{
		
		RandomAccessFile rafWriter = new RandomAccessFile(path, "rw");
		
		for (int i = 0; i < inputMatrix.length; i++) {
			for (int j = 0; j < inputMatrix[i].length; j++) {
				rafWriter.writeLong(inputMatrix[i][j]);
			}
		}
						
	}
	
	public static Long[][] readMatrixOfNumbersFromDisk(String path) throws IOException{
		
		RandomAccessFile rafReader = new RandomAccessFile(path, "rw");
						
		Long[][] resultMatrix = new Long[3][5];
		Long[] linesPositions = new Long[3];
		
		for (int i = 0; i < resultMatrix.length; i++) {
			for (int j = 0; j < resultMatrix[i].length; j++) {
				linesPositions[i] = rafReader.getFilePointer();
				resultMatrix[i][j] = rafReader.readLong();
			}
		}
		
		return resultMatrix;
			
	}
	
	public static void writeMatrixOfIntegersToDisk(String path, Long[][] inputMatrix) throws IOException{
		
		RandomAccessFile rafWriter = new RandomAccessFile(path, "rw");
		
		for (int i = 0; i < inputMatrix.length; i++) {
			for (int j = 0; j < inputMatrix[i].length; j++) {
				rafWriter.writeInt(Integer.parseInt(inputMatrix[i][j].toString()));
			}
		}
						
	}
	
	public static Integer readIntegerFromFile(String path, Integer numberPosition) throws IOException {
		
		RandomAccessFile rafReader = new RandomAccessFile(path, "r");
		
		rafReader.skipBytes(Integer.SIZE / 8 * (numberPosition - 1));
		
		return rafReader.readInt();
		
	}
	
}
