package bankSystem;

import arrays.Mass;

public class Account {
	
	private Long accountNumber;
	private Summ summ;
	private Byte accCod; 
	private Client client;
	
	public static Long lastAccNum;
	
	static {
		Account.lastAccNum = 0L;
	}
	
	public Long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public Byte getAccCod() {
		return accCod;
	}
	public void setAccCod(Byte accCod) {
		this.accCod = accCod;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	
	public Account(){
		this.accCod = 0;
		this.accountNumber = null;
		this.summ = new Summ(0L, "UAH");
	}
	
	public Account(Long accountNumber, Long summ, String valuta, Client client){
		this.accountNumber = accountNumber;
		this.summ = new Summ(summ, valuta);
		this.client = client;
	}
	
	public Account(Long summ, String valuta, Client client) {
	
		this.summ = new Summ(summ, valuta);;
		this.client = client;
	
		Account.lastAccNum++;
		this.accountNumber = Account.lastAccNum;
		
	}
	
	public static void main(String args[]){
		
		//System.out.println(new Account(1234567890L, 1000L, "UAH"));
		/*
		String[][] matrixOfClients = Mass.AddArr(
				Mass.AddArr(Mass.arr1, Mass.arr2), Mass.arr3);
		Client[] arrOfClients = Client.makeMassClient(matrixOfClients);

		Client.testMassClient(arrOfClients);

		System.out.println("����� ����� ���������� �������:");
		System.out.println(arrOfClients[(arrOfClients.length - 1)].getAccount()
				.getAccountNumber());

		System.out.println("�������� ���� lastAccNum:");
		System.out.println(Account.lastAccNum);
        */  
		
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "����� Account:[����� ����� = "+accountNumber+";"+summ.toString()+"]";
	}
	

	
}
