package bankSystem;

import java.io.IOException;

import parser.ParserOfBancomats;
import arrays.Mass;

public class Bankomat {

	private String numInvent;
	private String address;
	private Summ summ;
	private Integer errorCode;
	private static int bmatCount;
	private static int bmatErrNum;
	
	static{
		Bankomat.bmatCount = 0;
		Bankomat.bmatErrNum = 0;
	}
	
	public static int getBmatErrNum() {
		return bmatErrNum;
	}
	public static void setBmatErrNum(int bmatErrNum) {
		Bankomat.bmatErrNum = bmatErrNum;
	}
	public static int getBmatCount() {
		return bmatCount;
	}
	public static void setBmatCount(int bmatCount) {
		Bankomat.bmatCount = bmatCount;
	}
	public String getNumInvent() {
		return numInvent;
	}
	public void setNumInvent(String numInvent) {
		this.numInvent = numInvent;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public static void main(String args[]) throws IOException{
		
		/*
		Bancomat bancomat = new Bancomat("1", "���. ���������� ����������� �����", 1000L, "UAH");
		
		System.out.println("�� �������� � ���������:" + bancomat.getSumm().getSumm() + " " +
									bancomat.getSumm().getValuta());
		
		boolean result = bancomat.sumOut(new Summ(500L, "UAH"), 1000000000000004L);
		System.out.println(result);
		
		System.out.println("����� �������� � ���������:" + bancomat.getSumm().getSumm() + " " +
				bancomat.getSumm().getValuta());
		*/
		
		/*
		String adr [] = {"��.���������� ������� ���������",
                "��.�������� ������� ������",
                "��.������ ������� ������",
                "��.������� ������� �����",
                "��.��������� ������� �����"};
		
		Bankomat[] bancomats = Bankomat.makeMassBankomat(adr);
		Mass.showArrayOfObjects(bancomats);
		*/
		
		Bankomat[] bancomats = Bankomat.makeMassBankomat("src/bancomats.in");
		
		Mass.showArrayOfObjects(bancomats);
		
	}
		
	public Bankomat(String numInvent, String address) {
		super();
		this.numInvent = numInvent;
		this.address = address;
		this.summ = new Summ(0L, "UAH");
		Bankomat.bmatCount++;
	}
	
	public Bankomat(String numInvent, String address, Long summ, String valuta) {
		super();
		this.numInvent = numInvent;
		this.address = address;
		this.summ = new Summ(summ, valuta);
		Bankomat.bmatCount++;
	} 
	
	/**
	 * ����� ������ ������� �� ���������
	 * @param sumOutPar ���� Summ - ����� ������.
	 * @param cardNumber ���� Long - ����� ���������� �����.
	 *
	 * @return Boolean - ��������� ���������� ��������
	 */
	public Boolean sumOut(Summ sumOutPar, Long cardNumber){
		
		BankCard[] arrOfCards = BankCard.makeMassCard(BankCard.makeMassNums(10),
														"UAH", 500L);
		for (int i = 0; i < arrOfCards.length; i++) {
			if (arrOfCards[i].getCardNumber().longValue() == cardNumber
					.longValue()) {

				if (arrOfCards[i].getSumm().getSumm() - sumOutPar.getSumm() >= 0) {
					// ��������� � �����
					arrOfCards[i].getSumm().setSumm(
							arrOfCards[i].getSumm().getSumm()
									- sumOutPar.getSumm());
					// ��������� � ���������
					this.summ
							.setSumm(this.summ.getSumm() - sumOutPar.getSumm());
					return true;
				} else {
					System.out.println("������������ ����� �� �����!");
					return false;
				}

			}
		}
		System.out.println("����� �� �������!");
		return false;
		
	}
	
	/**
	 * �����, ��� �������� ������� ���� <b>Bancomat[]</b>
	 * @param arrOfAdr ���� <b>String[]</b>, ���������� ������ �������.
	 * @return arrOfBancomats ���� <b>Bancomat[]</b>, ���������� ������ �������� ���� <b>Bancomat</b>.
	 */
	public static Bankomat[] makeMassBankomat(String[] arrOfAdr) {
	
		Bankomat[] arrOfBancomats = new Bankomat[arrOfAdr.length];
		for (int i = 0; i < arrOfAdr.length; i++) {
			Bankomat bancomat = new Bankomat("0", arrOfAdr[i]);
			arrOfBancomats[i] = bancomat;
		}
		
		return arrOfBancomats;
		
	}
	
	public static Bankomat[] makeMassBankomat(String path) throws IOException {
		
		return ParserOfBancomats.createArrayOfBancomats(path);
		
	}
	
	@Override
	public String toString() {
		
		return "����� Bancomat:[����� = "+numInvent+"; ����� = "+address+";"+
				summ.toString()+"]";
	}
	

	
}
