package parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import bankSystem.Staff;

public class ParserUniversal {

	//static Client[] arrCl = new Client[14];

	public static Staff[] arrStaff;
	
	static String[] arrKeyWord = { "���", "���������", "�����" };

	// �������� �������
	public static void parserManagerClient(String fileName) throws IOException {
		
		BufferedReader strBuf = new BufferedReader(new FileReader(fileName));
		String str;
		int count = 0;
		while ((str = strBuf.readLine())!=null) {
			if (str.trim().equals("")) continue;
			count++;
		}
		
		arrStaff = new Staff[count];
		
		strBuf = new BufferedReader(new FileReader(fileName));
		
		int i = -1;
		for (; (str = strBuf.readLine()) != null;) {

			// ���� ������ ������ - �� �� ������������ ��
			if (str.trim().equals(""))
				continue;

			// ������� ������ �������� ����������
			i++;
			arrStaff[i] = new Staff();

			// �������� �������� - �������� ���������� ������ ���-��, ������ //
			// -- #
			if (str.indexOf("//") != -1) {
				str = str.substring(0, str.indexOf("//")); 
			}

			System.out.println(str); // ��� �������

			fillFIO(parseReaderUniversal(arrKeyWord, str, "���"), arrStaff[i]);

			arrStaff[i].setPost(parseReaderUniversal(arrKeyWord, str, "���������").trim());
			arrStaff[i].setDepartment(parseReaderUniversal(arrKeyWord, str, "�����").trim());

			// ������� ���� � ������� �� �������
			System.out.println(arrStaff[i]);

		}// for - ������� � ���������� ������ �����

	}// manager

	// ������������� �����
	static String parseReaderUniversal(String[] arrKeyWordsPar,
			String strFromFile, String keyWordNeed) {

		// ������� ������ �����������
		StringTokenizer tokzer = new StringTokenizer(strFromFile, " :\t");

		// ����� �� ������� ��������� ����� - ��� ����� ������ ������ ����
		while (tokzer.countTokens() > 0) {
			// ��������� ��������� ����� ������
			String word = tokzer.nextToken();
			// ���� ������� ����� ����� ������� ��������� ����� - �� ������
			// ������ ��� ����
			if (word.equalsIgnoreCase(keyWordNeed))
				// ������� ������ � ������ � ���������� ������ ����
				break;

		}// while - ���� ������� ������ �������� ����� - ������ ������ ����

		// ����� �� ������ ������� ��������� ����� - ��� ��������� ����
		String sentence = ""; // ������ ��� ���������� ����
		while (tokzer.countTokens() > 0) {
			// ��������� ��������� ����� ������
			String word = tokzer.nextToken();
			// ���������, �� �������� �� �� ������ ������� ��������� �����
			// ���� ������� ����� ���� � ������� ���� �������� ����
			if (isWordInArray(word, arrKeyWordsPar)) {
				// �������� ���������� ����
				break;
			} // if
			else {// ����������� ����
				sentence = sentence + " " + word;
			} // else

		} // while

		sentence = sentence.trim();
		
		if (sentence.startsWith("�")) {
			sentence = sentence.substring(1);
		}
		
		if (sentence.endsWith("�")) {
			sentence = sentence.substring(0, sentence.length()-1);
		}

		System.out.println("����������� ����: " + sentence);
		
		// ����������� ���� ������� ��� ��������� ������
		return sentence;
	}// reader

	static boolean isWordInArray(String word, String[] arrKeyWordsPar) {

		for (int i = 0; i < arrKeyWordsPar.length; i++) {
			// ���� ����� word ����� �������� �������� ������� - ������ �������
			// true
			if (word.equalsIgnoreCase(arrKeyWordsPar[i])) {
				return true;
			}// if
		}// for

		return false;
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		parserManagerClient("src/staff.in");

	}// main
	
	static void fillFIO(String strFIO, Staff staff) {
		String[] strArr = strFIO.trim().split("[ \t]");
		staff.setSurename(strArr[0]);
		staff.setName(strArr[1]);
		if (strArr.length > 2) {
			staff.setMiddleName(strArr[2]);
		}

	}

}// public class


