package lesson;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

//��������� � ���������� ����� ������ ������������ - �������.

//������� ��� - ���������� - ��, � �������� ���������, �  ��������� ��������� � ����� ������ this

//��������� ������� Action - ��� ���������� �������� ������� ��� �������� ��������� - 
//������, ���������,  ���������

public class GUI_EventProcess3  extends JFrame implements ActionListener {
//���� �������������� �������� ������ �����  �����������������- �� ������� 1-� ��������� � ������������� - 
		//����� ����� ������������� ��� ������-����������� ������� ������� ��������
		
	static JButton butt1 = new JButton("Ok");
	static JTextField tFld1 = new JTextField("���� ������� ���");
	
	static String [ ]  massStr = {"������", "������", "�������"};
	static JComboBox cBox = new JComboBox<>(massStr);
	
	static int  count=0;
	static Color oldBackground=null;
	
	public GUI_EventProcess3(String title) throws HeadlessException {  //'������� ��� ������� �� �������� ���� (������������� ������ �������)
		super(title);
		
		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);  //������� ����� ��� �������� ���� ���� ���������
		
		//setLayout(null);       //�������� ����� ���������, �� �� ��� �� �����
		setLayout( new FlowLayout()  );   //�������� ����������() - ����������, ��������� ���������� ����� �������
		
		//this.add(butt1);
		add(butt1);   //��� ����� ������ �� ������ ����� ���������� ������ �� ����� ������� ������ ����, �� ������� ����������� �� �����
				            //���� �� ������ - ��� �������� ������� ������ - ������ ����� ������� ����  - �.�. Action-�������
		
		//1. �������� �������  �������  add<...>Listener(), �������� ����� ������ - �� ����������, ������� �������  ����� ����������, 
		//�.�. �������� ������ �� ����������, � �������  �� ������������ ���� �������
		//ActionListener - ��������, ����� ������� � ����� ������������ � ����������
		//2. � ������� ������� � ��������� ������� �����, � ������� ����� ���������� (��� this  ���  ��� ����� �����)
		//3. ���� �������������� �������� ������ addActionListener (��� ����� ������ ��� 1� ������������� ����� ������) 
		//- ����� �������� ��������� ���������  � ������������� - ���������������� ActionListener - 
		//- ����� ����� ������ ����� ����� ������ ��������� implements ActionListener
		//4. ���� �������������� ��� ������ ������ (��� ����� ������ ��� ������ ������������� ����� ������ addActionListener), ��
		//-����� �������� ������ ��������� ����������� - �������� �������������������� ������
		//5. ����� ������������    �������� (��������)  ������ �����������
		butt1.addActionListener( this );  //� ��������� ���� ������� ������� - �.�. �����, ������� ������� ����� ������������ 
																	//� � ������������ �������
															//this - ��������, ��� ���������� ������� ����� � ���� ������
		
		//������� ��������� ������� ����� ������  �  ���������
		add(cBox);
		cBox.addActionListener(  this );
		
		add(tFld1);
		//��� ��������� ������� � ���������� ������������ ����� ���� ������ ���������� ������ ������
		//��� �������� ������� ���� ������� �����. ���������� ������ ���� ��� ���������� ������� ���
		tFld1.addActionListener(this);
		
	
		
		//--------------------------------------- ���� 31 -------------------------------------------------
		//��������� ������� ���� MouseListener (MouseEvent) - ������� ������� ���� � ����  ���������
		//������: ���������� ���� ���� �� ������ � ������� MouseListener
				//���������(��� ������ ������ , ������� ����� ������������ ��� ������ - � ������ ����������� ����������)
		
		//System.out.println("������������ ?");
		NotMyListeners   objNotMyListeners = new  NotMyListeners();   
		butt1.addMouseListener(  objNotMyListeners   /* NotMyListeners */);  //� ��������� ������ ���� ������, � �� �����(���)
																				//this �������� �� ����� - � ������� ����, � ������� ���� �������
		cBox.addMouseListener( objNotMyListeners  );
		tFld1.addMouseListener(  objNotMyListeners );
		
			
		
			setVisible(true);
	} //construcror



	public static void main(String[] args) {
		// TODO Auto-generated method stub
			new  GUI_EventProcess3("��������� ������� � �����");
	}


	@Override   //���������� ������������ ����� - �������� ������� ����������, 
	//���� ������ � ������� ����� ������������ ���������
	//����� ����� ������ ��������:    ���  � ��������
	public void actionPerformed(ActionEvent   event) {
		// TODO Auto-generated method stub
		
		//���������� ������� ����� ����
		//��� ������� ����� ������� ���  �����, �������  ��������� ������ �������� ����� ����� �����
		
		//������:  ����� ����� ����� �� butt1  ������� �� ������� "��� ���� �� ������ butt1"
		//  System.out.println("��� ���� �� ������ butt1.");
		
		//������:  ����������  ����� ���������� ����� ����������  �������, 
		//� ������� ������ ��������� �� �������
		System.out.println(event.paramString()  );
		//if (event.getActionCommand().equals("Ok") )  //������ ������ ��� ������� ������ �� �������, �.�. ���������� �������� ������ 
		
		if ( event.getSource() ==  butt1  )   //�������� ����� ��������� ������� � ������� ������ butt1
				System.out.println("��� ���� �� ������ butt1.");
		else if ( event.getSource() ==  cBox  ) { //���� ��� ���� �� ����������
				//�������� ������, �� ������� ���� ������ �����
				String userItemSel = (String) cBox.getSelectedItem();
				System.out.println("��� ���� ��� ������ � ����������. �������: " + userItemSel);
				
				}
		else if ( event.getSource() ==  tFld1  )   { //���� ��� ����� �����  ��� ������� � ����������
				String userTextTfld = (String) tFld1.getText();
				System.out.println("���� ������� ������  � ����������. ��� ���� � ���������� ���� ��������: " +
				                                  userTextTfld);
				}
				
				//�������� ��������� � ��� �� �����
				//�� �������, ��� ���� ������� � ��() ������������ , �� ��� ������� ���������� ifelse
		
	}  //����� ������-����������� �������




}//class
