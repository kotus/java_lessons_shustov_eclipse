package operations;

import bankSystem.BankCard;
import bankSystem.Bankomat;
import bankSystem.Staff;

public class OperCashIn extends Operation{

	private Bankomat side1;
	private BankCard side2;
	private People side3;
	
	public OperCashIn(Long iD, String date, String time, String text,
			Bankomat side1, BankCard side2, Staff maker, String phoneNumber) {
		
		super(iD, date, time, text, side1, side2, maker);
		super.setOperSub(this);
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = new People(phoneNumber);
		
	}
	
}

class People {
	
	String phoneNumber;
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public People(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}
	
}