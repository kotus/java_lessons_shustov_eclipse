package operations;

import bankSystem.BankCard;
import bankSystem.Staff;

public class OperPayBCard extends Operation{

	private BankCard side1;
	private BankCard side2;
	
	public OperPayBCard(Long iD, String date, String time, String text,
			BankCard side1, BankCard side2, Staff maker) {
		
		super(iD, date, time, text, side1, side2, maker);
		super.setOperSub(this);
		this.side1 = side1;
		this.side2 = side2;
		
	}
	
}
