package forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import bankSystem.Client;

public class PayComboBoxProcess extends MouseAdapter implements ListSelectionListener, KeyListener, ActionListener{

	@Override
	public void mouseClicked(MouseEvent event) {
		
		super.mouseClicked(event);
		
		if (event.getClickCount() == 2) {
			
			if (event.getButton() == event.BUTTON1 && 
					event.getModifiersExText(event.getModifiersEx())
					.equalsIgnoreCase("Ctrl")) {
									
				PayComboBox.winPayInside((Client) PayComboBox.list_platelhik.getSelectedValue(),
							(Client) PayComboBox.list_poluchatel.getSelectedValue(),
							Long.parseLong(PayComboBox.textField_summa.getText()));

					writeLogToDisk("src/Operations.out",
									(Client) PayComboBox.list_platelhik
											.getSelectedValue(),
									(Client) PayComboBox.list_poluchatel
											.getSelectedValue(),
									PayComboBox.textField_summa.getText());
					
					writeOperationToDisk("src/Operations.bs", 
							(Client) PayComboBox.list_platelhik
								.getSelectedValue(),
							(Client) PayComboBox.list_poluchatel
								.getSelectedValue());
				
			} 
			
		} else if (event.getButton() == event.BUTTON1) {
			
			PayComboBox.winPayInside(
					(Client) PayComboBox.list_platelhik.getSelectedValue(),
					(Client) PayComboBox.list_poluchatel.getSelectedValue(),
					Long.parseLong(PayComboBox.textField_summa.getText()));
			
			writeLogToDisk("src/Operations.out",
							(Client) PayComboBox.list_platelhik
									.getSelectedValue(),
							(Client) PayComboBox.list_poluchatel
									.getSelectedValue(),
							PayComboBox.textField_summa.getText());
			
			writeOperationToDisk("src/Operations.bs", 
					(Client) PayComboBox.list_platelhik
						.getSelectedValue(),
					(Client) PayComboBox.list_poluchatel
						.getSelectedValue());
			
		}
		
	}
	

	@Override //ListSelectionListener
	public void valueChanged(ListSelectionEvent event) {

		if (event.getSource() == PayComboBox.list_platelhik) {
			
			Client selectedPayer = (Client) PayComboBox.list_platelhik.getSelectedValue();
			PayComboBox.textField_rr_platelhik.setText(
					String.valueOf(
							selectedPayer.getAccount().getAccountNumber()));
			
		} else if (event.getSource() == PayComboBox.list_poluchatel) {
			
			Client selectedGetter = (Client) PayComboBox.list_poluchatel.getSelectedValue();
			PayComboBox.textField_rr_poluchatel.setText(
					String.valueOf(
							selectedGetter.getAccount().getAccountNumber()));			
			
		} 
		
	}

	@Override //KeyListener
	public void keyPressed(KeyEvent event) {

		if (event.getKeyChar() == '+'
				&& event.getKeyLocation() == KeyEvent.KEY_LOCATION_NUMPAD) {

			PayComboBox.winPayInside(
					(Client) PayComboBox.list_platelhik.getSelectedValue(),
					(Client) PayComboBox.list_poluchatel.getSelectedValue(),
					Long.parseLong(PayComboBox.textField_summa.getText()));
			
			writeLogToDisk("src/Operations.out",
					(Client) PayComboBox.list_platelhik
							.getSelectedValue(),
					(Client) PayComboBox.list_poluchatel
							.getSelectedValue(),
					PayComboBox.textField_summa.getText());
			
			writeOperationToDisk("src/Operations.bs", 
					(Client) PayComboBox.list_platelhik
						.getSelectedValue(),
					(Client) PayComboBox.list_poluchatel
						.getSelectedValue());

		} else {

			if (event.getSource() == PayComboBox.textField_summa) {

				allowNumbers(event);

			} else if (event.getKeyModifiersText(event.getModifiers())
					.equalsIgnoreCase("CTRL")
					&& event.getKeyText(event.getKeyCode()).equalsIgnoreCase(
							"t")) {

				PayComboBox
						.winPayInside((Client) PayComboBox.list_platelhik
								.getSelectedValue(),
								(Client) PayComboBox.list_poluchatel
										.getSelectedValue(), Long
										.parseLong(PayComboBox.textField_summa
												.getText()));

				writeLogToDisk("src/Operations.out",
								(Client) PayComboBox.list_platelhik
										.getSelectedValue(),
								(Client) PayComboBox.list_poluchatel
										.getSelectedValue(),
								PayComboBox.textField_summa.getText());
				
				writeOperationToDisk("src/Operations.bs", 
						(Client) PayComboBox.list_platelhik
							.getSelectedValue(),
						(Client) PayComboBox.list_poluchatel
							.getSelectedValue());

			}

		}

	}

	@Override //KeyListener
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override //KeyListener
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override //ActionListener
	public void actionPerformed(ActionEvent event) {

//		if (event.getSource() == PayComboBox.button_begin_tranzaction){
//			
//			PayComboBox.winPayInside((Client)PayComboBox.list_platelhik.getSelectedValue(),
//						(Client)PayComboBox.list_poluchatel.getSelectedValue(),
//							Long.parseLong(PayComboBox.textField_summa.getText()));
//				
//			writeLogToDisk("src/Operations.out", 
//					(Client)PayComboBox.list_platelhik.getSelectedValue(),
//						(Client)PayComboBox.list_poluchatel.getSelectedValue(),
//							PayComboBox.textField_summa.getText());
//				
//		}
		
		System.out.println("");
		
	}
	
	private void allowNumbers(KeyEvent event) {

		String oldText = PayComboBox.textField_summa.getText();

		if (event.getKeyChar() >= '0' && event.getKeyChar() <= '9') {

			String newText = PayComboBox.textField_summa.getText() + event.getKeyChar();
			PayComboBox.textField_summa.setText(newText);
		
		} else if (event.getKeyChar() == '.') {
			
			if (oldText.indexOf(".") == -1) {
				
				String newText = PayComboBox.textField_summa.getText() + event.getKeyChar();
				PayComboBox.textField_summa.setText(newText);
				
			}
			
		} else {

			if (event.getKeyCode() == KeyEvent.VK_BACK_SPACE) { // ��� �����
																// BackSpace

				PayComboBox.textField_summa
						.setText(PayComboBox.textField_summa.getText()
								.substring(
										0,
										PayComboBox.textField_summa.getText()
												.length() - 1));

			} else {

				PayComboBox.textField_summa.setText(oldText);
				System.out.println("�������� ������ �����!");
				
			}

		}
		
	}
	
	public boolean checkSum(String summString) { 
		
		if (summString.indexOf(".") != -1) {
			
			if (summString.substring(summString.indexOf("."), summString.length()).length()-1 == 2) {
				
				return true;
				
			} else {
				
				return false;
				
			}
			
		}
		
		return true;
		
	}
	
	public boolean writeLogToDisk(String path, Client payer, Client dest, String sum) {
		
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yy.MM.dd");
		SimpleDateFormat dateFormatTime = new SimpleDateFormat("HH:mm:ss");
		
		//(����: <YY.MM.DD>  �����: <HH:MM:SS>) ��������: ������� ������ �����  ����������: <� �����������>
		//��-����:<��/�> ����������: <� ����������>   ��-�����:<��/�>   �����:<�����>
		String resultSrtring = "(����: "+dateFormatYear.format(date) +" "+"�����: "+dateFormatTime.format(date)+") ��������: "+
			"������� ������ ����� ����������: " + 1 + " ��-����: " + payer.getAccount().getAccountNumber() + 
			" ����������: " + 1 + " ��-�����: " + dest.getAccount().getAccountNumber() + " �����: " + sum;
		
		FileWriter fileWriter = null;
		try {
			
			fileWriter = new FileWriter(new File(path), true);
			fileWriter.write(resultSrtring + "\n");
			fileWriter.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
		
	}
	
	public boolean writeOperationToDisk(String path, Client payer, Client dest) {
		
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yy.MM.dd");
		SimpleDateFormat dateFormatTime = new SimpleDateFormat("HH:mm:ss");
		
		String resultSrtring = "����: "+dateFormatYear.format(date) +" "+" | �����: "+dateFormatTime.format(date)+" | ��������: "+
			"������� ������ ����� | ����������: " + payer + " | ��-����: " + payer.getAccount().getAccountNumber() + 
			" | ����������: " + dest + " | ��-�����: " + dest.getAccount().getAccountNumber();
		
		FileWriter fileWriter = null;
		try {
			
			fileWriter = new FileWriter(new File(path), true);
			fileWriter.write(resultSrtring + "\n");
			fileWriter.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
		
	}
	
	
}
