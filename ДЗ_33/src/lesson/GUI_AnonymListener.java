package lesson;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

//��������� ������� ����������
//���������� ��������� ������� � ���
//��������� ��������  
//(//������� ���������� 1 ��� � �����, ������� ��� �� �� �����)
//+ ����� ������� ������������� � ���������� ����������, �.�. �� ���� ������ ������ ��() -���, getSource() 
//+ ������ ������������ � ������� ��������, � ������� ���, � �������, 
//� �������� ������� �������� � �������

// - ��� ����� ��������� , ������������������ ������  ���������� ������ ��������
//�.�. ��� ������������ �������������� � ����� ���������.
//- ����� ������ �� ������� ��� ������� ��������, �������� ���, � ��� ������ � ������� 
//- � ������ ������������ ������������ ������ ���������, �������� ���������� ���������� ���

public class GUI_AnonymListener extends JFrame   {
	
	JButton butt1 = new JButton("1");
	JButton butt2 = new JButton("2");
	JButton butt3 = new JButton("3");
	JButton buttPlus = new JButton("+");
	

	JTextField tFld1 = new JTextField("���� ������� ���");
	String oldTxt="";  //c����� ���������� ����������
			
	public GUI_AnonymListener(String title) throws HeadlessException { // '������� ������� �������� ����
																	
		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); 

		// setLayout(null); //�������� ����� ���������, �� �� ��� �� �����
		setLayout(new FlowLayout()); // �������� ����������() - ����������,
                 										// ��������� ���������� ����� �������

		// this.add(butt1);
		add(butt1); add(butt2); add(butt3); add(buttPlus);
		add(tFld1);
		
		
//-------------------------------������� ��������� ��������� �� ������ ���������  inner-���������� �������------------------------------
		//������: ������� ��������� ActionListener-�   ������� ���������� ������
		//1. ��������� ������ butt1
				//�� ������� 2 ������� ���������� ���  � ���
		//butt1.addActionListener(  this );
		//butt1.addActionListener(  /*��� ������ ������ implements ActionListener  � �������������*/  )
		
		setPreferredSize( new Dimension(500, 300));    //������ ����������    ��������� ������
		
		butt1.addActionListener(  new ActionListener() {   //��� ������� ������ ������� ������  BUTT1
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("���������� ���������� �������� �������1: ��� ���� �� ������ butt1.");
				
			}//���������� ����������
		}//����� �������� ����������
		) ;  //�������� �������������, �� �������� ������� ;  ����� )
	
		//��� ��������� ������� ������ butt2  ���� � ��� �������� ���� �����������  ��������� �������
		butt2.addActionListener(  new  ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("���������� ���������� �������� �������2: ��� ���� �� ������ butt2.");
			}
		});
		
		
		setVisible(true);
		
	} // construcror

	public static void main(String[] args) {
		// TODO Auto-generated method stub
					//��� ������ ���������� ������� - �� ���������, �.�. � ���� ��� �����
							//������� ���������� 1 ��� � �����, ������� ��� �� �� �����
		 new GUI_AnonymListener("��������� ��������");
	}


}// class
