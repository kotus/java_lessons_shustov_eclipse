package lesson;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

//��������� ������� ����������
//���������� keyPressed
//������� ��� - ���������� - ��, � �������� ���������, �  ��������� ��������� � ����� ������ this

public class GUI_EventKey extends JFrame implements KeyListener  {
	// ���� �������������� �������� ������ ����� �����������������- �� �������
	// 1-� ��������� � ������������� -
	// ����� ����� ������������� ��� ������-����������� ������� ������� ��������

	JButton butt1 = new JButton("Ok");
	JButton butt2 = new JButton("Cancel");

	JTextField tFld1 = new JTextField("���� ������� ���");
	String oldTxt="";  //c����� ���������� ����������
	

	String[] massStr = { "������", "������", "�������" };
	JComboBox cBox = new JComboBox<>(massStr);

	static int count = 0;
	
	public GUI_EventKey(String title) throws HeadlessException { // '������� ������� �������� ����
																	
		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); 

		// setLayout(null); //�������� ����� ���������, �� �� ��� �� �����
		setLayout(new FlowLayout()); // �������� ����������() - ����������,
										// ��������� ���������� ����� �������

		// this.add(butt1);
		add(butt1); 
		add(cBox);
		add(tFld1);
		add(butt2);
		
		// ��� ��������� ������� � ���������� ������������ ����� ���� ������
				// ���������� ������ ������
				// ��� �������� ������� ���� ������� �����. ���������� ������ ���� ���
				// ���������� ������� ���
		
		//����� ������ ��� ��� ������� �� ������� ������ � ����������, ���� ��� �������� ������
		//��� ���� ���� � �����-�� ��������� � ���� ��������� ������ ���������� , �� 
		//����� ���������� �����  �� ��������� ��������� - ����� �� ��������� ���������� ������ ������ � ���.
		//1. ���������� , ������� ����� ������������� - ������ ���� ��������� (������ ���� �������� ����������� �������� ������ �� ����)
		//2. ������ ��� ��������� ������ �������� 100% ������ ����� ������ ���������� �� ������ 
		// (�.�. ����� ���� ��� ����������  �����  ������� )
		
		tFld1.addKeyListener(this);
		
		setVisible(true);
		
				//��� 2 ������ ��� ������ ����� ������������ ����� ����, ��� ���������� ��������� �� ������
		tFld1.setFocusable(true);  //�������� ����������� ������ , �.�. ������� ���������� ���������
		tFld1.requestFocus();
		//tFld1.setFocusable(false);		//����� ����������� ������� �������� ��� ������ �����
		
		
	} // construcror

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GUI_EventKey("��������� ������� � �����");
	}

	@Override   //������� ������ �������� ������� 
	//���������, ���������� ������� � ���� ������-����������� �� ��������������
	//Extended ���� � �������() �� ��������������
	public void keyTyped(KeyEvent event) {
	
	
	}

	
	//� ���� ����������� keyPressed ��� 3 ������� ������� �� ����� ������� ��������, 
	//�� ������ - ����� ���� ��� ������
	//� ��������� - ����� �������� ���� ��������-�������� �����, ��� ����������� ��������� �������
	//�����������-��������� ������� - ������� ^ , Alt, Shift, � ��. �� �������� 
	
	@Override  //������� ����� �������, �������� � ����������
	public void keyPressed(KeyEvent event) {
		// TODO Auto-generated method stub
		System.out.println(event.paramString());
		
		//��� ������ � ���� ����������� 
	    System.out.println("getExtendedKeyCode: " + event.getExtendedKeyCode() );  // ��������, �.�. Ex 
	    System.out.println("event.getKeyText( event.getExtendedKeyCode(): " + event.getKeyText( event.getExtendedKeyCode() ) );  //����� ������  � ���� ����������� - ���� ������� �����������
	    
		System.out.println("getKeyCode:   " + event.getKeyCode() );   // //�������� ������ � ����������� ��������()
		System.out.println("event.getKeyText( event.getKeyCode() ):   "  + event.getKeyText( event.getKeyCode() ) );    //�������� ������ � ����������� ��������(), ���� ����. ����������� �����
		
		System.out.println("Location " + event.getKeyLocation() );		//����� �� ��������, ������ unknown_location? �������� � keyPressed()
		System.out.println("������ �������: " + event.getKeyChar() );   //�� ���� ������ ���� ��� ���������� ������
		System.out.println("------------------------------------------------------------------------------------------------------------");
		
		
		//������� ������� ������� �� �������� ������
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Page Up") ) {
			tFld1.setText( "���������� � �������");
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Page Down") ) {
			tFld1.setText( "���������� ������");
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Home") ) {
			tFld1.setText( "������� ���� �������");
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "End") ) {
			tFld1.setText( "������� 2� ������� ������ �������");
		}
		
	}

	@Override  //���������� ����� �������, �������� � ����������
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
	//������� ��������� � ������������
	//1. ������� ����� �������� ������ ������ ����������
	//2. ��������� ����� ������� �� �����, ����� ���� �������� ��������� � ����� ��������
	//3. ������� ������� ������� ������������ - �������� �� ������ ����������, ��� �������-������ ������ �� ���������� ����������, ��� ������� ������ � �.�.
	//�.�. ������� �� ��������, ������� ������������ ��� ����������, ������ �������� �� ��������� ����� ��������

}// class
