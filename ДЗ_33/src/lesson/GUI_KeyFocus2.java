package lesson;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

//��������� ������� ����������
//�������� ��������� ������ �����

//������� ������ - ����� ���������� ������ ������������ �����.
//������� ���, ��� ����� ������������ �� ����������, ������� � ������,
//��� ������� ����������
//���� ������ ���������� �� ����� � ������ - �� ����� �������������� �� �����
//�.�. ��� ��������� ����� ����� 2  �������:
//1. ���������� � ������
//2. � ���� ����������  ������ ���� ��������� ������� ����� keyListener

//2� ������ , ����� �������, ��  � ����� ���������� - ��� ������� ������ 1,2,3, ����� ����� ������������� �� 
//������ 1,2,3 - ����� ����� �����, ��� ������� �������  ����� �� ����� ����� 


public class GUI_KeyFocus2 extends JFrame implements KeyListener  {
	
	JButton butt1 = new JButton("1");
	JButton butt2 = new JButton("2");
	JButton butt3 = new JButton("3");
	JButton buttPlus = new JButton("+");
	

	JTextField tFld1 = new JTextField("���� ������� ���");
	String oldTxt="";  //c����� ���������� ����������
			
	public GUI_KeyFocus2(String title) throws HeadlessException { // '������� ������� �������� ����
																	
		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); 

		// setLayout(null); //�������� ����� ���������, �� �� ��� �� �����
		setLayout(new FlowLayout()); // �������� ����������() - ����������,
										// ��������� ���������� ����� �������

		// this.add(butt1);
		add(butt1); add(butt2); add(butt3); add(buttPlus);
		add(tFld1);
		
		//2� ������ ��������� ������ ��� ������ � ������:
		//����� ������� ������� �� ���������� ������ ������� ���������� � ���������� ���������� (�������)
				//������� �����: ���������� �������. � ������ ���������� ����, �� ������� ����� ���� ���� ���� ��� ���������� �����,
				//� ��� ������� �������
				//����  ����� ������ �� ������ - ����� ����� �����, ��� ������ ����� ��������� � ����� �� ��� ������������
		//������: ������� ���, ����� � ���������� ���������� ����������� �������� ��� ������ �����, 
		//� �� ������� �����.
		// ������� ���, ����� �����  ���������� ����  ����� ���������� ������� ����� 
		//� ��� ���� ����� �� ����� ���������� �� ��������������� ������, ����� ���� ����� ��� ������� ��������� 

		//1.  �� ���� �����������  ��������� keyListener()
		butt1.addKeyListener( this ); 
		butt2.addKeyListener( this ); 
		butt3.addKeyListener( this );
		buttPlus.addKeyListener( this ); 
		tFld1.addKeyListener( this );
		addKeyListener( this );   //� ����
		
		butt1.setFocusable(true);
		butt2.setFocusable(true);
		butt3.setFocusable(true);
		tFld1.setFocusable(true);
		setFocusable(true);
	
		
		//2. � ����������� ������� ����� ������������� �����  �� "�������"  �������
		//����� ��. ���������� keyPressed   �����
		
		setVisible(true);
		
	} // construcror

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GUI_KeyFocus2("��������� ������� � �����");
	}

	@Override   //������� ������ �������� ������� 
	//���������, ���������� ������� � ���� ������-����������� �� ��������������
	//Extended ���� � �������() �� ��������������
	public void keyTyped(KeyEvent event) {

	}

	@Override  //������� ����� �������, �������� � ����������
	public void keyPressed(KeyEvent event) {
		// TODO Auto-generated method stub
		System.out.println(event.paramString());
		
		//��� ������ � ���� ����������� 
	    System.out.println("getExtendedKeyCode: " + event.getExtendedKeyCode() );  // ��������, �.�. Ex 
	    System.out.println("event.getKeyText( event.getExtendedKeyCode(): " + event.getKeyText( event.getExtendedKeyCode() ) );  //����� ������  � ���� ����������� - ���� ������� �����������
	    
		System.out.println("getKeyCode:   " + event.getKeyCode() );   // //�������� ������ � ����������� ��������()
		System.out.println("event.getKeyText( event.getKeyCode() ):   "  + event.getKeyText( event.getKeyCode() ) );    //�������� ������ � ����������� ��������(), ���� ����. ����������� �����
		
		System.out.println("Location " + event.getKeyLocation() );		//����� �� ��������, ������ unknown_location? �������� � keyPressed()
		System.out.println("������ �������: " + event.getKeyChar() );   //�� ���� ������ ���� ��� ���������� ������
		System.out.println("------------------------------------------------------------------------------------------------------------");
		
		if (tFld1.getText().indexOf("����") != -1 )  //���� � ���������� ��������� ������ - �� �������� ���
			tFld1.setText("");
			
		
		//������ ������������� switch  case
				//������� � 7� ������ ����� � ����-��������� ����� ������������ ������
		switch (   event.getKeyChar()  )  {//������������ �� ����������� ������� �� �����
		case  '1':
					//�������� ������� ������ � ���������
				 tFld1.setText(  tFld1.getText() + '1');
					//����� ���������� �� ������, ��������������� ������� 1 - �.�. butt1
				// butt1.requestDefaultFocus();   //���� ����� ������������� - �.�. �� ����������� - �������� � �������������
				 butt1.requestFocus();   //��������� ������ �� ����������, ���� ����� �������� ������ ����� ��������� ���������� �� ������
			break;
		case  '2':
			 tFld1.setText(  tFld1.getText() + '2');
			 butt2.requestFocus();
			break;
		case  '3':
			 tFld1.setText(  tFld1.getText() + '3');
			 butt3.requestFocus();
			break;	
		case  '+':
			 tFld1.setText(  tFld1.getText() + '+');
			 buttPlus.requestFocus();
			break;	
		
		} //switch
		
		//������� ������� ������� �� �������� ������
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Page Up") ) {
			tFld1.setText( "���������� � �������");
			butt3.requestFocus();
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Page Down") ) {
			tFld1.setText( "���������� ������");
			butt3.requestFocus();
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Home") ) {
			tFld1.setText( "������� ���� �������");
			butt3.requestFocus();
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "End") ) {
			tFld1.setText( "������� 2� ������� ������ �������");
			butt3.requestFocus();
		}
		
	}//keyPressed

	@Override  //���������� ����� �������, �������� � ����������
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
	//������� ��������� � ������������
	//1. ������� ����� �������� ������ ������ ����������
	//2. ��������� ����� ������� �� �����, ����� ���� �������� ��������� � ����� ��������
	//3. ������� ������� ������� ������������ - �������� �� ������ ����������, ��� �������-������ ������ �� ���������� ����������, ��� ������� ������ � �.�.
	//�.�. ������� �� ��������, ������� ������������ ��� ����������, ������ �������� �� ��������� ����� ��������

}// class
