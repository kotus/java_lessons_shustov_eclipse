package lesson;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

//��������� ������� ����������
//�������� ��������� ������ �����

//������� ������ - ����� ���������� ������ ������������ �����,�.�. ���������� �� ����� �������,��� ����
//�� �� ��������, ����� ���������� ������ ������������ �����. � ������ ������� - �� ����� ���������� �������� - �� � �������� �������.
//� � ������ ����� ����������� ���.
//������� ���, ��� ����� ������������ �� ����������, ������� � ������,
//��� ������� ����������
//���� ������ ���������� �� ����� � ������ - �� ����� �������������� �� �����
//�.�. ��� ��������� ����� ����� 2  �������:
//1. ���������� � ������
//2. � ���� ����������  ������ ���� ��������� ������� ����� keyListener

//1� ������ , ����� �������, �� ����� ������� 

public class GUI_KeyFocus1 extends JFrame implements KeyListener  {
	// ���� �������������� �������� ������ ����� �����������������- �� �������
	// 1-� ��������� � ������������� -
	// ����� ����� ������������� ��� ������-����������� ������� ������� ��������

	JButton butt1 = new JButton("1");
	JButton butt2 = new JButton("2");
	JButton butt3 = new JButton("3");

	JTextField tFld1 = new JTextField("���� ������� ���");
	String oldTxt="";  //c����� ���������� ����������
			
	public GUI_KeyFocus1(String title) throws HeadlessException { // '������� ������� �������� ����
																	
		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); 

		// setLayout(null); //�������� ����� ���������, �� �� ��� �� �����
		setLayout(new FlowLayout()); // �������� ����������() - ����������,
										// ��������� ���������� ����� �������

		// this.add(butt1);
		add(butt1); add(butt2); add(butt3);
		
		//������: ������� ���, ����� � ���������� ���������� ����������� �������� ��� ������ �����, 
		//� �� ������� �����.
		//1. ������� ���, ����� ��� ������ ������ ����� �� ��������� ���������� �� ���������
		//��������� ������� ������ �� ������� 1,2,3
				///� �� ��� ��������� ����������
		butt1.setFocusable(false);
		butt2.setFocusable(false);
		butt3.setFocusable(false);
		tFld1.setFocusable(false);
		
		//2. ������� ����� �� ������ ����������,
		//������� ����� �� ����    JFRAME
		setFocusable(true);   	//��������� ������ ���� JFrame ����� ����� �� ���� , ���� ���������� �� �������� �� ���������
		this.requestFocus();  //�� ��� � ��������� ������ 		
		requestFocus();  			//2 �������� - ������� ������ �� ���� ���� �����
											//�������� ������������() �������� ����� ������ ���� �� �����
		
		//3. �  ���������� � ����   keyListener
		addKeyListener( this );  //����������  ���������� � ������ ���� �����
		
		add(tFld1);
		//tFld1.addKeyListener(this);
		
		setVisible(true);
		
				//��� 2 ������ ��� ������ ������������ ����� ����, ��� ���������� ��������� �� ������
		tFld1.setFocusable(true);  //�������� ����������� ������ , �.�. ������� ���������� ���������
		
		
		
	} // construcror

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GUI_KeyFocus1("��������� ������� � �����");
	}

	@Override   //������� ������ �������� ������� 
	//���������, ���������� ������� � ���� ������-����������� �� ��������������
	//Extended ���� � �������() �� ��������������
	public void keyTyped(KeyEvent event) {

	}

	@Override  //������� ����� �������, �������� � ����������
	public void keyPressed(KeyEvent event) {
		// TODO Auto-generated method stub
		System.out.println(event.paramString());
		
		//��� ������ � ���� ����������� 
	    System.out.println("getExtendedKeyCode: " + event.getExtendedKeyCode() );  // ��������, �.�. Ex 
	    System.out.println("event.getKeyText( event.getExtendedKeyCode(): " + event.getKeyText( event.getExtendedKeyCode() ) );  //����� ������  � ���� ����������� - ���� ������� �����������
	    
		System.out.println("getKeyCode:   " + event.getKeyCode() );   // //�������� ������ � ����������� ��������()
		System.out.println("event.getKeyText( event.getKeyCode() ):   "  + event.getKeyText( event.getKeyCode() ) );    //�������� ������ � ����������� ��������(), ���� ����. ����������� �����
		
		System.out.println("Location " + event.getKeyLocation() );		//����� �� ��������, ������ unknown_location? �������� � keyPressed()
		System.out.println("������ �������: " + event.getKeyChar() );   //�� ���� ������ ���� ��� ���������� ������
		System.out.println("------------------------------------------------------------------------------------------------------------");
		
		
		
				//������� ������� ������� �� �������� ������
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Page Up") ) {
			tFld1.setText( "���������� � �������");
			butt3.requestFocus();
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Page Down") ) {
			tFld1.setText( "���������� ������");
			butt3.requestFocus();
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "Home") ) {
			tFld1.setText( "������� ���� �������");
			butt3.requestFocus();
		}
		if (  event.getKeyText(  event.getExtendedKeyCode() ).equals( "End") ) {
			tFld1.setText( "������� 2� ������� ������ �������");
			butt3.requestFocus();
		}
		
	}

	@Override  //���������� ����� �������, �������� � ����������
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
	//������� ��������� � ������������
	//1. ������� ����� �������� ������ ������ ����������
	//2. ��������� ����� ������� �� �����, ����� ���� �������� ��������� � ����� ��������
	//3. ������� ������� ������� ������������ - �������� �� ������ ����������, ��� �������-������ ������ �� ���������� ����������, ��� ������� ������ � �.�.
	//�.�. ������� �� ��������, ������� ������������ ��� ����������, ������ �������� �� ��������� ����� ��������

}// class
