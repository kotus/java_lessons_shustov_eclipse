package forms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bankSystem.Account;
import bankSystem.Client;

public class FormNewClient extends JFrame{
	
	private static final long serialVersionUID = 2L;
	
	static JLabel label_surename = new JLabel("�������");
	static JLabel label_name = new JLabel("���");
	static JLabel label_middlename = new JLabel("��������");
	static JLabel label_numPassport = new JLabel("����� ���������");
	static JLabel label_postNumber = new JLabel("�������� ������");
	static JLabel label_identiCode = new JLabel("����������������� ���");
	static JLabel label_numberOfAccount = new JLabel("� ����/�����");
	
	static JTextField textField_surename = new JTextField("", 12);
	static JTextField textField_name = new JTextField("", 12);
	static JTextField textField_middlename = new JTextField("", 12);
	static JTextField textField_numPassport = new JTextField("", 12);
	static JTextField textField_postNumber = new JTextField("", 12);
	static JTextField textField_identiCode = new JTextField("", 12);
	static JTextField textField_numberOfAccount = new JTextField("", 12);
	
	static JButton button_createNewClient = new JButton("<html><p align='center'>������</p>"
													+ " ������ �������</html>");
	
	FormNewClientProcess clientProcess = new FormNewClientProcess();
	
	public FormNewClient() throws HeadlessException {
		super("���� ������ � ����� ������� �����");
		
		setLocation(300, 300); 
		setSize(600, 180);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE ); 
		
		JPanel mainPanel = new JPanel(new BorderLayout(2, 2));
		add(mainPanel);
		mainPanel.setBorder(new EmptyBorder(1, 3, 3, 3));
		
		JPanel centralPanel = new JPanel(new GridLayout(1, 1, 4, 2));
		mainPanel.add(centralPanel, BorderLayout.CENTER);
		
		
		JPanel hightPanel = new JPanel(new GridLayout(4, 3, 4, 2));
		centralPanel.add(hightPanel);
		
		hightPanel.add(label_surename);
		hightPanel.add(label_name);
		hightPanel.add(label_middlename);
		hightPanel.add(textField_surename);
		hightPanel.add(textField_name);
		hightPanel.add(textField_middlename);
		
		hightPanel.add(label_numPassport);
		hightPanel.add(label_postNumber);
		hightPanel.add(label_identiCode);
		hightPanel.add(textField_numPassport);
		hightPanel.add(textField_postNumber);
		hightPanel.add(textField_identiCode);
		
		JPanel lowPanel = new JPanel(new GridLayout(1, 3, 4, 2));
		mainPanel.add(lowPanel, BorderLayout.SOUTH);
		
		JPanel leftLowPanel = new JPanel(new GridLayout(2, 1, 4, 2));
		JPanel centerLowPanel = new JPanel(new GridLayout(1, 1, 4, 2));
		JPanel rightLowPanel = new JPanel(new GridLayout(1, 1, 4, 2));
		
		lowPanel.add(leftLowPanel);
		lowPanel.add(centerLowPanel);
		lowPanel.add(rightLowPanel);
		
		leftLowPanel.add(label_numberOfAccount);
		leftLowPanel.add(textField_numberOfAccount);
		
		centerLowPanel.add(new JLabel(" "));
		
		rightLowPanel.add(button_createNewClient);
		
		Client[] arrOFClients;
		try {
			arrOFClients = Client.makeMassClient("src/fio_clients.in");
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		textField_numberOfAccount.setText(String.valueOf(Account.lastAccNum + 1));
		
		textField_identiCode.addKeyListener(clientProcess);
		textField_identiCode.setEditable(false);
		textField_identiCode.setBackground(Color.WHITE);
		textField_identiCode.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1, false));
		
		textField_surename.addKeyListener(clientProcess);
		textField_surename.setEditable(false);
		textField_surename.setBackground(Color.WHITE);
		textField_surename.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1, false));
		
		textField_name.addKeyListener(clientProcess);
		textField_name.setEditable(false);
		textField_name.setBackground(Color.WHITE);
		textField_name.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1, false));
		
		textField_middlename.addKeyListener(clientProcess);
		textField_middlename.setEditable(false);
		textField_middlename.setBackground(Color.WHITE);
		textField_middlename.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1, false));
		
		button_createNewClient.addActionListener(clientProcess);
		
		setVisible(true);
		
	}

	public static void main(String[] args) {
		
		new FormNewClient();

	}

}
