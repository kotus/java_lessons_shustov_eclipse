package calculator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;





import java.io.IOException;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.BorderUIResource;

public class Calculator extends JFrame{

	//��������� ����
	static String operand1 = "";
	static String operation = "";
	static String operand2 = "";
	static Double result = 0D;
	static Double memory = 0D;
	
	static JTextField textField = new JTextField();
	
	static JButton[] arrOfButtons = new JButton[10];

	static JButton buttComa = new JButton(",");
	
	static JButton buttResult = new JButton("=");
	static JButton buttPlus = new JButton("+");
	static JButton buttMinus = new JButton("-");
	static JButton buttMultiply = new JButton("*");
	static JButton buttDevide = new JButton("/");
	
	static JButton mc = new JButton("MC");
	static JButton mr = new JButton("MR");
	static JButton ms = new JButton("MS");
	static JButton mPlus = new JButton("M+");
	static JButton mMinus = new JButton("M-");
	static JButton backspace = new JButton("<-");
	static JButton ce = new JButton("CE");
	static JButton c = new JButton("C");
	static JButton plusMinus = new JButton("�");
	static JButton sqrt = new JButton("x2");
	static JButton buttPercent = new JButton("%");
	
	public static void main(String[] args) throws HeadlessException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
	
		new Calculator("�����������");
		
	}

	public Calculator(String title) throws HeadlessException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		super(title);
		setLocation(300, 300);
		setSize(290, 250);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		Font fontOfNumbers = new Font("Arial", Font.BOLD, 15);
		Font fontOfTextField = new Font("Arial", Font.PLAIN, 35);	
		
		
		buttResult.setFont(fontOfNumbers);
		buttPlus.setFont(fontOfNumbers);
		buttMinus.setFont(fontOfNumbers);
		buttMultiply.setFont(fontOfNumbers);
		buttDevide.setFont(fontOfNumbers);
		buttComa.setFont(fontOfNumbers);
		
		
		for (int i = 0; i < arrOfButtons.length; i++) {
			arrOfButtons[i] = new JButton(String.valueOf(i));
			arrOfButtons[i].setFont(fontOfNumbers);
		}
				
		//������� ������. ������ ����� ����.
		JPanel mainPanel = new JPanel(new BorderLayout());
		add(mainPanel);
		
		textField.setHorizontalAlignment(JTextField.RIGHT);
		textField.setFont(fontOfTextField);
		mainPanel.add(textField, BorderLayout.CENTER);
				
		//��� ������ ����� �� ��-� ������� ������
		JPanel southMainPanel = new JPanel(new GridLayout(2, 1, 2, 2));
		mainPanel.add(southMainPanel, BorderLayout.SOUTH);
		
		JPanel functionPanel = new JPanel(new GridLayout(3, 5, 2, 2));
		Border borderOfFunctionPanel = new EmptyBorder(1, 1, 0, 1);
		functionPanel.setBorder(borderOfFunctionPanel);
		
		southMainPanel.add(functionPanel); // 1-� ������ southMainPanel
		functionPanel.add(mc);
		functionPanel.add(mr);
		functionPanel.add(ms);
		functionPanel.add(mPlus);
		functionPanel.add(mMinus);
		
		functionPanel.add(backspace);
		functionPanel.add(ce);
		functionPanel.add(c);
		functionPanel.add(plusMinus);
		functionPanel.add(sqrt);
		
		for (int i = 7; i <= 9; i++) {
			functionPanel.add(arrOfButtons[i]);
		}
		functionPanel.add(buttDevide);
		functionPanel.add(buttPercent);
		
		JPanel bottomBorderPanel = new JPanel(new BorderLayout(2,0));
		southMainPanel.add(bottomBorderPanel); // 2-� ������ southMainPanel
		// �� �� ������� BorderLayout. ������ "=" ����� �� ���� EAST
		Border borderOfBottomBorderPanel = new EmptyBorder(0, 3, 3, 3);
		bottomBorderPanel.setBorder(borderOfBottomBorderPanel);
				
		bottomBorderPanel.add(buttResult, BorderLayout.EAST);
		buttResult.setPreferredSize(new Dimension(54, 0));
				
		//�������� Grid ������ �� 1 ������� � 3 ����. 1-� �����, 2-� ����� 3-� - ������ � �����
		JPanel leftPanelGrid = new JPanel(new GridLayout(3, 1, 2, 2));
		bottomBorderPanel.add(leftPanelGrid, BorderLayout.CENTER);	
		
		JPanel firstNumberBottomGridPanel = new JPanel(new GridLayout(1, 4, 2, 2));
		leftPanelGrid.add(firstNumberBottomGridPanel);
		for (int i = 4; i <= 6; i++) {
			firstNumberBottomGridPanel.add(arrOfButtons[i]);
		}
		firstNumberBottomGridPanel.add(buttMultiply);
		
		JPanel secondBottomGridPanel = new JPanel(new GridLayout(1, 4, 2, 2));
		leftPanelGrid.add(secondBottomGridPanel);
		for (int i = 1; i <= 3; i++) {
			secondBottomGridPanel.add(arrOfButtons[i]);
		}
		secondBottomGridPanel.add(buttMinus);
				
		//����� ����� ���� ��� ����������, ������� BorderLayout
		JPanel thirdBottomGridPanel = new JPanel(new BorderLayout(2,0));
		leftPanelGrid.add(thirdBottomGridPanel);
		thirdBottomGridPanel.add(arrOfButtons[0], BorderLayout.CENTER);
		
		//��� ��������� ������ ������� � Grid � �� ������ ��������� ������.
		JPanel panelCommaPlus = new JPanel(new GridLayout(1, 2, 2, 2));
		thirdBottomGridPanel.add(panelCommaPlus, BorderLayout.EAST);
		
		panelCommaPlus.add(buttComa);
		buttComa.setPreferredSize(new Dimension(54, 0));
		panelCommaPlus.add(buttPlus);
		buttPlus.setPreferredSize(new Dimension(54, 0));
		
		//////////////////////////////////////////////////////////////////
		//����������� ������
		
		CalculatorProcess calculatorProcess = new CalculatorProcess();
		
		for (int i = 0; i < arrOfButtons.length; i++) {
			arrOfButtons[i].addActionListener(calculatorProcess);
			arrOfButtons[i].addKeyListener(calculatorProcess);
		}
		
		buttComa.addActionListener(calculatorProcess);
		buttComa.addKeyListener(calculatorProcess);
		
		buttResult.addActionListener(calculatorProcess);
		buttResult.addKeyListener(calculatorProcess);
		
		buttPlus.addActionListener(calculatorProcess);
		buttPlus.addKeyListener(calculatorProcess);
		
		buttMinus.addActionListener(calculatorProcess);
		buttMinus.addKeyListener(calculatorProcess);
		
		buttMultiply.addActionListener(calculatorProcess);
		buttMultiply.addKeyListener(calculatorProcess);
		
		buttDevide.addActionListener(calculatorProcess);
		buttDevide.addKeyListener(calculatorProcess);
		
		c.addActionListener(calculatorProcess);
		c.addKeyListener(calculatorProcess);
		
		ce.addActionListener(calculatorProcess);
		ce.addKeyListener(calculatorProcess);
		
		mc.addActionListener(calculatorProcess);
		mc.addKeyListener(calculatorProcess);
		
		mr.addActionListener(calculatorProcess);
		mr.addKeyListener(calculatorProcess);
		
		ms.addActionListener(calculatorProcess);
		ms.addKeyListener(calculatorProcess);
		
		mPlus.addActionListener(calculatorProcess);
		mPlus.addKeyListener(calculatorProcess);
		
		mMinus.addActionListener(calculatorProcess);
		mMinus.addKeyListener(calculatorProcess);
		
		textField.addKeyListener(calculatorProcess);
		textField.setFocusable(true);
		textField.setEditable(false);
		textField.setBackground(Color.WHITE);
		textField.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		
		
		setResizable(false);
		setVisible(true);
		
	}

	private Float convertOperandFromStringToNumber(String operand) {
		
		try {
			Float result = Float.parseFloat(operand);
			return result;
		} catch (NumberFormatException e) {
			System.out.println("�� ���� ��������� "+operand+" � �����...");
			return null;
		}
		
	}
	
}

