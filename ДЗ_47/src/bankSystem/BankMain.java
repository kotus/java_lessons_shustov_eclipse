package bankSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import arrays.Mass;

public class BankMain {

	public static Bankomat[] arrOfBancomats;
	public static Staff[] arrOfStaffs;
	public static ArrayList<Client> arrayListOfClients;
	public static Vector<BankCard> vectorOfBankCards;
	
	public BankMain() throws IOException {
		
		arrOfBancomats = Bankomat.makeMassBankomat("src/bancomats.in");
		arrOfStaffs = Staff.makeArrStaff("src/staff.in");
		arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
		vectorOfBankCards = BankCard.makeVectorOfBankCards("src/master-visa-2014-05-14---01.in");
		
	}

	public static void main(String[] args) throws IOException {
		
		BankMain mainBank= new BankMain();
		testMassAll();
	
	}//main

	public static void testMassAll() {
		
		Mass.showArrayOfObjects(arrOfBancomats);
		Mass.showArrayOfObjects(arrOfStaffs);
		Client.testMassClient(arrayListOfClients);
		BankCard.testVectorBankCards(vectorOfBankCards);
		
	}
	
}
