package lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

//������� �� ��������� � �������� � ��������

public class Coll_Arrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//1. �� �������� � ����������
		//int [ ] arrInt = {1,2,3,4,5,6,7}; 
		Integer [ ] arrInt = {1,2,3,4,5,6,7};
		String [ ]   arrStr = {"���", "���", "���", "�"};
		
		//1.1. ������� ����� ������ Arrays -
		//��� ����������� �����, ������� ����� ������ ����������� ������
		//Math.
		Collection<Integer> collList1 = Arrays.asList( arrInt );  //� 7-� ������ ����� �������� ������ ����� ��������������� ���� �������.
		List           <String>   collList2 = Arrays.asList( arrStr );
		
		//������� �  8� ������ ����� �������� �������������� � ���������� �� ���������� � �������� 
		//Collection<Number> collList1_2 = Arrays.asList( arrInt );
		//List           <Object>   collList2_2 = Arrays.asList( arrStr );
		
		//  collList1.
		//ArrayList  <Integer> collList3 = (ArrayList) Arrays.asList( arrInt ); //������� ������ - �.�. ������� � ����������� ��� �� ��������
		//ArrayList  <Integer> collList3 = Arrays.asList( arrInt );
		
		//1.2. ��������� , ���������� ������� asList �� ���������������� �� �������
		//collList1.add(  10 );    //������� UnsupportedOperationException - ���������������� ��������
		//collList2.remove(0);   //������� UnsupportedOperationException - ���������������� ��������
					//��� ���� ������ �������� ������ ��������� - �����
		collList2.set(0, "���"); 
		//collList1.set();  //� ���� ��������� ������ set() ���,  �.�. ����� ��� ���������� ��  Collection 
		
		//1.3. ����������� ��������� ���� ��������� �� ���������� asList()-�� � ������� ������������
		//������ ������������� ����� ������� ��������� ������ ����-������
		ArrayList<Integer>      coll1FromAsList = new ArrayList(collList1);   coll1FromAsList.add(10);
		LinkedList<String>     coll2FromAsList = new LinkedList(collList2); coll2FromAsList.add("��-��");
	//	LinkedList<Integer>     coll2FromAsList = new LinkedList(collList2);  //��������� ��������� ������ ��������������� �����
		List<String>     coll3FromAsList = new LinkedList(collList2);  //����� ��������� ����� ������������ ����, ��� �������� ��������� �������������
		
		//2. �������������� ��������: �� ��������� � ������
		collList1.toArray();   //��� ����������, ���������� ������ ��������� - ������� ��� �����
		/*Integer [ ] arr1 = null ;  //���� ���, �� ������ ������ ������ ���������� ���� ��� ������, �� �.�. � ����� ���������
										//����� ������ �� ������ ������������ � ������ ��������, �� arr1 �������� � null,
										//������� ���� ����� ���� ������� ������ �������� �  ��������� ������ ������� � ��� arr1 = �����()
										//�� ����� ��� ������ � ���������
		Integer [ ] arr2 = new Integer [ 100 ] ;  //���� ����� ��� ������� �������� �� ������, �� ������������ ����� ����� ��������� ������
		
		//arr1 = collList1.toArray		(   arr1);   //����� �� ����� ������ ��������� ���� ������ � ������ ��� ����� ���������,
		                                               //����� ��� ������ ���������� ������ ������ - 
														//����� �� �������� ���� ������ ����� ������
		*/
		
		Integer [ ] arr1 = collList1.toArray	(new Integer[0] );   // � ��������� �������� ������������� ������ �������, ���� ����
		
				//��������� ��� 2 ������ toArray() ���� � ������ Collection - �� ��� ���� �� ���� ��� �����������
				//-�.�. �� ���� ����������
		
	//3. �������� �������� �� �������� - 
		//������ - ��� ��������� ���� Liist � ��� ���������� ArrayList, LinkedList � Vector
		List<String>     coll4FromAsList = new LinkedList(collList2); 
		//coll4FromAsList.    //����� �������
		//3.1.   ���������� ������ 
		//List<Integer>     listColl = null;  // � ����� ��������� �� ����� ���� �������
		List<Integer>     listColl = new ArrayList<Integer>();
		listColl.add( 1 );   //��������� � ������  � ����� ������  ����� 1
		//0�   1�       2�
		//1      null    null
		
		
		listColl.add(  listColl.size(), 17);   //����� ���������� ��������� �����
		
		//listColl.add(3, 17);   //��������� � ������ � 3� �������  ����� 17, ��� ������� � 3-�� �������� ������������ ������
											//IndexOutOfBoundsException: - �.�. ������� �������� ����� ������� ������ ����������, � �� ����� ����������
		
		//3.2. �������� ���������� � �����
//	for (int i = 0; i < listColl.size()  ; i++)   //!!!* � ������� ����������� ������ ���������� size() 
																		//���� ������ ����� ���� add() � ��� �� ���������
//			listColl.add( i ); 					//������� OutOfMemory
	
		//���� ���������� ������ ��������� �� ����� � ����������, � � ����� ����� ��� ����������
		int sizeColl =  listColl.size() ;
		for (int i = 0; i < sizeColl ; i++)   //!!!* � ������� ����������� ������ ���������� size() 
			//���� ������ ����� ���� add() � ��� �� ���������
					listColl.add( i ); 	
		
		//3.3. ����������  � ������  ���������
		for (int i = 0; i < sizeColl /*����������� ����������*/ ; i++)   
					listColl.add( 0, i );   //0 - ���������� ��������� � ������ ���������, ���� ������� ����� ���������
													//��� ArrayList � Vector, � � ���������� ��� �� �������������, 
													//�.�. � ���� ������������� ������ �������� �������
		
		//3.4. ����������  � �������� ���������
				for (int i = 0; i < sizeColl ; i++)   
							listColl.add( 3, i );   //������ 3 - ������ ��������� �� �������, ��� �� �����-��������� �������
															//��� ArrayList � Vector, � � ���������� ��� �� �������������, 

	//3.5. ����-�������� ����������  (���� - ���������, ��� �����)
				listColl.addAll(listColl); 
				
	//3.6.  ������ � ����� �� ����������� ������
//	for (Integer elem : listColl)   //���� foreach()  ������ ����� ��� ���������� ���������, ������ ������ �� ��������� , ��� ���������
													//�.�. � ����� �����() ������ ���� ���� ��������, � �� ����������� � ������� ���()
	//	listColl.add( 7 );
			
	
	//���������� ��������� �� ��������� �����, �� �������� �� ����� ���������.
	for (Iterator iterator = listColl.iterator(); iterator.hasNext();) {
		Integer elem = (Integer) iterator.next();
		
	//	listColl.add( 7 );   		//ConcurrentModificationException - �.�. ������ ����������, � ���������� - �������()
					//���� ��������� �������� � ��������� - �� ������ �� ���������� ���� ������ ������� ������
		
	}
	
				
				
		System.out.println( listColl);
		
		
	
		
	}//main()

}
