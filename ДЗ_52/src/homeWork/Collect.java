package homeWork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.TreeSet;
import java.util.Vector;

import bankSystem.Bankomat;

public class Collect {

	
	public static List<Integer> mainArr = new ArrayList<Integer>();
	static {
		Collect.createRandomCollArrayList(mainArr);
	}
	
	public static void main(String[] args) {

		Double time = 0D;
		ArrayList<Integer> arrayList = new ArrayList<Integer>(mainArr);
		Vector<Integer> vector = new Vector<Integer>(mainArr);
		LinkedList<Integer> linkedList = new LinkedList<Integer>(mainArr);
		
		
		
		/*
		time = createRandomCollArrayList(arrayList);
		System.out.println("���������� 1000000 � ArrayList: " + time);
		
		time = createRandomCollArrayList(vector);
		System.out.println("���������� 1000000 � Vector: " + time);
		
		time = createRandomCollArrayList(linkedList);
		System.out.println("���������� 1000000 � LinkedList: " + time);
		*/
		
		/*
		time = addRandomListCollAtStart(arrayList, 1000);
		System.out.println("���������� 1000000 � ������ ArrayList: " + time);
		
		time = addRandomListCollAtStart(vector, 1000);
		System.out.println("���������� 1000000 � ������ Vector: " + time);
		
		time = addRandomListCollAtStart(linkedList, 1000);
		System.out.println("���������� 1000000 � ������ LinkedList: " + time);
		*/
		
		/*
		time = addRandomListCollAtRandomPosition(arrayList, 1000000, 1000);
		System.out.println("���������� 1000000 �������� � ArrayList: " + time);
		
		time = addRandomListCollAtRandomPosition(vector, 1000000, 1000);
		System.out.println("���������� 1000000 �������� � Vector: " + time);
		
		time = addRandomListCollAtRandomPosition(linkedList, 1000000, 1000);
		System.out.println("���������� 1000000 �������� � LinkedList: " + time);
		*/
		
		
		/*
		time = increaseElementsPerOne(arrayList);
		System.out.println("���������� �� 1 ����� ArrayList: " + time);
		
		time = increaseElementsPerOne(vector);
		System.out.println("���������� �� 1 ����� Vector: " + time);
		
		time = increaseElementsPerOne(linkedList);
		System.out.println("���������� �� 1 ����� LinkedList: " + time);
		*/
		
		/*
		List<Integer> list = new LinkedList(); 
		for (int i = 1; i <= 10; i++) {
			list.add(i);
		}
		System.out.println(list);
		
		increaseElementsPerOneWithFor(list);
		
		System.out.println(list);
		*/
		
		/*
		time = increaseElementsPerOneWithFor(arrayList);
		System.out.println("���������� �� 1 ����� ArrayList: " + time);
		
		time = increaseElementsPerOneWithFor(vector);
		System.out.println("���������� �� 1 ����� Vector: " + time);
		
		time = increaseElementsPerOneWithFor(linkedList);
		System.out.println("���������� �� 1 ����� LinkedList: " + time);
		*/
		
		/*
		time = increaseElementsPerOneWithForEach(arrayList);
		System.out.println("���������� �� 1 ����� ArrayList: " + time);
		
		time = increaseElementsPerOneWithForEach(vector);
		System.out.println("���������� �� 1 ����� Vector: " + time);
		
		time = increaseElementsPerOneWithForEach(linkedList);
		System.out.println("���������� �� 1 ����� LinkedList: " + time);
		*/
		
		/*
		time = RandomChangeListColl(arrayList);
		System.out.println("���������� �� 1 ����� ArrayList: " + time);
		
		time = RandomChangeListColl(vector);
		System.out.println("���������� �� 1 ����� Vector: " + time);
		
		time = RandomChangeListColl(linkedList);
		System.out.println("���������� �� 1 ����� LinkedList: " + time);
		*/
		
		/*
		time = deleteElement(arrayList, new Integer(10));
		System.out.println("���������� �� 1 ����� ArrayList: " + time);
		
		time = deleteElement(vector, new Integer(10));
		System.out.println("���������� �� 1 ����� Vector: " + time);
		
		time = deleteElement(linkedList, new Integer(10));
		System.out.println("���������� �� 1 ����� LinkedList: " + time);
		*/
		
		/*
		time = deleteAllElement(arrayList);
		System.out.println("�������� ��������� � ����� ArrayList: " + time);
		
		time = deleteAllElement(vector);
		System.out.println("�������� ��������� � ����� Vector: " + time);
		
		time = deleteAllElement(linkedList);
		System.out.println("�������� ��������� � ����� LinkedList: " + time);
		*/
		
		/*
		time = deleteElementFromEnd(arrayList, new Integer(10));
		System.out.println("�������� ���������� ���������� � ����� ArrayList: " + time);
		
		time = deleteElementFromEnd(vector, new Integer(10));
		System.out.println("�������� ���������� ���������� � ����� Vector: " + time);
		
		time = deleteElementFromEnd(linkedList, new Integer(10));
		System.out.println("�������� ���������� ���������� � ����� LinkedList: " + time);
		*/
		
		/*
		List<Integer> list = new LinkedList(); 
		for (int i = 1; i <= 10; i++) {
			list.add(i);
		}
		System.out.println(list);
		
		ListIterator iterator = list.listIterator();
		iterator.next();
		iterator.remove();
		
		list.remove(0);
		
		System.out.println(list);
		*/
		
		/*
		time = deleteElementFromBeginToEndWithFor(arrayList, new Integer(100));
		System.out.println("�������� ������ ���������� � ������ ArrayList: " + time);
		
		time = deleteElementFromBeginToEndWithFor(vector, new Integer(100));
		System.out.println("�������� ������ ���������� � ������ Vector: " + time);
		
		//time = deleteElementFromBeginToEndWithFor(linkedList, new Integer(100));
		//System.out.println("�������� ������ ���������� � ������ LinkedList: " + time);
				
		time = deleteElement(arrayList, new Integer(10));
		System.out.println("�������� �������� ���������� � ArrayList: " + time);
		
		time = deleteElement(vector, new Integer(10));
		System.out.println("�������� �������� ���������� � Vector: " + time);
		
		time = deleteElement(linkedList, new Integer(10));
		System.out.println("�������� �������� ���������� � LinkedList: " + time);
		
		time = removeAllSingleton(arrayList, new Integer(10));
		System.out.println("�������� �������� ���������� � ArrayList: " + time);
		
		time = removeAllSingleton(vector, new Integer(10));
		System.out.println("�������� �������� ���������� � Vector: " + time);
		
		time = removeAllSingleton(linkedList, new Integer(10));
		System.out.println("�������� �������� ���������� � LinkedList: " + time);
		*/
		
		/*
		time = clearAll(arrayList);
		System.out.println("�������� clearAll() � ArrayList: " + time);
		
		time = clearAll(vector);
		System.out.println("�������� clearAll() � Vector: " + time);
		
		time = clearAll(linkedList);
		System.out.println("�������� clearAll() � LinkedList: " + time);
		*/
		
		/*
		for (Integer integer : linkedList) {
			linkedList.remove(integer);
		}
		*/
		
		/*
		time = deleteAtRandomPosition(arrayList, 1000000);
		System.out.println("�������� �������� ��� �������� � ArrayList: " + time);
		
		time = deleteAtRandomPosition(vector, 1000000);
		System.out.println("�������� �������� ��� �������� � Vector: " + time);
		
		time = deleteAtRandomPosition(linkedList, 1000000);
		System.out.println("�������� �������� ��� �������� � LinkedList: " + time);
		*/
		
//		TreeSet<Integer> treeSet = new TreeSet<Integer>();
//		treeSet.add(3);treeSet.add(2);treeSet.add(1);
//		System.out.println(treeSet);
		
		TreeSet<Bankomat> treeSetBancomat = new TreeSet<Bankomat>();
		treeSetBancomat.add(new Bankomat("2121", "21212"));
		
		System.out.println(treeSetBancomat);
		
	}
	
	public static Double createRandomCollArrayList(ArrayList<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < 1000000; i++) {
			arr.add(new Random().nextInt(1000));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
	public static Double createRandomCollArrayList(List<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < 1000000; i++) {
			arr.add(new Random().nextInt(1000));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
//	�������� �����, ����������� � ������  ���������  ����  ���1 ��������� ����� �� 0 �� ���2 � 
//	����������� ���������� ����� ����������.
	
	public static Double addRandomListCollAtStart(List<Integer> arr, Integer numbersCount, Integer maxValue) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < numbersCount; i++) {
			arr.add(0, new Random().nextInt(maxValue));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
//	�������� �����, ����������� �����������, ������ ����������� � ���������  ����  ����� ������� 
//	�� ���������� ������� (�.�. ����� ������� ������ �������� ������������ ��������).   ��������� 
//	�� �� �����: ���1 ��������� ����� �� 0 �� ���2 � ����������� ���������� ����� ����������.	
	
	public static Double addRandomListCollAtRandomPosition(List<Integer> arr, Integer numbersCount, Integer maxValue) {
		
		Long time1 = System.currentTimeMillis();
		
		arr.add(0, 999);
		
		for (int i = 0; i < numbersCount; i++) {
			arr.add(new Random().nextInt(arr.size()), new Random().nextInt(maxValue));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
//	 �������� �����, �������� ������ �� ��������� � ����������  � ������ (����� ,������ ��� ������)  
//	 ��� �������� � ��������� �� 1 ������ � ���������� ����������������� ������ ������. ������������ 
//	 ��������. ���� ��������� � �� ����� ��������
	
	public static Double increaseElementsPerOne(List<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		int i = 0;
		for (Iterator iterator = arr.iterator(); iterator.hasNext(); i++) {
			Integer integer = (Integer) iterator.next();
			arr.set(i, integer.intValue()+1);
		}
				
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
//	������ ���: ��������  ����� �� �����, �� ������������ �� ��������, �  ������ ���������  ������, �����
//	� ������ (�.�. ������ ���������� ����)
	
	public static Double increaseElementsPerOneWithFor(List<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < arr.size(); i++) {
			arr.set(i, arr.get(i).intValue() + 1);
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
	public static Double increaseElementsPerOneWithForEach(List<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		int i = 0;
		for (Integer integer : arr) {
			arr.set(i, integer.intValue()+1);
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
//	������ ���������  ������� �� �������:  �������  ����� �� �����, ��  ����������������  �� � ������� 
//	�� ��������� ��� ������, �  ������� �� ��������� ��������. �.�. ���� ��������� ���� 1��� ���, �� ������
//	����������� �������� ��������� ������ ���� ������ ��� ���������.
	
	public static Double randomChangeListColl(List<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();

		int i = 0;
		for (Integer integer : arr) {
			arr.set(new Random().nextInt(arr.size()),
					integer.intValue() + 1);
		}

		Long time2 = System.currentTimeMillis();

		return ((double) (time2 - time1) / 1000);
		
	}
	
//	���������������� ������ � �������� ����������: �������� �����, ���������  ����� ���1, � ������� �� 
//	��������� �� ������ � ����� � ���������� ����� ������ ����������. ������������ ���������� 
//	(����� �� ������������ ����� ��������?) 
	
	public static Double deleteElement(List<Integer> arr, Integer element) {
		
		Long time1 = System.currentTimeMillis();
		
		//int removed = 0;
		
		for (Iterator iterator = arr.iterator(); iterator.hasNext(); ) {
			Integer integer = (Integer) iterator.next();
			if (integer.equals(element)) {
				iterator.remove();
				//removed++;
			}
		}
				
		//System.out.println("Removed " + removed);
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
	public static Double deleteAllElement(List<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		for (Iterator iterator = arr.iterator(); iterator.hasNext(); ) {
			Integer integer = (Integer) iterator.next();
			iterator.remove();
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
//	���������������� ������ � �������� ���������� �� ����� � ������: �������� �����, ���������  
//	����� ���1, � ������� �� ��������� �� ����� � ������ � ���������� ����� ������ ����������. 
//	������������ ���������� (����� �� ������������ ����� ��������?) 
	
	public static Double deleteElementFromEnd(List<Integer> arr, Integer element) {
		
		Long time1 = System.currentTimeMillis();
		
		ListIterator<Integer> listIterator = arr.listIterator(arr.size()-1); 
		while (listIterator.hasPrevious()) {
			Integer integer = (Integer) listIterator.previous();
			if (integer.equals(element)) {
				listIterator.remove();
			}
		}
				
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
//	���������������� ������ � �������� ������ ���(): �������� �����������  ����� , �� ��������� 
//	�������� �� ����������, � ���������� ��������� ���� (��� ���������� ����). � ������� � ������ � �����.
	
	public static Double deleteElementFromBeginToEndWithFor(List<Integer> arr, Integer element) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).equals(element)) {
				arr.remove(arr.get(i));
			}
		}
				
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
	public static Double removeAllSingleton(List<Integer> arr, Integer element) {
	
		Long time1 = System.currentTimeMillis();
		
		arr.removeAll( Collections.singleton(element));
				
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
	public static Double clearAll(List<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		arr.clear();
				
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
//	�������� �� ��������� ��������: �������� ����� �� ����� , �� ��������� �������� �� �������� �� ����
//	���������  ����������  ��� ���������� ��������� ���� , �  �������� � ��������� �������.
//	(������ ������������  �������� � �������� �� 0-�� �� ������� ���������)
	
	public static Double deleteAtRandomPosition(List<Integer> arr, Integer numbersCount) {
		
		Long time1 = System.currentTimeMillis();
						
		for (int i = 0; i < numbersCount; i++) {
			arr.remove(new Random().nextInt(arr.size()));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
	
	
}
