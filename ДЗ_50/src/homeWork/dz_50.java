package homeWork;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.text.html.HTMLDocument.Iterator;

public class dz_50 {

	public static void main(String[] args) {

		/*
		Vector<Integer> vector = new Vector<Integer>();
		for (int i = 1; i <= 10; i++) {
			vector.add(new Integer(i));
		}
		Integer etalon = new Integer(3); //������ ������� ������ 3.
		
		if(vector.get(2).equals(etalon)){
			System.out.println("�������� � ������� ��������� �����");
		} else {
			System.out.println("�������� � ������� ��������� �� �����");
		}
	
		for (java.util.Iterator iterator = vector.iterator(); iterator
				.hasNext();) {
			Integer current = (Integer) iterator.next();
		}
		*/
		
		//2.1.	������� ������ �� 14 ��������� ����. �����..
		Vector<Integer> vector = new Vector<Integer>(14);
		for (int i = 1; i <= 14; i++) {
			vector.add((int) (Math.random() * (100)));
		}

		System.out.println("");

		//2.1.1.	��������� ��� �������� �� 20. ��� ����������� ������ ��������� �����?

		/*
		System.out.println( "���������: " + vector  );
		System.out.println("�������� �������: " + vector.capacity());
		
		vector.setSize(20);
		System.out.println("�������� ������� ����� ����������: " + vector.capacity());
		System.out.println( "���������: " + vector  );
		*/
		
		//2.1.2.	�������� ������ �� 10 ����� � ������� �� 20-��. �������� ��  ����� � ��������� � 11-�� �� 20-�?

		/*
		System.out.println( "���������: " + vector  );
		vector.setSize(10);
		vector.setSize(20);
		System.out.println( "��������� ����� �������: " + vector  );
				
		//2.1.3.	��������� �������� ����� ������� ������� �� 10.
		
		vector.setSize(10);
		vector.trimToSize();
		System.out.println("��������� ����� ����������: " + vector);
		System.out.println("�������� ����� ����������: " + vector.capacity());
		*/
		
		Integer [ ]  intArr = {1,2,3,4,5,6} ;
		Integer [ ]  intArr2 = {4,5,6,7,8,9} ;
			
		List<Integer> intList = Arrays.asList(  intArr ) ;  //�� �������������� �� �������
		Set<Integer>  intSet1_Etalon = new LinkedHashSet<Integer>(  intList   );  //2� �������� �� �����
		//System.out.println( intSet1_Etalon );
		
		intList = Arrays.asList(  intArr2 ) ;  //�� �������������� �� �������
		Set<Integer>  intSet2_Etalon= new LinkedHashSet<Integer>(  intList   );  //2� �������� �� �����
		//System.out.println( intSet2_Etalon );
		
		/*
		//�������� � ����������� - � ����� � ������� ����-������� (�� ������ All �  ��������� )
				//1. ����������� �������� (�������� ��������)
		Set set1 = new LinkedHashSet(intSet1_Etalon);		//������� ����� ���������, ����� �� ��������� ��������� ���������
		Set set2 = new LinkedHashSet(intSet2_Etalon);
		//set1 = intSet1_Etalon;  //��� �������, �� ��� �� ��������� ����� ���������, � ��� ����� ����� �������� ��������� �� ����� ���� �������
												//�.�. ��� ������-������  ��������� �� ���� � �� �� ���������
		
		set1.addAll( set2 );
		System.out.println( "����������� (��������) ��������: " +  set1);
		
		//2. ���������  �������� ( �������� � 1-� ��������� ��, ���� ��� �� 2-� ���������)
					//������� �� 1-�� ��������� ���, ��� ���� �� 2-� ���������
		 set1 = new LinkedHashSet(intSet1_Etalon);
		 set2 = new LinkedHashSet(intSet2_Etalon);
		
		 //�1 - �2
		 set1.removeAll( set2 );
		 System.out.println( "M1 - M2 :" +  set1);
		 
		//�2 - �1
		 set1 = new LinkedHashSet(intSet1_Etalon);
		 set2 = new LinkedHashSet(intSet2_Etalon);
		 
		 set2.removeAll( set1 );
		 System.out.println( "M2 - M1 :" +  set2);
		 
		 //3.  ����������� �������� (����� ����� ��������)
		 		//��������  � 1-� ��������� ��� �� , ��� ���� �� 2-� ���������
		 set1 = new LinkedHashSet(intSet1_Etalon);
		 set2 = new LinkedHashSet(intSet2_Etalon);
		 
		 set1.retainAll( set2 );
		 System.out.println(  "����� ����� ��������: " + set1);
		 */
		
		 //4. ������������ �������� (����������� - �����������) (��� � ����� �������� , ����� �� ����� �����)
		 //������� ����
		 
		Set set1 = new LinkedHashSet(intSet1_Etalon);
		Set set2 = new LinkedHashSet(intSet2_Etalon);
		 
		System.out.println(set1);
		System.out.println(set2);
		
		set1.removeAll(set2);
		set2.removeAll(set1);
		set1.addAll(set2);
		
		System.out.println(set1);
		
		 //5. ���� ��������   containsAll - �������� �� ���������-�������� ������ ���, ��� � ��������� -���������
//		 System.out.println( "�������� �� 2-� ��������� ��� , ��� � 1-� ���������: " +  set2.containsAll( set1));
//		 System.out.println( "�������� �� 1-� ��������� ��� , ��� � 2-� ���������: " +  set1.containsAll( set2));
		
	}

}
