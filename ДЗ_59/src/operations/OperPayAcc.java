package operations;

import bankSystem.Account;
import bankSystem.Staff;
import bankSystem.Summ;

public class OperPayAcc extends Operation{

	private Account side1;
	private Account side2;
	
	public OperPayAcc(Long iD, String date, Summ summ, String time, String text,
			Account side1, Account side2, Staff maker) {
		
		super(iD, date, time, summ, text, side1, side2, maker);
		super.setOperSub(this);
		this.side1 = side1;
		this.side2 = side2;
		
	}

}
