package bankSystem;

import java.io.IOException;
import java.util.*;

public class Person {

	private String surename;
	private String name;
	private String middleName;
	private String street;
	private short numHouse; //����� ����� �� �������
	private short numHouseExt;
	private short numApprtment;
	private String numPassport;
	private Date datePassport; //import java.util.*; ������ ��� �� ����.
	private String offPassport;
			
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((surename == null) ? 0 : surename.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surename == null) {
			if (other.surename != null)
				return false;
		} else if (!surename.equals(other.surename))
			return false;
		return true;
	}

	public String getSurename() {
		return surename;
	}

	public void setSurename(String surename) {
		this.surename = surename;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Person(String surename, String name, String middleName) {
		super();
		this.surename = surename;
		this.name = name;
		this.middleName = middleName;
	}
			
	public Person(String surename, String name, String middleName,
			String street, short numHouse, short numHouseExt,
			short numApprtment, String numPassport, Date datePassport,
			String offPassport) {
		super();
		this.surename = surename;
		this.name = name;
		this.middleName = middleName;
		this.street = street;
		this.numHouse = numHouse;
		this.numHouseExt = numHouseExt;
		this.numApprtment = numApprtment;
		this.numPassport = numPassport;
		this.datePassport = datePassport;
		this.offPassport = offPassport;
	}
	
	public Person(){
		
	}
	
	public static void main(String[] args) throws IOException{
		
		Person person = new Person("������", "����", "��������");
		System.out.println(person);
		
	}

	public String toString() {
		// TODO Auto-generated method stub
		return "����� Person:[������� = " + surename + "; ��� = " + name + "; " +
		"�������� = " + middleName + "]";
	}

	
	
}
