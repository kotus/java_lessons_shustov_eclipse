package bankSystem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class Client extends Person implements Comparable<Client>{

	private BankCard bankCard;
	private Account account; // = new Account();
	private Integer ID;

	private static Integer lastID;
	
	static {	Client.lastID = 1;   }
	
	public BankCard getBankCard() {
		return bankCard;
	}

	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		this.ID = iD;
	}

	public Client(String surname, String name, String middleName) {
		super(surname, name, middleName);

		String accNumber = "27007000"
				+ String.valueOf(100000L + Math.random() * (999999L - 100000L));
		accNumber = accNumber.substring(0, 13);
		this.account = new Account(Long.parseLong(accNumber), 0L, "UAH", this);
		this.ID = Client.lastID;
		Client.lastID++;
	}

	public Client(String surname, String name, String middleName,
			BankCard bankCard) {
		super(surname, name, middleName);
		
		this.bankCard = bankCard;
		this.ID = Client.lastID;
		Client.lastID++;
		
	}

	public Client(String surname, String name, String middleName,
			BankCard bankCard, Account account) {
		super(surname, name, middleName);
		
		this.bankCard = bankCard;
		this.account = account;
		this.ID = Client.lastID;
		Client.lastID++;
		
	}

	public Client(String surename, String name, String middleName,
			Account account) {
		super(surename, name, middleName);
		
		this.account = account;
		this.ID = Client.lastID;
		Client.lastID++;
		
	}
	
	@Override
	public int compareTo(Client client2) {
		
		String FIO_1 = this.getSurename() + " " + this.getName() + " " + this.getMiddleName();
		String FIO_2 = client2.getSurename() + " " + client2.getName() + " " + client2.getMiddleName();
		
		int result = FIO_1.compareToIgnoreCase(FIO_2);
		
		if (result == 0) {
			return 1;
		} else {
			return result;
		}
		
	}

	// ������� � ������� �����, �� ����� ������� �������
	public String payBCard(Summ summa, Client client) {

		Long summaThisClient = this.bankCard.getSumm().getSumm();
		Long summaComingClient = client.bankCard.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();

		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0) {
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0) {
			return "������������ ������� ��� ������";
		}

		this.bankCard.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.bankCard.getSumm().setSumm(summaComingClient + summaOfPayment);

		return "1";

	}
	
	public String payBAccount(Summ summa, Client client) {

		Long summaThisClient = this.account.getSumm().getSumm();
		Long summaComingClient = client.account.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();

		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0) {
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0) {
			return "������������ ������� ��� ������";
		}

		this.account.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.account.getSumm().setSumm(summaComingClient + summaOfPayment);

		return "1";

	}

	public static void main(String[] args) throws IOException{
		
		ArrayList<Client> arrOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
		ArrayList<BankCard> bankCards = BankCard.makeCollOfBankCards("src/master-visa-2014-05-14---01.in");
		
		for (int i = 0; i < arrOfClients.size(); i++) {
			arrOfClients.get(i).setBankCard(bankCards.get(i));
			bankCards.get(i).setClient(arrOfClients.get(i));
		}
		
		TreeSet<BankCard> setOfCards = createSetOfBankCards(arrOfClients);
		System.out.println(setOfCards);
		
		TreeSet<BankCard> setOfCardsSorted = new TreeSet<BankCard>(new COMPARATOR_BANK_CARDS_FIO());
		setOfCardsSorted.addAll(setOfCards);
		System.out.println(setOfCardsSorted);
		
		
	}

	/**
	 * ����������� �����, ��������� ������ �������� Client[] �������� ��
	 * ����������� � ���� ���������� �������.
	 * 
	 * @param matrixFromFile
	 *            - ��� String[][], ��������� ������ ��� �������: { { "������",
	 *            "����", "��������" } , ...}
	 * 
	 * @return arrClients - ��� Client [], ���������� ������ �������� ����
	 *         Client.
	 */
	public static Client[] makeMassClient(String[][] matrixFromFile) {

		Client[] arrClients = new Client[matrixFromFile.length];
		for (int i = 0; i < arrClients.length; i++) {
			
			String[] arrayOfName = matrixFromFile[i][0].split(" ");
			
			arrClients[i] = new Client(arrayOfName[0], arrayOfName[1], arrayOfName[2]);
			arrClients[i].setAccount(new Account(Long.parseLong(matrixFromFile[i][1]), 0L, "UAH", arrClients[i]));
			
		}

		return arrClients;

	}

	/**
	 * ����������� �����, ��������� ������ �������� Client[] �������� ��
	 * ����������� � ���� ���� � ����� � ��� ��������.
	 * 
	 * @param fileNameFIO
	 *            ���� String, ��� ����� �� �����.
	 * @return <b>arrClients</b> ���� Client [] ���� ������ ������ ��� NULL ����
	 *         ������ �� �������.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Client[] makeMassClient(String fileNameFIO)
			throws FileNotFoundException, IOException {

		String[][] arrayFromFile = Client.makeFIOstrFromFile(fileNameFIO);

		if (arrayFromFile.length != 0) {
			Client[] arrClients = Client.makeMassClient(arrayFromFile);
			return arrClients;
		} else {
			return null;
		}

	}

	/**
	 * ����� �������� ���� �� ������ "src/fio_clients.in" � ��������� �� ����
	 * ������� ���.<br>
	 * ��������� ����� ������ ���� ���������:<br>
	 * ������� ���� ��������<br>
	 * ������� ���� ��������<br>
	 * ����� ����� ��������<br>
	 * ...<br>
	 * ������������ �� ��������� ��������� ������.
	 * 
	 * @param fileNameFIO
	 *            ���� String, ��� ����� �� �����.
	 * @return <b>resultMatrix<b> ���� String[][], ��������� ������ ��� �������:
	 *         { { "������", "����", "��������" } , ...}
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String[][] makeFIOstrFromFile(String fileNameFIO)
			throws FileNotFoundException, IOException {

		BufferedReader reader = new BufferedReader(new FileReader(fileNameFIO));
		String str;
		int count = 0;
		while ((str = reader.readLine()) != null) {
			if (str.trim().equals(""))
				continue;
			count++;
		}

		String[][] resultMatrix = new String[count][5];

		reader = new BufferedReader(new FileReader(fileNameFIO));

		int i = 0;
		while ((str = reader.readLine()) != null) {

			if (str.trim().equals("")) {
				i++;
				continue;
			}
				
			StringTokenizer tokenizer = new StringTokenizer(str, "\t");

			int j = 0;
			while (tokenizer.hasMoreTokens()) {

				String tempString = tokenizer.nextToken().trim();
				String[] arrOfTempString = tempString.split(":");
				
				resultMatrix[i][j] = arrOfTempString[1];
				j++;

			}

			i++;

		}

		return resultMatrix;

	}
	
	/**
	 * 
	 * @param fileNameFIO
	 * @return listOfClients
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static ArrayList<Client> makeCollClients(String fileNameFIO)
			throws FileNotFoundException, IOException {

		ArrayList<Client> listOfClients = new ArrayList<Client>();
		BufferedReader reader = new BufferedReader(new FileReader(fileNameFIO));
		String str;
		Integer id = 1;
		
		while ((str = reader.readLine()) != null) {

			if (str.trim().equals("")) {
				continue;
			}
				
			StringTokenizer tokenizer = new StringTokenizer(str, "\t");

			Client currentClient = new Client("", "", "");
			
			while (tokenizer.hasMoreTokens()) {

				String tempString = tokenizer.nextToken().trim();
				String[] arrOfTempString = tempString.split(":");
				
				switch (arrOfTempString[0]) {
				case "���":

					String[] arrayOfName = arrOfTempString[1].split(" ");
					
					currentClient.setSurename(arrayOfName[0]);
					currentClient.setName(arrayOfName[1]);
					currentClient.setMiddleName(arrayOfName[2]);					
					break;

				case "�/�":

					currentClient.setAccount(new Account(
							Long.parseLong(arrOfTempString[1]), 0L, "UAH", currentClient));
					break;

				case "�������":

					break;

				case "zip":

					break;

				case "���":
					
					break;

				default:
					break;
				}
				
			}
			
			listOfClients.add(currentClient);

		}

		return listOfClients;

	}

	/**
	 * ����� ����������� ������ ������ makeMassClient, �� �������� �������
	 * �������� ���� Client
	 * 
	 * @param arrOfClients
	 *            - ��� Client [], ���������� ������ �������� ���� Client.
	 */
	public static void testMassClient(Client[] arrOfClients) {

		for (int i = 0; i < arrOfClients.length; i++) {
			System.out.println(arrOfClients[i]);
		}

	}
	
	/**
	 * 
	 * @param listOfClients ArrayList
	 */
	public static void testCollClient(ArrayList<Client> listOfClients) {

		for (Iterator iterator = listOfClients.iterator(); iterator.hasNext();) {
			Client client = (Client) iterator.next();
			System.out.println(client);
		}
		
	}

	public static TreeSet<BankCard> createSetOfBankCards(List<Client> listOfClients) {
		
		TreeSet<BankCard> setOfBankCards = new TreeSet<BankCard>( new Comparator<BankCard>() {
			@Override
			public int compare(BankCard card1, BankCard card2) {
				return card1.getCardNumber().compareTo(card2.getCardNumber());
			}
		});
		
		for (Client client : listOfClients) {
			setOfBankCards.add(client.bankCard);
		}
		
		return setOfBankCards;
		
	}
	
	@Override
	public String toString() {
	
		/*
		return super.getSurename() + " " + super.getName() + " "
				+ super.getMiddleName() + " pp:" + getAccount().getAccountNumber()
				+ " �����:"+getAccount().getSumm().getSumm();
		*/
		
		return super.getSurename() + " " + super.getName() + " " + super.getMiddleName();
		
	}

}

class COMPARATOR_BANK_CARDS_FIO implements Comparator<BankCard> {

	@Override
	public int compare(BankCard bankCard1, BankCard bankCard2) {
		
		Client client1 = bankCard1.getClient();
		Client client2 = bankCard2.getClient();
		
		String FIO_1 = client1.getSurename() + " " + client1.getName() + " " + client1.getMiddleName();
		String FIO_2 = client2.getSurename() + " " + client2.getName() + " " + client2.getMiddleName();
		
		int result = FIO_1.compareToIgnoreCase(FIO_2);
		
		if (result == 0) {
			return 1;
		} else {
			return result;
		}
		
	}
	
}

class COMPARATOR_CLIENT_ID implements Comparator<Client>{

	@Override
	public int compare(Client client1, Client client2) {
		
		int result = client1.getID().compareTo(client2.getID());
		
		if (result == 0) {
			return 1;
		} else {
			return result;
		}
		
	}
	
}

class COMPARATOR_CLIENT_ACCOUNT_SUMM implements Comparator<Client>{
	
	@Override
	public int compare(Client client1, Client client2) {
		
		int result = client1.getAccount().getSumm().getSumm().
				compareTo(client2.getAccount().getSumm().getSumm());
		
		if (result == 0) {
			return 1;
		} else {
			return result;
		}
		
	}
	
}


