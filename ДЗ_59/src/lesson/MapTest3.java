package lesson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;



//���� - ������������ ��� ����-��������
//����� - ���������, � �������� - ����� �����������
//� ������ ������ ����� �������, ���� ����� ��� ������. � � ��������� - ������ �������
//� ����� ���  ����������, 


public class MapTest3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1. ����������
		Map map1 = new TreeMap<Integer, String>();   //������� ��� ���������� ��� ������������ ���� ��� �� ����
		TreeMap<Integer, String>  treeMap = new TreeMap();   //����� �����, � ���� ��������
		HashMap<String, String> hashMap = new HashMap();  //����� �������, � ������������ ��������������� ������ 
																								//������() � ������() ��� ������ � ��������� �� ������� �����
																							//������ ��� ������ � ����� �� �������� ����
		LinkedHashMap<Integer, Student> linkhashMap = new LinkedHashMap<Integer, Student>(); 
		
				//�������� ��������
		//1�. ���������� ��� ���������� � ���, ������ ������ add() -  ����� put()
		 //for (String elem : map1) {		}   //� ����� ���� ����������, �.�. ��� �� ��������� ��������� ��������
		
		map1.put(1, new Student("������", "����")) ;
		map1.put(2, new Student("�������", "�����")) ;
		map1.put(3, new Student("������", "����")) ;
		map1.put(4, new Student("������", "����")) ;
		
		map1.put(4, new Student("���������", "����")) ;  //��������� ��������� ������ ������� �����, 
																								//�.�. ���� ��� ����� ��
		map1.put(5, 7);    //�.�. ��� ���������� ���� ����� ��� ����������, �� ��� �������� �����,
										//�.�. ��� ���������� ����� � �������� ���� ������� �������� - � � ����� 
										//����� ������  ���� ��������� �������� - ������� ���� ���� � ���� ������������
		
		System.out.println( map1 ); //�� ������������� �� ���, �.�. ������� ��������� � 2� ����� ���, � ��������� 
													//� ���������� �� ������������ � ����� ��� �������� 
		
		
		//2� �������� : �����, �������
		//������: �������� ��������� � ������ 2 � �����
		
		//������� 1 - ��� ����������
			//1. �������, ��� ����� ����, �� ���� �� ����� ����������� � ������, �.�. �� ����� �� �������� �� ���������
			//2. ��� ������� ������ ��� ���� ������������� � ���, 
			//    ������ ����� ���, ������� ��� ������� ������ ����
		Set setFromMap = map1.entrySet() ;  //����� ���������� ���, � ������� ���������� �������� ���� ����-��������
			//����� ������� ��� ��� ���
		Set  setEntry = setFromMap ;
			//3. ����� ���� �� ����
				//���  ���������
		for (Object elem : setEntry) {  //���� ���������� ENTRY (����)
			//����������� elem  � �����-����, �.�. ��� ��������� �� �������� � ����� elem ���� Object - 
			//� � ���� ��� ��������� �������
			//elem.
			Map.Entry entry = (Map.Entry) elem;
			Integer key1 = (Integer) entry.getKey();
			
			if (key1 >= 2)
				System.out.println("����: ��������  :" + entry.getKey() + "=" + entry.getValue());
			Object val1 = new Student();
						//������� ������������ ��������� � ������� ������ ��������, ���� � ������
			val1 = /*(Student)*/ entry.getValue();   //��� �����������, ��������� � �������������� ������� ���������� ���������
			
			//val1.  //�.�. ��� ��� ������ - �� ��� ������� � ����� � ������� ������ Student
			
			//3� �������� - ������ ������ ��������� �� �����
			if ( entry.getValue() instanceof Student)		//������� ������� ������ ������ �������, � ����� �������
					entry.setValue( new Student( "���������", "���")) ;  //������ �� ������ ������ 
			
		}//foreach
		
		System.out.println( "����� ��������� ���������? :" + map1);

		//�� �� ������ � ������� ����-�������� - ������������ �� ������ - �.�. �� ������
		treeMap.putAll(map1);  //�������� ������, � �������� ����������� ����� 
											//�������� �� �����������, �.�. � ��������� ��� ����������,
											//��������� �� ����� �������������� ������������ - �.�. ������� ������ �� ����� ������ ��� ������
		
		System.out.println("�������� � ������ 2 � �����: " + treeMap.tailMap(2, true));
		
		//-----------------------------------------------------���� 58-----------------------------------
		//�� �� ������, ��  � �����������
		//� ������� �����
		//0.   ������� ��������� � �����������
		LinkedHashMap<Integer, Student> linkhashMap2 = new LinkedHashMap( treeMap); 
		//1. �������� �� ���� ��������
		Set <Map.Entry<Integer, Student>>  setEntry2 = linkhashMap2.entrySet() ;
		int  totalSumm = 0;  //��� ������������ ����
		TreeMap<Integer, Student> resMap     = new TreeMap(); 					 //���  ��� ���������� ��������� ��������
		int resKey = 0;   //��� ������������ ����� ����
		ArrayList<Student>               resArrList = new ArrayList<Student>();    //������ ��� ���������� ��������� ��������
		Set<Map.Entry<Integer, Student>>  
		                                               resEntrySet = new LinkedHashSet();      //�������� ��� ���������� ��������� ���
		
		//2. � ����� ������� ��������
		for (Map.Entry<Integer, Student> elem : setEntry2) {
					Integer key1 =  elem.getKey();
			
			if (key1 >= 2)
				System.out.println("����: �������� � ����������� :" + elem.getKey() + "=" + elem.getValue());
			Student val1 = new Student();		//��� �� �������, � Student, ��� �������� ����������
																  //� ������� �  �����  ������ Student
						//������� ������������ ��������� � ������� ������ ��������, ���� � ������
			if (elem.getValue() instanceof Student)
				{val1 =  elem.getValue();   //��� �����������, ��������� � �������������� ������� ���������� ���������
													//����� ����������� � ����� �������� � �����������, �.�. 
													//�� ����� �������� �� ����� ��������� � ������ �������� map1
												//��� ��������� ������������� ���������� �������� ������������ �� ��������
												//������� ��������� ���� ������������ �� ���� �����-��������� ���������
			
			//3� �������� - ������ ������ ��������� �� �����
			elem.setValue( new Student( "������", "����")) ;  //������ �� ������ ������
			
			//4-� ��������: ��������� ���������� �� �����  ���������� � ������ ���� ������� � ����� ��� ������
			//������: 1. �������������� ����� ���������
			//                  2. ��������� �������, ������ �� ������-������, ��������� � ������ ���  ����������
			
			//��������� �� ���� ����� �������� �������� � �������� ��� ����� � ����������
			//totalSumm +=  elem.getValue().summ;			//���� ���� ������ ������ � �����
			totalSumm +=  elem.getValue().getSumm();  //����� ��������� ������� � ��������
			
			//�������� �������� ���� - ������: ��������� ���� ����� �� 1000
			//elem.getValue().summ += 1000; 		//���� ���� ������ ������ � �����
			elem.getValue().setSumm(  elem.getValue().getSumm() + 1000 );
			
			//5� ��������: ���������� ��������� �������� � ���� ����������
					//� ���������-���������, - ������
			resArrList.add(  elem.getValue() );
					//� ��� - -����������
			resMap.put( ++resKey , elem.getValue() ) ;   //���� ���������� �� ������ ��������� ������������ 
					//� ��������
			if (resEntrySet.add(elem) )   System.out.println("���� ���������");
			else System.out.println("���� �� ���������");
			
				}  //if
			
					
		} //foreach
		
		System.out.println( "����� ������ ���� ���������? :" + linkhashMap2);
		
		System.out.println("����� �����: " + totalSumm);
		
		
		//6-� ��������: ����� � �������������� ������� ����������
		//6.1.  ����� �� ������ - ������
		//������:  ����� ������� ����� �� ������-������
				//1. �������� ��� �� �������
		Student [ ] arrStudent = { new Student("������", "����"),
													 new Student("�������", "�����") ,
													 new Student("������", "����") ,
													 new Student("������", "����") } ;
		// ����� ������� ����� �� �������
		Student etalonAdr = arrStudent [ 3 ] ;  //�����-������ ������ �� �������
		
		HashMap<Integer, Student>  treeMap2 = new HashMap(); 
				//���������� ���� � �����
		for (int i = 0; i < arrStudent.length; i++) {
			treeMap2.put(i + 1, arrStudent [ i ] );
					
		}//for
		System.out.println("---------------------------------------------------------------------------------");
		System.out.println(treeMap2);
		
		Student   resRefAddr = null;		//���� �������� ��������� ������, ������ ��� �����
		
		//����� �� ������ ����� �������� , ����� ������ ������() � ������()  �� ��������������
					//containsValue() �������� ���  � ������� �������� ����� contains()
		boolean res = treeMap2.containsValue(  etalonAdr   );  // ��� ������� ������ �� ������-������ 
																	//������ ������() � ������ ������ ���� �� ��������������
		System.out.println("������ ������ ���� �� ������? : " + res);
		
		//2� �������: ����� �� �������� ����� �������
		Student etalon = new Student("������", "����", 0, 0);  //����������� ������ �� ��� ���� ������� ��� ��������
					//���� ������� � ������� ������� �������� ���� �� ������� �����
					//� ���� ����� �����������, ������� ��� ������� ����� � ���������� ������� �� �������� ��� ������ ������()
		
		
		//����� ��� ����� ������() � ������() ������ ���� �������������� � ������������ 
		//��� ����� �������� ������������ ��������
		//��� ������ containsValue() - ������� ���������� ����� ������() ��� ���������.
		
		System.out.println("������ ������ ���� �� ��������� ����� �������? : " + 
											treeMap2.containsValue(  etalon ) );
		
		//�����: ��� ������������ ������ �������� containsValue() � ����� � contains() � �������� � �������
		//����� ����� ��������:
		//1. �������������� ������ ������() � ������()  � ������, ��� �������� ���� ��� ��������� ���������
		//2. ��������� ����� ����� ��� ������ � ������� ����� ��������� ��� ����, ��
		//  �������� �����������  ���   ������ ������() � ������() ���, ����� ��� ������ �������� � ����������� ���������� ��������
		// 2.1 � ������ ������()  �������������� �� ����, ������� � �������� ����� ���� ������
		// 2.2 � ������ ������() �������� if (���� == 0 ). ��� if (���� == null)  �� ��� ���� �������, ������� this ������ �������()
		// 3.  ������� � ��� ������ ���� ��������� ��������� ����������� �� ����� ������  � ����������
		//4. ������� ������� ������� ������������ ����   Student etalon = new Student("������", "����", 0, 0); 
		//5.  ������� ����� ������ containsValue() � ����  ��� contains() � ������� ��� ������
		//���������: 
		//1. containsValue()  �������� ��������� � � �������� � � �������� - �.�. �� �������� 
		//   � ������ ������ ��� - � ��� �� ��������� �  � ���� ������� ��� �� ��������
		//2. ��� ��� ���� ����� ���� ����������� ������, ������� �������������� � �������� � 
		// ������������ ������� contains()/
		//  ������� ������� ���������� �� ���������� ��������� �������� ������
		
		
		//6.3.  ����� containsKey()  ���������� � �������� ���� ������() � ������(), ��� � � ��������
		//                      � � �������� �� ���������� ����������, ���  � � �������� 
		//  ��  �.�. ����� � �������� ����� ��� �������� ��� ������ - �� ��� ��� ����������� ��� ����.
		//������:  �����  ������� � ������ 2
		//�.�. ����� containsKey() ���������� ��������  ����������� ������, �� ��-����������� ���� ��� ������������
		//� �� ������������ ������
		 treeMap2.containsKey( 2 ) ;
		System.out.println("������� � ������ 2 ����?  : " +  treeMap2.containsKey( 2 ) );
		//.containsKey( 2 )   ���������� ��������� �������� ������  � �������� - ���������� ������ �����
		//� � ��������  - ������() � ������()   - ������  �����, � �� ������ , ������� � Value
		//
		
		//� ��� ���� ���� �������� ����� ����� ��������  - ����� ����
		
		//7. ������� ���� ��� ������ � ������
		//7.1 �������� ��������� ��������  ������ ����� �����
		//  �.�. �������� � ����� �� ���������, �� �� ���� �������� �� � ��� , � � ������-����
		Collection  coll = treeMap2.values();
		List  listValuesStudents =  new ArrayList(  treeMap2.values() );  //� ��������� Collection, �.�. values() ���������� ���������
		
		//7.2.  �������� ���������-��� ������ �� ����� ����� �����
		//�.�. ����� ���������, �� ������ ������������ ��������� � ���� ���.
		Set<Integer> setKeysStudents =  treeMap2.keySet();
		
		//7.3. �������� ��� , � ������� ���� (Entry) �� ����
		 Set<Map.Entry<Integer, Student>>  setEntryStudents =     treeMap2.entrySet();
		 
		 //7.4. ���������� �������� �����  foreach
		 for (Entry<Integer, Student> elemEntry :  treeMap2.entrySet()   ) {
			 Integer  key = elemEntry.getKey();
			 Student obj = elemEntry.getValue();
		}
		
		//7.5. ���������� ���� �� ��������� � ����������
		 //1� ������� � ���������
		 for (Iterator /*��� ��� ���������*/ iterator  = treeMap2.entrySet().iterator();     iterator.hasNext();            ) {
			
			 Entry<Integer, Student> elemEntry = (Entry<Integer, Student>) iterator.next(); //������� �����, �.�. � ����� ����� ��� ���������
			 
			 Integer  key = elemEntry.getKey();
			 Student obj = elemEntry.getValue();
		}
		 
		 //����� �� ����, �� ��� ��������
		 for (Iterator< Entry<Integer, Student>>  iterator  = treeMap2.entrySet().iterator();     iterator.hasNext();            ) {
				
			 Entry<Integer, Student> elemEntry = iterator.next(); //������� �����, �.�. � ����� ����� ��� ���������
			 
			 Integer  key = elemEntry.getKey();
			 Student obj = elemEntry.getValue();
		}
		 
		 //7.6.  ���� � ���������� �� ���� ������
		 for (Iterator<Integer> iterator =  treeMap2.keySet().iterator(); iterator.hasNext();          ) {
			Integer  key =  iterator.next();
		}

		 //7.7. ���� �� �������� ��������
		 for (Iterator<Student> iterator =  treeMap2.values().iterator(); iterator	.hasNext();                   ) {
			Student   elem =   iterator.next();
			
			elem.ID += 100;
			elem.setSumm( elem.getSumm() + 1000 );
		}
		 
		 //��� ��� � ����� ����� ����������, �.�. ���� �� ��������
		// for (Student elem : treeMap2)
		 
	} //main

}

//����� � ����������� Comparable  
//��� ������� ����� ������ �������, ����� ����� ����������� � ������ ������ ������ ��������� 
//��������� ������� � ����������� �������
class Student  implements Comparable<Student> {//�������� ��� �������� �������������� ���������
													//����� ����� ����� ������� ������� � ������� � � Student 
			String lastName;
			String name;
			int ID;
			private long summ;
			
			public long getSumm() { 	return summ; }

			public void setSumm(long summ) { 	this.summ = summ; 	}

			static int count = 0;
					//
			public Student(String lastNamePar, String namePar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = count;
				summ = ID * 1000;
			}
			
			public Student(String lastNamePar, String namePar, int  ID_Par, long sumPar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = ID_Par;
				summ = sumPar;
			}
			
			public Student() {
						// TODO Auto-generated constructor stub
					}
			//@Override	//����� ��������� comparable
			public int compareTo(Student oPar) {//�������� ����
				// TODO Auto-generated method stub
				Student tmp = oPar;
					
				int res = lastName.compareTo(oPar.lastName);
				return (res == 0  ? -name.compareTo(oPar.name): -res);
			}//compareTo
			
					//��� ������ ��������� �� �����
			public String toString (){
				return ID + " "+lastName + " "+name + " " + summ;
			}
			
			//----------------------------------------------------------------------
			//��� ����, ����� ����� � ������������� ���������� �� �� �������-������, � �� ��������� ����� - 
			//����� �������������� ������ ������() � ������()
			
			//������ �������� ������� ������() � ������() �� �����  �������������� ��������
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result
						+ ((lastName == null) ? 0 : lastName.hashCode());
				result = prime * result
						+ ((name == null) ? 0 : name.hashCode());
				
			//����, ������� ����� ���� �� ��������� � ������� �� ������ ��������� ������� 
				//� ������������ �������
			//�� ���� ������� ������ ������������ �� ������ ��������������� ���� ����� ��������
	//				result = prime * result + ID;
	//				result = prime * result + (int) (summ ^ (summ >>> 32));
				return result;
			}
			
			@Override
			public boolean equals(Object obj) {  
				//���� �������� �� ����, ��� ������-�������� ������ contains()  - �.�. ������
				//���� ��������  ������ equals()
	
				Student other = (Student) obj;
				if (this.lastName == null) {   //���� � �������� ������ ������� Null - �� �� ���������� ������� 
											         //� ���������� ����� ������
				} else if (!lastName.equals(other.lastName))
					return false;
				if (this.name == null) { //���� � �������� ������ ����� Null - �� �� ���������� ����� 
												//� ���������� ����� ������
				} else if (!name.equals(other.name))
					return false;
				if ( this.ID == 0 ) {		//���� � �������� ������ ID ���� - �� �� ���������� ����� 
											//� ���������� ����� ������
					
				}
				else if (ID != other.ID)  
					return false;
				if (this.summ == 0) {		//���� � �������� ������ summ ���� - �� �� ���������� ����� 
												//� ���������� ����� ������
					
				}
				else if (summ != other.summ)
					return false;
				return true;		//���� ��� �������� - �� ������� ������
			}
			

			
		}//class
		

