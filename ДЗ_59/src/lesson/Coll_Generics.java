package lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


//������� ������� ��� ��������� ������������� ����������

public class Coll_Generics {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Student2 [ ] arrStudent = { new Student2("������", "����"),
				 new Student2("�������", "�����") ,
				 new Student2("������", "����") ,
				 new Student2("������", "����") } ;
		
		Set setObj = new TreeSet();
		Set<Student2> setGenericStudent = new TreeSet();
		
		//������� ������� ������� �� ������� - �����, �.�. � �������� ���� ����������
		setObj.addAll(  Arrays.asList(arrStudent)  );
		setGenericStudent.addAll( setObj );
		System.out.println( setObj);
		System.out.println( setGenericStudent);
		
		//setObj.add(7);			//� ����  ��������� � ����  �����
		//setGenericStudent.add(7);  //� ��� ��������� ���������� ���������� ������� 
														//�������������� �����
		
		setObj.clear();
		setObj.add( 7 );  //� ��������� ������ 7
		
			//1� ������ ������������� ������ � ��������� - ����������	
			//��-�� ��������� ������������� ���������� ���������� ��������� ����������� 7-�� � ��������� ���������
		setGenericStudent = setObj;   //���������� ������, ���� ����� �������� <Student>, � ������ � ��������� �����
														//������� ������� �������� ����������� - ��������� ������������� ���������
												//��������� ���, ���� � setGenericStudent ��� ������ �������� Student
											//7-�� �������� � ��������� � ����������
		
		System.out.println( setObj);
		System.out.println( setGenericStudent);  //���������� ��� ��� ���, �.�. �� ���������� ������ ����� �������,
														//� ���������� ������, ������� ���� � ������ �������
		//2� ������ ������������� ������ � ��������� - �������������
		setGenericStudent  = new TreeSet<Student2>(setObj) ;  //	���������� ������, �.�. ��������� ������ �� �������������, 
																				//�.�. ������ ������-����������� ����� �������� ����� ��������������
		
		System.out.println( setGenericStudent);  //7-�� �������� � ��������� � ����������
		
		//3� ������ ������������� ������ � ��������� - �������
//��� ���������� � ������� �������, �.�. 7-�� ������������� � add()  
//��� ��������� ������������ � ������  �������� �� ����� ���������� ������ �������
		//setGenericStudent.addAll(  Arrays.asList(arrStudent)  ); 
		//setGenericStudent.add( arrStudent[0] ); 			            

//��� ���������� � ������ ������� �������� ���, �.�. 7-�� �� ������������� � add()  
//�.�. ��������� ������������ �������� � ������ �������� ������� equals() ������ �������,
//� ������ ������ ������� �� �������� ����������, �.�. �������� � ������ ��������		
		HashSet<Student2> hashGenericStudent = new  HashSet(); 
		hashGenericStudent.addAll(setObj);  //����� 7-�� ����������� � ������
		hashGenericStudent.addAll(Arrays.asList(arrStudent)  );
		hashGenericStudent.add( arrStudent[0]  );
		System.out.println( "������ � �������� 7-���: " + hashGenericStudent );
		
//��� ���������� � ������ �������� ���, �.�. 7-�� �� ������������� � add()  
//�.�. ���  ��������� ������������ � ������ �������� 
		List<Student2> listGenericStudent = new  ArrayList(); 
		listGenericStudent.addAll(setObj);  //����� 7-�� ����������� � ������
		listGenericStudent.addAll(Arrays.asList(arrStudent)  );
		listGenericStudent.add( arrStudent[0]  );
		System.out.println( "������ � �������� 7-���: " + listGenericStudent );
		
		//����� ������ ������� - �.�. ����� ���������� ������� ???
		//������� ���������� �����, ����� �� ������� ������������ ����� ������  Student c 7-��� ���
		//����� ������� ������������ 7-�� � (Student)
		//for (Student elem : setGenericStudent) {  //���� ���� ����� ����� ������� �������
			//elem.name = "ljhrkltj";
		//}
		
		//���� ������ ��� ������������� ������� �� ���, �� �������� ������ ���� ����� �� ����
		for (Object elem : setGenericStudent) {   //   Student �������� �� Object
			//������� �� ���� �������� � �������� ������ Student
					//������� - ���� �������
		 //String str = 	((Student )elem).name;  //�������, ��� ������� �� �������� 7-��
			
							//����� �� �������� � ��������� ��-�� ������ ����� ������ �������� ����������
							//��� ���� ������ ��� ������� �� ���,
						//� ���� ��� ����� �� - �� ���������� ����� ������� � ����������
			if (elem instanceof Student2)
				System.out.println("��� student");
			else 
				System.out.println("� ��������� ����� �� i-�� �������� �����");  //� ���� ����� ������ ������� ����� ��������
			
					//��� ���������� - �.�. ������ ���� ������� ����� �� ���������.
			System.out.println( elem.getClass() );  //������� �������� ������ � ��������� ����
			
		}
		
		
	} //main

}


//����� � ����������� Comparable  
//��� ������� ����� ������ �������, ����� ����� ����������� � ������ ������ ������� ��������� 
//��������� ������� � ����������� ������� - ������ �� � ������ �������
		class Student2  implements Comparable<Student2> {//�������� ��� �������� �������������� ���������
													//����� ����� ����� ������� ������� � ������� � � Student 
			String lastName;
			String name;
			int ID;
			private long summ;
			
			public long getSumm() { 	return summ; }

			public void setSumm(long summ) { 	this.summ = summ; 	}

			static int count = 0;
					//
			public Student2(String lastNamePar, String namePar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = count;
				summ = ID * 1000;
			}
			
			public Student2(String lastNamePar, String namePar, int  ID_Par, long sumPar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = ID_Par;
				summ = sumPar;
			}
			
			public Student2() {
						// TODO Auto-generated constructor stub
					}
			//@Override	//����� ��������� comparable
			public int compareTo(Student2 oPar) {//�������� ����
				// TODO Auto-generated method stub
				Student2 tmp = oPar;
					
				int res = lastName.compareTo(oPar.lastName);
				return (res == 0  ? -name.compareTo(oPar.name): -res);
			}//compareTo
			
					//��� ������ ��������� �� �����
			public String toString (){
				return ID + " "+lastName + " "+name + " " + summ;
			}
			
			//----------------------------------------------------------------------
			//��� ����, ����� ����� � ������������� ���������� �� �� �������-������, � �� ��������� ����� - 
			//����� �������������� ������ ������() � ������()
			
			//������ �������� ������� ������() � ������() �� �����  �������������� ��������
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result
						+ ((lastName == null) ? 0 : lastName.hashCode());
				result = prime * result
						+ ((name == null) ? 0 : name.hashCode());
				
			//����, ������� ����� ���� �� ��������� � ������� �� ������ ��������� ������� 
				//� ������������ �������
			//�� ���� ������� ������ ������������ �� ������ ��������������� ���� ����� ��������
	//				result = prime * result + ID;
	//				result = prime * result + (int) (summ ^ (summ >>> 32));
				return result;
			}
			
			@Override
			public boolean equals(Object obj) {  
				//���� �������� �� ����, ��� ������-�������� ������ contains()  - �.�. ������
				//���� ��������  ������ equals()
	
				Student2 other = (Student2) obj;
				if (this.lastName == null) {   //���� � �������� ������ ������� Null - �� �� ���������� ������� 
											         //� ���������� ����� ������
				} else if (!lastName.equals(other.lastName))
					return false;
				if (this.name == null) { //���� � �������� ������ ����� Null - �� �� ���������� ����� 
												//� ���������� ����� ������
				} else if (!name.equals(other.name))
					return false;
				if ( this.ID == 0 ) {		//���� � �������� ������ ID ���� - �� �� ���������� ����� 
											//� ���������� ����� ������
					
				}
				else if (ID != other.ID)  
					return false;
				if (this.summ == 0) {		//���� � �������� ������ summ ���� - �� �� ���������� ����� 
												//� ���������� ����� ������
					
				}
				else if (summ != other.summ)
					return false;
				return true;		//���� ��� �������� - �� ������� ������
			}
			

			
		}//class
		