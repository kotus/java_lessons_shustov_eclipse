package homeWork;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import bankSystem.Client;
import bankSystem.Summ;

public class ClientSetMap extends HashMap<Integer, Client>{
	
	private static final long serialVersionUID = 1L;
	private HashMap<Integer, Client> clientHashMap = new HashMap<Integer, Client>();
	
	{
		Long summ = 100L;
		try {
			ArrayList<Client> arrOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
			for (Client client : arrOfClients) {
				client.getAccount().setSumm(new Summ(summ, "UAH"));
				summ = summ + 100;
				clientHashMap.put(client.getID(), client);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
			
	public ClientSetMap(HashMap<Integer, Client> treeMapOfClients) {
		super(treeMapOfClients);
		this.clientHashMap = treeMapOfClients;
	}
	
	public ClientSetMap() {
		super();
	}

	public static void main(String args[]) {
		
		ClientSetMap clientMap = new ClientSetMap();
		clientMap.putAll(clientMap.clientHashMap);
		
		LinkedHashMap<Integer, Client> linkedMapClent= new LinkedHashMap<Integer, Client>(clientMap);
		
		//System.out.println(clientMap.myGetMaxSummFromAllClients(clientMap.clientHashMap));
		System.out.println(clientMap.myGetMaxSummFromAllClientsTree(clientMap.clientHashMap));
		
	}
	
	public Client myGetMaxSummFromAllClients(HashMap<Integer, Client> clientHashMap) {
		
		Long maxSumm = 0L;
		Client clientMaxSum = null;
		
		Set<Map.Entry<Integer, Client>> entrySet = clientHashMap.entrySet(); 
		for (Map.Entry<Integer, Client> entry : entrySet) {
			if (entry.getValue().getAccount().getSumm().getSumm() > maxSumm)
				clientMaxSum = entry.getValue();
				maxSumm = clientMaxSum.getAccount().getSumm().getSumm();
		}
		
		return clientMaxSum;
		
	}
	
	public Client myGetMaxSummFromAllClientsTree(HashMap<Integer, Client> clientHashMap) {
		
		Client clientMaxSum = null;
		
		TreeMap<Integer, Client> treeMapOfClients = new TreeMap<Integer, Client>(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1,
					Integer o2) {
				// TODO Auto-generated method stub
				return 0;
			}

			
		});
		
		treeMapOfClients.putAll(clientHashMap);
		
		System.out.println(treeMapOfClients);
		
		return clientMaxSum;
		
	}
	
}
