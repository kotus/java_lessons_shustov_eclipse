package homeWork;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;

public class Delete {

	public static void main(String[] args) {

		// TreeSet<Integer> treeSet = new
		// TreeSet<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
		// System.out.println(treeSet.descendingSet());

		// 5.1. ������� ������� ��������������� �� 10-�� ��������� .

		Deque<Integer> deque = new LinkedList<Integer>();

		for (int i = 1; i <= 5; i++) {
			deque.addLast(i);
		}
		for (int i = 6; i <= 10; i++) {
			deque.addFirst(i);
		}

		System.out.println(deque);
		
		TreeSet<Integer> less5 = new TreeSet<Integer>();
		LinkedList<Integer> bigger5 = new LinkedList<Integer>();
		for (Iterator iterator = deque.iterator(); iterator.hasNext();) {
		    Integer currentElement = (Integer) iterator.next();
		    if (currentElement < 5) {
		        less5.add(currentElement);
		    } else if (currentElement > 5){
		        bigger5.add(currentElement);
		    }
		}

		System.out.println("less5: " + less5);
		System.out.println("bigger5: " + bigger5);

		Collections.sort(bigger5, Collections.reverseOrder());
		System.out.println("bigger5: " + bigger5);
		
		
		System.out.println();
		
	}

}
