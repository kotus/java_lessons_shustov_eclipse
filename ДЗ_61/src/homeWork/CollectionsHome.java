package homeWork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import bankSystem.Account;


public class CollectionsHome{

	public static void main(String[] args) {

		/*
		Set set = new TreeSet();
		
		set.add(1);set.add(2);set.add(2);set.add(1);
		
		System.out.println(Collections.frequency(set, 1));
		*/
		
		ArrayList<Integer> coll1 = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7));
		ArrayList<Integer> coll2 = new ArrayList<Integer>(Arrays.asList(5,6,7));
		
		//System.out.println(Collections.indexOfSubList(coll1, coll2));
		
//		System.out.println(coll1);
//		
//		Collections.swap(coll1, 3, 4);
//		
//		System.out.println(coll1);
//		
//		System.out.println(Collections.indexOfSubList(coll1, coll2));
		
//		System.out.println(coll2);
//		
//		coll2 = (ArrayList<Integer>) new CollectionsHome().addNumbersToCollection(coll2, 1,2,3,4,5,6,7);
//		
//		System.out.println(coll2);
		
		Account acc1 = new Account();
		Account acc2 = new Account();
		Account acc3 = new Account();
				
		ArrayList<Account> listAcc = new ArrayList<Account>();
		listAcc = (ArrayList<Account>) new CollectionsHome().addNumbersToCollection(listAcc, acc1, acc2, acc3);
		
		System.out.println(listAcc.size());
		
	}
	
	/*
	4.1. ������� �����, ������� ����� �������� � ��������� ���1 ��������� ����� �� ���� �����,
	� ���������� ��� ���������. ����� �� ���������� � ������, � �������� ��� ����� �� ���� �����
	������.
	*/
	
	private <T> Collection<T> addNumbersToCollection(Collection<T> collection, T...elements) {
		
		for (int i = 0; i < elements.length; i++) {
			collection.add(elements[i]);
		}
		
		return collection;
		
	}

}
