package paramTypes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import bankSystem.BankCard;
import bankSystem.Bankomat;
import bankSystem.Client;
import bankSystem.Staff;

public class ParamType <T1, K1> {

	T1 fld1;
	K1 fld2;
		
	public ParamType(T1 fld1, K1 fld2) {
		super();
		this.fld1 = fld1;
		this.fld2 = fld2;
	}
	
	public ParamType() {
		super();
	}
	
	@Override
	public String toString() {
		return "ParamType [fld1=" + fld1 + ", fld2=" + fld2.toString() + "]";
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {

		Vector<ParamType<Integer, Client>> vectorClient = new Vector<ParamType<Integer,Client>>();
		Vector<ParamType<Integer, Staff>> vectorStaff = new Vector<ParamType<Integer,Staff>>();
		Vector<ParamType<Integer, BankCard>> vectorBankCard = new Vector<ParamType<Integer,BankCard>>();
		Vector<ParamType<Integer, Bankomat>> vectorBankomat = new Vector<ParamType<Integer,Bankomat>>();
		
		ArrayList<Bankomat> arrOfBancomats = Bankomat.makeCollBankomat("src/bancomats.in");
		ArrayList<Staff> arrOfStaffs = Staff.makeCollStaff("src/staff.in");
		ArrayList<Client> arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
		ArrayList<BankCard> arrayOfBankCards = BankCard.makeCollOfBankCards("src/master-visa-2014-05-14---01.in");
		
		ParamType pt = new ParamType();
		
		pt.addObjectsToVector(vectorClient, arrayListOfClients);
		pt.addObjectsToVector(vectorStaff, arrOfStaffs);
		pt.addObjectsToVector(vectorBankCard, arrayOfBankCards);
		pt.addObjectsToVector(vectorBankomat, arrOfBancomats);
		
		System.out.println(vectorClient);
		System.out.println(vectorStaff);
		System.out.println(vectorBankCard);
		System.out.println(vectorBankomat);
			
	}
	
	private void addObjectsToVector(Vector<ParamType<Integer, K1>> vector, Collection<K1> array) {
		
		int i = 1;
		for (K1 elem : array) {
			ParamType<Integer, K1> paramType = new ParamType<Integer, K1>(i, elem);
			vector.add(paramType);
		}
									
	}
	
	public static Double createRandomCollArrayList(Collection<Integer> arr) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < 1000000; i++) {
			arr.add(new Random().nextInt(1000));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
}

