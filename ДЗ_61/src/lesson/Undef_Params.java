package lesson;

import javax.swing.JButton;

//�������������� ��������� - ��� ��������� �������  � �������������� �����������, 
//�� ���������� �����

//String [ ]  args  - ��������� ��������� ������ 

public class Undef_Params {

	void met( int par1, int par2) {
		System.out.println("������� ����� � 2-�� �����������");
	}

	//����� � ��������������� �����������
	void met( int ... pars) {  //��� ����������, � ���������� - �����
		//pars - ��������������� ��� �� ���� ������
		System.out.println("����� � �����. �����������");
		for (int elem : pars) {
				System.out.println("��������� ��������: " + elem);
		}
		int  x = pars[0];  //���� �������, ����� ������ �� �������� ���������� ���������� -1
}
	
	void met( String ... pars) {  //��� ����������, � ���������� - �����
		for (String elem : pars) {
			System.out.println("��������� ��������: " + elem);
		}
		String  x = pars[0]; 
	}
	
	//�����������:
	//1. ��������� ... �������� ������ � ��������� ���������
	//void met(long par1, int ... pars, String par3) {
	void met(long par1, /* int ... pars,*/ String par3, int ... pars) {
		
	}
	//2. �������� � ����������� �������
	
	//������ ����� �����. ��������� � ��������� ���,
	int  met(long par1, String par2, JButton ... pars) { 
				//�� ���� ��������, ��� ��� �������� ���, � ���������� ��������� ����  ������ ���� ���������
				//� �� ���� ������� ����� ������ ����������� ���������
		return 0;
	}
	
	int  met(long par1, String par2, int  par3, JButton   [ ]  parArr) {  //� ����� ������ �������� � �������� 
			///�� ��������� ������� ��������� ������ ������ � ������ ��� ���������� ������ � ������� �����
				//��� ������� �������
		return 0;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Undef_Params obj = new Undef_Params();
		obj.met(1,3);
		obj.met(1);
		obj.met(1,3,7);
		
		obj.met("", "���", "���");
		
		obj.met("", new String("kjfhgkfjh"), "���");
		
		//--------------------------------             String [ ]  args  ------------------------------------------
		System.out.println("--------------------------------------------------------------------");
		for (int i = 0; i < args.length; i++) {
			System.out.println( args[ i ] );
		}
		for (String elem : args) {
			System.out.println( elem );
		}
		
	}//main

}
