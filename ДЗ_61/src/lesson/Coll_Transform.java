package lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

public class Coll_Transform {

	int[ ] makeArr(/* int[ ]   arrLocalPar */      ){
		int [ ]  arrLocalPar = {1,3,4,6,7,8,8,};
		return arrLocalPar;
	}
	
	
	public static void main(String[] args) {
		
		//1. �������������� �� �������� � ���� ������������� ������� - Arrays.asList() 
		//asList() ���������� ������ List
		Integer [ ]   arr = {1,4,2,5,43,3,5,6,7,7};
		Arrays.asList();
		
		//2. �� ��������� � �������
		Collection<Integer>  collInt = Arrays.asList( arr );
		//2.1. toArray() - [] Object
		Object [ ]    arrObj = collInt.toArray(); //�������� ����� ����������
		System.out.println( Arrays.asList(arrObj)  );
		//arrObj[0].   //�������� ������ ������� ����������� �� ����� �������, �.�. ������ ������ ������ �������
		//Student [ ]    arrStudent = (Student [ ]) collInt.toArray();   //������� �������
		Integer [ ]    arrInt = (Integer [ ]) collInt.toArray();
		
		//2.2 toArray( T [ ]   arr  ) - T [ ]  -  ���� ����� ����� ��������� ��������
		//Integer [ ] arr2  = collInt.toArray( arr2 );  //����������� ��������� ������� � ��������� � 
										//����������� ��� �� ����� �� - ����������� � ��� �� �� �����\���, �.�. ��������� � ����� ����������
		Integer [ ] arr2  = collInt.toArray( new Integer[0] );  //� ��������� �������� ����� ������ ���� ����, 
																		//������ ������� �� ����� �������
		//Student [ ] arrSt  = collInt.toArray( new Integer[0] );  //�������� ����� �� ����� ���������� 
		//arr2[0].		//������ ������� ������� ����� ������ ������ ����
		
		System.out.println( Arrays.asList(arr2)  );
		
		//3. �������������� �� ��������� � ���������
		//3.1. � ������� ������������� - ��������, ��� �������� ����� ���������
		// �� �����, � ����� ������������� 
		//�� ��������� ������ ���� � ����-� ������ ����
		Vector<Boolean> vecBool = new Vector(collInt);
		Vector<Student> vecSt = new Vector(collInt); //�� ������� , �� ���������� �� ����� �������������� ��������� ������
		System.out.println( vecSt );
		//vecSt.get(0).lastName = "������";  //�������, �.�. ������������� ���� ��� ������ �� ��������
		
		//3.2. � ������� �������� ���������� - ������, �� �� ������ ��������
		//��� ���������� �������� �������� 2 �������������� �/��� �������������
		//1 - �������������� ����� ����� ���������
		//2 - �������������� ����� ��������� ������ ��������� ( �.�. ����������)
		//��� ��������� ��-������������ ��������� - �.�. ���������� ������ ����� ������������� ������������� � ��������
		//�� ��������� ��-��������� (�.�. �� ������������� �� ���������� � ��������)
		
		//3.2.1.
		Vector<Integer> vecInt  = new Vector (Arrays.asList( arr ) );
		collInt = vecInt;   //��������� ��-���������, � ���������  ���������
								//��� ������ ��������� �� ���� � �� �� ���������
								//������ ����� ����� ������ ������ ������������� �� ������ ���������
								//���������� - ��� �� �����-�����������
		// �������������� � ������������ ������ ��������� ������ �������� ����� ����� ��������� 
		//����� ������ ���������, �.�. ������-������ ���������, ����� ������ ��������� �� �����
		//����� ������ ������������ ��������� �������� ������ �����.
		//�� ����� ��������. ��� ��������� �������� ����� ������������ ������� - 
		//�..� ��� �� ���������� ����� ���� Collection, � ������  ����� �� ����� ������. 
		//� ����� ������ ��������� ������� � ������ ����� � ����� ����� ���������
								
		
		
		//3.2.2
		//Collection<Number>  collNum = vecInt;  //�������������� ���������� - �������� �� ��-������������ ��������� �
															//��������� ��-������������ ����������  
															//��������� ������ ���������
		
		//3.2.3			//��������� ��-���������, �� ��������� �� �������
		//collInt = vecSt;		
		//collInt = vecBool;
		
		//3.2.4.  �� ��-���������� ��������� ������ �������������
		//vecInt = collInt ;  
		
		//4. ������������� ��������������.
		//4.1. null - ��������� - ��� �����, �..�. �� ������������� � ��������� � �� ������� - ����� ������� ����������
		Collection<Integer> coll2 = new ArrayList<Integer>(collInt);  //���������� ���������� - ��������� ���������, �������� ��-���������
		Collection<Integer> coll3 ;
		//Collection<Integer> coll4 = new ArrayList<Integer>(coll3);  //��� ���������� �������� �� �� �������������� �����, 
																	//��� �������� � ���, ��� ��������� � ��������� �� ����������������
		//4.2 ��������� ������ ���������, ��� ��-���������
		//Collection<Object> coll5 = new ArrayList<Integer>(collInt);  //����� ��������������-���������� ����������
						//�.�. ��������� ��-���������
		
		//4.3. �������� ������ ���� ��-��������� (����� ��������, � ������ - ���������) 
		ArrayList<Integer> arrList = new ArrayList<Integer>();
		
		//vecInt = arrList;  //��� ��������� �� ������-��������. �.�. �� ��_��������� - 
									//������� ��������������� ���������� - ��. ����������
		
		
		//5. �������������� ��������� ������� Collections
		//���� ����� ��������� ��������� ��������� ������������  ��������� ���� �����������
		//����� ��������� �� ���������� �� �� ������� �� �� ����������� - �.�. ���� �������� ������ ������ ������
		//����� �� - ��� ��� �� ����-������ ��������� � ������������ ������ ������� (�������� ���� �����)
		Collection<Integer> collNotModified = Collections.unmodifiableCollection( arrList );
		System.out.println(collNotModified);
		
		//------------------------------------------------------------����� COLLECTIONS------------------------
		//����� �����������, ��� � Arrays , Math - �.�. � ��� ������ ������-������
		
		//�������� ������-���������� , ��������� �� ���������� ������
		// empty... - ������� ������ ���������, ������������, ����� �������� null � ���������� � �������
		//               ������� �����������.  Null ���������� �� ������������ ��� ������  ���������, �.�. 
		//   Null 1. �� ����� ������� � 2. ���� � ���������� � ��. ����� ����������� nullPointerException
		//asLifoQueue (Dequeue) -  ����������� ��� (2-��������� �������) � ������� ���� (LIFO)
		//binarySearch - �������� ������ � ��������������� ���������� ��� ������� ���������
		//                  ������ ����� ���� ����������
		//checked...SortedSet - ���������� ���������, � ������� �������� ��-��������� - �.�. �� ����������� 
		//				�������������� � ���������, ��� ��� ���������� - ��� ���������, ��� ����������
		//disjoint - ���������� true ���� ��������� � 2� ���������� �� ����� ������ ������
		//newSetFromMap - ��� �����, � ������� ��� ������� keySet - ��� ��������� ���� ��� ��-�����������
		//						��� ������� ����� ���� ����� �� �����
		//reverseOrder() - Comparator   - ������ ��� ������������� ����� � ���������� ������� ����������.
		//      ��� �� ��� ������� "������������� ��������"
		//   Arrays.sort( coll,  Collections.reverseOrder() )
		//singleton( o )  - ��� ��������, ������� ���������, � ������� ���� ������� - ������ �� ���������.
		//                          ������ ��� ����� � ���������� �����������, ������� � ��������� ������ ���������
		//                       , ��������  removeAll( Collections. singleton( o ) )  //�������� ���� �������� ������ ��������� 
		
		
		SortedSet sortedSet = new TreeSet();		
		
		Set<Integer> set = new HashSet<Integer>();
		
		TreeSet treeSet = new TreeSet();
		
		sortedSet = treeSet;
		
		
		
	} //main

}

//����� � ����������� Comparable  
//��� ������� ����� ������ �������, ����� ����� ����������� � ������ ������ ������� ��������� 
//��������� ������� � ����������� ������� - ������ �� � ������ �������
		class Student  implements Comparable<Student> {//�������� ��� �������� �������������� ���������
													//����� ����� ����� ������� ������� � ������� � � Student 
			String lastName;
			String name;
			int ID;
			private long summ;
			
			public long getSumm() { 	return summ; }

			public void setSumm(long summ) { 	this.summ = summ; 	}

			static int count = 0;
					//
			public Student(String lastNamePar, String namePar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = count;
				summ = ID * 1000;
			}
			
			public Student(String lastNamePar, String namePar, int  ID_Par, long sumPar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = ID_Par;
				summ = sumPar;
			}
			
			public Student() {
						// TODO Auto-generated constructor stub
					}
			//@Override	//����� ��������� comparable
			public int compareTo(Student oPar) {//�������� ����
				// TODO Auto-generated method stub
				Student tmp = oPar;
					
				int res = lastName.compareTo(oPar.lastName);
				return (res == 0  ? -name.compareTo(oPar.name): -res);
			}//compareTo
			
					//��� ������ ��������� �� �����
			public String toString (){
				return ID + " "+lastName + " "+name + " " + summ;
			}
			
			//----------------------------------------------------------------------
			//��� ����, ����� ����� � ������������� ���������� �� �� �������-������, � �� ��������� ����� - 
			//����� �������������� ������ ������() � ������()
			
			//������ �������� ������� ������() � ������() �� �����  �������������� ��������
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result
						+ ((lastName == null) ? 0 : lastName.hashCode());
				result = prime * result
						+ ((name == null) ? 0 : name.hashCode());
				
			//����, ������� ����� ���� �� ��������� � ������� �� ������ ��������� ������� 
				//� ������������ �������
			//�� ���� ������� ������ ������������ �� ������ ��������������� ���� ����� ��������
	//				result = prime * result + ID;
	//				result = prime * result + (int) (summ ^ (summ >>> 32));
				return result;
			}
			
			@Override
			public boolean equals(Object obj) {  
				//���� �������� �� ����, ��� ������-�������� ������ contains()  - �.�. ������
				//���� ��������  ������ equals()
	
				Student other = (Student) obj;
				if (this.lastName == null) {   //���� � �������� ������ ������� Null - �� �� ���������� ������� 
											         //� ���������� ����� ������
				} else if (!lastName.equals(other.lastName))
					return false;
				if (this.name == null) { //���� � �������� ������ ����� Null - �� �� ���������� ����� 
												//� ���������� ����� ������
				} else if (!name.equals(other.name))
					return false;
				if ( this.ID == 0 ) {		//���� � �������� ������ ID ���� - �� �� ���������� ����� 
											//� ���������� ����� ������
					
				}
				else if (ID != other.ID)  
					return false;
				if (this.summ == 0) {		//���� � �������� ������ summ ���� - �� �� ���������� ����� 
												//� ���������� ����� ������
					
				}
				else if (summ != other.summ)
					return false;
				return true;		//���� ��� �������� - �� ������� ������
			}
			

			
		}//class
