package pack3;

public class Client {

	private String surname;
	private String name;
	private String patronymic;
		
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public Client(String surname, String name, String patronymic) {
		super();
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(new Client("������", "����", "��������"));
	}
	
	@Override
	public String toString() {
		return "����� Client:[���=" + surname + "; ���=" + name
				+ "; ��������=" + patronymic + "]";
	}

	
	
}
