package pack3;

public class Account {
	
	private Long accountNumber;
	private Summ summ;
	
	public Long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Summ getSumm() {
		return summ;
	}
	
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	

	public static void main(String args[]){
	
		//System.out.println(new Account(1234567890L, 1000L, "UAH"));
		
	}
	
	public Account(Long accountNumber, Long summ, String valuta){
		super();
		this.accountNumber = accountNumber;
		this.summ = new Summ(summ, valuta);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "����� Account:[����� ����� = "+accountNumber+";"+summ.toString()+"]";
	}
	

	
}
