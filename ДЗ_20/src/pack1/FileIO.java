package pack1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;

import arrays.Mass;



public class FileIO {

	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		FileIO dz20 = new FileIO();
		
		/*
		dz20.dz_20_punkt_1_3("src/DZ_20.in");
		
		int[][] resMatr = dz20.dz_20_punkt_1_2("src/DZ_20.in");
	
		dz20.writeMatrixToDisk("src/DZ_20.out", resMatr);
		dz20.writeMatrixToDisk("src/DZ_20.out", resMatr);
		dz20.writeMatrixToDisk("src/DZ_20.out", resMatr);
		
		dz20.dz_20_punkt_1_6("src/DZ_20.in", "src/file_rez_1.5.out");
		dz20.dz_20_punkt_1_5("src/DZ_20.in", "src/file_rez_1.6.out");
		*/
		
		//dz20.dz_20_punkt_1_7("src/forum.log", "src/DZ20_1.6.txt", 10, 15);
		dz20.dz_20_punkt_1_5("src/DZ_20.in", "src/file_rez_1.5.out", 10, 100);
		
	}
	
	private void dz_20_punkt_1_5(String inputFileName, String outputFileName, int lowerValue, int upperValue) throws FileNotFoundException, IOException {
		
		FileIO dz20 = new FileIO();

		int [][] inputMatrix = dz_20_punkt_1_2(inputFileName);
		int [] array = Mass.dz_20_task_1_5(inputMatrix, 10, 100);
		dz20.writeArrayToDisk(outputFileName, array);
	
	}
	
	private void dz_20_punkt_1_6(String inputFileName, String outputFileName) throws FileNotFoundException, IOException {
		
		FileIO dz20 = new FileIO();

		int [][] inputMatrix = dz_20_punkt_1_2(inputFileName);
		int [] array = Mass.findMaxInLine(inputMatrix);
		dz20.writeArrayToDisk(outputFileName, array);
	
	}
	
	private void dz_20_punkt_1_7(String inputFileName, String outputFileName, int startRow, int endRow) throws FileNotFoundException, IOException {
		
		BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName, true));
		String str = "";
		for (int i = 0; (str = reader.readLine()) != null; i++) {
			if(i < (startRow-1)){ 
				continue;				
			}
			else if (i > (endRow-1)){
				break;
			}
			writer.append(str);
			writer.newLine();
		}
		reader.close();
		writer.close();
	
	}
	
	private void dz_20_punkt_1_3(String fileName) throws FileNotFoundException, IOException{
		
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		reader.mark((int) (new File("src/DZ_20.in").length() + 1));
		
		int rowCount = 0;
		int colCount = 0;
		String str = "";
		while ((str = reader.readLine()) != null) {
			rowCount++;
			if (str.split(",").length > colCount){
				colCount = str.split(",").length;
			} 
		}
		
		reader.reset();
		
		System.out.println("row = "+ rowCount + " col = " + colCount);
		
		Long[][] resMatr = new Long[rowCount][colCount];
		for (int i = 0; (str = reader.readLine()) != null; i++) {
			String[] strbuf = str.split(",");
			for (int j = 0; j < strbuf.length; j++) {
				if (!strbuf[j].isEmpty()){
				resMatr[i][j] = Long.valueOf(strbuf[j].trim());
				}
				else continue;
			} 
		}
		
		reader.close();
		
		Mass.showMatixOfNumbers(resMatr);
		
	}
	
	private int[][] dz_20_punkt_1_2(String fileName) throws FileNotFoundException, IOException{
		
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		reader.mark((int) (new File("src/DZ_20.in").length() + 1));
		
		int rowCount = 0;
		int colCount = 0;
		String str = "";
		
		while ((str = reader.readLine()) != null) {
			
			rowCount++;
			StringTokenizer tokenizer = new StringTokenizer(str, ",");
			if(tokenizer.countTokens() > colCount){
				colCount = tokenizer.countTokens();
			}
			
		}
		
		reader.reset();
		
		System.out.println("row = "+ rowCount + " col = " + colCount);
				
		int[][] resMatr = new int[rowCount][colCount];
		for (int i = 0; (str = reader.readLine()) != null; i++) {
			
			StringTokenizer tokenizer = new StringTokenizer(str, ",");
			for (int j = 0; tokenizer.hasMoreTokens() ; j++) {
				resMatr[i][j] = Integer.valueOf(tokenizer.nextToken().trim());
			}
			
		}
		
		reader.close();
		
		return resMatr;
		
	}

	private boolean writeMatrixToDisk(String fileName, int[][] arrayToWrite) throws IOException, FileNotFoundException{
		
		BufferedWriter bufferedWriter = null;
		bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));
		
		for (int i = 0; i < arrayToWrite.length; i++) {

			String bufString = "";
			
			for (int j = 0; j < arrayToWrite[i].length; j++) {
				bufString = bufString + arrayToWrite[i][j] + " ";
			}
			
			try {
				bufferedWriter.write(bufString);
				bufferedWriter.newLine();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}

		}
		
		bufferedWriter.close();
		
		return true;
				
	}
	
    private boolean writeArrayToDisk(String fileName, int[] arrayToWrite) throws IOException, FileNotFoundException{
		
		BufferedWriter bufferedWriter = null;
		bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));
		
		String bufString = "";
		
		for (int i = 0; i < arrayToWrite.length; i++) {

			bufString = bufString + arrayToWrite[i] + " ";
		
		}
		
		try {
			bufferedWriter.write(bufString);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		bufferedWriter.close();
		
		return true;
				
	}
	
}
