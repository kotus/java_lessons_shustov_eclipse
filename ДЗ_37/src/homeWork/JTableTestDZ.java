package homeWork;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

@SuppressWarnings(value = {"serial", "unused"})
public class JTableTestDZ extends JFrame{

	static Object[][] data = {};
	{
		data = getDataFromOperationsBs("src/Operations.bs");
	}
	String [] colNames = {"����", "�����", "��� ��������", "�����"};
	JTable table = new JTable(data, colNames);
	
	public JTableTestDZ(String title) throws IOException {

		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		setLayout(new FlowLayout());
		
		JScrollPane  scrollPan = new JScrollPane(table);
		scrollPan.setPreferredSize( new Dimension(480, 100) );
		
		table.setPreferredScrollableViewportSize(new Dimension(400, 100));
		add(scrollPan);
		
		table.setRowHeight(20);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setCellSelectionEnabled( true );
		table.setSelectionBackground(Color.RED);
		table.setSelectionForeground(Color.BLUE);
		
		
		table.getModel().addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent event) {

				int col = table.getSelectedColumn();
				int row = table.getSelectedRow();
				
				if (col == 3) {
					Boolean result = checkSumm((String) table.getValueAt(row, col));
					if (!result) System.out.println("������� �� ���������� �����"); 
				} else if (col == 1) {
					Boolean result = checkTime((String) table.getValueAt(row, col));
					if (!result) System.out.println("������� �� ���������� �����"); 
				}
				
				
				
				try {
					table.setColumnSelectionInterval(col+1, col+1);  //����� �������� ����� � 3� �������
					table.setRowSelectionInterval(row, row);	
				} catch (Exception e) {
					table.setColumnSelectionInterval(0, 0);  
					table.setRowSelectionInterval(row+1, row+1);
				}
				
			}
		
		});
		
		table.addMouseListener(new MouseAdapter() {
					
			@Override
			public void mouseClicked(MouseEvent event) {

				//Point point = event.getPoint();
				if ( (event.getClickCount() == 2) && 
					(event.getButton() == event.BUTTON3) &&
						(table.getSelectedColumn() == 0) ) {
					
					cheakColumnOfDates();
					
				}
							
			}
		});
		
		setVisible(true);
		
	}

	private static String[][] getDataFromOperationsBs(String path) throws IOException {
		
		BufferedReader reader = new BufferedReader(new FileReader(path));
		String str;
		int count = 0;
		while ((str = reader.readLine()) != null) {
			if (str.trim().equals(""))
				continue;
			count++;
		}

		String[][] resultMatrix = new String[count][4];

		reader = new BufferedReader(new FileReader(path));

		int i = 0;
		while ((str = reader.readLine()) != null) {

			if (str.trim().equals("")) {
				i++;
				continue;
			}
				
			StringTokenizer tokenizer = new StringTokenizer(str, "|");

			int j = 0;
			while (tokenizer.hasMoreTokens()) {

				String tempString = tokenizer.nextToken().trim();
				String[] arrOfTempString = tempString.split(": ");
				
				resultMatrix[i][j] = arrOfTempString[1].trim();
				j++;
				
				if(j == 4){
					break;
				}

			}

			i++;

		}

		return resultMatrix;
		
	}

	public static void main(String[] args) throws IOException {
		
		new JTableTestDZ("JTable");
		

	}

	private boolean checkSumm(String value) {
		
		try {
			Double newValue = Double.parseDouble(value);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	private boolean checkTime(String value) {
		
		try {
			GregorianCalendar calendar = new GregorianCalendar(1900, 1, 1, 0, 0, 0);
			
			String[] arrOfTime = value.split(":");
			calendar.set(1900, 1, 1, Integer.parseInt(arrOfTime[0]),
					Integer.parseInt(arrOfTime[1]), Integer.parseInt(arrOfTime[2]));
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			data[table.getSelectedRow()][table.getSelectedColumn()] = dateFormat.format(calendar.getTime());
			
			return true;
			
		} catch (Exception e) {
			return false;
		}
		
	}
	
	private boolean checkDate(String value) {
		
		try {
			GregorianCalendar calendar = new GregorianCalendar(1900, 1, 1, 0, 0, 0);
			
			String[] arrOfTime = value.split("\\.");
			calendar.set(Integer.parseInt(arrOfTime[0]),
					Integer.parseInt(arrOfTime[1]), Integer.parseInt(arrOfTime[2]), 0, 0, 0);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yy.MM.dd");
			data[table.getSelectedRow()][table.getSelectedColumn()] = dateFormat.format(calendar.getTime());
			
			return true;
			
		} catch (Exception e) {
			return false;
		}
		
	}
	
	private void cheakColumnOfDates() {
		
		for (int i = 0; i < data.length; i++) {
			System.out.println(checkDate((String)data[i][0])); 
		}
		
	}
	
}
