import pack1.Summ;

public class BankCard {
	
	private Long cardNumber;
	private String expDate;
	private Short cvCode;
	private Summ summ = new Summ();
	private String surname;
	private String name;
	private String patronymic;
	
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public Short getCvCode() {
		return cvCode;
	}
	public void setCvCode(Short cvCode) {
		this.cvCode = cvCode;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	
	
	
	
}
