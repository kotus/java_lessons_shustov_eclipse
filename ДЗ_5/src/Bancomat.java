import pack1.Summ;

public class Bancomat {

	private String numInvent;
	private String address;
	private Summ summ = new Summ();
	private Integer errorCode;
	private Short num;
	
	public String getNumInvent() {
		return numInvent;
	}
	public void setNumInvent(String numInvent) {
		this.numInvent = numInvent;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public Short getNum() {
		return num;
	}
	public void setNum(Short num) {
		this.num = num;
	} 
	
	
	
	
}
