package homeWork;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;





import java.io.IOException;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class Calculator extends JFrame implements ActionListener{

	String operand1 = "";
	String operation = "";
	String operand2 = "";
	Float result = 0F;
	
	JTextField textField = new JTextField();
	
	JButton[] arrOfButtons = new JButton[10];

	JButton buttComa = new JButton(",");
	
	JButton buttResult = new JButton("=");
	JButton buttPlus = new JButton("+");
	JButton buttMinus = new JButton("-");
	JButton buttMultiply = new JButton("*");
	JButton buttDevide = new JButton("/");
	
	JButton mc = new JButton("MC");
	JButton mr = new JButton("MR");
	JButton ms = new JButton("MS");
	JButton mPlus = new JButton("M+");
	JButton mMinus = new JButton("M-");
	JButton backspace = new JButton("←");
	JButton ce = new JButton("CE");
	JButton c = new JButton("C");
	JButton plusMinus = new JButton("±");
	JButton sqrt = new JButton("√");
	JButton buttPercent = new JButton("%");
	
	public static void main(String[] args) {
	
		new Calculator("Калькулятор");
		
	}

	public Calculator(String title) throws HeadlessException {
		super(title);
		setLocation(300, 300);
		setSize(290, 250);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		Font fontOfNumbers = new Font("Arial", Font.BOLD, 15);
		Font fontOfTextField = new Font("Arial", Font.PLAIN, 35);	
		
		buttResult.setFont(fontOfNumbers);
		buttPlus.setFont(fontOfNumbers);
		buttMinus.setFont(fontOfNumbers);
		buttMultiply.setFont(fontOfNumbers);
		buttDevide.setFont(fontOfNumbers);
		buttComa.setFont(fontOfNumbers);
		
		for (int i = 0; i < arrOfButtons.length; i++) {
			arrOfButtons[i] = new JButton(String.valueOf(i));
			arrOfButtons[i].setFont(fontOfNumbers);
		}
				
		//Главная панель. Основа всего окна.
		JPanel mainPanel = new JPanel(new BorderLayout());
		add(mainPanel);
		
		textField.setHorizontalAlignment(JTextField.RIGHT);
		textField.setFont(fontOfTextField);
		mainPanel.add(textField, BorderLayout.CENTER);
		//textField.setText("0");
		
		//Все кнопки будут на ЮГ-е главной панели
		JPanel southMainPanel = new JPanel(new GridLayout(2, 1, 2, 2));
		mainPanel.add(southMainPanel, BorderLayout.SOUTH);
		
		JPanel functionPanel = new JPanel(new GridLayout(3, 5, 2, 2));
		Border borderOfFunctionPanel = new EmptyBorder(1, 1, 0, 1);
		functionPanel.setBorder(borderOfFunctionPanel);
		
		southMainPanel.add(functionPanel); // 1-я строка southMainPanel
		functionPanel.add(mc);
		functionPanel.add(mr);
		functionPanel.add(ms);
		functionPanel.add(mPlus);
		functionPanel.add(mMinus);
		
		functionPanel.add(backspace);
		functionPanel.add(ce);
		functionPanel.add(c);
		functionPanel.add(plusMinus);
		functionPanel.add(sqrt);
		
		for (int i = 7; i <= 9; i++) {
			functionPanel.add(arrOfButtons[i]);
		}
		functionPanel.add(buttDevide);
		functionPanel.add(buttPercent);
		
		JPanel bottomBorderPanel = new JPanel(new BorderLayout(2,0));
		southMainPanel.add(bottomBorderPanel); // 2-я строка southMainPanel
		// ее мы сделаем BorderLayout. Кнопка "=" будет на весь EAST
		Border borderOfBottomBorderPanel = new EmptyBorder(0, 3, 3, 3);
		bottomBorderPanel.setBorder(borderOfBottomBorderPanel);
				
		bottomBorderPanel.add(buttResult, BorderLayout.EAST);
		buttResult.setPreferredSize(new Dimension(54, 0));
				
		//Добавить Grid панель на 1 колонку и 3 ряда. 1-й цифры, 2-й цифры 3-й - панель с нулем
		JPanel leftPanelGrid = new JPanel(new GridLayout(3, 1, 2, 2));
		bottomBorderPanel.add(leftPanelGrid, BorderLayout.CENTER);	
		
		JPanel firstNumberBottomGridPanel = new JPanel(new GridLayout(1, 4, 2, 2));
		leftPanelGrid.add(firstNumberBottomGridPanel);
		for (int i = 4; i <= 6; i++) {
			firstNumberBottomGridPanel.add(arrOfButtons[i]);
		}
		firstNumberBottomGridPanel.add(buttMultiply);
		
		JPanel secondBottomGridPanel = new JPanel(new GridLayout(1, 4, 2, 2));
		leftPanelGrid.add(secondBottomGridPanel);
		for (int i = 1; i <= 3; i++) {
			secondBottomGridPanel.add(arrOfButtons[i]);
		}
		secondBottomGridPanel.add(buttMinus);
				
		//Здесь чтобы ноль был растянутый, сделаем BorderLayout
		JPanel thirdBottomGridPanel = new JPanel(new BorderLayout(2,0));
		leftPanelGrid.add(thirdBottomGridPanel);
		thirdBottomGridPanel.add(arrOfButtons[0], BorderLayout.CENTER);
		
		//Две остальные кнопки загоним в Grid и на Восток последней панели.
		JPanel panelCommaPlus = new JPanel(new GridLayout(1, 2, 2, 2));
		thirdBottomGridPanel.add(panelCommaPlus, BorderLayout.EAST);
		
		panelCommaPlus.add(buttComa);
		buttComa.setPreferredSize(new Dimension(54, 0));
		panelCommaPlus.add(buttPlus);
		buttPlus.setPreferredSize(new Dimension(54, 0));
		
		//////////////////////////////////////////////////////////////////
		//Обработчики кнопок
		for (int i = 0; i < arrOfButtons.length; i++) {
			arrOfButtons[i].addActionListener(this);
		}
		buttComa.addActionListener(this);
		buttResult.addActionListener(this);
		buttPlus.addActionListener(this);
		buttMinus.addActionListener(this);
		buttMultiply.addActionListener(this);
		buttDevide.addActionListener(this);
		c.addActionListener(this);
		
		setResizable(false);
		setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		String[] arrOfNumbers = {"1","2","3","4","5","6","7","8","9","0"};
		String[] arrOfOperands = {"+","-","*","/"};
		String eventString = event.getActionCommand();
		
		if (stringInArray(arrOfNumbers, eventString)) {
			
			System.out.println("была нажата цифра "+eventString);
			
			processClickNumberButton(eventString);			
			renewMainTextfield();
			
		} else if (stringInArray(arrOfOperands, eventString)){
			
			System.out.println("был нажат операнд "+eventString);
			
			processClickOfOperand(eventString);
			renewMainTextfield();
			
		} else if (event.getActionCommand() == "="){
			
			System.out.println("было нажато = ");
			
			calculate();
			
		} else if (event.getActionCommand() == "C"){
			
			System.out.println("было нажато С ");
			resetMainTextfield();
			
		} else if (event.getActionCommand() == ",") {
			
			System.out.println("была нажата ,");
			processClickButtonComa(eventString);
			
		}
				
	}
	
	private void processClickButtonComa(String eventString) {
		
		if (operation == "") {
			if ( (operand1 != "") && (operand1.indexOf(".") == -1) ) {
				operand1 = operand1 + ".";
			}
		} else {
			if ( (operand2 != "") && (operand2.indexOf(".") == -1) ) {
				operand2 = operand2 + ".";
			}
		}
		
		renewMainTextfield();
				
	}

	private void processClickOfOperand(String eventString) {

		if (operation == "") {
			if (operand1 != "") {
				operation = eventString;
			}
		} else if (operation != "") {
			if (operand2 == "") {
				operation = eventString;
			}
		}
		
		renewMainTextfield();
		
	}

	private void processClickNumberButton(String buttonString) {

		if (operation == "") {
			operand1 = operand1 + buttonString;
		} else if (operation != ""){
			operand2 = operand2 + buttonString;
		} 
		renewMainTextfield();
		
	}

	private boolean stringInArray(String[] inputArray, String findingString) {
		
		for (int i = 0; i < inputArray.length; i++) {
			if (inputArray[i].equalsIgnoreCase(findingString)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	private void renewMainTextfield() {
		
		textField.setText(operand1+operation+operand2);
		
	}

	private void resetMainTextfield() {
		
		operand1 = "";
		operand2 = "";
		operation = "";
		textField.setText("");
		
	}
	
	private void setResultToTextField(Double result) {
		
		if (Double.compare(result, Math.round(result)) == 0 ) { //равны
			String resultString = String.valueOf(result);
			textField.setText(resultString.substring(0, resultString.indexOf(".")));
		} else { //не равны
			textField.setText(String.format("%.2f", result));
		}
		
	}
	
	private void calculate() {
		
		StringBuffer javascript = null;
		ScriptEngine runtime = null;

		try {
			
			runtime = new ScriptEngineManager().getEngineByName("javascript");
			javascript = new StringBuffer();
			javascript.append(operand1+operation+operand2);
			
			double result = (Double) runtime.eval(javascript.toString());
			setResultToTextField(result);
			
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
			resetMainTextfield();
			
		}
		
		clearCerviceFields();
		
	}
	
	private void clearCerviceFields() {

		operand1 = "";
		operand2 = "";
		operation = "";
		
	}

	private Float convertOperandFromStringToNumber(String operand) {
		
		try {
			Float result = Float.parseFloat(operand);
			return result;
		} catch (NumberFormatException e) {
			System.out.println("Не смог перевести "+operand+" в число...");
			return null;
		}
		
	}
	
}









