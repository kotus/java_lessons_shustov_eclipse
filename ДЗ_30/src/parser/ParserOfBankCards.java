package parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import gui.BankCard;

public class ParserOfBankCards {

	public static BankCard[] createArrayOfBankCards(String path) throws IOException {
		
		BufferedReader strBuf = new BufferedReader(new FileReader(path));
		strBuf.mark((int) new File(path).length() + 1);
		String str;
		int count = -1;
		while ((str = strBuf.readLine())!=null) {
			if (str.trim().equals("")) continue;
			count++;
		}
		
		BankCard[] arrOfBankCards = new BankCard[count];
		strBuf.reset();
		
		int i = -1;
		while ((str = strBuf.readLine()) != null) {
			
			if (i == -1) {
				i++;
				continue;
			}
			
			StringTokenizer tokenizer = new StringTokenizer(str, "\t");
			while (tokenizer.hasMoreElements()) {
				
				String tempStr = tokenizer.nextToken().trim();
				String cardNumber = tokenizer.nextToken().trim(); 
				cardNumber = cardNumber.replaceAll(" ", "");
				String cvCode = tokenizer.nextToken().trim();
				String dateOfExp = tokenizer.nextToken().trim();
				
				arrOfBankCards[i] = new BankCard(Long.parseLong(cardNumber), dateOfExp, Short.parseShort(cvCode));
				
			}

			i++;
			
		}
		
		return arrOfBankCards;
		
	}
	
}
