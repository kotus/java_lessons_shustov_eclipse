package homeWork;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

@SuppressWarnings(value = {"serial", "unused"})
public class JTableTestDZ extends JFrame{

	static Object[][] data = {};
	{
		data = getDataFromOperationsBs("src/Operations.bs");
	}
	String [] colNames = {"����", "�����", "��� ��������", "�����"};
	JTable table = new JTable(data, colNames);
	
	
	
	public JTableTestDZ(String title) throws IOException {

		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		setLayout(new FlowLayout());
		
		JScrollPane  scrollPan = new JScrollPane(table);
		scrollPan.setPreferredSize( new Dimension(480, 100) );
		
		table.setPreferredScrollableViewportSize(new Dimension(400, 100));
		add(scrollPan);
		
		table.setGridColor(Color.RED);
		
		table.setRowHeight(20);
		
		//table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		
		table.setSelectionBackground(Color.PINK);
		
		setVisible(true);
		
	}

	private static String[][] getDataFromOperationsBs(String path) throws IOException {
		
		BufferedReader reader = new BufferedReader(new FileReader(path));
		String str;
		int count = 0;
		while ((str = reader.readLine()) != null) {
			if (str.trim().equals(""))
				continue;
			count++;
		}

		String[][] resultMatrix = new String[count][4];

		reader = new BufferedReader(new FileReader(path));

		int i = 0;
		while ((str = reader.readLine()) != null) {

			if (str.trim().equals("")) {
				i++;
				continue;
			}
				
			StringTokenizer tokenizer = new StringTokenizer(str, "|");

			int j = 0;
			while (tokenizer.hasMoreTokens()) {

				String tempString = tokenizer.nextToken().trim();
				String[] arrOfTempString = tempString.split(": ");
				
				resultMatrix[i][j] = arrOfTempString[1].trim();
				j++;
				
				if(j == 4){
					break;
				}

			}

			i++;

		}

		return resultMatrix;
		
	}

	public static void main(String[] args) throws IOException {
		
		new JTableTestDZ("JTable");
		

	}

}
