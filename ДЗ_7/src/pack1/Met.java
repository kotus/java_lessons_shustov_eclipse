package pack1;

public class Met {

	int x = 0;

	void met1(int xPar) {
		int x = 100;
		System.out.println("local x = "+x);
		x = xPar;
		System.out.println("Met.x = "+x);
		this.x = 1;
		System.out.println("Met.x = "+x);
		System.out.println("xPar �� = "+xPar);
		xPar = 2;
		System.out.println("xPar ����� = "+xPar);
	}

	void met2(Met objPar) {
		x = objPar.x;
		System.out.println("Met.x = "+x);
		this.x = 1;
		System.out.println("Met.x = "+x);
		objPar.x = 2;
		System.out.println("Met.x = "+x);
	}

	public static void main(String[] args) {

		Met obj = new Met();
		obj.met1(obj.x);
		System.out.println("1.obj.x=" + obj.x);
		obj.met2(obj);
		System.out.println("2.obj.x=" + obj.x);

	}

}
