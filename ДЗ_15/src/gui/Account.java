package gui;

public class Account {
	
	private Long accountNumber;
	private Summ summ;
	private Byte accCod; 
	
	public Long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public Byte getAccCod() {
		return accCod;
	}
	public void setAccCod(Byte accCod) {
		this.accCod = accCod;
	}
		
	
	public Account(){
		this.accCod = 0;
		this.accountNumber = null;
		this.summ = new Summ(0L, "UAH");
	}
	
	public Account(Long accountNumber, Long summ, String valuta){
		this.accountNumber = accountNumber;
		this.summ = new Summ(summ, valuta);
	}
	
	public static void main(String args[]){
		
		//System.out.println(new Account(1234567890L, 1000L, "UAH"));
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "����� Account:[����� ����� = "+accountNumber+";"+summ.toString()+"]";
	}
	

	
}
