package gui;

public class BankCard {
	
	private Long cardNumber;
	private String expDate;
	private Short cvCode;
	private Summ summ;
	private Client client;
	
	public static BankCard [] arrOfCards = new BankCard[10];
	static {
	    Long arrOfNumbers [] = new Long [10];
	    Long variable = new Long(1000000000000001L);
	    for (int i = 0; i < arrOfNumbers.length; i++) {
	    	arrOfNumbers[i] = variable;
			variable++;
		}
    	
	    for (int i = 0; i < arrOfCards.length; i++) {
			arrOfCards[i] = new BankCard(arrOfNumbers[i], new Summ(500L, "UAH"));
		}	
	}
	
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public Short getCvCode() {
		return cvCode;
	}
	public void setCvCode(Short cvCode) {
		this.cvCode = cvCode;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	public BankCard(Long cardNumber) {
		this.cardNumber = cardNumber;
		this.cvCode = 0;
		this.expDate = "";
		this.summ = new Summ((long)0, "UAH");
	}
	
	public BankCard(Long cardNumber, String expDate, Short cvCode, Long summ, String valuta) {
		super();
		this.cardNumber = cardNumber;
		this.expDate = expDate;
		this.cvCode = cvCode;
		this.summ = new Summ(summ, valuta);
	}
	
    public BankCard(Long cardNumber, Summ summ) {
		super();
		this.cardNumber = cardNumber;
		this.summ = summ;
	}

	public BankCard(Long cardNumber, Summ summ, Client client) {
		this.cardNumber = cardNumber;
		this.summ = summ;
		this.client = client;
	}
    
    public static void main(String args[]){
	

    	
    	    	
	    
	}
    
	@Override
	public String toString() {
		return "����� BankCard:[cardNumber=" + cardNumber + "; expDate=" + expDate
				+ "; cvCode=" + cvCode + ";" + summ + "]";
	}
	
}
