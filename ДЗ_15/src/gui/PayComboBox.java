package gui;

import java.awt.HeadlessException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import arrays.Mass;

public class PayComboBox extends JFrame{
		
	private static final long serialVersionUID = 1L;
	//������� ���� � �������� �����
	private static final String [] arrayOfValutes = {"���", "����", "����"};
	
	JLabel label_platelhik = new JLabel("����������");
	JLabel label_poluchatel = new JLabel("����������");
	JLabel label_rr_1 = new JLabel("��������� ����");
	JLabel label_rr_2 = new JLabel("��������� ����");
	JLabel label_summa = new JLabel("�����");
	
	JTextField textField_rr_platelhik = new JTextField("", 12);
	JTextField textField_rr_poluchatel = new JTextField("", 12);
	JTextField textField_summa = new JTextField("", 10);
	
	JButton button_begin_tranzaction = new JButton("<html>������<br>����������");
	
	JComboBox comboBox_platelhik = new JComboBox(getArrayOfPersons());
	JComboBox comboBox_poluchatel = new JComboBox(getArrayOfPersons());
	
	// ������� ������ �������������� �� ����� ������� buttonGroup
	// � ����� ������������� ��������
	JRadioButton radioButtons_valutes [] = new JRadioButton [3];
	ButtonGroup buttonGroup = new ButtonGroup();
	{
		for (int i = 0; i < radioButtons_valutes.length; i++) {
			radioButtons_valutes[i] = new JRadioButton(arrayOfValutes[i]);
		}
		radioButtons_valutes[0].setSelected(true);
	}
	
	JCheckBox checkBox_commission = new JCheckBox("��������� ��������", true);
	
	public PayComboBox() throws HeadlessException {
		
		setLocation(300, 300); 
		setSize(600, 220);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE ); 
		
		setLayout(null);
		
		label_platelhik.setBounds    (10, 10, 80,  20);
		comboBox_platelhik.setBounds(10, 30, 150, 20);
				
		add(label_platelhik);
		add(comboBox_platelhik);
		
		label_poluchatel.setBounds    (10, 60, 80,  20);
		comboBox_poluchatel.setBounds(10, 80, 150, 20);
		
		add(label_poluchatel);
		add(comboBox_poluchatel);
		
		label_rr_1.setBounds            (180, 10, 100,  20);
		textField_rr_platelhik.setBounds(180, 30, 150, 20);
		
		add(label_rr_1);
		add(textField_rr_platelhik);
		
		label_rr_2.setBounds             (180, 60, 100,  20);
		textField_rr_poluchatel.setBounds(180, 80, 150, 20);
		
		add(label_rr_2);
		add(textField_rr_poluchatel);
		
		label_summa.setBounds(350, 30, 150, 20);
		textField_summa.setBounds(350, 50, 100, 20);
		
		add(label_summa);
		add(textField_summa);
		
		button_begin_tranzaction.setBounds(180, 120, 100, 40);
		add(button_begin_tranzaction);
		
		//������ ���������� �������������� � �������� �� � ������
		for (int i = 0; i < radioButtons_valutes.length; i++) {
			radioButtons_valutes[i].setBounds(460, 17 + 30 * i, 70, 20);
			add(radioButtons_valutes[i]);
			buttonGroup.add(radioButtons_valutes[i]);
		}
		
		checkBox_commission.setBounds(350, 130, 200, 20);
		add(checkBox_commission);
		
		setVisible(true);
		
	}

	public static void main(String[] args) {
	
		new PayComboBox();
		
	}
	
	/**
	 * ����� ������������ �� ������ Mass ������ � ���������
	 * @return String[] ��������
	 */
	private String[] getArrayOfPersons(){
		
		String [][] matr1 = Mass.AddArr(Mass.arr1, Mass.arr2);
		String [][] matr2 = Mass.AddArr(matr1, Mass.arr3);
		return Mass.splitMatrix(matr2);
		
	}
	
}
