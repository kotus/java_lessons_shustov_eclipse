package homeWork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import bankSystem.Staff;

public class Reports {

	TreeMap<String, Staff> mapOfWorkers;
	
	{
		mapOfWorkers = new TreeMap<String, Staff>();
	}
	
	public static void main(String[] args) throws IOException {
		
		Reports reports = new Reports("src/staff.in");
		Set<Entry<String, Staff>> set = reports.mapOfWorkers.entrySet();
//		for (Entry<String, Staff> entry : set) {
//			System.out.println(entry);
//		}
		System.out.println("----------------------------------");
		
		//"����� ������ � ���������"
		
		//System.out.println(reports.mapOfWorkers.tailMap("����� ������ � ���������", true));
		
		TreeMap<String, Staff> newMap = new TreeMap<String, Staff>();
		for (Entry<String, Staff> entry : reports.mapOfWorkers.entrySet()) {
			String firstKey = entry.getKey();
			String[] arrOfKey = firstKey.split("[-]");
			String newKey = arrOfKey[arrOfKey.length-2] + "-" + arrOfKey[arrOfKey.length-1];
			newMap.put(newKey, entry.getValue());
		}
		
		for (Entry<String, Staff> entry : newMap.entrySet()) {
			System.out.println(entry);
		}
		
		System.out.println("----------------------------------");
		
						
		System.out.println(newMap.subMap("�����-3", true, "�����-9", true));

		HashMap<String, Staff> hashMap = new HashMap<String, Staff>(newMap);
		
		

	}

	public Reports(TreeMap<String, Staff> mapOfWorkers) {
		this.mapOfWorkers = mapOfWorkers;
	}
	
	public Reports(String pathToStaffs) throws IOException {

		ArrayList<Staff> listOfStaffs = Staff.makeCollStaff(pathToStaffs);
		for (int i = 0; i < listOfStaffs.size(); i++) {
			Staff currentStaff = listOfStaffs.get(i);
			String key = currentStaff.getDepartment() + "-" + currentStaff.getPost() + "-" + (i+1);
			this.mapOfWorkers.put(key, currentStaff);
		}
		
	}

	@Override
	public String toString() {
		return this.mapOfWorkers.toString();
	}
	
}

