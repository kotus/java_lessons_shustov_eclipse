package lesson;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

//1. ��������� ��� � ��������� ���������� �� ������� - ������� �������� ������������ � ��. ��������

//�������� �������� � �����������

//�����������- ��������
//1. �� ��������� ������ ���������� �������������� ��� �� ��� � ������� ��������� 
//		���������� �� ������� - �������������� �����  ��������� ������ � ����������� �����������


//�����������:
//1. ������ ������������ ����������� ���� � ���������� (��� �� ��� � � ����������) 
//      (�.�. � ����������� ��� ����� ��������, ������� �������� � � ���������)
//2. ������ ������������ ��������� ��� �������� �������� (�������� ������� ������)
//    �.�. ������ �� new ��������� ����������
//3.  ������ ��������� ���������� �������� �����������
//4. static ���������� ������������ � ������� � ����� � ����������� - 
//    �.�. ������ ������������� �������������� , � ��������� - ������������
//5.  ���������� ��������  � ������ ��������, � ������� �������� ��� ������������� -
//     �.�. ���������� ���������� ������������� ������������� ������ �������� ������ ������� - 
//      ������� ������  �� ����� ������� ����� ���� ����� ������������ � ������, ���� �� ���� ������ ������� 
//		������� �� �����  

//�����������:
//1. ����� ������ ���������� ������ �������� ������ ==  � ������� equals
//2. ����� ������������ ��� ������ ������ Object
//3. ����� ������������ ������, � ������� � ���������� ������������ ���������




public class ParType_Opers2 <T1,T2> extends TreeMap<Integer, Student> {

	T1 key;
	T2  value;
	
	Student  st = new Student("���������", "���");  //��� ������ 
	Student[] massSt =  
	{new Student("������",	"����"),
	new Student("������",	"����"),
	new Student("�������",	"����"),
	new Student("������",	"����"),
	st, st};		//��� ������
	
	
	
	//������� ����� � ����������� � �����������
/*	Set<Integer>   met1 (Set<Integer> setPar) {
		//100500 ����� ����
		return setPar;
	}
	*/
	
	//�����������: ��������� � ����� ���������� ���������� �� �������
	//������� ���������� ���� ������, ��� 2 ���������� ������
	/*
	Set<String>   met1 (Set<String> setPar) {
		//100500 ����� ����
		return setPar;
	}
	
	 Set   met1 (Set  setPar) {
		//100500 ����� ����
		return setPar;
	}
	*/
	
	//�������������� ���� � ����� ������. ���������� �� �������
	Set<T1>   met1 (Set<T1> setPar) {
		//100500 ����� ����
		return setPar;
	}
	
	//1� ��������: ��������� ������� � ������� ����������
	//������: ������� ������ �� ������� ������ ����.
 static	<V> TreeSet<V>  makeTreeSet( V [ ]  arrPar) {
	//��������� ��������� �� ������� ���������� ����� Arrays.asList
	TreeSet<V>   coll = new TreeSet ( Arrays.asList( arrPar ) );
	
	System.out.println("TreeSet  � ����������: " + coll);
	
	return  coll;
}

 /*  //static ���������� ������������ � ����� ������ , ������������ � ������
   //������ static ������������� ������������ ��� ���� �������� ������
   //� ������������� �����-���� � ������ ������������� ��������� ���� � ��������� ��������
      
 //  <T1> //����  ���� �������� ��� �1 ����� ������� �� ��� ��� �� �1 , ������� �����-��� ������, 
//  � ��� �� ��������� , ���������� ��� ����� ����� ��������. 
 static	TreeSet<T1>  makeTreeSet_T1( T1 [ ]  arrPar) {
	
		//��������� ��������� �� ������� ���������� ����� Arrays.asList
		TreeSet<T1>   coll = new TreeSet ( Arrays.asList( arrPar ) );
		
		System.out.println("TreeSet  � ����������: " + coll);
		
		return  coll;
	}
	*/	
	
 	<V> TreeSet<V>  makeTreeSet_notStatic( V [ ]  arrPar) {
		//��������� ��������� �� ������� ���������� ����� Arrays.asList
		TreeSet<V>   coll = new TreeSet ( Arrays.asList( arrPar ) );
		
		System.out.println("TreeSet  � ���������� � �������� ������: " + coll);
		
		//������: ������� ���������� ���������� ��������� (� ����������� ��������) � �������
		//����� ������� ��� ������ � ���� ������ � �����������, �.�. ������� - ��� �� ��������, � �������� ��� ��� ����
		int  count = 0;
		for (int i = 0; i < arrPar.length-1; i++) {
			for (int k = i+1; k < arrPar.length; k++) {
				if ( arrPar [ i ] == arrPar[ k ]) {  // ���� == ���������� ������ - ���������� ����� ������ ������, �.�. ��������
													//����� -������� ������������ �� ���������, � �� �� �������
					 count ++ ;
				}//if
			}//k
		}// i 
		
		System.out.println("���������� ��������� : " + count);
		
		return  coll;
	}
 	
 	//2� �������� ��������  : ���������� � ���������
 	//����������� � ��������� ���������� ����� add()
 	//Set<Integer> addTreeSet(Set<Integer> setPar,  Integer objPar){
 	<V> Set<V> addSet(Set<V> setPar,  V objPar){
 		setPar.add( objPar );   //����� add() � ������������� ������� ������������ ��� ����� ��� � ���������
 		System.out.println( setPar);
 		
 		return  setPar;
 	}
 	
 	//3� �������� �������� : �������� �������� �� �������� �������� � ����������� ����������, ������ ������ 
 	//������ add(�������� Object)  ������������ ����� remove(�������� Object )
 	
 	//4� �������� �������� : ������ �� ��������� � �������
 	//����� ���� 2� ����� - �� ��������-�������    � �� ��������� �����.
 	//4.1. ����� �� �������-�������
 	<T>  T findInSet (Set<T> setPar,  T objPar) {
 		//����� ������������ � �����
 		for (T elem : setPar) {
		//	if (elem == objPar) {  //���� == ���������� ������ ������ �������
 							//100% ����� ����� ����� ������(), �.�. equlas() ���� � ������ Object
 								//������ ��� ������ �� ��������� ����� ���� ��������������� ����� equals()
 			if (elem.equals(objPar)) {  //�� ��������� ����� �������� �� �������-�������
 													//�� ���� ��������������� ����� ������() �� ��� ��������� ����������
 													//� �� ������� � �� ��������� �����
				System.out.println("������� ������ � ��������� � ������� ����� foreach");
				System.out.println( elem );
				return   elem;
			}
		}
 		
 		//����� contains �� ���������� � ������ Object - �.�. �������� ��� ������������� � ����������� ���.
 		//�� ����� contains() ���� � Collection, � ������ � � ����� - �.�. ������� ������������� ����
 		//�� ����� contains() � ��������  ����������� ����� compareTo() - � �� �������� ��� � ������ - ��� ���������
 		//�  ����� contains() � ������� � �������� ����������� ����� equals() 
 		
 		System.out.println("������� �� ������ � ���������.");
 		return null; 			//���� ������ �� �������
 	}

	// ����� ����������� � ������� ���������
	<T> T findInSetIterator(Set<T> setPar, T objPar) {
		// ����� ������������ � �����

		for (Iterator iterator = setPar.iterator(); iterator.hasNext();) {
			T elem = (T) iterator.next();
			// 100% ����� ����� ����� ������(), �.�. equlas() ���� � ������
			// Object
			// ������ ��� ������ �� ��������� ����� ���� ��������������� �����
			// equals()
			if (elem.equals(objPar)) { // �� ��������� ����� �������� ��
										// �������-�������
										// �� ���� ��������������� �����
										// ������() �� ��� ��������� ����������
										// � �� ������� � �� ��������� �����
				System.out
						.println("������� ������ � ��������� � ������� ����� ����������");
				System.out.println(elem);
				return elem;
			}// if
		}// for
		System.out.println("������� �� ������ � ���������.");
		return null; // ���� ������ �� �������
	}// metod

	<T> void findInTreeSet(TreeSet<T> collPar, T etalonPar) {
		for (T elem : collPar) {
			// elem.compareTo() //��� ������ ������ � �����-�����, �.�. ��� ���
			// � ������ Object
		}

		// ������������ contains() � TreeSet ���� ���������, �����
		// ������ � ���������� ���� IMPLEMETS COMPARABLE
		// ����� �������, ������ ���������� �� ����� ����������������� ���
		// ������
		// ��� ������������ �����-����� , ���� ��� ���������� ����������
		// ���������� ������������ ��� ������.

		System.out.println(collPar.contains(etalonPar));

	}
 	
	public static void main(String[] args) {
		// TODO Auto-generated method stub


		//�������� ������� �������� �������� Student
		Student  st = new Student("���������", "���");  //��� ������ 
		Student[] massSt =  
		{new Student("������",	"����"),
		new Student("������",	"����"),
		new Student("�������",	"����"),
		new Student("������",	"����"),
		st, st};		//��� ������
		//���������� ������������� ��� ������ � ����������� ���������
		//������ ��� ���� �� �����������
		Set<Student> objDescTree = new TreeSet<>(Arrays.asList(massSt));
		System.out.println("�������. �� �������� � ���������"+objDescTree);

		ParType_Opers2.makeTreeSet(massSt);
		
				//������������� ��-������������ ������ � ����������� ������� �������� ������� ������� ������
		ParType_Opers2<String, Student> obj  = new ParType_Opers2<String, Student>();
		obj.makeTreeSet_notStatic(massSt);
		
				//�� �����  ������ ������ � ���� ������� ������ � ������� ������ �����
		ParType_Opers2<Integer, Student> obj2  = new ParType_Opers2<Integer, Student>();
		TreeSet<Student>  treeSetStudent = obj2.makeTreeSet_notStatic(massSt);

			//�� �����  ������ ������ � ���� ������� ��������� � ������� ������ ���������
		Integer [ ]  arrInt = {1,2,3,4,5,6,7};
		ParType_Opers2<Integer, Integer> obj3  = new ParType_Opers2<Integer, Integer>();
		obj.makeTreeSet_notStatic(arrInt);
		
		obj2.addSet(treeSetStudent, new Student("�������", "����")) ;
		
		Student etalon = st;	//����� ������ �� ������ ��� ����������
		obj2.findInSet(treeSetStudent, etalon);
		
					//� �� �������� ����� - �������� �������������� ����� ������() 
		etalon = new Student("�������", "����");  //����� ����� �������� ������� �� ��������� � ��������� � ���������
		obj2.findInSet(treeSetStudent, etalon);
		obj2.findInSetIterator(treeSetStudent, etalon);
		obj2.findInTreeSet(treeSetStudent, etalon);
		
		obj2.put(1, st);
		System.out.println( obj2 );
		
	}//main

}

// --------------------------------------------------------------------------------------------------
// ����� � ����������� Comparable �������� � �����()
class Student implements Comparable<Student> {// �������� ��� ��������
												// �������������� ���������
	// ����� ����� ����� ������� ������� � ������� � � Student
	String lastName;
	String name;
	int number;
	long summ;

	//
	public Student(String lastNamePar, String namePar) {
		// super();
		/* this. */lastName = lastNamePar;
		/* this. */name = namePar;
	}

	public Student() {
		// TODO Auto-generated constructor stub
	}

	// @Override //����� ��������� comparable
	public int compareTo(Student oPar) {// �������� ����
		// TODO Auto-generated method stub
		Student tmp = oPar;

		int res = lastName.compareTo(oPar.lastName);
		return (res == 0 ? -name.compareTo(oPar.name) : -res);
	}// compareTo

	// ��� ������ ��������� �� �����
	public String toString() {
		return number + " " + lastName + " " + name;
	}

	// ��� ������ �� �������� � �������
	// ��������� ������ �� ����, �.�. ����� ������� � ����� ������ � ���������
	// �� ��������� - �.�.
	// ��������� ���� ����� ������� ��������, ���� ����������� �����������
	// ��������
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + number;
		result = prime * result + (int) (summ ^ (summ >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number != other.number)
			return false;
		if (summ != other.summ)
			return false;
		return true;
	}

}// class

