package pack1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;


//������  � �������� ������������ ���� � ��������

//�������� �������� ��������
//1. ������ �����������  - ����.�����,   
//������� ����� ������ ����� �������� � ��������� �������- ����� equals() �  compareTo()

//2. ����� ����������� - ����. �������� �����, 
//�� � ������� �������� - ����� ������� - ���������� �����������
//�������� ������ ���������  �������� ����� -
//����  �������� � ��������� �������- ����� equals()

//3. ����� ������ ����� (�����������) - ����� nextToken()   
//��� ��� ������������� ������ �����() - ������ ������� � ��������� ������ � �����

//������ ������ ������ �� 2-� ���������: �����-��������, � ������-�������� 

public class ParserStaff {

	
	//�������, �� �� ������ ������
	//����� ������ ������� �� 2� ������ - ��������� � ��������
	//�������� - �����, ������� ���� ������ �������� ����� � �����
	//�������� - ������, ������� ���������� ������������� (�������) ���� ����� ������� ��������� �����.
	//� ��� 4 ������-��������, �.�. 4 �������� ����� � ����� � �������
	
	//�����-�������� - ���� ������ �������� ����� � �����
	//1. ��������� �� ����� ������ � ������� �� ��������� ������� (� ������� �������-������)
	//2. ���������������� ��������� ����� (  split()   ��� ����������  )
	//3. ����� ������ �������� �����   ( ���������� ��()  � ����������)
	//4. ������� ������ �����-��������, ������� ������ ��������� ����  ����� ��������� ����� � �����
	static void parseManagerSimple (   String  fileName, Staff [ ]  arrStaff    ) throws IOException {
		BufferedReader strBuf = new BufferedReader( new FileReader(  fileName  ));
		String str;
		int i=-1 ;
		for (  ;  (str = strBuf.readLine() ) != null      ; ) {
	  
			//���� ������ ������ - �� �� ������������ ��
			if ( str.trim().equals("")   )
						continue;
			
			//������� ������ �������� ����������
			i++;
			arrStaff[ i ]  = new Staff();
			
			
			  //�������� �������� - �������� ���������� ������ ���-��, ������ //  --  # 
			 if ( str.indexOf("//")  != -1)  {
			  	str = str.substring(0,  str.indexOf("//")  )  ;  //indexOf() ����������  ��� -1, ��� ������ ��������� ������-��������� � ������-���������
			 }
			  
			  System.out.println( str );  //��� �������
			  
			  //������� ������ �����������
			  StringTokenizer tokzer = new StringTokenizer(str, " :\t");
			  
			  //� ����� ������ �� ���� ������ ������, ���� �� ����� ������ �������� �����   - nextToken
			  //������ �� ������, ������ �����
			  String  word;
			  while (  tokzer.countTokens()  > 0  ) {
				
				 word =  tokzer.nextToken();  //������ ��������� �����
				 System.out.println( "������� �����: " + word);
				 
				 //�������� ������� ����� �� ����� ��������� �������
				 if  (  word.equalsIgnoreCase("�����")  )  {  //���� ����������� �������� ����� �������
					//4. ������� ������ �����-��������, 
					
					//���� ����� ������ �����, ��� ���������� �������� ������
					  //��� ����� �������� ��: 
					  //�������� indexOf - ��������� ������ ���� ����� ����� � ������ �� �����
					  //+ ����� ��������� ����� �����
					  //������� ���������� � ������� ����� ������ � ����������� ������� ������
					  //� �������� �����  ��� ������ ���� �� ������
					  
					  //������ ����� ��������� ����� ������ �� � �����  parseDepart
					  //������ ��������� � �������� ������� ������� ����������
					
					
					   arrStaff [ i ].depart =  readDepart( tokzer     );
				 }
				 else if  (  word.equalsIgnoreCase("���������")  )  {  //���� ����������� �������� ����� ���
						//4. ������� ������ �����-��������, 
					 //arrStaff [ i ].position   =  readPosition( tokzer );
					 }
			    else  if  (  word.equalsIgnoreCase("���")  )  {  //���� ����������� �������� ����� ���
						//4. ������� ������ �����-��������, 
						 readFIOStaff(tokzer,  arrStaff [ i ] );
					 }
				 
			}//while - ������ ����� � ������
			  
			  
		}
		
	} //parseManagerSimple
	
//�������� ��� ���� �������� ����

	static void readFIOStaff(StringTokenizer   tokzerPar,  Staff  staffPar)  {
		 System.out.println("----�������� ������ FIO  ������----");
		 
		 		//��������� ���
		 staffPar.fam    = tokzerPar.nextToken();
		 staffPar.name = tokzerPar.nextToken();
		 	//���� � ����� ���� ������� - �� �� ������ ��������,- ��� ����� ������
		if (  staffPar.name.indexOf("�")  >= 0   )   {// \" ������� ������� �������� ����� ��������� �����
			                                                               //���� � ����� ���� �������
			staffPar.surName = "";  //����� �������� ������	
		}
		else  //�������� ������ �� ������
			staffPar.surName   = tokzerPar.nextToken();
		 
		 System.out.println("���:    " + staffPar.fam + "   " + staffPar.name +" " + staffPar.surName);
	 }

	static String readDepart (StringTokenizer tokzerPar     )  {
		//��������� ����� � �������� ������ 
		String word = tokzerPar.nextToken();
		System.out.println("1� ����� � ������: " + word);
		
		//���� � �������� ������ ��� ������� �����������, �� ��� ��������� �������� ������
		if ( word.indexOf("�") == -1) 
			//return word;  //��� ��� �������
				;
		//�����, ������ ��� �� �����������(���������) ������� � ��� ��� �������� , ��� �������� ������
		//����� ������� � ����� ����� ����������() � ��������� ����, ����� � ����� �������� ����������� �������
		else  {
			word = word + tokzerPar.nextToken("�") + "�";
		}
		
		return  word;
	}
	
//����� ��� ��������� - ����� �� �� ��������, ��� � �����
	
		
	//------------------------------------------------------------------------------------------------------
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		Staff []  arrStaff = new Staff [ 10 ];  //����� ��� �������
		
		//������� �����-�������� �������
		parseManagerSimple( "src//staff.in",  arrStaff);
		
		
		//�������  �������������� ������ �������� �� �������
		System.out.println("-----------------------------------------------���������-----------------------------------------------------------");
		for (int i = 0; i < arrStaff.length; i++) {
			//System.out.println(  arrCl[ i ].fam + "  "+ arrCl [ i ].name + "  "+  arrCl [ i ].surName /*...*/  );
			
			//������ �������������� ����� toString()  � ����� ������ Staff 
			//��� �������� ������������� �    sysout
			System.out.println(  arrStaff[ i ] );
		}
		

	} //main

} //class

class Staff {
	String fam, name, surName; //���
	String depart;
	String position;
	
	
	public String toString() {
		return this.fam + "  "+ this.name + "  "+  this.surName  + "  "+  this.depart+ "  "+  this.position;   
	}
	
}
