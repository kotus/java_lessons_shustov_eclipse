package lesson;

import java.util.HashSet;

//������ � �������������� - 
//��������������� ������� ������() � ������()
//hashCode()  -  

public class HashCode_equals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  Object  obj = new Object();
  //HashSet<Client> collHash = new HashSet(10);  //��������� ������������ � ����������������� ��������(��������)
  																//��� 10-�� ��������� ���� ������
 // HashSet<Client> collHash = new HashSet(10, 0.9); // �������  � ����-�������� 0.9 - (�.�. ��������� ����� �����������, ����� ��� ��������� �� 90%)
  															//��� ������ ����-������, ��� ������ ������ � ���-�������, 
  															//��� ������ ��������, ��� ������ ������ ������ ������ ����������,
  															//��� ������� ����� ��������� ������ ������� �������
  														//��� ������� ����� �� ���������, �� ��� ������ ����� ����� ������ ����� ���������.
  														//����� ����������� ���� �������� �� ��������� - 0.75
		
  HashSet<Client> collHash = new HashSet(); 
  //� ��������� ������� 3 �������
  Client cl1 = new Client("������");
  Client cl2 = new Client("������");
  Client cl3 = new Client("�������");
  
  collHash.add(cl1); collHash.add(cl2);collHash.add(cl3);
  
  System.out.println(collHash);
  
  
  //�� �������� ���� �������� ������� : ����� ������� 
  //1. ���� � �����. ��������� ���� ���� ��������.
  //����� ������� ����� �.�.: - ���� ������ ����, ����� ������ - ��� ������ ���� ������ ����� ��� ���������(50% � �������)
  //������� ��� ������������� ��� �� ����� - �.�. ��� ��� ��� ��� �����-���
  //2. � ������� ������������ ��������� ���-������  - � ������� ����-������ ������
  //���������� �� ����� �������������� ������� ������ ���������� ��������
  Client Etalon = new Client(2, "������");  	//  ������ - ��� �� ����, ������� ������ �� �������� ����
  
boolean res =   collHash.contains(  Etalon  );  //���� �������� �� ��������, �.�. ���������� ����� ��������()  ������� 
  													//������-�����  �������� �������
  System.out.println("������ �� �������? :" + res);
  
   res =   collHash.contains(  cl2  );  //���� �������� �� ��������, �.�. ���������� ����� ��������()  ������� 
	//������-�����  �������� �������
System.out.println("������ �� ������ ������ �� ������? :" + res);
  
//� ������ �������� ��� ����� ����������� ������ �� ��������� �����,  � �� �� ������-������
//���������� ��������� ��� ������� ������, � ��� ���� ������ �����������
//� �������� �� ����� ������ �� �������� ����� � ������� ������������ �� ������� �������� �����������
//� ������� �������� � ������ �������� ��� ��������� - �.�. ��� �����-���

//� ���-���������� ��� ��������� �� ��������� �����  ���� �������������� ����� 
//����� EQUALS()  � � ��������� � ��� � ������()
//�.�. ���� equals()  ������ ���������� ������� �� ��������� �����, �� �������� � ������ �����������
// �� ��������� ���� �����, ����� �� ������� �������� � ������ ��������

//����� ������������� ��� ������ � ������� ������� ���� Source-Generate
//�� ��� ���� ������ ������ ���� � ������ ����� � ��� ������, ��� ��������
//����� �������������� equals() �  hashCode() 
//� � ���� ������ ������ ���� ���� - �.�. �������������� ������ ����� ����� ������ ���� 
//����� ������ �� ��������� �����
  
//���������� ������� ����� �������� equals() hashCode() ������� �������� � ��������
//������� ���� ��� �� ���� �������� �� ���������� ����� - �� � ��������������� equals() hashCode() 
//�� ����


  
	} //main

	
	
}//public class


class Client {
	int ID;  //���, ����������� ���������� ����� �������
	String  name;
	long    sum;
	
//-------------------------
	
	
	static int countClients=0;
	

	public Client(String name) {
		//super();
		this.name = name;
		countClients++;  
		ID = countClients;
		
		//  ID = ++  countClients;
	}
	public Client() {
		//super();
		// TODO Auto-generated constructor stub
	}

	
	public Client(int iD, String name) {
		super();
		ID = iD;
		this.name = name;
	}
	//����� toString()  ���  ������ ��������
	@Override
	public String toString() {
		return "Client [ID=" + ID + ", name=" + name + ", sum=" + sum + "]\n" ;
	}
	
	
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ID;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}
		@Override  //����� �������� � ������� ����� �� ��������������� � ������ equals()
				//���� � ��������� ��� �������, � ������� ��� �� ���� ������� - �.�. ����� �� ������ equals() ���� ���� �� �������
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Client other = (Client) obj;
			if (ID != other.ID)
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		
		
		
	
}//Client
