package forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JTextField;

public class FormNewClientProcess implements KeyListener, ActionListener{

	final String filePath = "src/ClientsAllAcc.in";
	
	//KeyListener
	@Override
	public void keyPressed(KeyEvent event) {
		
		if (event.getSource() == FormNewClient.textField_identiCode) {
			
			allowNumbers(event, FormNewClient.textField_identiCode); 
		
		} else if (event.getSource() == FormNewClient.textField_surename ||
				event.getSource() == FormNewClient.textField_middlename ||
				event.getSource() == FormNewClient.textField_name) {
			
			allowLetters(event, (JTextField)event.getComponent());
			
		} else if (((event.getModifiers() & event.CTRL_MASK) != 0)
				&& (event.getKeyCode() == event.VK_N)) {
			
			writeClientToDisk();
			
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent event) {}

	//ActionListener
	@Override
	public void actionPerformed(ActionEvent event) {
		
		if (event.getSource() == FormNewClient.button_createNewClient) {
			
			writeClientToDisk();
			
		}
		
	}
		
	private void allowNumbers(KeyEvent event, JTextField textField) {

		String oldText = textField.getText();

		if (event.getKeyChar() >= '0' && event.getKeyChar() <= '9') {

			String newText = textField.getText() + event.getKeyChar();
			textField.setText(newText);
		
		} else if (event.getKeyCode() == KeyEvent.VK_BACK_SPACE){

			textField.setText(textField.getText().substring(0, textField.getText().length() - 1));

		} else {

			textField.setText(oldText);
				System.out.println("�������� ������ �����!");
				
		}
		
	}

	private void allowLetters(KeyEvent event, JTextField textField) {
		
		String oldText = textField.getText();

		if ( (event.getKeyChar() >= 'a' && event.getKeyChar() <= 'z') ||
				(event.getKeyChar() >= 'A' && event.getKeyChar() <= 'Z') ||
				(event.getKeyChar() >= '�' && event.getKeyChar() <= '�') || 
				(event.getKeyChar() >= '�' && event.getKeyChar() <= '�')) {

			String newText = textField.getText() + event.getKeyChar();
			textField.setText(newText);
			System.out.println("����� "+ event.getKeyChar());
		
		} else if (event.getKeyCode() == KeyEvent.VK_BACK_SPACE){

			textField.setText(textField.getText().substring(0, textField.getText().length() - 1));

		} else {

			textField.setText(oldText);
				System.out.println("�������� ������ ������!");
				
		}
		
	}

	
	private boolean writeClientToDisk() {
		
		String resultString = "���:"+FormNewClient.textField_surename.getText()+" "+FormNewClient.textField_name.getText()+" "+FormNewClient.textField_middlename.getText()+ 
			"\t"+"�/�:"+FormNewClient.textField_numberOfAccount.getText()+"\t"+"�������:" + FormNewClient.textField_numPassport.getText()+"\t"+"zip:"+FormNewClient.textField_postNumber.getText()+
			"\t"+"���:"+FormNewClient.textField_identiCode.getText();
		
		System.out.println(resultString);
		
		FileWriter fileWriter = null;
		try {
			
			fileWriter = new FileWriter(new File(this.filePath), true);
			fileWriter.write(resultString + "\n");
			fileWriter.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
		
	}
	
}
