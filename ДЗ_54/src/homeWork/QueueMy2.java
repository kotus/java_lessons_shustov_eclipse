package homeWork;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;

public class QueueMy2 {

	public static void main(String[] args) {
		
//		 ������ ���������: �������� ����� , ��������� ������� DEQUE �� 1 000 000 ��������� ����� �� 
//		 0 �� 1000 � ���������� ����� �������� �������. ������� 1000 � 100000 ������� � ����������.
//		 � ������� ����� ������ �������� ����� �������� �������, ����������� �������� ���
		
		Double time = 0D;
		
		Deque<Integer> arrayDeque = new ArrayDeque<Integer>();
		time = addQueueCollAtStart(arrayDeque, 1000000, 1000);
		System.out.println("���������� 1000000 � ArrayDeque: " + time);		
		
		Deque<Integer> linkedList = new LinkedList<Integer>();
		time = addQueueCollAtStart(linkedList, 1000000, 1000);
		System.out.println("���������� 1000000 � LinkedList: " + time);
		
		Queue<Integer> priorityQueue = new PriorityQueue<Integer>();
		time = addQueueCollAtStart(priorityQueue, 1000000, 1000);
		System.out.println("���������� 1000000 � PriorityQueue: " + time);
		
		Stack<Integer> stack = new Stack<Integer>();
		time = addQueueCollAtStart(stack, 1000000, 1000);
		System.out.println("���������� 1000000 � Stack: " + time);		
		
		
	}

	public static Double addQueueCollAtStart(Queue<Integer> queue, Integer numbersCount, Integer maxValue) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < numbersCount; i++) {
			queue.add(new Random().nextInt(maxValue));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
	public static Double addQueueCollAtStart(Stack<Integer> queue, Integer numbersCount, Integer maxValue) {
		
		Long time1 = System.currentTimeMillis();
		
		for (int i = 0; i < numbersCount; i++) {
			queue.add(new Random().nextInt(maxValue));
		}
		
		Long time2 = System.currentTimeMillis();
		
		return ((double) ( time2 - time1) / 1000);
		
	}
	
}
