package homeWork;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

import sun.reflect.generics.tree.Tree;
import bankSystem.Client;

public class HomeWork54 {

	public static void main(String[] args) throws FileNotFoundException, IOException {

		/*
		ArrayList<Client2> innerClients = new ArrayList<Client2>();
		innerClients.add(new Client2(1, "������ ���� ��������"));
		innerClients.add(new Client2(2, "������ ���� ��������"));
		innerClients.add(new Client2(3, "������� ����� ��������"));
		innerClients.add(new Client2(4, "������� ����� ���������"));
		innerClients.add(new Client2(5, "������ ����� ���������"));
		innerClients.add(new Client2(5, "������ ����� ���������"));
		innerClients.add(new Client2(7, "������ ����� ���������"));
		
		TreeSet<Client2> treeSetClients = new TreeSet<Client2>(innerClients);
		
		ArrayList<Client2> tempArray = new ArrayList<Client2>(treeSetClients);
		Collections.reverse(tempArray);
				
		treeSetClients.clear();
		treeSetClients.addAll(tempArray);
		
		System.out.println(tempArray);
		*/
		
//		ArrayList<Client> arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
//		TreeSet<Client> setOfClients = new TreeSet<Client>(new COMPARATOR_CLIENT_ACCOUNT_SUMM());
//		for (Client client : arrayListOfClients) {
//			setOfClients.add(client);
//		}
		
//		TreeSet<Client> setOfClients = new TreeSet<Client>(arrayListOfClients);
//		for (Client client : arrayListOfClients) {
//			setOfClients.add(client);
//		}
		
//		System.out.println(setOfClients);
		
		Integer arr[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
		
		LinkedHashSet<Integer> hashSet = new LinkedHashSet<Integer>(Arrays.asList(arr));
		System.out.println(hashSet);
		
		ArrayList<Integer> tempArray = new ArrayList<Integer>(hashSet);
		tempArray.add(4, 100);
		
		hashSet.clear();
		hashSet.addAll(tempArray);
		
		System.out.println(hashSet);
		
		
	}

}

class Client2 implements Comparable<Client2>{
	Integer ID;
	String name;
	long sum;

	static int countClients = 0;

	public Client2(String name) {
		this.name = name;
		countClients++;
		ID = countClients;
	}

	public Client2() {
	}

	public Client2(int iD, String name) {
		super();
		ID = iD;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Client [ID=" + ID + ", name=" + name + ", sum=" + sum + "]\n";
	}

	@Override
	public int compareTo(Client2 client2) {
		return this.ID.compareTo(client2.ID);
	}

	/*
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (ID != other.ID)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	*/

}// Client

class COMPARATOR_CLIENT_ID implements Comparator<Client>{

	@Override
	public int compare(Client client1, Client client2) {
		
		int result = client1.getID().compareTo(client2.getID());
		
		if (result == 0) {
			return 1;
		} else {
			return result;
		}
		
	}
	
}

class COMPARATOR_CLIENT_ACCOUNT_SUMM implements Comparator<Client>{
	
	@Override
	public int compare(Client client1, Client client2) {
		
		int result = client1.getAccount().getSumm().getSumm().
				compareTo(client2.getAccount().getSumm().getSumm());
		
		if (result == 0) {
			return 1;
		} else {
			return result;
		}
		
	}
	
}