package homeWork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.TreeMap;
import java.util.TreeSet;

import bankSystem.Client;
import bankSystem.Summ;

public class HomeWork {

		
	public static void main(String[] args) throws IOException {
		
		ArrayList<Client> arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
		HashMap<Integer, Client> hashMap = new HashMap<Integer, Client>();
		for (Client client : arrayListOfClients) {
			//client.getBankCard().setSumm(new Summ(1000L, "UAH"));
			client.getAccount().setSumm(new Summ(2000L, "UAH"));
			hashMap.put(client.getID(), client);
		}
		
		/*
		TreeMap<Integer, Client> treeMap = new TreeMap<Integer, Client>(new Comparator<Integer>() {
			@Override
			public int compare(Integer key_1, Integer key_2) {
				int res = key_1.compareTo(key_2);
				return ((res == 0) ? -1: res);
			}
		});
		*/
		TreeMap<Integer, Client> treeMap = new TreeMap<Integer, Client>(hashMap);
		//treeMap.putAll(hashMap);
				
		//NavigableMap<Integer,Client> finalTreeMap = treeMap.headMap(7, true);
		
		
		
		System.out.println(treeMap);
		
		new HomeWork().changeFioOfClient(1, treeMap, "��������", "��������", "��������������");
		
		System.out.println(treeMap);
		
		/*
		TreeSet<Client> treeSetOfClients = new TreeSet<Client>(arrayListOfClients);
		
		ClientColl clientColl = new ClientColl(treeSetOfClients);
		
		System.out.println(clientColl.myGetSummFromAllClients());
		*/
		
		
	}
	
//	private void changeFioOfClient(Integer key, TreeMap<Integer, Client> treeMapOfClients, String surename, String name, String middleName) {
//		
//		Map.Entry<Integer, Client> clientEntry = treeMapOfClients.floorEntry(key);
//		if(clientEntry != null){
//			clientEntry.getValue().setName(name);
//			clientEntry.getValue().setMiddleName(middleName);
//			clientEntry.getValue().setSurename(surename);
//		} else {
//			System.out.println("������ �� ������!!!");
//		}
//
//	}

	private void changeFioOfClient(Integer key, TreeMap<Integer, Client> treeMapOfClients, String surename, String name, String middleName) {
		
		TreeSet<Map.Entry<Integer, Client>> clientTreeSet = new TreeSet<Map.Entry<Integer,Client>>(new Comparator<Map.Entry<Integer, Client>>() {

			@Override
			public int compare(Entry<Integer, Client> entry_1,
					Entry<Integer, Client> entry_2) {
				
				String FIO_1 = entry_1.getValue().getSurename() + " " + entry_1.getValue().getName() + " " + entry_1.getValue().getMiddleName();
				String FIO_2 = entry_2.getValue().getSurename() + " " + entry_2.getValue().getName() + " " + entry_2.getValue().getMiddleName();
				
				return FIO_1.compareToIgnoreCase(FIO_2);
																
			}
			
		});
		
		clientTreeSet.addAll(treeMapOfClients.entrySet());
		
		Entry<Integer, Client> entryEtalon = treeMapOfClients.ceilingEntry(key);
		
		NavigableSet<Map.Entry<Integer, Client>> findetEntry = clientTreeSet.subSet(entryEtalon, true, entryEtalon, true);
		
		if (!findetEntry.isEmpty()) {
			Client findetClient = findetEntry.first().getValue();
			findetClient.setName(name);
			findetClient.setMiddleName(middleName);
			findetClient.setSurename(surename);
		}

	}
	
}
