package lesson;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;


//Iterator - ��� ����. ����� ��� �������� �������� �� ���������� 
//� �� ������� �������  �������� � ��������� �  �������� ��� ���� ��������� ���������
//�.�. �������� ��������� � ��������� ����� ������ ��� ������� �� ������� ������, � ����������.
//��� ������� � ���, ��� �������� � ���������� ����� ������� ���� � ������, � ��� �������� ��� ����� ����� ����������
//���� �� ������������ ��������.
//���� ����� ������� ����, �� �� �� ��������� �������� ��������� ������ ����� ���������� ���������, 
//� �������� �������  ConcurrentModificationException

public class IteratorTest {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//������ ����� � ����������
		//1. �������� ���������
		//LinkedList<Integer>    //� ��������� ���� LinkedList  ���������� �������� ���� Integer  
		LinkedList<Integer>  collLinked = new LinkedList<>() ;   //������� ������ ���������   
		//2. �������� � ��������� �����-�� ������
		collLinked.add( 10 ); collLinked.add( 11 );  collLinked.add( 12 );
		collLinked.add( new Integer(17) );
		
		System.out.println( collLinked   );
		
		//���� ��������� - ����� ����� , ���� ����� ���-�� ������� � ���������
		//������ ��������� ����� ������ �� ���������������� - ���� ���� ������ next()  � hasNext()
		for (Iterator iterator = collLinked.iterator();         iterator.hasNext();           ) {
			Integer currElem = (Integer) iterator.next();   //��������� �������� �������� ��������� � ����������
			
			currElem += 10;
			System.out.println(  currElem );
			
			iterator.remove();    //�������� �������� ��������
			//collLinked.remove() ;  //����� ������� ConcurrentModificationException
			//iterator.     //iterator �������� ����� 3 ������: next(), hasNext(),  remove() 
			
		}  //for
		
		System.out.println(   collLinked  );
		
		
		
	}  //main

}
