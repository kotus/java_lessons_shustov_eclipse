package pack1;

public class Client {

	private String surname;
	private String name;
	private String patronymic;
	private BankCard bankCard;
	private Account account;
		
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	public BankCard getBankCard() {
		return bankCard;
	}
	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Client(String surname, String name, String patronymic) {
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
	}
		
	public Client(String surname, String name, String patronymic, BankCard bankCard) {
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.bankCard = bankCard;
	}
	
	public Client(String surname, String name, String patronymic,
			BankCard bankCard, Account account) {
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.bankCard = bankCard;
		this.account = account;
	}
	
	//������� � ������� �����, �� ����� ������� �������
	public String payBCard(Summ summa, Client client){
		
		Long summaThisClient = this.bankCard.getSumm().getSumm();		
		Long summaComingClient = client.bankCard.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();
		
		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0){
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0){
			return "������������ ������� ��� ������";
		}
		
		this.bankCard.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.bankCard.getSumm().setSumm(summaComingClient + summaOfPayment);
				
		return "OK";
		
	}
	
	public static void main(String[] args) {
		
		/*Client client1 = new Client("", "client1", "", new BankCard(1234567732144563L, new Summ(1000L, "978")));
		Client client2 = new Client("", "client2", "", new BankCard(3215369874521254L, new Summ(2000L, "978")));
		
		System.out.println("������ 1 ����� = " + client1.getBankCard().getSumm().getSumm());
		System.out.println("������ 2 ����� = " + client2.getBankCard().getSumm().getSumm());
		
		String result = client1.payBCard(new Summ(1000L, "978"), client2);
		if (result != "OK") {
			System.out.println(result);
			return;};
		
		result = client2.payBCard(new Summ(2000L, "978"), client1);
		if (result != "OK") {
			System.out.println(result);
			return;};
		
		System.out.println("����� ����� ��������:");	
		System.out.println("������ 1 ����� = " + client1.getBankCard().getSumm().getSumm());
		System.out.println("������ 2 ����� = " + client2.getBankCard().getSumm().getSumm());*/
		
	}
		
}
