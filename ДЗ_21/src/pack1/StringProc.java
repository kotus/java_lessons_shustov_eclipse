package pack1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import sun.text.resources.FormatData;

public class StringProc {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException{

		/*
		BufferedReader reader = new BufferedReader(new FileReader("src/forum.log"));
		BufferedWriter writer = new BufferedWriter(new FileWriter("src/8april.res"));
		
		String strBuff = "";
		String neededString = "2010-04-08 13:";
		while ((strBuff = reader.readLine()) != null) {
			
			if(strBuff.startsWith(neededString)){
				writer.append(strBuff);
				writer.newLine();
			}
			
		}
		
		reader.close();
		writer.close();
		*/
		
		//StringProc.dz_21_punkt_1_4("src/forum.log", "src/14���15-^17.res");
		StringProc.dz_21_punkt_1_4("src/forum.log", "src/14���15-^17.res", "15", "17", "2010-08-14");
		//StringProc.dz_21_punkt_1_5("src/forum.log", "src/�����16.txt");
		//StringProc.dz_21_punkt_1_6("src/forum.log", "src/�����17.txt");
		//StringProc.dz_21_punkt_1_7("src/14���15-^17.res", "src/14���-3������.res", -3);
		//StringProc.dz_21_punkt_1_8("src/forum.log", "src/2010-08�����7����.res", "2010-08-12", -7);
		
		
		
	}
	
	
	public static void dz_21_punkt_1_4(String inputFileName, String outputFileName, 
			String startTime, String endTime, String date) throws FileNotFoundException, IOException{
		
		BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
		
		String strBuff = "";
		
		while ((strBuff = reader.readLine()) != null) {
			
			if(!strBuff.startsWith(date)) continue;
			
			if ((startTime.compareTo(strBuff.substring(11,13)) <= 0) && 
					(endTime.compareTo(strBuff.substring(11,13)) > 0)){
				writer.append(strBuff);
				writer.newLine();
			}
						
		}
		
		reader.close();
		writer.close();
		
	}
	
	public static void dz_21_punkt_1_5(String inputFileName, String outputFileName) throws FileNotFoundException, IOException{
		
		BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
		
		String strBuff = "";
		
		while ((strBuff = reader.readLine()) != null) {
			
			if(strBuff.indexOf(" : !") != -1){
				
				if (strBuff.indexOf(" : !!!") == -1){
					writer.append(strBuff);
					writer.newLine();
				}
				
			}
			
		}
		
		reader.close();
		writer.close();
		
	}
	
	public static void dz_21_punkt_1_6(String inputFileName, String outputFileName) throws FileNotFoundException, IOException{
		
		BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
		
		String strBuff = "";
		
		while ((strBuff = reader.readLine()) != null) {
			
			if(strBuff.indexOf(" : !") != -1){
				
				if (strBuff.indexOf(" : !!!") == -1){
					
					String resString = strBuff.substring(0, 2)+ strBuff.substring(4, 11) + "����������� " + 
							strBuff.substring(11, strBuff.length());
					
					writer.append(resString.trim());
					writer.newLine();
					
				}
				
			}
			
		}
		
		reader.close();
		writer.close();
		
	}

	public static void dz_21_punkt_1_7(String inputFileName, String outputFileName, int minutesCount) throws FileNotFoundException, IOException, ParseException{
		
		BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
		
		String strBuff = "";
		
		while ((strBuff = reader.readLine()) != null) {
			
			if(strBuff.indexOf(" : ") != -1){
				
				String readedString = "";
				try {
					readedString = strBuff.substring(0,19);
				} catch (Exception e) {
					continue;
				}
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = dateFormat.parse(readedString);	
				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				
				calendar.add(Calendar.MINUTE, minutesCount);
				
				String dateFormatted = dateFormat.format(calendar.getTime());
				
				writer.append(dateFormatted + " " + strBuff.substring(20,strBuff.length()));
				writer.newLine();
				
			}
			
		}
		
		reader.close();
		writer.close();
		
	}
	
	public static void dz_21_punkt_1_8(String inputFileName, String outputFileName, String data, int daysCount) throws FileNotFoundException, IOException, ParseException{
		
		BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
		
		String strBuff = "";
		
		while ((strBuff = reader.readLine()) != null) {
			
			if(strBuff.indexOf(" : ") != -1){
				
				String readedString = "";
				try {
					readedString = strBuff.substring(0,10);
				} catch (Exception e) {
					continue;
				}
				
				if(!readedString.equals(data)){
					continue;
				}
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = dateFormat.parse(readedString);	
				
				GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
				calendar.setTime(date);
				
				calendar.add(GregorianCalendar.DAY_OF_MONTH, daysCount);
				
				String dateFormatted = dateFormat.format(calendar.getTime());
				
				writer.append(dateFormatted + " " + strBuff.substring(11,strBuff.length()));
				writer.newLine();
				
			}
			
		}
		
		reader.close();
		writer.close();
		
	}
	
}
