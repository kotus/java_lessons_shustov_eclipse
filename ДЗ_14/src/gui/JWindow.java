package gui;

import java.awt.HeadlessException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class JWindow extends JFrame{
		
	private static final long serialVersionUID = 1L;
	JLabel label_platelhik = new JLabel("����������");
	JLabel label_poluchatel = new JLabel("����������");
	JLabel label_rr_1 = new JLabel("��������� ����");
	JLabel label_rr_2 = new JLabel("��������� ����");
	JLabel label_summa = new JLabel("�����");

	JTextField textField_platelhik = new JTextField("", 12);
	JTextField textField_poluchatel = new JTextField("", 12);
	JTextField textField_rr_platelhik = new JTextField("", 12);
	JTextField textField_rr_poluchatel = new JTextField("", 12);
	JTextField textField_summa = new JTextField("", 10);
	
	JButton button_begin_tranzaction = new JButton("<html>������<br>����������");
	
	public JWindow() throws HeadlessException {
		
		setLocation(300, 300); 
		setSize(500, 220);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE ); 
		
		setLayout(null);
		
		label_platelhik.setBounds    (10, 10, 80,  20);
		textField_platelhik.setBounds(10, 30, 150, 20);
		
		add(label_platelhik);
		add(textField_platelhik);
		
		label_poluchatel.setBounds    (10, 60, 80,  20);
		textField_poluchatel.setBounds(10, 80, 150, 20);
		
		add(label_poluchatel);
		add(textField_poluchatel);
		
		label_rr_1.setBounds            (180, 10, 100,  20);
		textField_rr_platelhik.setBounds(180, 30, 150, 20);
		
		add(label_rr_1);
		add(textField_rr_platelhik);
		
		label_rr_2.setBounds             (180, 60, 100,  20);
		textField_rr_poluchatel.setBounds(180, 80, 150, 20);
		
		add(label_rr_2);
		add(textField_rr_poluchatel);
		
		label_summa.setBounds(350, 30, 150, 20);
		textField_summa.setBounds(350, 50, 100, 20);
		
		add(label_summa);
		add(textField_summa);
		
		button_begin_tranzaction.setBounds(180, 120, 100, 40);
		add(button_begin_tranzaction);
		
		setVisible(true);
		
	}

	public static void main(String[] args) {
	
		new JWindow();
		
	}
	
}
