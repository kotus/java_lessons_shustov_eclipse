package test_6;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import test_2.Test_2;

public class Test_6 {

	
	public static void main(String[] args) throws IOException{

		int [][] arrTable = Test_2.arrTable;
		int maxValue = 0;
		int minValue = 0;
		
		for (int i = 0; i < arrTable.length; i++) {
			for (int j = 0; j < arrTable[i].length; j++) {
				if (arrTable[i][j] > maxValue) {
					maxValue = arrTable[i][j];
				} else if (arrTable[i][j] < minValue) {
					minValue = arrTable[i][j];
				}
			}
		}
		
		String resultString = new String();
		resultString += "max value = " + String.valueOf(maxValue) +
						" , " +
						"min value = " + String.valueOf(minValue);
		
		FileWriter fw = new FileWriter(new File("src/test_6/Limits.txt"));
		fw.append(resultString);
	    fw.close();
	    System.out.println("���� Limits.txt �������.");

	}

}
