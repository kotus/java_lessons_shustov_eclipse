package test_1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Test_1 {

	
	public static void main(String[] args) throws IOException {
		
		int[][] table = new int[6][7];
		int counter = 1;
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				//���� �� i
				if ((i == 0) || (i == (table.length-1))){
					table[i][j]= counter;
				}
				//���� �� j
				if ((j == 0) || (j == (table[i].length-1))){
					table[i][j]= counter;
				}
				counter++;
			}
		}
		
		// ��� ����� ������� ������ ����, ��� ������ �����
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				if ((i > 0) && (i<3)){
					if ((j == 0) || (j == (table[i].length-1))) continue;
					table[i][j] = table[i-1][j-1] * 3;
				}
			}
		}
		
		// ��� ����� ������� ����� �����, ��� ��������� �����
		for (int i = table.length-1; i >= 0; i--) {
			for (int j = table[i].length-1; j >= 0; j--) {
				if ((i != table.length-1) && (i > 2)){
					if ((j == 0) || (j == (table[i].length-1))) continue;
					table[i][j] = table[i+1][j+1] * 2;
				}
			}
		}	
				
		//����� ������� �������	� ������	
		String resultString = new String();
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				resultString = resultString + (table[i][j] + "\t");
			}
			resultString += "\n";
		}
		
		//������ � ���� - ������ �������.
		FileWriter fw = new FileWriter(new File("src/test_1/Matrix.txt"));
		fw.append(resultString);
	    fw.close();
	    System.out.println("���� �������.");
		
	}

}
