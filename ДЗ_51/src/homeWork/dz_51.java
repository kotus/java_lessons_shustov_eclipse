package homeWork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

public class dz_51 {

	public static void main(String[] args) {
		
//		1.1.	� �����() ������ Collect c������ ��� ���������  ���� TreeSet  � ������� : �� 10 �� 20  �   �� 15 �� 30 
//		� ������� � ����  ������������ �������� (������� ����� ���������). ������� �� ������� ��������� : 
//		����� �� ��  ����, ��� ������ ���� ? 
		
		/*
		TreeSet<Integer> treeSet1 = new TreeSet<Integer>();
		for (int i = 10; i <= 20; i++) {
			treeSet1.add(i);
		}
		System.out.println("Tree Set 1: " + treeSet1);
		
		TreeSet<Integer> treeSet2 = new TreeSet<Integer>();
		for (int i = 15; i <= 30; i++) {
			treeSet2.add(i);
		}
		System.out.println("Tree Set 2: " + treeSet2);
		
		treeSet1.removeAll(treeSet2);
		treeSet2.removeAll(treeSet1);
		treeSet1.addAll(treeSet2);
		
		System.out.println(treeSet1);
		*/
		
		/*
		TreeSet treeSet = new TreeSet();
		ArrayList arrayList = new ArrayList();
		HashSet hashSet = new HashSet();
		*/
		
//		1.2.	������ �������� ����� ���������:
//		1.2.1.	��� ���� �������� ������ �� instanceOf  , ����� ��������  ������ TreeSet �� HashSet ? 
		
		/*
		System.out.println(treeSet instanceof TreeSet);
				
		System.out.println(arrayList instanceof java.util.List);
		System.out.println(hashSet instanceof java.util.List);
		*/
		
		
//		1.2.2.	��� ���� �������� ������ �� instanceOf  , ����� ��������  ������ ArrayList �� HashSet ? 
//		1.2.3.	��� ���� �������� ������ �� instanceOf  , ����� ��������  ������ ArraySet �� HashSet ? 
//		1.3.	������������� ���������  �� 5.1  �� �������� � ������� ����������� . ������ � ��������� 
//		����� TreeSet ���������,  � ������ - � �������   ������-������ Collections. ���������� ������� 
//		��������� �������, ����� � ��� �� �����, ���  ����� Collect
		
		TreeSet<Integer> treeSet1 = new TreeSet<Integer>();
		for (int i = 10; i <= 20; i++) {
			treeSet1.add(i);
		}
		System.out.println("Tree Set 1: " + treeSet1);
		
		TreeSet<Integer> treeSet2 = new TreeSet<Integer>();
		for (int i = 15; i <= 30; i++) {
			treeSet2.add(i);
		}
		System.out.println("Tree Set 2: " + treeSet2);
		
		TreeSet<Integer> treeSet1Sorted = new TreeSet<Integer>( new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.compareTo(o1);
			}
		} );
		treeSet1Sorted.addAll(treeSet1);
		System.out.println("Tree Set 1 sorted: " + treeSet1Sorted);
		
		List<Integer> list = new ArrayList<Integer>(treeSet2); 
		Collections.sort(list, new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.compareTo(o1);
			}
		});
		System.out.println("Tree Set 2 sorted: " + list);
		
		
//		1.4.	������������� ��� �� ���������  , �� ���������� ������� ��������� ������� -  �.�. ����������
//		(inner) ������� ��� �� ������ �� ������,  �� ��� �����., ��� ������ � ��� � ������������. 

		
	}

	
}

class COMPARATOR_UP_INTEGER implements Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		
		return o2.compareTo(o1);
		 
	}
	
}

