package lesson;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

//�������� �� ����������� ���������.. ����������� � ������� �����



public class SetOperations2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Integer [ ]  intArr = {1,2,3,4,5,6} ;
		Integer [ ]  intArr2 = {4,5,6,7,8,9} ;
			
		//�� ��������� intList ������� ���
		//1. �����������.  
		List<Integer> intList = Arrays.asList(  intArr ) ;  //�� �������������� �� �������
		Set<Integer>  intSet1_Etalon = new LinkedHashSet<Integer>(  intList   );  //2� �������� �� �����
		System.out.println( intSet1_Etalon );
		
		intList = Arrays.asList(  intArr2 ) ;  //�� �������������� �� �������
		Set<Integer>  intSet2_Etalon= new LinkedHashSet<Integer>(  intList   );  //2� �������� �� �����
		System.out.println( intSet2_Etalon );
		
		//�������� � ����������� - � ����� � ������� ����-������� (�� ������ All �  ��������� )
				//1. ����������� �������� (�������� ��������)
		Set set1 = new LinkedHashSet(intSet1_Etalon);		//������� ����� ���������, ����� �� ��������� ��������� ���������
		Set set2 = new LinkedHashSet(intSet2_Etalon);
		//set1 = intSet1_Etalon;  //��� �������, �� ��� �� ��������� ����� ���������, � ��� ����� ����� �������� ��������� �� ����� ���� �������
												//�.�. ��� ������-������  ��������� �� ���� � �� �� ���������
		
		set1.addAll( set2 );
		System.out.println( "����������� (��������) ��������: " +  set1);
		
		//2. ���������  �������� ( �������� � 1-� ��������� ��, ���� ��� �� 2-� ���������)
					//������� �� 1-�� ��������� ���, ��� ���� �� 2-� ���������
		 set1 = new LinkedHashSet(intSet1_Etalon);
		 set2 = new LinkedHashSet(intSet2_Etalon);
		
		 //�1 - �2
		 set1.removeAll( set2 );
		 System.out.println( "M1 - M2 :" +  set1);
		 
		//�2 - �1
		 set1 = new LinkedHashSet(intSet1_Etalon);
		 set2 = new LinkedHashSet(intSet2_Etalon);
		 
		 set2.removeAll( set1 );
		 System.out.println( "M2 - M1 :" +  set2);
		 
		 //3.  ����������� �������� (����� ����� ��������)
		 		//��������  � 1-� ��������� ��� �� , ��� ���� �� 2-� ���������
		 set1 = new LinkedHashSet(intSet1_Etalon);
		 set2 = new LinkedHashSet(intSet2_Etalon);
		 
		 set1.retainAll( set2 );
		 System.out.println(  "����� ����� ��������: " + set1);
		 
		 //4. ������������ �������� (����������� - �����������) (��� � ����� �������� , ����� �� ����� �����)
		 //������� ����
		 
		 //5. ���� ��������   containsAll - �������� �� ���������-�������� ������ ���, ��� � ��������� -���������
		 System.out.println( "�������� �� 2-� ��������� ��� , ��� � 1-� ���������: " +  set2.containsAll( set1));
		 System.out.println( "�������� �� 1-� ��������� ��� , ��� � 2-� ���������: " +  set1.containsAll( set2));
		 
		 //� �������-������  ���� ���� ����� ����-������ (All), �� ��� ��������� �� ����������.
		 
		 //-------------------------------------------------------------------------------------------------------------------------------
		 //���������� ���������� ����� - ������ ����� (����������, ������, ������	 )
		 Set<Integer>  linkHSet =  new  LinkedHashSet<Integer>();   //��������� � ������� ������� (���), � � ��������� �������  ����������
		 HashSet<Integer>  hashSet =  new  HashSet<Integer>();		//��������� � ������� �������, �� ������� ���������� ��������, ������ ����� ������� ����� �����
		 Set<Integer>  treeSet   =  new  TreeSet<Integer>();		//��������� ������������� (���������, �� ����� - ����� ������ ����_�������)
		 
		//�������� ���� ��������� ���������� - �������� intanceof
		 //������:  ���������, ��� ��������, � ��� �� �������� ���� treeSet
		 //�������, �� ���� �������� ���������� � ������������ �� �����, ��� intanceof  
		 //���������� true ���� ������ ����� �������� �������������� ������, ������� ������ ��� ��� �����������
		 //� ��� ���� ����������� ��� ��� �������, ������� ��� ������ ������ �� NEW 
	//	 Integer x;   if  (x instanceof Number)  //�.�. � Integer ����� ������������� ��������������� � Number - �� �  ���� Number
		 
		if ( treeSet instanceof Set)	  //��� treeSet  ���� �� Set ?
			System.out.println("Set");		//�� - �.�. ������ ���� ��������� ���������� ���
		if ( treeSet instanceof Collection)	  //��� treeSet  ���� �� Collection?
			System.out.println("Collection");		//�� - �.�. ������ ���� ��������� ���������� ��� � ��� ����� ��������
		if ( treeSet instanceof TreeSet)	  //��� treeSet  ���� �� TreeSet?
			System.out.println("TreeSet");		//�� - �.�. ������ ������ �� NEW
		if ( treeSet instanceof HashSet)	  //��� treeSet  ���� �� Set?
			System.out.println("HashSet");		//��� - �.�. ������ ���� ��������� ���������� ���
		if ( treeSet instanceof List)	  //��� treeSet  ���� �� ����?
			System.out.println("List");			//��� - �.�. ������ ���� ��������� ���������� ���
		if ( linkHSet instanceof List)	  //��� treeSet  ���� �� ����?
			System.out.println("List");			//��� - �.�. ������ ���� ��������� ���������� ���

		if ( hashSet instanceof LinkedHashSet)	  //
			System.out.println("LinkedHashSet");			//��� - �
		if ( linkHSet instanceof HashSet)	  //
			System.out.println("HashSet");			//�� - �.�. ������ ���� ��������� ���������� ���
		
		
		//instanceof - �������� �� ����� ���������� - �.�. ������ ����� , � ����� ����������� ����� ��������� 
		//� ������� �����, ����� ������ ���������� �� ����� ����������
		
		//---------------------------------------------------------------------------------
		
		
	}//main

}
