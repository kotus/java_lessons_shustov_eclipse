package lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

//������������� ���������  ������ TreeSet

//�������������
//���������
//����� - ����� �����������, � ��. �������


public class TreeSet_Operations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Integer [ ]  intArr = {1,2,3,4,5,6,7,1,2,2,2,3,3,3, 7, 500,1000} ;
		List<Integer> intList = Arrays.asList(  intArr ) ;  //�� �������������� �� �������
			
		//�� ��������� intList ������� ������
		// ������.  
		TreeSet<Integer>  treeSet = new TreeSet<Integer>(  intList   );  //2� �������� �� �����
		System.out.println( treeSet );
		
		//� ��� ����� ������������� ������� (�������, ���������, ��������� � ��.)?
		//��������: ������ ������ ����������� �������� �� ����� �� ���������
		//  ���   ������ ������ ����������� ��� �� �������� �� ID (����. �����)
		//  ���  ������ ������ ����������� �� ������ ����������
		//�.�. ���������� �������� ����� ���� � ������ ������� �� ������ �����
		
		//� ����� ��������� ����� ����������: ���� ����������� ������ ������, ��� ����� 
		//����������� ����� �������
		
		//���������� ���������� �������� ������-�� ����������� ������ ���������� �����������
		
		//������ ��� �����������   ����� ������������:
		//1. � ������������ ���������
		TreeSet<Client>  treeClSet1 = new TreeSet<Client>( new COMP_CLIENT_ID() );      //������ ��������� ����� ����������� �������� �� ID
		TreeSet<Client>  treeClSet2 = new TreeSet<Client>( new COMP_CLIENT_FAM() ); //������ ��������� ����� ����������� �������� �� �������

		String [ ]  arrClFam = { "������", "������", "�������"} ;
		for (int i = arrClFam.length-1; i >= 0; i--) {
			Client clCurr = new Client( arrClFam[ i ] );
			treeClSet1.add(clCurr) ;   //�������� �������� ������� � ����������
			treeClSet2.add(clCurr) ;  
		}
		System.out.println(treeClSet1  );
		System.out.println("-----------------------------------------------------");
		System.out.println(treeClSet2  );
		
		//2. � ������ Arrays - ��� ���������� �������� �������� �� ������� ��� ��������
		Client [ ]  arrCl = {new Client("������"), new Client("������"), new Client("�������") } ;
		Arrays.sort(arrCl, new COMP_CLIENT_ID()  );   //������ ����������� �� ���� ID, �.�. ��� ������ � ����� �����������-2� ��������
		Arrays.sort(arrCl, new COMP_CLIENT_FAM()  );//������ ����������� �� �������� �������, �.�. ��� ������ � ����� �����������-2� ��������
		
		//3. � ������ Collections -  ��� ���������� ����� ���������
		System.out.println("-----------------���������� ������� Collections.sort()  ----------------------------");
		List collListCl = new ArrayList (treeClSet1);  //������� ����. ���� ������ - �.�. ������ Collections ��������� ������ ������
		System.out.println(collListCl);
		Collections.sort(  collListCl  ,  new COMP_CLIENT_FAM()  );  //���������� ����. ������ �� �������� ������� - �.�. ��� ��������� ��� ����������
		System.out.println(collListCl);
		
		//������� ������������ ����� ���� �����, ���� ��� ������� ���� ������.
		//����� �������� ���������� �� ��������.
		Collections.reverseOrder( new COMP_CLIENT_FAM()    );  //���������� �� � �� �
		//��� ��������� ����� ����������� �� �����, � ��� �������� ��� ���������� -
		//��� ����������� ������ �� �������, �� ������ ���� � �������� ����������� �������
		
		//--------------------------------------------------------------------------------------------------------
		// ������� ��������� ����������� � �����-������ - ��� �� ��������� ����� � ������
		//����� ������� ������ ������ ����������
		Comparator<Client> COMP_CLIENT_FAM_1  = new Comparator<Client>() {

			@Override
			public int compare(Client o1, Client o2) {
				return    o1.name.compareToIgnoreCase( o2.name ) ;   
			}
		};
		
		//������������� ����� ����������� - �� �� 4 ��������
		TreeSet<Client>  treeClSet3 = new TreeSet<Client>(  COMP_CLIENT_FAM_1 ); //��� NEW � ��� ������ ()
		Arrays.sort(arrCl, COMP_CLIENT_FAM_1  );//������ ����������� �� �������� �������, �.�. ��� ������ � ����� �����������-2� ��������
		Collections.sort(  collListCl  ,  COMP_CLIENT_FAM_1  );
		Collections.reverseOrder(  COMP_CLIENT_FAM_1    );  
		
	} //main

}//public class

//����� ���������� - ��� ���������� �������� �� ID
class  COMP_CLIENT_ID implements Comparator<Client> {

	@Override
	public int compare(Client arg0, Client arg1) {  //� ���� ������ ����������� ������ ������ ������� ���������� 
																				//������ ������ �� �����-�� �����
		//���� ����� ������ ����������:
		//0 - ��� ��������� ��������
		//-1  ���� 1� ������ ������  2-��
		//+1 ���� 1� ������ ������ 2-��
		int res=0;
		
				//���������� ID �������� - �.�. ID �������� ���� int - �� �������� ������������ ���()
		if (arg0.ID < arg1.ID)  res = -1;
		else if (arg0.ID > arg1.ID) res = 1;
		else res = 0;
		
		return   res;
	}
	
}//COMP1

//����� ���������� - ��� ���������� �������� �� �������
class  COMP_CLIENT_FAM implements Comparator<Client> {

	@Override
	public int compare(Client o1, Client o2) {
		
		return    o1.name.compareToIgnoreCase( o2.name ) ;  //�.�. ���������� ������, �� ����� ������� ����� ��������� 
																			//������, ����������� �� �������� ����������� - ���������� <0 ==0 >0 
																			//����� ����� � ����� ���� - ��� compareTo()  -  ���������� <0 ==0 >0
	}
	
}//COMP2

//����� ���������� - ��� ���������� �������� �� �������
class  COMP_CLIENT_ID_Integer implements Comparator<Client> {

	@Override
	public int compare(Client o1, Client o2) {
		return 	o1.ID_Integer.compareTo(  o2.ID_Integer  );  //�.�. ������������ ���� ���� Integer - �� � ���� ���� �����
																				//���������� �� ��������� ����������� - compareTo() - �.�. ���������� <0 ==0 >0
	}
	
}



class Client {
	int ID;  //���, ����������� ���������� ����� �������
	String  name;
	long    sum;
	
	Integer ID_Integer;
	
	static int countClients=0;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Client(String name) {
		//super();
		this.name = name;
		countClients++;  
		ID = countClients;
		
		//  ID = ++  countClients;
	}
	public Client() {
		//super();
		// TODO Auto-generated constructor stub
	}

	
	public Client(int iD, String name) {
		super();
		ID = iD;
		this.name = name;
	}
	//����� toString()  ���  ������ ��������
		@Override
	public String toString() {
		return "Client [ID=" + ID + ", name=" + name + ", sum=" + sum + "]\n" ;
	}
	
}//Client


