package bankSystem;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

import arrays.Mass;

public class BankMain {

	public static ArrayList<Bankomat> arrOfBancomats;
	public static ArrayList<Staff> arrOfStaffs;
	public static ArrayList<Client> arrayListOfClients;
	public static ArrayList<BankCard> arrayOfBankCards;
	
	public BankMain() throws IOException {
		
		arrOfBancomats = Bankomat.makeCollBankomat("src/bancomats.in");
		arrOfStaffs = Staff.makeCollStaff("src/staff.in");
		arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
		arrayOfBankCards = BankCard.makeCollOfBankCards("src/master-visa-2014-05-14---01.in");
		
		makeMassBCardGiveOut();
		testMassBCardGiveout();
			
	}
	
	private void makeMassBCardGiveOut() throws IOException {
		
		for (int i = 0; i < arrayListOfClients.size(); i++) {
			spreadCardsAndClients(i);
		}
		
	}

	public static void main(String[] args) throws IOException {
		
		BankMain mainBank= new BankMain();
		testMassAll();
	
	}//main

	public static void testMassAll() {
		
		Bankomat.testCollBankomats(arrOfBancomats);
		Staff.testCollStaff(arrOfStaffs);
		Client.testCollClient(arrayListOfClients);
		BankCard.testCollBankCards(arrayOfBankCards);
		
	}
	
	public static void testMassBCardGiveout(){
				
		int i = 1;
		for (Client client : arrayListOfClients) {
			
			System.out.println(i + ". ������:" + client.toString() + " ���� ����� �� �������:" + client.getBankCard().toString() + "\t" +
			" ������ �� ���� �����: " + client.getBankCard().getClient().toString());
			i++;
		}
			
	}
	
	@SuppressWarnings("resource")
	private ArrayList<String> getDataFromFileBankCardsToClient(String path, int indexOfString) throws IOException {
		
		
		RandomAccessFile fileReader = new RandomAccessFile(path, "r");
		String stringFromFile = "";
		for (int i = 0; i < fileReader.length()-1; i++) {
			
			if (i == indexOfString) {
				stringFromFile = fileReader.readLine();
				stringFromFile = new String(stringFromFile.getBytes("ISO-8859-1"), "CP1251");
				break;
			} else {
				fileReader.readLine();
			}
				
		}			
		
		ArrayList<String> list = new ArrayList<String>();

		StringTokenizer tokenizer = new StringTokenizer(stringFromFile, "\t");
		while (tokenizer.hasMoreTokens()) {
			String tempString = tokenizer.nextToken().trim();
			list.add(tempString.trim());
		}
		
		return list;
			
	}
	
	private void spreadCardsAndClients(int index) throws IOException {
		
		ArrayList<String> listFromString = getDataFromFileBankCardsToClient("src/BCards_to_clients.out", index);
		
		BankCard bankCard = findStringAboveCardNumber(listFromString.get(4));
		if (bankCard != null) {
			Client client = arrayListOfClients.get(index);
			setBankCardToClient(client, bankCard);
			setClientToBankCard(bankCard, client);
		} 
		
	}
	
	private BankCard findStringAboveCardNumber(String cardNumber) {
		
		for (BankCard bankCard : arrayOfBankCards) {
			String currCardNumber = Long.toString(bankCard.getCardNumber());
			if(currCardNumber.equalsIgnoreCase(cardNumber)){
				return bankCard;
			}
		}
		
		System.out.println("������ � ������� ����� "+ cardNumber + " �� �������!!!");
		
		return null;
		
	}
	
	private void setBankCardToClient(Client client, BankCard bankCard) {
		client.setBankCard(bankCard);
	}
	
	private void setClientToBankCard(BankCard bankCard, Client client) {
		bankCard.setClient(client);
	}
	
	
}
