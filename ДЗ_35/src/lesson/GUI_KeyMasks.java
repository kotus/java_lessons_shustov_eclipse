package lesson;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
//��������� ��������� ������� ���  ��������� ������� ��� ��������� ���������
//��������� �������	 ����� � �������������� - ���. ���������
//����� �������������
//��������� ����� ���������� ������ � ������� ������

public class GUI_KeyMasks extends JFrame {

	JButton butt1 = new JButton("1");
	JButton butt2 = new JButton("2");
	JButton butt3 = new JButton("3");
	JButton buttPlus = new JButton("+");

	JTextField tFld1 = new JTextField("���� ������� ���");
	String oldTxt = ""; // c����� ���������� ����������

	String[] arrCBOX_Items = { "������", "������", "�������" };
	JComboBox cBox = new JComboBox<>(arrCBOX_Items);

	JList list = new JList<>(arrCBOX_Items);
	int count = 0; // ����� ������ ������� ������������ ��������

	boolean flagHomeKeyPressed = false; // ��� ������������ ������� � ����������
										// ������� � ���������� � ������
										// ��������

	public GUI_KeyMasks(String title) throws HeadlessException { // '�������
																	// �������
																	// ��������
																	// ����

		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); // ������� - �������
													// ��������� ����� ��������
													// ���������� ����

		// setLayout(null); //�������� ����� ���������, �� �� ��� �� �����
		setLayout(new FlowLayout()); // �������� ����������() - ����������,
										// ��������� ���������� ����� �������

		// this.add(butt1);
		add(butt1);
		add(butt2);
		add(butt3);
		add(buttPlus);
		add(tFld1);

		// -------------------------------������� ��������� ��������� �� ������
		// ��������� inner-���������� �������------------------------------

		setPreferredSize(new Dimension(500, 300)); // ������ ����������
													// ��������� ������

		// ������ ����������� - ���������� ����������� �� �������������� ��
		// ����� � �����������, ���� �� ����� ������������ final
		final int varOfMetodOutside = 7; // final ���������� ����� ���� ����� ��
											// = ������ 1 ��� ��� ����������
											// final - ������� ��������� �
											// ������ � � �++

		// ���������� ����� ������� ������������������
		// 1. �������� �������� ���������� ������������ � ��������������
		// � ����� 3 ����������� - �.�. �� ������ ��� 5 ������� ������� ������
		// ����� � ������.��������
		// ����� ��������� �����������:
		// ������� - ��� �����, ������� � ���� ��� �������� ��� �������� ������
		// �������
		// 1. ������� ������� �������. ���������� ( new Mou �������-������)
		// 2. �������� ����� Listener �� ������� ������ Adapter � ������ ������
		// ������ � ��������� ������
		// 3. ������ ����� ������ ��� �� ������ �������� �������-������������
		// � ���������� ��� ���� ��� �������� �� �����

		// 2. �������� ��������� ���������� ��� ��������� ���������

		// ��� ���������� , ������� ��������� � ������-������� (�����
		// ����������������)
		// �� �����. ���� �� ����� ������������ final
		// ������ 2:
		// 1. ������� � ���������� ����������� Final - �� ���������� ����������
		// � ���������
		final JTextField tFld2 = new JTextField(20); // final ��� ������
														// ��������� - ���
														// ���������,
														// �.�. ������ ���������
														// ������ ���� ��
														// ��������.
														// �������� ����,
														// ��������� , ��. �� ��
														// ������-�����
														// ����������
		final int Y_CONST = 10; // ��� ���������� �������� ������ ����� ��
								// ���������
		add(tFld2);

		// 2. ���� ���������� ������ ������� Final (�.�. ��� ����� ������
		// ����������)
		// �� �������� ��������� ��� ���������� � ������� ����� ������ (������)
		// ����� ���������� ������ ����� ������.
		addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent event) {
				// TODO Auto-generated method stub
				// ���������� ������� ������� �������-S Ctrl-S ^-S (Ctrl = ^)
				if (event.getKeyModifiersText(event.getModifiers())
						.equalsIgnoreCase("ctrl")
						&& event.getKeyText(event.getKeyCode())
								.equalsIgnoreCase("S")) {
					System.out.println("������ Ctrl-S   ^-S.");
				}

				// �� �� ����� � ������� �����
				if (((event.getModifiers() & event.CTRL_MASK) != 0) // �����
																	// ������������
						&& (event.getKeyCode() == event.VK_S) // �� �����, � ���
																// �������
				) {
					System.out
							.println("����� ������������: ���� ������ ������� � S.");
				}

				// �������� �� ������� ���
				// ������ � �� � �������� �������� � ������� �� ������ DOWN
				if ((event.getModifiersEx() & event.ALT_DOWN_MASK) != 0) {
					System.out
							.println("����� EX-DOWN ������������: ���� ������ ���.");
				} else
					System.out
							.println("����� EX-DOWN ������������: �� ����  ������ ���.");
			}
		});

		// ������ ��������� ����� ���������� ������, � �� ������ �
		// ��������������
		// ����� ���� , ������� ����� ����������� ������� � �������� ����� ��
		// ������ ����������
		// ���� - ��� ������ ���������� ���� boolean
		// boolean flag=false; //���� ���� �������� �����, �� �� �� ����� ������
		// ������ �����������,
		// ���� �� ���������� � ���� ����������� final
		//
		// final JButton butt; //��� ��������� final ��������, �� ��� ����� -
		// ���
		addKeyListener(new KeyAdapter() {

			// ������: �������� ���������� Home-End -
			// �.�. Home ���������� � ��������, � ����� ������ ���������� End

			@Override
			// ��������� ���������� �������
			public void keyReleased(KeyEvent evt) {
				// ���� ������������ - �.�. ������� ��������
				if (evt.getKeyText(evt.getKeyCode()).equalsIgnoreCase("HOME")) {
					flagHomeKeyPressed = false;
					System.out.println("Home ��������");
				}
			}

			@Override
			// ��������� ������� �������
			public void keyPressed(KeyEvent evt) {
				// ���� ������������ � ���
				if (evt.getKeyText(evt.getKeyCode()).equalsIgnoreCase("HOME")) {
					flagHomeKeyPressed = true;
					System.out.println("Home ������");
				}

				if (evt.getKeyText(evt.getKeyCode()).equalsIgnoreCase("END")
						&& (flagHomeKeyPressed == true)) {
					System.out.println("Home+End  ������");
				}

				/*
				 * //����� ������� �� ���������, �.�. � ������� �������� ����
				 * ������ �� ����� ������� ������� //� ������ � ������ ������
				 * ������� if (evt.getKeyText( evt.getKeyCode()
				 * ).equalsIgnoreCase("HOME") && evt.getKeyText(
				 * evt.getKeyCode() ).equalsIgnoreCase("END") ) {
				 * System.out.println("****"); }
				 */

			}
		});

		setVisible(true);
		// ���� ���������� ����.������� � ����, �� ��� ���� JFrame ���� 2 ������
		setFocusable(true); // �.�. ���� ���������� �� ���������
		requestFocus(); // ����� �� ����

	} // construcror

	public static void main(String[] args) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		// TODO Auto-generated method stub

		// ������� ������� ��� ��������� ��� � ��
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

		// ��� ������ ���������� ������� - �� ���������, �.�. � ���� ��� �����
		// ������� ���������� 1 ��� � �����, ������� ��� �� �� �����
		new GUI_KeyMasks("��������� ��������");

	}// main()

}// class
