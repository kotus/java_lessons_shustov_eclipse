package lesson;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class GUI_MouseMasks extends JFrame {

	JButton butt1 = new JButton("1");
	JButton butt2 = new JButton("2");
	JButton butt3 = new JButton("3");
	JButton buttPlus = new JButton("+");

	JTextField tFld1 = new JTextField("���� ������� ���");
	String oldTxt = ""; // c����� ���������� ����������

	String[] arrCBOX_Items = { "������", "������", "�������" };
	JComboBox cBox = new JComboBox<>(arrCBOX_Items);

	JList list = new JList<>(arrCBOX_Items);
	int count = 0; // ����� ������ ������� ������������ ��������

	public GUI_MouseMasks(String title) throws HeadlessException { // '�������
																	// �������
																	// ��������
																	// ����

		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setLayout(new FlowLayout()); // �������� ����������() - ����������,
										// ��������� ���������� ����� �������

		// this.add(butt1);
		add(butt1);
		add(butt2);
		add(butt3);
		add(buttPlus);
		add(tFld1);

		// -------------------------------������� ��������� ��������� �� ������
		// ��������� inner-���������� �������------------------------------
		// ������: ������� ��������� ActionListener-� ������� ���������� ������
		// 1. ��������� ������ butt1
		// �� ������� 2 ������� ���������� ��� � ���
		// butt1.addActionListener( this );
		// butt1.addActionListener( /*��� ������ ������ implements
		// ActionListener � �������������*/ )

		setPreferredSize(new Dimension(500, 300)); // ������ ����������
													// ��������� ������

		// ����������� ������� - �� ����������� �� ����� �������. � �� �
		// ������������������ ������ ���������
		butt1.addActionListener(new ActionListener() { // ��� ������� ������
														// ������� ������ BUTT1

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out
						.println("���������� ���������� �������� �������1: ��� ���� �� ������ butt1.");

			}// ���������� ����������
		}// ����� �������� ����������
		); // �������� �������������, �� �������� ������� ; ����� )

		// ��� ��������� ������� ������ butt2 ���� � ��� �������� ����
		// ����������� ��������� �������
		butt2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out
						.println("���������� ���������� �������� �������2: ��� ���� �� ������ butt2.");
			}
		});

		// ------------------------------------------------------------------
		// ���� 34 ------------------------------------------------------------
		// ���������� ComboBox-� ������� ���������� ��������
		add(cBox);
		cBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out
						.println("��� ������. ������� , �������������� � ��������� cBox");
				System.out.println(cBox.toString());
				System.out.println(cBox.getSelectedItem().toString());
				// ����� cBox.getSelectedItem(). ���������� ������ �� ���������
				// ������ ������ ����������
				// ���� � ��������� ����� ������ �������� - �� ����� �����
				// �������� ������

				// �������� � �������� ���������� ������ �����
				tFld1.setText(cBox.getSelectedItem().toString());
			}
		});

		add(list);
		list.addListSelectionListener(new ListSelectionListener() {

			@Override
			// ������ �������. ������������� 2 ���� - �.�. 2 �������
			// - ����� ������� ������ � ��������� ������ ������
			// ���������� JList ����� ������ � � ������� � � ������ ������
			// � ���� ����������� ������� ������������ ������ ������ JList , �
			// �� �������
			public void valueChanged(ListSelectionEvent event) {
				// TODO Auto-generated method stub
				System.out
						.println("��� ������. ������� , �������������� � ������ list . "
								+ count++);

				// � ���� ����������� ������� ������������ ������ ������ JList ,
				// � �� �������

				// ---------------------------------------------------------------------
				// ������������ ������������ �������� ��������� ���������
				// �������
				// �� ���� ������� ���������� ��������� ��� - �� ����������
				// �������� -
				// �� ����� �������� � ����������� �������� ����� ����������� 2
				// ����
				// �������� ���� �� ��������� ���-�� ����-�� - ����� �����
				// ������� ����������
				// �� ���� ������ ������� �����, ����� ���������� ���������
				// ������������ ��������
				if (event.getValueIsAdjusting() == true) // ����� ��������,
															// event ��� ���
															// ����������(true)
															// ��� ���(flse)
															// �.�. ���� true -
															// �� ������ ��
															// �������� �����
															// �����������
															// ���� false - ��
															// ������ ���������
															// ����� �����������
															// �������.
					System.out.println("��� ��  ��������� ����� �����������");
				else {
					System.out.println("��� ��������� ����� �����������");
					// ����� ����� �������� ������ � ������ ��������� , �������
					// �� ������ ����������� � �������������
				}
				// --------------------------------------------------------------------

				// //�������� � �������� ���������� ������ ����� � ������
				tFld1.setText("JList:" + list.getSelectedValue()); // ��������
																	// ������
																	// ���
																	// ����������
																	// ��������

				// �������� �� �� ����� � ���������, �� �������������� ������
				// getSourse()
				tFld1.setText("JList:    "
						+ ((JList) event.getSource()).getSelectedValue());

				// �� �� �����, �� � 2 ������
				JList obj = (JList) event.getSource(); // �������� ������-�����
														// �� ������ - ��������
														// �������
				tFld1.setText("JList:    " + obj.getSelectedValue());

				// ������� ��������� -
				// 1. ��������� ����� �������� ������ ��������. ������ ����� �
				// ������
				// 2. ��������� ����� ������� �� ���� - � ������ �������
				// 3. ������� ������� ��� �������� - �������� �� ������
				// ����������
				// ����� ����� ���������� ����� ���� � ����� ������ �������� �
				// ��������� � ����� ��������

			}
		});

		// ���������� ���� ������� ������������������
		// 1. �������� �������� ���������� ������������ � ��������������
		// � ������������ 5 ������������ - �.�. �� ������ ��� 5 ������� �������
		// ������ ����� � ������.��������
		// ����� ��������� �����������:
		// ������� - ��� �����, ������� � ���� ��� �������� ��� �������� ������
		// �������
		// 1. ������� ������� �������. ���������� ( new Mou �������-������)
		// 2. �������� ����� Listener �� ������� ������ Adapter � ������ ������
		// ������ � ��������� ������
		// 3. ������ ����� ������ ��� �� ������ �������� �������-������������
		// � ���������� ��� ���� ��� �������� �� �����

		// 2. �������� ��������� ���������� ��� ��������� ���������

		// ��� ���������� , ������� ��������� � ������-������� (�����
		// ����������������)
		// �� �����. ���� �� ����� ������������ final
		// ������ 2:
		// 1. ������� � ���������� ����������� Final - �� ���������� ����������
		// � ���������
		final JTextField tFld2 = new JTextField(20); // final ��� ������
														// ��������� - ���
														// ���������,
														// �.�. ������ ���������
														// ������ ���� ��
														// ��������.
														// �������� ����,
														// ��������� , ��. �� ��
														// ������-�����
														// ����������
		final int Y_CONST = 10; // ��� ���������� �������� ������ ����� ��
								// ���������
		add(tFld2);

		// 2. ���� ���������� ������ ������� Final (�.�. ��� ����� ������
		// ����������)
		// �� �������� ��������� ��� ���������� � ������� ����� ������ (������)
		// ����� ���������� ������ ����� ������.

		tFld2.addMouseListener(new MouseAdapter() {
			// ���������� ����� ���� ��������� ����� - ������ ���������� ������
			// ��� ����� ����� � ��������� ������
			int x = 0; // �����, ����� ������� ������������ � ��� ��������
						// ����������� ���� ���������� ������
			long thisFieldOfAnonym;
			JButton butt = new JButton("cancel"); // �������������� ����
													// ������������

			@Override
			public void mouseClicked(MouseEvent e) {

				// ����� ����������� ��������� ���������� ������-����������� �
				// ������� ��������������
				int x = 0, y;

				// TODO Auto-generated method stub
				// � ��������� ����������� ����� ����������������������-��������
				// �������
				// � ��������������� �� ����� ���������� ������-��������,
				// � ������� �������� ��������� �����. ���� � ���� ����������
				// ��� ������������ Final
				System.out.println("� ��������� ������ ����� �����: "
						+ tFld2.getText()); //
				System.out.println(x);
				// Y_CONST = 7;
			}
		});

		// ----------------------------------------------------------�������
		// ������ ����� ----------------------------
		tFld1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent event) {
				// TODO Auto-generated method stub
				// event.
				// ���������� ����� ����
				System.out.println("����������  ������ ����: "
						+ event.getClickCount());

				// ����������. ����� ������ ������� ���� ���������
				System.out.println("����� ��������� ������� �����: "
						+ event.getButton());

				// ���� ������ 2� ���� ������ ������� ���� , �� ������� ��
				// ������ ���������
				if ((event.getClickCount() == 2)
						&& (event.getButton() == event.BUTTON3)) {
					System.out.println("������ 2� ���� ������ �������");
				}

				// ��������� ������� ������-������������� ������������ � ������
				// ����
				// ������������ - �������, ���, ����
				System.out.println(event.getModifiers()); // ���� ��� (�����)
															// �������
															// �������������
				// ������� �������� � ���������� �������������
				System.out.println(event.getMouseModifiersText(event
						.getModifiers()));
				if (event.getMouseModifiersText(event.getModifiers())
						.equalsIgnoreCase("Ctrl+Button1")) {
					// ������� ������ �������� ������� ������+��������� ����
				}

				System.out
						.println("--------------------------------------------------------------------------------------");
				// ������ ���� � �������������, ��� ���� (���� ���� �����������
				// ������� event.getButton() )
				System.out.println(event.getModifiersEx()); // ���� ��� (�����)
															// �������
															// �������������
				// ������� �������� � ���������� �������������
				System.out.println(event.getModifiersExText(event
						.getModifiersEx()));
				if (event.getModifiersExText(event.getModifiersEx())
						.equalsIgnoreCase("Ctrl")) {
					// ������� ������ ����� ��� ������� ������
				}

				// ��� �� ����� �������� ������� ������������� � �������
				// �������� �����
				// ������: �������� ������� ������ ���
				if ((event.getModifiers() & event.ALT_MASK) != 0) {
					System.out.println("����� ������������: ���� ������ ���.");
				} else
					System.out
							.println("����� ������������: �� ����  ������ ���.");

				// ������: �������� ������������� ������� ������ Ctrl � Shift
				if (((event.getModifiers() & event.CTRL_MASK) != 0)
						&& ((event.getModifiers() & event.SHIFT_MASK) != 0)) {
					System.out
							.println("����� ������������: ���� ������ ������� � ����.");
				}

				// ������ � �� � � ������� �������� � ������� �� ������ DOWN
				if ((event.getModifiersEx() & event.ALT_DOWN_MASK) != 0) {
					System.out
							.println("����� EX-DOWN ������������: ���� ������ ���.");
				} else
					System.out
							.println("����� EX-DOWN ������������: �� ����  ������ ���.");

			}
		});

		setVisible(true);

	} // construcror

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// ��� ������ ���������� ������� - �� ���������, �.�. � ���� ��� �����
		// ������� ���������� 1 ��� � �����, ������� ��� �� �� �����
		new GUI_MouseMasks("��������� ��������");
	}

}// class
