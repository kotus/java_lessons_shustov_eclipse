package forms;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import bankSystem.BankCard;
import bankSystem.Client;

public class CardIssuingClient extends JFrame{

	final String filePath = "src/BCards_to_clients.out";
	
	static JList listOfClients;
	static Client[] arrOfClients;
	{
		try {
			arrOfClients = Client.makeMassClient("src/ClientsAllAcc.in");
			listOfClients = new JList( arrOfClients );
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	static JList listOfBankCards;
	BankCard[] arrOfBankCards;
	{
		try {
			arrOfBankCards = BankCard.makeMassCard("src/master-visa-2014-05-14---01.in");
			listOfBankCards = new JList( arrOfBankCards );
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	static JCheckBox checkBox_kreditnost = new JCheckBox("�����������", true);
	
	private static final String [] arrayOfCommission = {"0% ��� ������", "0% ��� ����������"};
	static JRadioButton radioButtons_comission [] = new JRadioButton [arrayOfCommission.length];
	static ButtonGroup buttonGroup = new ButtonGroup();
	{
		for (int i = 0; i < arrayOfCommission.length; i++) {
			radioButtons_comission[i] = new JRadioButton(arrayOfCommission[i]);
		}
		radioButtons_comission[0].setSelected(true);
	}
	
	static JButton buttonIssueCard = new JButton("��������� ����� �������");
	
	public CardIssuingClient(String title) throws HeadlessException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		
		super(title);
		
		setLocation(300, 300); 
		setSize(600, 180);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE ); 
		
		JPanel mainPanel = new JPanel(new BorderLayout(2, 2));
		add(mainPanel);
		
		mainPanel.setBorder(BorderFactory.createEmptyBorder(4, 2, 4, 2));
		
		JPanel centralPanel = new JPanel(new GridLayout(1, 2, 4, 4));
		mainPanel.add(centralPanel, BorderLayout.CENTER);
		
		JScrollPane scrollPaneClients = new JScrollPane(listOfClients);
		centralPanel.add(scrollPaneClients);
		
		JScrollPane scrollPaneCards = new JScrollPane(listOfBankCards);
		centralPanel.add(scrollPaneCards);
		
		JPanel panelOfFunctions = new JPanel(new GridLayout(3, 1, 2, 2));
		mainPanel.add(panelOfFunctions, BorderLayout.EAST);
		
		panelOfFunctions.add(checkBox_kreditnost);
		
		JPanel panelOfComboBox = new JPanel();
		for (int i = 0; i < radioButtons_comission.length; i++) {
			panelOfComboBox.add(radioButtons_comission[i]);
			buttonGroup.add(radioButtons_comission[i]);
		}
		panelOfComboBox.setLayout(new BoxLayout(panelOfComboBox, BoxLayout.Y_AXIS));
		
		panelOfFunctions.add(panelOfComboBox);
		
		buttonIssueCard.setMargin(new Insets(1, 0, 1, 0));
		panelOfFunctions.add(buttonIssueCard);
		
		//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		buttonIssueCard.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				
				if (listOfClients.getSelectedIndex() == -1) {
					JOptionPane.showMessageDialog((Component)event.getSource(),
							"�� ������ �� ���� ������",
							"������ ����������",
							JOptionPane.ERROR_MESSAGE);	
					return;
				} else if (listOfBankCards.getSelectedIndex() == -1) {
					JOptionPane.showMessageDialog((Component)event.getSource(),
							"�� ������� �� ���� �����",
							"������ ����������",
							JOptionPane.ERROR_MESSAGE);	
					return;
				}
				
				writeIssuingToDisk();
								
			}
		});
		
		setFocusable(true);
		setVisible(true);
		
	}

	public static void main(String[] args) throws HeadlessException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		
		new CardIssuingClient("������ ����� �������");
		
	}
	
	private boolean writeIssuingToDisk() {
		
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yy-MM-dd");
		SimpleDateFormat dateFormatTime = new SimpleDateFormat("HH-mm-ss-S");
		
		int numberOfClient = 0;
		for (int i = 0; i < arrOfClients.length; i++) {
			if (arrOfClients[i].equals(listOfClients.getSelectedValue())) {
				numberOfClient = i;
				break;
			} 
		}
		
		String selectedRadio = "";
		for (int i = 0; i < radioButtons_comission.length; i++) {
			
			if (radioButtons_comission[i].isSelected()) 
				selectedRadio = radioButtons_comission[i].getText();
			
		}
		
		/*
		String selectedRadio = "";
		Enumeration<AbstractButton> buttons = buttonGroup.getElements();
		while (buttons.hasMoreElements()) {
			AbstractButton abstractButton = (AbstractButton) buttons
					.nextElement();
			if (abstractButton.isSelected()) {
				selectedRadio = abstractButton.getText();
			}
		}
		*/
		
		String resultString = numberOfClient + "\t" + dateFormatYear.format(new Date(System.currentTimeMillis()))+ "\t" +
				dateFormatTime.format(new Date(System.currentTimeMillis()))+ "\t" +
				numberOfClient + "\t" + ((BankCard)listOfBankCards.getSelectedValue()).getCardNumber() + "\t" + 
				(checkBox_kreditnost.isSelected()?"���������":"�����������") + "\t" + selectedRadio;
		
		System.out.println(resultString);
		
		FileWriter fileWriter = null;
		try {
			
			fileWriter = new FileWriter(new File(this.filePath), true);
			fileWriter.write(resultString + "\n");
			fileWriter.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
		
	}
	
}
