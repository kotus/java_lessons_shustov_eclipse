package bankSystem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

//import pack1.Parser;

import java.util.StringTokenizer;

import arrays.Mass;

public class Client extends Person {

	private BankCard bankCard;
	private Account account; // = new Account();

	public BankCard getBankCard() {
		return bankCard;
	}

	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Client(String surname, String name, String middleName) {
		super(surname, name, middleName);

		String accNumber = "27007000"
				+ String.valueOf(100000L + Math.random() * (999999L - 100000L));
		accNumber = accNumber.substring(0, 13);
		this.account = new Account(Long.parseLong(accNumber), 0L, "UAH", this);

	}

	public Client(String surname, String name, String middleName,
			BankCard bankCard) {
		super(surname, name, middleName);
		this.bankCard = bankCard;
	}

	public Client(String surname, String name, String middleName,
			BankCard bankCard, Account account) {
		super(surname, name, middleName);
		this.bankCard = bankCard;
		this.account = account;
	}

	public Client(String surename, String name, String middleName,
			Account account) {
		super(surename, name, middleName);
		this.account = account;
		
	}

	// ������� � ������� �����, �� ����� ������� �������
	public String payBCard(Summ summa, Client client) {

		Long summaThisClient = this.bankCard.getSumm().getSumm();
		Long summaComingClient = client.bankCard.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();

		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0) {
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0) {
			return "������������ ������� ��� ������";
		}

		this.bankCard.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.bankCard.getSumm().setSumm(summaComingClient + summaOfPayment);

		return "1";

	}
	
	public String payBAccount(Summ summa, Client client) {

		Long summaThisClient = this.account.getSumm().getSumm();
		Long summaComingClient = client.account.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();

		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0) {
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0) {
			return "������������ ������� ��� ������";
		}

		this.account.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.account.getSumm().setSumm(summaComingClient + summaOfPayment);

		return "1";

	}

	public static void main(String[] args) {

		Client[] arrOfClients = null;
		try {
			arrOfClients = Client.makeMassClient("src/ClientsAllAcc.in");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Client.testMassClient(arrOfClients);
		
	}

	/**
	 * ����������� �����, ��������� ������ �������� Client[] �������� ��
	 * ����������� � ���� ���������� �������.
	 * 
	 * @param matrixFromFile
	 *            - ��� String[][], ��������� ������ ��� �������: { { "������",
	 *            "����", "��������" } , ...}
	 * 
	 * @return arrClients - ��� Client [], ���������� ������ �������� ����
	 *         Client.
	 */
	public static Client[] makeMassClient(String[][] matrixFromFile) {

		Client[] arrClients = new Client[matrixFromFile.length];
		for (int i = 0; i < arrClients.length; i++) {
			
			String[] arrayOfName = matrixFromFile[i][0].split(" ");
			
			arrClients[i] = new Client(arrayOfName[0], arrayOfName[1], arrayOfName[2]);
			arrClients[i].setAccount(new Account(Long.parseLong(matrixFromFile[i][1]), 0L, "UAH", arrClients[i]));
			
		}

		return arrClients;

	}

	/**
	 * ����������� �����, ��������� ������ �������� Client[] �������� ��
	 * ����������� � ���� ���� � ����� � ��� ��������.
	 * 
	 * @param fileNameFIO
	 *            ���� String, ��� ����� �� �����.
	 * @return <b>arrClients</b> ���� Client [] ���� ������ ������ ��� NULL ����
	 *         ������ �� �������.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Client[] makeMassClient(String fileNameFIO)
			throws FileNotFoundException, IOException {

		String[][] arrayFromFile = Client.makeFIOstrFromFile(fileNameFIO);

		if (arrayFromFile.length != 0) {
			Client[] arrClients = Client.makeMassClient(arrayFromFile);
			return arrClients;
		} else {
			return null;
		}

	}

	/**
	 * ����� �������� ���� �� ������ "src/fio_clients.in" � ��������� �� ����
	 * ������� ���.<br>
	 * ��������� ����� ������ ���� ���������:<br>
	 * ������� ���� ��������<br>
	 * ������� ���� ��������<br>
	 * ����� ����� ��������<br>
	 * ...<br>
	 * ������������ �� ��������� ��������� ������.
	 * 
	 * @param fileNameFIO
	 *            ���� String, ��� ����� �� �����.
	 * @return <b>resultMatrix<b> ���� String[][], ��������� ������ ��� �������:
	 *         { { "������", "����", "��������" } , ...}
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String[][] makeFIOstrFromFile(String fileNameFIO)
			throws FileNotFoundException, IOException {

		BufferedReader reader = new BufferedReader(new FileReader(fileNameFIO));
		String str;
		int count = 0;
		while ((str = reader.readLine()) != null) {
			if (str.trim().equals(""))
				continue;
			count++;
		}

		String[][] resultMatrix = new String[count][5];

		reader = new BufferedReader(new FileReader(fileNameFIO));

		int i = 0;
		while ((str = reader.readLine()) != null) {

			if (str.trim().equals("")) {
				i++;
				continue;
			}
				
			StringTokenizer tokenizer = new StringTokenizer(str, "\t");

			int j = 0;
			while (tokenizer.hasMoreTokens()) {

				String tempString = tokenizer.nextToken().trim();
				String[] arrOfTempString = tempString.split(":");
				
				resultMatrix[i][j] = arrOfTempString[1];
				j++;

			}

			i++;

		}

		return resultMatrix;

	}

	/**
	 * ����� ����������� ������ ������ makeMassClient, �� �������� �������
	 * �������� ���� Client
	 * 
	 * @param arrOfClients
	 *            - ��� Client [], ���������� ������ �������� ���� Client.
	 */
	public static void testMassClient(Client[] arrOfClients) {

		for (int i = 0; i < arrOfClients.length; i++) {
			System.out.println(arrOfClients[i]);
		}

	}

	@Override
	public String toString() {
	
		/*
		return super.getSurename() + " " + super.getName() + " "
				+ super.getMiddleName() + " pp:" + getAccount().getAccountNumber()
				+ " �����:"+getAccount().getSumm().getSumm();
		*/
		
		return super.getSurename() + " " + super.getName() + " " + super.getMiddleName();
		
	}

}
