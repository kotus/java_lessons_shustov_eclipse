package bankSystem;

import java.io.IOException;

import arrays.Mass;

public class BankMain {

	public static Bankomat[] arrOfBancomats;
	public static Staff[] arrOfStaffs;
	public static Client[] arrOfClients;
	public static BankCard[] arrOfBankCards;
	
	public BankMain() throws IOException {
		
		arrOfBancomats = Bankomat.makeMassBankomat("src/bancomats.in");
		arrOfStaffs = Staff.makeArrStaff("src/staff.in");
		arrOfClients = Client.makeMassClient("src/ClientsAllAcc.in");
		arrOfBankCards = BankCard.makeMassCard("src/master-visa-2014-05-14---01.in");
		
	}

	public static void main(String[] args) throws IOException {
		
		BankMain mainBank= new BankMain();
		testMassAll();
	
	}//main

	public static void testMassAll() {
		
		Mass.showArrayOfObjects(arrOfBancomats);
		Mass.showArrayOfObjects(arrOfStaffs);
		Mass.showArrayOfObjects(arrOfClients);
		BankCard.testMassBankCard(arrOfBankCards);
		
	}
	
}
