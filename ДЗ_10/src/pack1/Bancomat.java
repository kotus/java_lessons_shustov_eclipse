package pack1;

public class Bancomat {

	private String numInvent;
	private String address;
	private Summ summ;
	private Integer errorCode;
	
	public String getNumInvent() {
		return numInvent;
	}
	public void setNumInvent(String numInvent) {
		this.numInvent = numInvent;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public static void main(String args[]){
		
		System.out.println(new Bancomat("1", "���. ���������� ����������� �����", 10000L, "RUR"));
		
	}
		
	public Bancomat(String numInvent, String address) {
		super();
		this.numInvent = numInvent;
		this.address = address;
	}
	
	public Bancomat(String numInvent, String address, Long summ, String valuta) {
		super();
		this.numInvent = numInvent;
		this.address = address;
		this.summ = new Summ(summ, valuta);
	} 
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "����� Bancomat:[����� = "+numInvent+"; ����� = "+address+";"+
				summ.toString()+"]";
	}
	

	
}
