package homeWork;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.StringTokenizer;

public class DB_Postgres {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException{

		DB_Postgres db_Postgres = new DB_Postgres();
		
//		db_Postgres.readTable("b2", "bankomats");
//		db_Postgres.readTable("b2", "bankomats");
		
//		db_Postgres.readTableSQL("b2", "SELECT \"numInvent\", \"valutaCode\", summ FROM bankomats;");
		
//		db_Postgres.createFileBankomatsIn();
		
		db_Postgres.createFileBankCardsIn();
		

	}
	
	private void readTable(String DBname, String TableName) throws ClassNotFoundException, SQLException{
		
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + DBname, "postgres", "postgres");

		String strSQL = "SELECT * FROM " + TableName;
		
		PreparedStatement ps = conn.prepareStatement(strSQL);
		try {
			ResultSet res = ps.executeQuery();
			int numColl = res.getMetaData().getColumnCount();
			while (res.next()) {
				String resString = "";
				for (int i = 1; i <= numColl; i++) {
					resString = resString + (res.getString(i) + "\t | ");
				}
				System.out.println(resString);
				writeLogToDisk(resString, "src/readTable.log");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			} 
		}
			
	}
	
	private void readTableSQL(String DBname, String strSQL) throws ClassNotFoundException, SQLException{
		
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + DBname, "postgres", "postgres");

		PreparedStatement ps = conn.prepareStatement(strSQL);
		try {
			ResultSet res = ps.executeQuery();
			int numColl = res.getMetaData().getColumnCount();
			while (res.next()) {
				String resString = "";
				for (int i = 1; i <= numColl; i++) {
					resString = resString + (res.getString(i) + "\t | ");
				}
				System.out.println(resString);
				writeLogToDisk(resString, "src/readTable.log");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			} 
		}
			
	}
	
	private boolean writeLogToDisk(String stringToWrite, String filePath){
		
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormatTime = new SimpleDateFormat("HH-mm-ss-S");
		
		String resultString = dateFormatYear.format(new Date(System.currentTimeMillis()))+ " " +
				dateFormatTime.format(new Date(System.currentTimeMillis()))+ "\t | " + stringToWrite;
				
		FileWriter fileWriter = null;
		try {
			
			fileWriter = new FileWriter(new File(filePath), true);
			fileWriter.write(resultString + "\n");
			fileWriter.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	private void createFileBankomatsIn() throws IOException {
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/kbuhantsev/��������/����_66/�����-������������.txt"));
		FileWriter fileWriter = new FileWriter(new File("src/Bankomats.in"), true);
		
		String strFromFile = bufferedReader.readLine();
		
		StringTokenizer tokenizer = new StringTokenizer(strFromFile, "�\t");

		int i = 0;
		Random random = new Random();
		
		while (tokenizer.hasMoreElements()) {
			
			String resultString = "";
			String stringAddress = tokenizer.nextToken() + "�";
			
			resultString = ++i + " | " + "1" + " | " + stringAddress.trim() + " | " + (random.nextInt(3) + 1) + 
					" | " + random.nextInt(100000) + " | " + "���";
			
			try {
				fileWriter.write(resultString + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		fileWriter.close();
		bufferedReader.close();
		
	}

//	������-����.txt
//	��/� | # ����� 16 ���� | ����.��� 3 ����� | �����-��� �������� | ��� 4 ����� | ���.���|�����
//	����������:
//	3.2.2. � �/� � ���������
//	3.2.3. � ����� � ��������, ������ ����� �� 0, 
//	3.2.4. ���� ��� � ��������
//	3.2.5. ����� �������� � ������� + 2 ����
//	3.2.6. ��� � ��������
//	3.2.7. ��� ���: �������� , 0-3
//	3.2.8. ����� � �������� �� 0 �� 100000
//	������������� ������ � 50 ���� ������-����.
	
	private void createFileBankCardsIn() throws IOException {
		
		FileWriter fileWriter = new FileWriter(new File("src/master-visa.txt"), true);
		
		Random random = new Random();
		
		long max = 9999999999999999L;
		long min = 1000000000000000L;
		
		GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
		calendar.add(GregorianCalendar.YEAR, 2);
		
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy/MM");
		String dateExp = dateFormatYear.format(calendar.getTime());
				
		for (int i = 1; i <= 50; i++) {
			
			String resultString = "";
						
			resultString = i + " | " + String.format("%.0f", (min + (Math.random() * (max-min))) ) + " | " +
					(random.nextInt(899) + 100) + " | " + dateExp + " | " + (random.nextInt(899) + 100) + " | " + 
					((random.nextInt(3) + 1))  + " | " + random.nextInt(100000);
			
			System.out.println(resultString);
			
			
			try {
				fileWriter.write(resultString + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		fileWriter.close();
				
	}
	
}
