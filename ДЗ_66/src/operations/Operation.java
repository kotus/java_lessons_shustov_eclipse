package operations;

import bankSystem.Staff;

public class Operation {

	private Long ID;
	private String date;
	private String time;
	private String text;
	private Object side1;
	private Object side2;
	private Staff maker;
	private Operation operOwner;
	private Operation operSub;
		
	
	public Staff getMaker() {
		return maker;
	}

	public void setMaker(Staff maker) {
		this.maker = maker;
	}

	public Operation getOperSub() {
		return operSub;
	}

	public void setOperSub(Operation operSub) {
		this.operSub = operSub;
	}
	
	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Object getSide1() {
		return side1;
	}

	public void setSide1(Object side1) {
		this.side1 = side1;
	}

	public Object getSide2() {
		return side2;
	}

	public void setSide2(Object side2) {
		this.side2 = side2;
	}

	public Operation getOperOwner() {
		return operOwner;
	}

	public void setOperOwner(Operation operOwner) {
		this.operOwner = operOwner;
	}

	public Operation(Long iD, String date, String time, String text,
			Object side1, Object side2, Staff maker) {
		ID = iD;
		this.date = date;
		this.time = time;
		this.text = text;
		this.side1 = side1;
		this.side2 = side2;
		this.maker = maker;
		this.operOwner = this;
	}
	
	public Operation(Long iD, String date, String time, String text,
			Object side1, Object side2) {
		ID = iD;
		this.date = date;
		this.time = time;
		this.text = text;
		this.side1 = side1;
		this.side2 = side2;
		this.operOwner = this;
	}


}
