package lesson64;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JButton;


public class ParType_Masks_Pattern {
	Integer x;

	public ParType_Masks_Pattern() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	//---------------------------------------����������� ����������, ����������  ���������������-----------------------------------------------------------
	//1. ������ ���-�� � ��������� new
	//T val = new T();	//���������� �����
	//2. �� ����� ������ ���������� � ������� ��� !!!
	//� ������� ������� � ������� � �����������
	//ArrayList<T> obj = new ArrayList<>();  //����� � �����-������� � �����-������� 
	
	//3. ������ ����-�� �����.���� � ������ �����
	class inClass<T> { 
		//private static T key = null; 
	}
	
	//4. ������ ���-�� � ��������� instanceof
	//if (obj10 instanceof T)  //������ ������������ � ������ �� ���������
//	ParType_Masks  pT10 = 	new ParType_Masks();  //������������ - �.�. ����� ������������ � ������� ������������� ������ ����� �� ������
//	ParType_Masks pT11 = 	new ParType_Masks();		

	//5. ���������� ������� � ����������
	List<JButton> l1 = new ArrayList<>();
	ArrayList<JButton> l2 = (ArrayList<JButton>)l1;
	//List<Component> l3 = (List<Component>)l1;  //������. �� ���������
	
	//6. ������ ������� ������� � ������������
	List[] arrOfLists = new ArrayList[3];
	//List<Integer>[] arrOfLists1 = new ArrayList<>[3]; //compile error
	
	//7. throws ...  - ����� ���� ������ ������������ ��� ������������
	//������� Exception � Throwable
	//class Except<T> extends Exception { } //����������-��. ���������� 
	//catch (T e) {}						//����������-��. ����������
	//class Except1<T> extends Throwable { }//����������-��. ����������
	
	
	//------------------------------------------------------------------------------------------------------------------
	//����� ���������� �����  - ���������� �������� ������������, 
	//������� �������� ������� ������� ������� 
		class Point
		{int data; void met1(int dataPar){data = dataPar;} }
		
		class Square extends Point { int  x;  }
		class Cube    extends Square{ int y; }
			 
		
		<T>void  makeList(List<T> collPar, T objPar) {
			collPar.add(objPar);
		}	
	
		//****************************************����� �������� ����� ���������� ******************
		
	
	


	public static void main(String[] args) {
		// TODO Auto-generated method stub
				//��� ������������� ������� � ����������� �������� ������� �����, �.�. ������-������ ����������
				ParType_Masks_Pattern obj = new ParType_Masks_Pattern();
				
				//������� ��� ���������.
				Student1[] arrSt =  
					{new Student1("������",	"����", 1000),
					new Student1("������",	"����", 0),
					new Student1("�������",	"����", 0 ),
					new Student1("������",	"����", 1000) };
					//���������� ������������� ��� ������ � ����������� ���������
					//������ ��� ���� �� �����������
		

BCard2[ ] arrBCard = {new BCard2(1, 1000),new BCard2(2, 2000),new BCard2(3, 3000),new BCard2(3, 3000)};


//������� ��������� �� ��������, �� ���� ��� �������
					Vector<Student1> vecSt = new Vector<>();
					Vector<BCard2>   vecBC= new Vector<>();
				for (int i = 0; i < 4; i++) {
					//obj.makeList(vecSt, arrBCard[i]);  //������. ����� ������
					obj.makeList(vecSt, arrSt[i]);
					obj.makeList(vecBC, arrBCard[i]);
				}
				System.out.println(vecBC);
				System.out.println(vecSt);
	//******************************* ����� �������  � �����() ************************************  			

		
	}//main()

}//public class


// --------------------------------------------------------------------------------------------------
// ����� ��� ���������� Comparable - ����� ����� ��������� �����������
class Student1 {
	String lastName;
	String name;
	int number;
	long summ;

	static int lastNum = 0;

	//
	public Student1(String lastNamePar, String namePar, long summPar) {
		// super();
		/* this. */lastName = lastNamePar;
		/* this. */name = namePar;
		summ = summPar;
		number = lastNum++;
	}

	public Student1() {
		// TODO Auto-generated constructor stub
	}

	// ��� ������ ��������� �� �����
	public String toString() {
		return number + " " + lastName + " " + name + "  " + summ;
	}

}// class Student

class BCard2 {
	long num16;
	long sum;

	public BCard2(long num16, long sum) {
		super();
		this.num16 = num16;
		this.sum = sum;
	}

	public BCard2() {

	}

	public String toString() {
		return num16 + " " + sum;
	}

}// BCard
	


