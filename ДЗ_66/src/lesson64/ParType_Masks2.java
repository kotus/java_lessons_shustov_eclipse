package lesson64;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JButton;


public class ParType_Masks2 {
	Integer x;

	public ParType_Masks2() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	//---------------------------------------����������� ����������, ����������  ���������������-----------------------------------------------------------
	//0. ��� ������ � ������� ���������� ���������� ������ ������� �������� ��������� �� �������, ��������
	//The method set(int,          capture#3-of ? extends ParType_Masks2.Point)                       
	//in the type           List      <capture#3-of ? extends ParType_Masks2.Point>                          
	//is not applicable for the arguments                  (int, ParType_Masks2.Point)
	
	//1. ������ ���-�� � ��������� new
	//T val = new T();	//���������� �����
	//2. �� ����� ������ ���������� � ������� ��� !!!
	//� ������� ������� � ������� � �����������
	//ArrayList<T> obj = new ArrayList<>();  //����� � �����-������� � �����-������� 
	
	//3. ������ ����-�� �����.���� � ������ �����
	class inClass<T> { 
		//private static T key = null; 
	}
	
	//4. ������ ���-�� � ��������� instanceof
	//if (obj10 instanceof T)  //������ ������������ � ������ �� ���������
//	ParType_Masks  pT10 = 	new ParType_Masks();  //������������ - �.�. ����� ������������ � ������� ������������� ������ ����� �� ������
//	ParType_Masks pT11 = 	new ParType_Masks();		

	//5. ���������� ������� � ����������
	List<JButton> l1 = new ArrayList<>();
	ArrayList<JButton> l2 = (ArrayList<JButton>)l1;
	//List<Component> l3 = (List<Component>)l1;  //������. �� ���������
	
	//6. ������ ������� ������� � ������������
	List[] arrOfLists = new ArrayList[3];
	//List<Integer>[] arrOfLists1 = new ArrayList<>[3]; //compile error
	
	//7. throws ...  - ����� ���� ������ ������������ ��� ������������
	//������� Exception � Throwable
	//class Except<T> extends Exception { } //����������-��. ���������� 
	//catch (T e) {}						//����������-��. ����������
	//class Except1<T> extends Throwable { }//����������-��. ����������
	
	
	//------------------------------------------------------------------------------------------------------------------
	//����� ���������� �����  - ���������� �������� ������������, 
	//������� �������� ������� ������� ������� 
		class Point
		{int data; void met1(int dataPar){data = dataPar;} }
		
		class Square extends Point { int  x;  }
		class Cube    extends Square{ int y; }
			 
		
        //��� �������� ��������� �� ��������.
		<T>void  makeList(List<T> collPar, T objPar) {
			collPar.add(objPar);
		}	
	
		//****************************************����� ������� ����� �������� ����� ���������� ******************
		
	
		//������� ������������� ����� ����������   � ������� �����������
		//��������� ����� ������������ � �������� � ���������� (� ����� super ������ � ������� ������� �� �����)
		//�� �� ���������� ����� ��������� ������ � ���������� ���������
		//1. ����� ������������� (������������) ����� ���� < ? extends Point >
		void   metMask1(  List < ? extends Point> collPar   ) {  // � ��������� ����� : ����� �����-��������� ������
			//������ �� ���������
			//�.�. ����� ���� extends, �� �������� ��������� ����� ����� �������  ������ �� =
			int count=0;
			for (Point elem : collPar) {
				//����� � ���� ������:
				//1.1  ������������ ������ ������ Point - ������ ��������� ���������
					elem.met1( count++ );
				//1.2.  ������������ ���� ��������� ���������
					int x = elem.data;
					elem.data = x;				//���� ����� �������������� �����  �� =
					
				//1.3 ������ ��������� , �� ���������� �������� ��������� (�� ��������� � �� �������� ������ ���������)
					//� ������ ��� ����������
					Point obj = collPar.get(0);
					collPar.contains( elem);
					collPar.clear();  //���� � ������ ���������, �� �� �������� � ���.

				//1.4	 ��������� ������ ���������, ���������� ��������(�� ������=������)  
					//collPar.set(0, elem);  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������
					//x = y;   //�   ���������������  � �������� ���������� �
										//�.�. ��������� �������� - ������ ���������� ����� �� =
					//collPar.add( elem );  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������
						
				//Collections.	//� ������������� ����� �������� ������������� ����� ����������
		
					
					//�� �� ����� , �� ������ �� ��������� ����������
					for (Iterator iterator  = collPar.iterator(); iterator 	.hasNext();                                    ) {
						Point point = (Point) iterator .next();
						elem.met1( count++ );
							 x = elem.data;
							elem.data = x;				//���� ����� �������������� �����  �� =
					    
							obj = collPar.get(0);
							collPar.contains( elem);
							collPar.clear();  //���� � ������ ���������, �� �� �������� � ���.

							//collPar.set(0, elem);  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������
							//collPar.add( elem );  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������
								
					}
					
			}
		}//metMask1
		
		//2. ����� ������������� (������������) ����� ���� < ? extends Point >
		void   metMask2(  List < ? super Cube> collPar   ) {  // � ��������� ����� : ����� �����-��������� ������
			//������ �� ���������
			//�.�. ����� ���� extends, �� �������� ��������� ����� ����� �������  ������ �� =
			int count=0;
		//	for (Cube elem : collPar) {   //���� ���� ����������, �.�. �� ������� � ������ ������������� ����
														//��������������� ��� �������� ��������
			//��� ������ ��������� ������ ����������������� � ����������� ������������� ������ ���������
			//�� ��� ��� ������� - �������� ����� ������������ ����������
			Point elem=(Point) collPar.get(0); 
				//����� � ���� ������ c ������������: �������� ������� � ����� ������ Point:
				//2.1  ������������ ������ ������ Point - ������ ��������� ���������
					elem.met1( count++ );
				//2.2.  ������������ ���� ��������� ���������
					int x = elem.data;
					elem.data = x;				//���� ����� �������������� �����  �� =
					
				//2.3 ������ ��������� , �� ���������� �������� ��������� (�� ��������� � �� �������� ������ ���������)
					//� ������ ��� ����������
					Point obj = (Point) collPar.get(0);  //���������� �� ��������, �� ������ � ����  ������ Cube �������� �� �����
					//Cube objCube = collPar.get(0);  //���������� ���� ������, �.�. ��� �������� �������������� Point � Cube 
					
					collPar.contains( elem);
					collPar.clear();  //���� � ������ ���������, �� �� �������� � ���.

				//2.4	 ������ ���������, ���������� ��������(�� ������=������)  ���������
					collPar.set(0, new Cube() );  //�������� ����� - 
					collPar.add( new Cube() );  //�������� ����� 
						
				//Collections.	//� ������������� ����� �������� ������������� ����� ����������
		
					//�� �� ����� , �� ������ �� ��������� ����������
					for (Iterator iterator = collPar.iterator(); iterator
							.hasNext();) {
						/*Object*/ Point elem2 = (/*Object*/ Point) iterator.next();
						
				
						elem2.met1( count++ );
							 x = elem2.data;
							elem2.data = x;				//���� ����� �������������� �����  �� =
					    
							//obj = collPar.get(0);
							collPar.contains( elem2);
							collPar.clear();  //���� � ������ ���������, �� �� �������� � ���.

							collPar.set(0, new Cube());  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������
							collPar.add( new Cube() );  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������

			}
		}//metMask2
		
		
		//3. ����� extends � ����������
		<T> void   metMask3(  List < ? extends T> collPar   ) {  // � ��������� ����� : ����� �����-��������� �
			//������ �� ���������
			//�.�. ����� ���� extends, �� �������� ��������� ����� ����� �������  ������ �� =
			int count=0;
			for (T elem : collPar) {   //���� ���� ����������, �.�. �� ������� � ������ ������������� ����
														//��������������� ��� �������� ��������
			//��� ������ ��������� ������ ����������������� � ����������� ������������� ������ ���������
			//�� ��� ��� ������� - �������� ����� ������������ ����������
			   T obj= collPar.get(0); 
			   obj = elem;
				//�� ����� � ���� ������ c ������ <? extends T>: �������� ������� � ����� ����������� ������� �
				//3.1  �� ����� ������������ ������ ������ � - 
					//elem.met1( count++ );
				//3.2. �� �����  ������������ ���� ��������� ���������
					//int x = elem.data;
					//elem.data = x;				
					
				//3.3 ����� ������ ��������� , �� ���������� �������� ��������� (�� ��������� � �� �������� ������ ���������)
					//� ������ ��� ����������
					T obj2 =  collPar.get(0);  //���������� �� ��������, �� ������ � ����  ������ Cube �������� �� �����
					
					collPar.contains( elem);
					collPar.clear();  //���� � ������ ���������, �� �� �������� � ���.
			}//foreach
			
				//3.4	 �� ����� ����� ������ ���������, ���������� ��������(�� ������=������)  ���������
					//collPar.set(0, new T() );  //�������� ����� - 
			T obj = collPar.get(0); ;
					// collPar.add( obj );  //�������� ����� 
						
				//Collections.	//� ������������� ����� �������� ������������� ����� ����������
		
					//�� �� ����� , �� ������ �� ��������� ����������
					for (Iterator iterator = collPar.iterator(); iterator
							.hasNext();) {
						T elem2 =  (T) iterator.next();
					
				//������ � ������ � ���� ���� �� ����� ��������������
					//	elem2.met1( count++ );
					//		 x = elem2.data;
					//		elem2.data = x;				//���� ����� �������������� �����  �� =
					    
						//������ ��������
							obj = collPar.get(0);
							collPar.contains( elem2);
							collPar.clear();  //���� � ������ ���������, �� �� �������� � ���.

							//��������� ����������
							//collPar.set(0, new T());  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������
							//collPar.add( obj );  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������

			}
		}//metMask3
		
		
		//3. ����� extends � ����������
		<T> void   metMask3(  List < ? super T> collPar,  T  objPar ) {  // � ��������� ����� : ����� �����-��������  �
			//������ ���� � ������ �������� � ������ <? super �>, �� ������ ���� 2� ��������, ������� ��������
			//�������� ����, ������ ��������� � �������� � ������ <? super �>
			//��� ������ 2-�� ��������� ������������ �����-�� ����, ������ � �.�. ����� ���������� � ��������� � 
			//� ������ <? super �>
			
			//��������� ��������� � ������ <? super �> ����������, �� ������ � ����� �� ��� ������ ������.
						
			//������ �� ��������� - ����������
			//�.�. ����� ���� SUPER, �� ������ �� ��������� ����������, �� � �� �������
//for (T elem : collPar) {   //���� ���� ����������, �.�. �� ������� � ������ ������������� ����
														//��������������� ��� �������� ��������
			//�� ����� ������ �� ����� ��������� - �.�. ��������� �������� ����� ������ � ������ ���� Object
			   Object obj= collPar.get(0); //���� ����� Object ���������� ��������� ��������, 
			   												//�� �� ������ ��� ���� � ������ ���� � - ����� �� �������, ����� ���� ����� ����� - 
			   										//����� ������ ���������  <Object> � �� �������� ������ ������ <?  super T>
				//�� ����� � ���� ������ ���� ������ � ���� ���� � c ������ <? super T>: �������� ������� � ����� - ����������� ������� �
			   
				//4.1  �� ����� ������������ ������ ������ � - 
					//elem.met1( count++ );
				//4.2. �� �����  ������������ ���� ��������� ���������
					//int x = elem.data;
					//elem.data = x;			
					
				//4.3 ����� ������ ��������� , �� ���������� �������� ��������� (�� ��������� � �� �������� ������ ���������)
					//� ������ ��� ����������
					//T obj2 =  collPar.get(0);  //����������  ��������, 
			 //�� ����� ������ �� ����� ��������� - �.�. ��������� �������� ����� ������ � ������ ���� Object
			   Object obj2= collPar.get(0); //���� ����� Object ���������� ��������� ��������, 
			   												//�� �� ������ ��� ���� � ������ ���� � - ����� �� �������, ����� ���� ����� ����� - 
			   										//����� ������ ���������  <Object> � �� �������� ������ ������ <?  super T>
					
					collPar.contains( obj2);  //������ �����, �� ������ �� �����, �.�. �������� ������ ��� Object,
												//� ��������������� equals() ��� �������� ����� ���� � ����������
					
					collPar.clear();  

			
				//4.4	 �� ����� ����� ������ ���������, ���������� ��������(�� ������=������)  ���������
					collPar.set(0, objPar);  //�������� ����� - �.�. ����� super
				   collPar.add( objPar );  //�������� ����� -- �.�. ����� super
						
				 //4.5. ����� ��� ����� ������ � ����������� � ����������
					//� ������ � Object � ����������
					Collections.fill(collPar, objPar);
					collPar.indexOf( objPar );  
				   
				//Collections.	//� ������������� ����� �������� ������������� ����� ����������
		
					//�� �� ����� , �� ������ �� ��������� ����������
				   //����  �� ����� ������ �.�.	 ��������� ����������
					/*for (Iterator iterator = collPar.iterator(); iterator
							.hasNext();) {
						T elem2 =  (T) iterator.next();
					}
					*/
					
				//������ � ������ � ���� ���� �� ����� ��������������
					//	elem2.met1( count++ );
					//		 x = elem2.data;
					//		elem2.data = x;				//���� ����� �������������� �����  �� =
					    
						//������ ��������
							obj = collPar.get(0);  //������ ������ ���� Object, � ������� ����� � ������� ������ �
							collPar.contains( obj);  //������ �� �������, �.�. ������ ������� ������ Object
							collPar.clear();  //���� � ������ ���������, �� �� �������� � ���.

							//��������� ��������
							collPar.set(0, objPar);  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������
							collPar.add( objPar );  //���������� ����� - ������ ����������� - �.�. ������ ������ ���������� ���������

							//4.5. ����� ��� ����� ������ � ����������� � ����������
							//� ������ � Object � ����������
							Collections.fill(collPar, objPar);
							collPar.indexOf( objPar );  
							
		}//metMask4
		
	//5.  ������: ������� � �������������� ����� ����������  ����� -������, ������� � ������� 
		//������� ������������ �������������� ������� ����� ����� �� �������� ������-������
		//� ��������������� ���������� - ������
		//� ���������� �������� ��������� � �����-�� ���������-������ ���� �� ������ ����
		//���������: 
		//1. ��������� � ������� ����
		//2. �������
		//3. ����������
		//�������: ��������� ���������, � ���������������� ���������
		
	//����� � ������� ����� ������ �����  
		//1. ���� ��������� , � �������� ����� ������ 999
		//2. ����� ��� ��������� ����� - � ������� ������� �� ��������� < 0 
		
<T>  Vector<?  super   T>  filtr(Vector<?  extends   T> sourcePar,  T etalonKey, Comparator<T>   COMP ){
	Vector<?  super   T> resVect = new Vector();   //��������� ����������, � ��� ����� - ������� ����� �����
	
	//������ � ����� �� �������� ���������
	for (T elem : sourcePar) {
		//�������� ������ ����  � ��������� ����() �� ����������
		//1. ��������� � ������ �� �������� - �� �� ������� ������� �� ����� ���������� 
		//�������� ������������ � ����������� Comparable
		//� �� ������� ��������� ������������ ���������� ������� �����������
					//����� COMP.compare()  ����� 2 ��������� (������������) � ���������� <0, >0, ==0
				//� ���������� � �������� ��()
		if (  COMP.compare(  etalonKey, elem  )  > 0     ) {  //������ ����-������ ������ �������� ��������
																		//�������� ������� ������� � ��������� ���������
			//����� ��� �������� �������� ����������� ���� ������ ���� ��(), ���� �� ���������� ������� ��������
			resVect.add(elem);   
		}//if
		
	}//foreach
	
	 return   resVect;
}//met filtr


	public static void main(String[] args) {
		// TODO Auto-generated method stub
				//��� ������������� ������� � ����������� �������� ������� �����, �.�. ������-������ ����������
				ParType_Masks2 obj = new ParType_Masks2();
				
				//������� ��� ���������.
				Student[] arrSt =  
					{new Student("������",	"����", 1000),
					new Student("������",	"����", 0),
					new Student("�������",	"����", 0 ),
					new Student("������",	"����", 1000) };
					//���������� ������������� ��� ������ � ����������� ���������
					//������ ��� ���� �� �����������
		

BCard[ ] arrBCard = {new BCard(1, 1000),new BCard(2, -2000),new BCard(3, 3000),new BCard(3, -3000)};


//������� ��������� �� ��������, �� ���� ��� �������
					Vector<Student> vecSt = new Vector<>();
					Vector<BCard>   vecBC= new Vector<>();
				for (int i = 0; i < 4; i++) {
					//obj.makeList(vecSt, arrBCard[i]);  //������. ����� ������
					obj.makeList(vecSt, arrSt[i]);
					obj.makeList(vecBC, arrBCard[i]);
				}
				System.out.println(vecBC);
				System.out.println(vecSt);
	//******************************* ����� �������  � �����() ************************************  			

				
				
				//��� ������ ������ �������, ���� ��� ��������� ������� ����������
//������� ���������� ��� ������ ��������� �� ����� .
Comparator<Student>	COMP_ST_SUM = new Comparator<Student>() {

	@Override
	public int compare(Student o1, Student o2) {
		// TODO Auto-generated method stub
				//��������� ����� ���� ���������
/*		if (o1.summ > o2.summ) 
			return  (int)(o1.summ - o2.summ);  //������� ������� �� ������ >0 , � ������� , ��������� ������
		if (o1.summ < o2.summ) 
			return  (int)(o1.summ - o2.summ);  //������� ������� �� ������ >0 , � ������� , ��������� ������
	*/
				//�� �������, ������ ����� �������
/*		if (o1.summ != o2.summ) 
			return  (int)(o1.summ - o2.summ); 
		return 0;
*/
					//�� �� ����� , �� � ���� ������ 
		return  (o1.summ != o2.summ)  ?    (int)(o2.summ - o1.summ)   :   0 ;
	}
};	  		
				
//�������� ������ ������� �����, � ������ ������������� � ���������
//1. ���� ��������� , � �������� ����� ������ 999
Vector<? super Student>  resCollSt = obj.filtr(vecSt, new Student("", "", 999), COMP_ST_SUM);
			System.out.println("�������� � ������ ������� 999:" + resCollSt);	
				
			
			//������: �������� ��������� � �������� ������ 2-�
			//1. �������� ������ ����������, ������� ����� ���������� ������ ���������
			Comparator<Student>	COMP_ST_NUMBER = new Comparator<Student>() {

				@Override
				public int compare(Student o1, Student o2) {
					// TODO Auto-generated method stub
							//��������� ������  ���� ���������

					return  (o1.number != o2.number)  ?    (int)(o2.number - o1.number)   :   0 ;
				}
			};	  					
			//2. ������� �����
			Student  etalonKey =  new Student("", "", 0); etalonKey.number=2 ; 
		 resCollSt = obj.filtr(vecSt,   etalonKey    , COMP_ST_NUMBER);
			System.out.println("�������� � ������� ������� 2:" + resCollSt);	

	//������: //3. ����� ��� ��������� ����� - � ������� ������� �� ��������� < 0 
	//1. 		 �������� ������ ����������, ������� ����� ���������� ����� ��������
			Comparator<BCard>	COMP_BC_SUM = new Comparator<BCard>() {

				@Override
				public int compare(BCard o1, BCard o2) {
					// TODO Auto-generated method stub
							//��������� ������  ���� ���������

					return (o1.sum != o2.sum)  ?    (int)(o1.sum - o2.sum)   :   0 ;
				}
			};	  								
			
			//2. ������� �����
				Vector<? super BCard> resCollBC = obj.filtr(vecBC,   new BCard(-1, 0)    , COMP_BC_SUM);
				System.out.println("��������� ����� � ��������� ������ 0: " + resCollBC);	
			
				
				
				//------------------------------------------------------------------------------------------------
				//�������� ������� �����-�������
				//������� ������ ����-������ Point
				//ParType_Masks2.Point objPoint = ParType_Masks2.new Point();  //�.�. ����� .new �� ������� (�� ������ ������)
				//ParType_Masks2.Point objPoint = new ParType_Masks2.Point();
				ParType_Masks2.Point objPoint = obj.new Point();
				//objPoint.
				
				Point objPoint2 = new  ParType_Masks2().new Point(); 
				
	}//main()

}//public class


//--------------------------------------------------------------------------------------------------
//����� ���  ���������� Comparable - ����� ����� ��������� �����������
class Student   {
String lastName;
String name;
int number;
long summ;

static int lastNum=0;
//
public Student(String lastNamePar, String namePar, long summPar) {
//super();
/*this.*/lastName = lastNamePar;
/*this.*/name = namePar;
         summ = summPar;
         number = lastNum++;
}
public Student() {
	// TODO Auto-generated constructor stub
}

//��� ������ ��������� �� �����
public String toString (){
			return number + " "+lastName + " "+name + "  " + summ;
}


}//class Student


//-------------------------------------------------------------------------
class BCard {
	long num16;
	long sum;
	public BCard(long num16, long sum) {
		super();
		this.num16 = num16;
		this.sum = sum;
	}
	
	public BCard() {
		
	}

	public String toString (){
		return num16 + " "+ sum ;
}
	

}//BCard
	


