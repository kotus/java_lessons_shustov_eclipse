package product;

import java.util.LinkedList;

public class Product<V, T>{

	T type;
	V val;
	LinkedList<V> coll;
	
	public T getType() {
		return type;
	}

	public void setType(T type) {
		this.type = type;
	}

	public V getVal() {
		return val;
	}

	public void setVal(V val) {
		this.val = val;
	}
	
	@Override
	public String toString() {
		return "Product [type=" + type + ", val=" + val.toString() + "]";
	}
	
	public Product() {
		super();
	}
		
	public Product(T type, V val) {
		super();
		this.type = type;
		this.val = val;
	}

	
//	public Product(String type, ProdFlash prodFlash) {
//		
//		super();
//		this.type = (T) type;
//		this.val = (V) prodFlash;
//
//	}

	public static void main(String[] args) {
		
		Product<String, String> product = new Product<String, String>("������", "������ GoodRam 16-2");
		System.out.println(product);
		
		ProdFlash flash = new ProdFlash();
		
//		Product<String, ProdFlash> product2 = new Product<String, ProdFlash>("������", flash);
//		System.out.println(product2);
		
	}
	
	V metTest(V par){
		
		return par;
		 
	}
	
	<V2> V2 metTest2(V2 par){
		
		return par;
		 
	}
	
	int metTest(LinkedList<V> par){
		
		return par.size();
		 
	}
	
	LinkedList<V> metTest2(LinkedList<V> par){
		
		return par;
		 
	}

}

class ProdFlash {
	
	String model; //������ ������
	String size; //������ ������
	int price; //����
	int rest; //������� �� ������
	
	
	
} 








