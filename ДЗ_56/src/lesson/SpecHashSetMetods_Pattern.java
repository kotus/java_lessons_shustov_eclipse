package lesson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArraySet;


public class SpecHashSetMetods_Pattern {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			//�������� ������� �������� �������� Student
		Student[] massSt =  
				{new Student("������",	"����"),
					new Student("������",	"����"),
					new Student("�������",	"����"),
					new Student("������",	"����")};
			//������� ��������� - �� �����������, ������� �� �����������
		Set<Student> objHashSet = new HashSet<>(Arrays.asList(massSt));
		System.out.println("�������. ������ � ���������"+objHashSet);
		
		List<Student> objList = new ArrayList<>(objHashSet);
		System.out.println("�������. ������ � ���������"+objList);

		//����������� ������� �������� - �.�. ������ contains() ---------------------------------------------
		//������:  ����� ������� �����
		//������������ �� ���� �������� � ��������� ���� ������-������
		//������� ����� ����� �������� ������ ��  �������  ������ ��� �� ������ ��������� �����
		//������� ������� � �������� ���������� ������ ����� contains(), 
		//�����  ������� ���������� �� ���������� ������� ����� ���������, �.�. ���������� �������� �� �������.
		//1. ������� ������� ��� ������
		Student etalon = new Student("������", "����", 0, 0);  //���� �� ��������� ��� ���� ��������, �� 
											//������� �� ����� ������ ��� �������� ������� ������() � ������()
		System.out.println( "������� �� ����� Contains() ������� ����� � ������������: "  + 
		                                                                                           objHashSet.contains( etalon) );  //�� �������� ��������(), ���� �� ��������� ������() � ������()
		
		System.out.println( "������� �� ����� Contains() ������� ����� � �������������: "  + 
				objList.contains( etalon) );  //� ������ ������ ��������() �������� �� ��������� - �.�. �������������� ������() � ������()
		
		//���� � �������� �� ��������� ��� ����, �� ������ ���� ��������� �� ��������� - 0-�� � null-��,
		//� ����� ��������() �������� ������ �� ������ . � � ��������� � �������� ����� ��� ������ ������ �������� � �������
		//� �������� ���������  ����� ������ ���� ���� (����� ���������� ����)
		//�� ����� �������� �� ����� �������� ��� �������� ������� ������() � ������()
		
		//����� ������ � ������������� ����:
		//1. ��� ��������� � �������� ��� ����.
		//2. ��� (��� ����������) �������������� ������ ������() � ������() ������ � ������
		//  ���������������� ��� ������ (�� ����� ��������� ������� �������� �� ���������, � ���������
		//  ������������ ����� ������� �����������, ���� ��� ������ ������ �����)
		//  � ������� �� ������������, ����� ����� �������� � ����������� ��������������  �� ���� ���-����������
		// �� Student - �.�. ������������ ����� ������� �����, � ����� ������()-������() - ���� ��������
		//3. � ������ � ����� ���������� ���������� ����, �� ����� �� �������� ������������ ������� 
		// �������� ������ ���������� 
		//4. ������������ ��� ������ ������� - ��� ����� ������� ����� ������������, �� ������� ��������� ��������
		//5. ��������� ������ ������() � ������()  �� ��������������� ��������� �����. -
		//��. ����� ������ ������() � ������() , (��  ��������� ��������� �� ����� ������ ����  - 
		//�.�..  ����, ������� ����� ���� ������ � ������� �� ������ ��������� ������� � ������������ �������())
		
		
		//����������� ������� ������ � ������()
		//1. ��� ������������ ������� contains()   � ���-����������
		//2. ��� ������ �� �������� ����� �������� ����� ��������� ����������� ������ ������() � ������()  
		//     �� ��������������� ��������� ����� ��� ������ � . 
		// �������� ������ �� ���������  �������� , � �� ����� ������������ ������ (��� ������ ����� ���������) 

	}//main

} //public class

//--------------------------------------------------------------------------------------------------
		//����� � ����������� Comparable �������� ������  �����() 
//��� ������� ����� ������ �������, ����� ����� ����������� � ������ ������ ������ ��������� 
//��������� ������� � ����������� �������
		class Student  implements Comparable<Student> {//�������� ��� �������� �������������� ���������
													//����� ����� ����� ������� ������� � ������� � � Student 
			String lastName;
			String name;
			int ID;
			long summ;
			
			static int count = 0;
					//
			public Student(String lastNamePar, String namePar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = count;
				summ = ID * 1000;
			}
			
			public Student(String lastNamePar, String namePar, int  ID_Par, long sumPar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = ID_Par;
				summ = sumPar;
			}
			
			public Student() {
						// TODO Auto-generated constructor stub
					}
			//@Override	//����� ��������� comparable
			public int compareTo(Student oPar) {//�������� ����
				// TODO Auto-generated method stub
				Student tmp = oPar;
					
				int res = lastName.compareTo(oPar.lastName);
				return (res == 0  ? -name.compareTo(oPar.name): -res);
			}//compareTo
			
					//��� ������ ��������� �� �����
			public String toString (){
				return ID + " "+lastName + " "+name + " " + summ;
			}
			
			//----------------------------------------------------------------------
			//��� ����, ����� ����� � ������������� ���������� �� �� �������-������, � �� ��������� ����� - 
			//����� �������������� ������ ������() � ������()
			
			//������ �������� ������� ������() � ������() �� �����  �������������� ��������
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result
						+ ((lastName == null) ? 0 : lastName.hashCode());
				result = prime * result
						+ ((name == null) ? 0 : name.hashCode());
				
			//����, ������� ����� ���� �� ��������� � ������� �� ������ ��������� ������� 
				//� ������������ �������
			//�� ���� ������� ������ ������������ �� ������ ��������������� ���� ����� ��������
	//				result = prime * result + ID;
	//				result = prime * result + (int) (summ ^ (summ >>> 32));
				return result;
			}
			
			@Override
			public boolean equals(Object obj) {  
				//���� �������� �� ����, ��� ������-�������� ������ contains()  - �.�. ������
				//���� ��������  ������ equals()
	
				Student other = (Student) obj;
				if (lastName == null) {   //���� � �������� ������ ������� Null - �� �� ���������� ������� 
											         //� ���������� ����� ������
				} else if (!lastName.equals(other.lastName))
					return false;
				if (name == null) { //���� � �������� ������ ����� Null - �� �� ���������� ����� 
												//� ���������� ����� ������
				} else if (!name.equals(other.name))
					return false;
				if ( ID == 0 ) {		//���� � �������� ������ ID ���� - �� �� ���������� ����� 
											//� ���������� ����� ������
					
				}
				else if (ID != other.ID)  
					return false;
				if (summ == 0) {		//���� � �������� ������ summ ���� - �� �� ���������� ����� 
												//� ���������� ����� ������
					
				}
				else if (summ != other.summ)
					return false;
				return true;		//���� ��� �������� - �� ������� ������
			}
			

			
		}//class
		
