package pack1;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import arrays.Mass;

public class FileIO {

	public static void main(String[] args) throws IOException {

		dz_26_task_1_4("src/input.txt", "src/massFiltr20_100.txt", 20, 100);

	}

	public static String[] readLinesFromFile(String path, int beginRow,
			int endRow) throws IOException {

		Scanner scanner = new Scanner(new File(path));
		String[] resultArray = new String[endRow - beginRow];

		int indOfArray = 0;
		for (int i = 1; scanner.hasNextLine(); i++) {

			if (i < beginRow) {
				scanner.nextLine();
				continue;
			} else if (i >= endRow) {
				break;
			}

			resultArray[indOfArray] = scanner.nextLine();
			indOfArray++;

		}

		return resultArray;

	}

	public static Integer[][] readMatrixOfNumbersFromDisk(String path,
			String delimetr) throws IOException {

		Scanner scanner = new Scanner(new File(path));

		int linesCounter = 0;
		int columnsCounter = 0;

		for (; scanner.hasNextLine();) {

			String tempStr = scanner.nextLine();
			String[] tempArray = tempStr.trim().split(delimetr);
			if (tempArray.length > columnsCounter) {
				columnsCounter = tempArray.length;
			}

			linesCounter++;
		}

		scanner = new Scanner(new File(path));

		Integer[][] resultMatrix = new Integer[linesCounter][columnsCounter];
		for (int i = 0; scanner.hasNextLine(); i++) {
			String tempStr = scanner.nextLine();
			String[] tempArray = tempStr.split(delimetr);
			for (int j = 0; j < tempArray.length; j++) {
				resultMatrix[i][j] = Integer.valueOf(tempArray[j].trim());
			}
		}

		for (int i = 0; i < resultMatrix.length; i++) {
			for (int j = 0; j < resultMatrix[i].length; j++) {
				if (resultMatrix[i][j] == null) {
					resultMatrix[i][j] = 0;
				}
			}
		}

		return resultMatrix;

	}

	public static void dz_26_task_1_4(String pathInput, String pathResult,
			int lowerValue, int upperValue) throws IOException {

		Integer[][] matrix = readMatrixOfNumbersFromDisk(pathInput, ",");
		int[] array = Mass.dz_26_task_1_4(matrix, lowerValue, upperValue);
		Mass.writeArrayToDisk(pathResult, array);

	}

}
