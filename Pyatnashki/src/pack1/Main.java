package pack1;

import java.util.Scanner;

public class Main {
	
	//����� ������� ���������.
	public static void main(String[] args) {
		
		//���� ����������� ������� ����.
		int size = -1;
		while (size == (-1)) {
			System.out.print("������� ������ �������: ");
			size = keyboardInt();
		}
		
		//��������� ��� ������������ � ����������� � ������ �� ����.
		System.out.println("��� ������, � ���������� ������� [-1][-1]");
						
		//������� ������ Pyatnaski. 
		Pyatnaski obj = new Pyatnaski(size, size);
		System.out.println(obj);
		
		//������ ����������� ���� � ������� ����� ��������� �������� �������������
		//������ ����� � ������� ��� ����������� �������� NULL �� �������� ����.
		//���� ���� �� ����� ����������� ����������, �.�. �� ���������� �������� 
		//�������� �������� ���� �� �������� ������� ������������� �����.
		while (true) {
			
			//���� ������ ������ � �������.
			System.out.print("������� � ������ ��������: ");
			int nextRow = keyboardInt();
			System.out.print("������� � ������� ��������: ");
			int nextCol = keyboardInt();
						
			//�������� �� ���� ������������� ������ ��� ���������� ����.
			//������� ������ ������ � ������� �� ��������� -1.
			if (nextRow == (-1) & nextCol == (-1)){
				System.out.println("GAME OVER");
				break;
			} 
			//�������� �� ����������� ����� ������������� ��������� �������� ������.
			else if ((nextRow > obj.getMAX_ROWS()) || nextRow < 1){
				System.out.println("��� ����� ������!");
				continue;
			} else if ((nextCol > obj.getMAX_COLLS()) || nextCol < 1){
				System.out.println("��� ����� �������!");
				continue;
			}
			
			//��� �������� ������������ ������ �������� �������� ����� � �������.
			//�������� �� ����� ������� ������ ������, ������ �������. ��� �������.
			//�� ���, ����� �� ������ ������ �������� � �� �������� � ��������, ����� 
			//����� ������� ��������� ���� ��������. 
			nextRow--;
			nextCol--;
			
			//����� ������, ������� �������� ������� �������� ������������� ������
			//� ������� ���������� NULL ��������
			int res = obj.rotate(nextRow, nextCol);
			
			if (res == -1) System.out.println("����������� ���!"); 
			System.out.println(obj);
			
		}
	
	}
	
	//�����, ���������� � ���������� ����� ���� int
	private static int keyboardInt(){
		
		Scanner sc = new Scanner(System.in);
		try {
			int res = sc.nextInt();
			return res;
		} catch (Exception e) {
			System.out.println("���������� �����!");
		}
		return -1;
						
	}

}
