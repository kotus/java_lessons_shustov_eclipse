import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

import org.omg.CORBA.COMM_FAILURE;


public class GUI_EventProcess extends JFrame implements ActionListener, MouseListener{

	JButton butt1 = new JButton("OK");
	JTextField jFld1 = new JTextField("���� ������� ���");
	
	String[] massStr = {"������", "������", "�������"};
	JComboBox cBox = new JComboBox(massStr);
	
	public GUI_EventProcess(String title) throws HeadlessException {
		
		super(title);
		
		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setLayout(new FlowLayout());
		
		add(butt1);
		butt1.addActionListener(this);
		
		add(cBox);
		cBox.addActionListener(this);
		
		add(jFld1);
		jFld1.addActionListener(this);
		
		//��������� ������� ����� Mouse Ivent
		
		butt1.addMouseListener( this );
		
		
		
		
		
		setVisible(true);
		
	}

	public static void main(String[] args) {
		
		GUI_EventProcess eventProcess = new GUI_EventProcess("����� 1");
	
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		
		System.out.println(event.paramString());
		if (event.getSource() == butt1) {
			System.out.println("Button");
		} else if(event.getSource() == cBox){
			System.out.println("ComboBox");
			String selItem = (String) cBox.getSelectedItem();
			System.out.println("������ "+selItem);
		} else if(event.getSource() == jFld1){
			System.out.println("TextField");
			String text = jFld1.getText();
			System.out.println("� �������� ����: "+jFld1.getText());
		}
		
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("���� �� ������ But1");
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
