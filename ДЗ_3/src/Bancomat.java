
public class Bancomat {

	private String inventNumber;
	private String address;
	private Float summ;
	private Integer errorCode;
	
	public String getInventNumber() {
		return inventNumber;
	}
	public void setInventNumber(String inventNumber) {
		this.inventNumber = inventNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Float getSumm() {
		return summ;
	}
	public void setSumm(Float summ) {
		this.summ = summ;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public Bancomat(String inventNumber, String address, Float summ,
			Integer errorCode) {
		this.inventNumber = inventNumber;
		this.address = address;
		this.summ = summ;
		this.errorCode = errorCode;
	}
	
	public Bancomat(String inventNumber, String address, Float summ) {
		this.inventNumber = inventNumber;
		this.address = address;
		this.summ = summ;
	}
	
	@Override
	public String toString() {
		return "����� Bancomat [inventNumber=" + inventNumber + ", address="
				+ address + ", summ=" + summ + ", errorCode=" + errorCode + "]";
	}
	
}
