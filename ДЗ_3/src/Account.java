import java.util.Formatter;


public class Account {
	
	private Long accountNumber;
	private Float accountSumm;
	
	public Long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Float getAccountSumm() {
		return accountSumm;
	}
	public void setAccountSumm(Float accountSumm) {
		this.accountSumm = accountSumm;
	}
	
	public Account(Long accountNumber, Float accountSumm) {
		this.accountNumber = accountNumber;
		this.accountSumm = accountSumm;
	}
	
	@Override
	public String toString() {
		Formatter fmt = new Formatter();
		fmt.format("����� Account [accountNumber=%d; accountSumm=%.2f]", accountNumber, accountSumm);		
		return fmt.toString();
	}

}
