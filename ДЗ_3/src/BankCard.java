
public class BankCard {
	
	private Long cardNumber;
	private String expDate;
	private Short cvCode;
	private Float summ;
	private String surname;
	private String name;
	private String patronymic;
	
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public Short getCvCode() {
		return cvCode;
	}
	public void setCvCode(Short cvCode) {
		this.cvCode = cvCode;
	}
	public Float getSumm() {
		return summ;
	}
	public void setSumm(Float summ) {
		this.summ = summ;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	
	public BankCard(Long cardNumber, String expDate, Short cvCode, Float summ,
			String surname, String name, String patronymic) {
		this.cardNumber = cardNumber;
		this.expDate = expDate;
		this.cvCode = cvCode;
		this.summ = summ;
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
	}
	
	public BankCard(Long cardNumber, Short cvCode, String surname, String name,
			String patronymic) {
		this.cardNumber = cardNumber;
		this.cvCode = cvCode;
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.summ = 0f;
		this.expDate = "";
		
	}
	
	@Override
	public String toString() {
		return "����� BankCard [cardNumber=" + cardNumber + ", expDate=" + expDate
				+ ", cvCode=" + cvCode + ", summ=" + summ + ", surname="
				+ surname + ", name=" + name + ", patronymic=" + patronymic
				+ "]";
	}
	
}
