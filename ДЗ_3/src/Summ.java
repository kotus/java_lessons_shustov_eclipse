
import java.util.Formatter;

public class Summ {

	private Float summ;
	private Short valuta;
		
	public Float getSumm() {
		return summ;
	}
	public void setSumm(Float summ) {
		this.summ = summ;
	}
	public Short getValuta() {
		return valuta;
	}
	public void setValuta(Short valuta) {
		this.valuta = valuta;
	}
	
	public Summ(Float summ, Short valuta) {
		this.summ = summ;
		this.valuta = valuta;
	}
		
	@Override
	public String toString() {
		Formatter fmt = new Formatter();
		fmt.format("����� Summ [summ=%.2f; valuta=%d]", summ, valuta);
		return fmt.toString();
	}
	
}
