package gui;

import java.io.FileNotFoundException;
import java.io.IOException;

//import pack1.Parser;


import arrays.Mass;

public class Client extends Person{
	
	private BankCard bankCard;
	private Account account; //= new Account();
	
	public BankCard getBankCard() {
		return bankCard;
	}
	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Client(String surname, String name, String middleName) {
		super(surname, name, middleName);
		
		//this.account = new Account(1L, 0L, "USD", this);
		this.account = new Account(1L, "UAH", this);
		
	}
		
	public Client(String surname, String name, String middleName, BankCard bankCard) {
		super(surname, name, middleName);
		this.bankCard = bankCard;
	}
	
	public Client(String surname, String name, String middleName,
			BankCard bankCard, Account account) {
		super(surname, name, middleName);
		this.bankCard = bankCard;
		this.account = account;
	}
	
	//������� � ������� �����, �� ����� ������� �������
	public String payBCard(Summ summa, Client client){
		
		Long summaThisClient = this.bankCard.getSumm().getSumm();		
		Long summaComingClient = client.bankCard.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();
		
		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0){
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0){
			return "������������ ������� ��� ������";
		}
		
		this.bankCard.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.bankCard.getSumm().setSumm(summaComingClient + summaOfPayment);
				
		return "OK";
		
	}
	
	public static void main(String[] args) {
		
		/*
		String [][] matrixOfClients = Mass.AddArr(Mass.AddArr(Mass.arr1, Mass.arr2),
													Mass.arr3);
		Client [] arrOfClients =  Client.makeMassClient(matrixOfClients);
		
		Client.testMassClient(arrOfClients);
		
		System.out.println("����� ����� ���������� �������:");
		System.out.println(arrOfClients[(arrOfClients.length-1)].getAccount().getAccountNumber());
		
		System.out.println("�������� ���� lastAccNum:");
		System.out.println(Account.lastAccNum);
		*/
		
		/*
		try {
			Client[] massCl_DZ19 = Client.makeMassClient("src/fio_clients.in");
			Mass.showArrayOfObjects(massCl_DZ19);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/
		
		/*
		try {
			
			Client[] arrOfClients = Parser.makeMassClient("src/clients_all.in");
			
			for (int i = 0; i < arrOfClients.length; i++) {
				System.out.println(arrOfClients[i]);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/ 
		
		
	}
		
	/**
	 * ����������� �����, ��������� ������ �������� Client[] �������� �� ����������� � ����
	 * ���������� �������.
	 * 
	 * @param fio - ��� String[][], ��������� ������ ��� �������: { { "������", "����", "��������" } , ...} 
	 * 
	 * @return arrClients - ��� Client [], ���������� ������ �������� ���� Client. 
	 */
	public static Client[] makeMassClient(String[][] fio) {
		
		Client [] arrClients = new Client[fio.length];
		for (int i = 0; i < arrClients.length; i++) {
			arrClients[i] = new Client(fio[i][0], fio[i][1], fio[i][2]);
		}
		
		return arrClients;
		
	}
	
	/**
	 * ����������� �����, ��������� ������ �������� Client[] �������� �� ����������� � ���� ����
	 * � ����� � ��� ��������.
	 * @param fileNameFIO ���� String, ��� ����� �� �����.
	 * @return <b>arrClients</b> ���� Client [] ���� ������ ������ ��� NULL ���� ������ �� �������.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Client[] makeMassClient(String fileNameFIO) throws FileNotFoundException, IOException {
		
		String[][] arrayFromFile = Client.makeFIOstrFromFile(fileNameFIO);
		
		if (arrayFromFile.length != 0) {
			Client [] arrClients = Client.makeMassClient(arrayFromFile);
			return arrClients;
		} else {
			return null;
		}
		
	}
	
	
	/**
	 * ����� �������� ���� �� ������ "src/fio_clients.in" � ��������� �� ���� ������� ���.<br>
	 * ��������� ����� ������ ���� ���������:<br>
	 * ������� ���� ��������<br>
	 * ������� ���� ��������<br>
	 * ����� ����� ��������<br>
	 * ...<br>
	 * ������������ �� ��������� ��������� ������.
	 * 
	 * @param fileNameFIO ���� String, ��� ����� �� �����.
	 * @return <b>resultMatrix<b> ���� String[][], ��������� ������ ��� �������: { { "������", "����", "��������" } , ...} 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String[][] makeFIOstrFromFile(String fileNameFIO) throws FileNotFoundException, IOException {
		
		String[] arrayFromFile = Mass.readFileFromDisk(fileNameFIO);
		if(arrayFromFile == null) return null;
		
		String[][] resultMatrix = new String[arrayFromFile.length][3];
		
		for (int i = 0; i < arrayFromFile.length; i++) {
			
			String[] decompositedString = Mass.decomposeStringIntoArrayOfSubstrings(arrayFromFile[i], " ");
						
			for (int j = 0; j < decompositedString.length; j++) {
				
				resultMatrix[i][j] = decompositedString[j];
				
			}
			
		}
		
		return resultMatrix;
		
	}
	
	/**
	 * ����� ����������� ������ ������ makeMassClient, �� �������� ������� �������� ���� Client
	 * 
	 * @param arrOfClients - ��� Client [], ���������� ������ �������� ���� Client.  
	 */
	public static void testMassClient(Client [] arrOfClients) {

		
		for (int i = 0; i < arrOfClients.length; i++) {
			System.out.println(arrOfClients[i]);
		}		
		
	}
	
	@Override
	public String toString() {
		return super.getSurename() + " " + super.getName() + " " + super.getMiddleName();
	}
			
}
