package parser;

import gui.Staff;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;




//������  � �������� ������������ ���� � ��������

//�������� �������� ��������
//1. ������ �����������  - ����.�����,   
//������� ����� ������ ����� �������� � ��������� �������- ����� equals() �  compareTo()

//2. ����� ����������� - ����. �������� �����, 
//�� � ������� �������� - ����� ������� - ���������� �����������
//�������� ������ ���������  �������� ����� -
//����  �������� � ��������� �������- ����� equals()

//3. ����� ������ ����� (�����������) - ����� nextToken()   
//��� ��� ������������� ������ �����() - ������ ������� � ��������� ������ � �����

//������ ������ ������ �� 2-� ���������: �����-��������, � ������-�������� 

public class ParserStaff {

	public static Staff[] staffs;
	
	static void parseManagerSimple ( String  fileName ) throws IOException {
		
		BufferedReader strBuf = new BufferedReader(new FileReader(fileName));
		String str;
		int count = 0;
		while ((str = strBuf.readLine())!=null) {
			if (str.trim().equals("")) continue;
			count++;
		}
		
		ParserStaff.staffs = new Staff[count];
				
		strBuf = new BufferedReader( new FileReader(  fileName  ));
		int i=-1 ;
		for (  ;  (str = strBuf.readLine() ) != null      ; ) {
	  
			//���� ������ ������ - �� �� ������������ ��
			if ( str.trim().equals("")   )
						continue;
			
			//������� ������ �������� ����������
			i++;
			staffs[ i ]  = new Staff();
						
			  //�������� �������� - �������� ���������� ������ ���-��, ������ //  --  # 
			 if ( str.indexOf("//")  != -1)  {
			  	str = str.substring(0,  str.indexOf("//")  )  ;  //indexOf() ����������  ��� -1, ��� ������ ��������� ������-��������� � ������-���������
			 }
			  
			  System.out.println( str );  //��� �������
			  
			  //������� ������ �����������
			  StringTokenizer tokzer = new StringTokenizer(str, " :\t");
			  
			  //� ����� ������ �� ���� ������ ������, ���� �� ����� ������ �������� �����   - nextToken
			  //������ �� ������, ������ �����
			  String  word;
			  while (  tokzer.countTokens()  > 0  ) {
				
				 word =  tokzer.nextToken();  //������ ��������� �����
				 System.out.println( "������� �����: " + word);
				 
				if (word.indexOf(":") != -1) {
					readProblemString(word, "���������", staffs[i]);			
					continue;
				}
				 
				 //�������� ������� ����� �� ����� ��������� �������
				if (word.equalsIgnoreCase("�����")){
					
					readerDepartOrPosition("�����", tokzer, staffs[i]);
				
				}
				else if (word.equalsIgnoreCase("���������")) {
					
					readerDepartOrPosition("���������", tokzer, staffs[i]);					
					
				}
				else if (word.equalsIgnoreCase("���")) { 
										
					readFIOStaff(tokzer, staffs[i]);
				}
				 
			}
			  
			  
		}
		
	} //parseManagerSimple

	public static Staff[] makeMassStaffs(String fileName) throws IOException {
				
		parseManagerSimple(fileName);
		
		return staffs;
		
	}
	
	static void readFIOStaff(StringTokenizer   tokzerPar,  Staff  staffPar)  {
		 System.out.println("----�������� ������ FIO  ������----");
		 
		 		//��������� ���
		String surename = tokzerPar.nextToken();
		if(surename.startsWith("�")){
			surename = surename.substring(1);
		}
		staffPar.setSurename(surename);
		
		String name = tokzerPar.nextToken();
		if(name.endsWith("�")){
			name = name.substring(0, name.length()-1);
		}
		staffPar.setName(name);
		
		 	//���� � ����� ���� ������� - �� �� ������ ��������,- ��� ����� ������
		if (  staffPar.getName().indexOf("�")  >= 0   )   {// \" ������� ������� �������� ����� ��������� �����
			                                                               //���� � ����� ���� �������
			staffPar.setMiddleName("");  //����� �������� ������	
		}
		else {  //�������� ������ �� ������
		
			String middleName = tokzerPar.nextToken();
			if(middleName.endsWith("�")){
				middleName = middleName.substring(0, middleName.length()-1);
			}
			staffPar.setMiddleName(middleName);
			
		}	
	
	}

	static void readerDepartOrPosition(String mode, StringTokenizer tokzerPar, Staff  staffPar){
		
		String word = tokzerPar.nextToken();
				
		String tempStr = "";
		
		if (word.startsWith("�") && word.endsWith("�")) {

			tempStr = word.substring(1, word.length()-1);

		} else if (word.startsWith("�")) {

			tempStr = (word +" "+ tokzerPar.nextToken("�")).substring(1);
			
		} else {
			
			tempStr = word;
		
		}
		
		
		if(mode == "���������"){
		
			staffPar.setPost(tempStr);
			
		} else {
			
			staffPar.setDepartment(tempStr);
			
		}
		
		System.out.println("");
		
		
	}
	
	static String readDepart (StringTokenizer tokzerPar     )  {
		//��������� ����� � �������� ������ 
		String word = tokzerPar.nextToken();
		System.out.println("1� ����� � ������: " + word);
		
		//���� � �������� ������ ��� ������� �����������, �� ��� ��������� �������� ������
		if ( word.indexOf("�") == -1) 
			return word;  //��� ��� �������
				
		//�����, ������ ��� �� �����������(���������) ������� � ��� ��� �������� , ��� �������� ������
		//����� ������� � ����� ����� ����������() � ��������� ����, ����� � ����� �������� ����������� �������
		else  {
			word = word + tokzerPar.nextToken("�") + "�";
		}
		
		return  word;
	}
	
	static String readPosition(StringTokenizer tokzerPar){
		
		String word = tokzerPar.nextToken();
		System.out.println("1� ����� � ���������: " + word);
		
		if ( word.indexOf("�") == -1) 
			return word;  //��� ��� �������
				
		//�����, ������ ��� �� �����������(���������) ������� � ��� ��� �������� , ��� �������� ������
		//����� ������� � ����� ����� ����������() � ��������� ����, ����� � ����� �������� ����������� �������
		else  {
			word = word + tokzerPar.nextToken("�") + "�";
		}
		
		return  word;
		
		
				
	}
	
	static void readProblemString(String value, String mode, Staff  staffPar){
		
		String word = value.substring(value.indexOf(":")+1, value.length());
		
		if(mode == "���������"){
			
			staffPar.setPost(word);
			
		} else {
			
			staffPar.setDepartment(word);
			
		}
		
	}

	public static void main(String[] args) throws IOException {

	} //main

} //class


	

