package homeWork;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Calc2 extends JFrame{

	JButton arrOfbuttons[] = new JButton[9];
	{
		for (int i = 1; i <= arrOfbuttons.length; i++) {
			arrOfbuttons[i-1] = new JButton(String.valueOf(i));
			arrOfbuttons[i-1].setFont(new Font("Arial", Font.BOLD, 16));
			arrOfbuttons[i-1].setForeground(Color.BLUE);
		}
	}
	
	JButton button0 = new JButton("0");
	JButton buttonComma = new JButton(",");
	JButton buttonInvert = new JButton("+/-");
	{
		Font font = new Font("Arial", Font.BOLD, 16);
		button0.setFont(font);
		button0.setForeground(Color.BLUE);
		buttonComma.setFont(font);
		buttonComma.setForeground(Color.BLUE);
		buttonInvert.setFont(font);
		buttonInvert.setForeground(Color.BLUE);
	}
	
	public static void main(String[] args) {
		
		new Calc2("Calc_2");

	}

	public Calc2(String title) throws HeadlessException {
		
		super(title);
		setLocation(300, 300);
		setSize(250, 200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		setLayout(new BorderLayout());
		
		JPanel  mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(4, 3));
		
		add(mainPanel, BorderLayout.CENTER);
		
		int indOfelem = 6;
		for (int i = 0; i < arrOfbuttons.length; i++) {
			mainPanel.add(arrOfbuttons[indOfelem]);
			if(indOfelem == 8){
				indOfelem = 3;
				continue;
			} else if(indOfelem == 5){
				indOfelem = 0;
				continue;
			}
			indOfelem++;
		}
		
		mainPanel.add(buttonComma);
		mainPanel.add(button0);
		mainPanel.add(buttonInvert);
		
		
		setResizable(false);
		setVisible(true);
		
	}

}
