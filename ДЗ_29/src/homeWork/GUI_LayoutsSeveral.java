package homeWork;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//������� (��������� ���������� ���������) - ��������� ��������������� ���������� ���������
//���������� ��������� ������������ ������ � ������� �����
//� � ������� �������� ������������ �� ����������, � �������
//�.�. ��� ����� ������ ��� ��������� ���� (��� ����� �������, ���������� , �������� �������� � �.�.)


public class GUI_LayoutsSeveral extends JFrame{

		//������ ������ ���������� � ���� ����� ������ � ������������
	JButton butt1 = new JButton("Ok");
	JButton butt2 = new JButton("2");
	JButton butt3 = new JButton("3");
	
	JTextField tFld1 = new JTextField("���� ������� �������");
	JTextField tFld2 = new JTextField(30); // ����� - ������ ���������� � ������� ����. ������ ���� W
	JTextField tFld3 = new JTextField(30);
	
	JLabel label1 = new JLabel("��������� ����� 1");
	JLabel label2 = new JLabel("��������� ����� 2");
	JLabel label3 = new JLabel("��������� ����� 3");
	
	//�.�. ��� ����� ���� ��������� ��������, 
	//�� � ������������� � ������� ����� ������ ��� ������ �������� ��� ��������
	public GUI_LayoutsSeveral(String title) throws HeadlessException {
		super(title);
		
		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);  //������� ����� ��� �������� ���� ���� ���������
		
		//��� ������ � ����������� ��������� ������ ����������� ������
		//� ������ ������ ���� ������
		
		//1. �������� ������� ������, ������ ContentPane
		JPanel  mainPanel = new JPanel();
		
		//2. ��������� �� ���� ������
		mainPanel.setLayout(  new BorderLayout()  );  
		
		//3. �������� ��� ������ �� ����� ������������ �� ���������
		//add(mainPanel);
		setContentPane(mainPanel);
		mainPanel.setBorder(  BorderFactory.createTitledBorder("��� ������� ������"));
		
		//4. �������� �� ������ �������� ������ ������ ������ � ������ ��������
		JPanel  panelNorth = new JPanel();
		panelNorth.setLayout( new GridLayout( 4,3,5,5));  //2 ������, 2 �������, � 5 - ��������� ����� ������������
				//�������� � ������ ��������� ������
		JButton [ ]  arrButt = new JButton [ 10 ];  //10 ������ ��� ����
		for (int i = 0; i < arrButt.length; i++) {
			arrButt [ i ] = new JButton( String.valueOf(   i  )   );  //������� i-� ������
					//�������� ������ � ������
			panelNorth.add( arrButt [ i ]  );
		}
		//�������� ������  panelNorth   ��   ������  mainPanel
		mainPanel.add(  panelNorth ,  BorderLayout.NORTH );
		//�������� ����� � ������. ����� ������ ������� ���������� 
		panelNorth.setBorder(  BorderFactory.createTitledBorder("��� �������� ������"));
		
		//5. �������� ������ �� �� ������� �� ����� ��������
		JPanel  panelSouth = new JPanel();
		panelNorth.setLayout( new FlowLayout() );  //FlowLayout  ����� ���� �� ��������� - � ������ JPanel �� �� ���������
		mainPanel.add(  panelSouth ,  BorderLayout.SOUTH );
		//�������� ����� � ������. ����� ������ ������� ���������� 
		panelSouth.setBorder(  BorderFactory.createTitledBorder("��� ����� ������"));
		panelSouth.add(butt2);panelSouth.add(butt3);
		
		
		//6. �������� ������ � ����� ������� �� ����� ��������
				JPanel  panelCenter = new JPanel();
				mainPanel.add(  panelCenter ,  BorderLayout.CENTER );
				//�������� ����� � ������. ����� ������ ������� ���������� 
				panelCenter.setBorder(  BorderFactory.createTitledBorder("��� �����������  ������"));
				panelCenter.add(tFld1);
				panelCenter.add(tFld2);
		
		//setResizable(false);  //���������� ������ ����, �.�. ������� ���� ������������� ������ �������
		setVisible(true);
	}  //constructor
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//JFrame win = new JFrame("������ � ���������");
		
		new  GUI_LayoutsSeveral("������ � ���������");
		
		
	}//main

	

}
