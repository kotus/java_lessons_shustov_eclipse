package homeWork;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



//������� (��������� ���������� ���������) - ��������� ��������������� ���������� ���������
//���������� ��������� ������������ ������ � ������� �����
//� � ������� �������� ������������ �� ����������, � �������
//�.�. ��� ����� ������ ��� ��������� ���� (��� ����� �������, ���������� , �������� �������� � �.�.)




public class GUI_Layouts extends JFrame{

		//������ ������ ���������� � ���� ����� ������ � ������������
	JButton butt1 = new JButton("Ok");
	JButton butt2 = new JButton("2");
	JButton butt3 = new JButton("3");
	
	JTextField tFld1 = new JTextField("���� ������� �������");
	JTextField tFld2 = new JTextField(30); // ����� - ������ ���������� � ������� ����. ������ ���� W
	JTextField tFld3 = new JTextField(30);
	
	JLabel label1 = new JLabel("��������� ����� 1");
	JLabel label2 = new JLabel("��������� ����� 2");
	JLabel label3 = new JLabel("��������� ����� 3");
	
	//�.�. ��� ����� ���� ��������� ��������, 
	//�� � ������������� � ������� ����� ������ ��� ������ �������� ��� ��������
	public GUI_Layouts(String title) throws HeadlessException {
		super(title);
		
		setLocation(300, 300);
		setSize(500, 150);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);  //������� ����� ��� �������� ���� ���� ���������
		
		//setLayout(null);  //��������� ������ ���������
		//setLayout(  new FlowLayout() );  //���� ������ ��������� ���������� �����-�������
		
		//---------------------------------------------GridLayout (�����-�������)-----------------------------------------
		//1. ��� ���������� �������������.  - ������ �������� �� ��������� �� ���������� ��� ���� �������
		
		setLayout(  new GridLayout(3, 3, 20, 20));  
		//setLayout(  new GridLayout(1, 0, 20, 20));  //1 ������ 0 ������� - ��� ���������� ����� � ���� �������������� ���
		//setLayout(  new GridLayout(0, 1, 20, 20));  //0 ����� � 1 ������� - ��� ���������� ����� � ����  ������������ ���
		//0 � ������� ��� �������� �������� ����� ���������� ����� ��� �������
		
				//������ ���������� �������
		//getContentPane().setLayout(  new GridLayout(3, 3, 20, 20));  //getContentPane  ����� ��� �� ������������
		
		add(label1); add(tFld1); add(butt1);  //���� �����-�� ���������� ��������� 2 ���� - �� ��������� 2-� ���������
		add(label2); add(tFld2); add(butt2);
		add(label3); add(tFld3); add(butt3);
		
		//butt1.setSize(100, 30);                          //setSize()  �������� ������ � ������ ���������
		//butt1.setBounds(10, 10, 100, 100);    //���� �������� ������ � ������ ���������
		butt1.setPreferredSize(  new Dimension(100, 50));   //�� �������� � GridLayout. �.�. ��� ���������� � ����� - �������������
		
		
		//----------------------------------------BorderLayout----------------------------------------------------------
		//�� ������ � Border ��������� (���������) ���������� ����� � �����������
		//������������� - ����������� ����������?
		//���� � ��� - ������������� ������
		//����� � ������ ������������� ������
		setLayout(  new BorderLayout() );
		
		//�������� ���������� �� ��������  �����-������-�����-��
		add(butt1, BorderLayout.CENTER);  //��������� ������ �� ������
		add(butt2, BorderLayout.NORTH);   //��������� ������ ������ (�� ������)
		add(butt3, BorderLayout.SOUTH);   //��������� ������ ����� (�� ���)
		
		//��������� ������ (��� ����� ������� ������ � ��������������� �������� - ������ ������������� ����������)
		butt1.setPreferredSize(  new Dimension(100, 70));
		butt2.setPreferredSize(  new Dimension(100, 70));  
		
		add(tFld1, BorderLayout.WEST);
		add(tFld2, BorderLayout.EAST);
		tFld1.setPreferredSize( new Dimension(50, 100));
	
		
		//---------------------------------------BoxLayout-------------------------------------------------------------
		//������������� ���������� - ��������� 
		//������� ��������� ��������������� ������� ������������ ��� Y_AXIS,  
		//                                                                           � �������������� ��� X_AXIS
		
		//���� ������ ����� ����������� ������ ������������
		//1-� ����������� ������ ������ ������������ ����� �� ��������
		//setLayout( new BoxLayout( new JPanel(),  BoxLayout.X_AXIS   ));  //������� �������, �.�.  JPanel  ������ ������������ �� ������ ������������ BoxLayout()
		
		//2� ������ - ������� ������� ��������� - ������ JPanel 
		JPanel  panel  = new JPanel();
		//setLayout( new BoxLayout( panel,  BoxLayout.X_AXIS   ));  //������� can't be shared - �.�. panel - ���� ������
		                            //� setLayout ��������� ������ ������ ������ - ���, ������ �� ��������� � ��������
		                            //�.�. �� ���� ������ ������� (���� panel �  ContentPane- ������  �� ���������) 
		                              //�������� ������������ ���� ������ �������
		panel.setLayout( new BoxLayout( panel,  BoxLayout.X_AXIS   ));   //������ ������� ������������
		
		//3� ������ 
		setLayout( new BoxLayout( getContentPane(),  BoxLayout.Y_AXIS   ));  
		butt1.setPreferredSize( new Dimension(100, 50));
		
		
		//setResizable(false);  //���������� ������ ����, �.�. ������� ���� ������������� ������ �������
		setVisible(true);
		
	}  //constructor
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//JFrame win = new JFrame("������ � ���������");
		
		new  GUI_Layouts("������ � ���������");
		
		
	}//main

	

}
