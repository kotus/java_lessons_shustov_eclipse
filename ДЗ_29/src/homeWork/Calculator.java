package homeWork;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Insets;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class Calculator extends JFrame{

	JTextField textField = new JTextField();
	
	JButton[] arrOfButtons = new JButton[10];

	JButton buttComa = new JButton(",");
	
	JButton buttResult = new JButton("=");
	JButton buttPlus = new JButton("+");
	JButton buttMinus = new JButton("-");
	JButton buttMultiply = new JButton("*");
	JButton buttDevide = new JButton("/");
	
	JButton mc = new JButton("MC");
	JButton mr = new JButton("MR");
	JButton ms = new JButton("MS");
	JButton mPlus = new JButton("M+");
	JButton mMinus = new JButton("M-");
	JButton backspace = new JButton("←");
	JButton ce = new JButton("CE");
	JButton c = new JButton("C");
	JButton plusMinus = new JButton("±");
	JButton sqrt = new JButton("√");
	JButton buttPercent = new JButton("%");
	
	public static void main(String[] args) {
	
		new Calculator("Калькулятор");
		
	}

	public Calculator(String title) throws HeadlessException {
		super(title);
		setLocation(300, 300);
		setSize(290, 250);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		Font fontOfNumbers = new Font("Arial", Font.BOLD, 15);
		Font fontOfTextField = new Font("Arial", Font.PLAIN, 35);	
		
		buttResult.setFont(fontOfNumbers);
		buttPlus.setFont(fontOfNumbers);
		buttMinus.setFont(fontOfNumbers);
		buttMultiply.setFont(fontOfNumbers);
		buttDevide.setFont(fontOfNumbers);
		buttComa.setFont(fontOfNumbers);
		
		for (int i = 0; i < arrOfButtons.length; i++) {
			arrOfButtons[i] = new JButton(String.valueOf(i));
			arrOfButtons[i].setFont(fontOfNumbers);
		}
				
		//Главная панель. Основа всего окна.
		JPanel mainPanel = new JPanel(new BorderLayout());
		add(mainPanel);
		
		textField.setHorizontalAlignment(JTextField.RIGHT);
		textField.setFont(fontOfTextField);
		mainPanel.add(textField, BorderLayout.CENTER);
		
		//Все кнопки будут на ЮГ-е главной панели
		JPanel southMainPanel = new JPanel(new GridLayout(2, 1, 2, 2));
		mainPanel.add(southMainPanel, BorderLayout.SOUTH);
		
		JPanel functionPanel = new JPanel(new GridLayout(3, 5, 2, 2));
		Border borderOfFunctionPanel = new EmptyBorder(1, 1, 0, 1);
		functionPanel.setBorder(borderOfFunctionPanel);
		
		southMainPanel.add(functionPanel); // 1-я строка southMainPanel
		functionPanel.add(mc);
		functionPanel.add(mr);
		functionPanel.add(ms);
		functionPanel.add(mPlus);
		functionPanel.add(mMinus);
		
		functionPanel.add(backspace);
		functionPanel.add(ce);
		functionPanel.add(c);
		functionPanel.add(plusMinus);
		functionPanel.add(sqrt);
		
		for (int i = 7; i <= 9; i++) {
			functionPanel.add(arrOfButtons[i]);
		}
		functionPanel.add(buttDevide);
		functionPanel.add(buttPercent);
		
		JPanel bottomBorderPanel = new JPanel(new BorderLayout(2,0));
		southMainPanel.add(bottomBorderPanel); // 2-я строка southMainPanel
		// ее мы сделаем BorderLayout. Кнопка "=" будет на весь EAST
		Border borderOfBottomBorderPanel = new EmptyBorder(0, 3, 3, 3);
		bottomBorderPanel.setBorder(borderOfBottomBorderPanel);
				
		bottomBorderPanel.add(buttResult, BorderLayout.EAST);
		buttResult.setPreferredSize(new Dimension(54, 0));
				
		//Добавить Grid панель на 1 колонку и 3 ряда. 1-й цифры, 2-й цифры 3-й - панель с нулем
		JPanel leftPanelGrid = new JPanel(new GridLayout(3, 1, 2, 2));
		bottomBorderPanel.add(leftPanelGrid, BorderLayout.CENTER);	
		
		JPanel firstNumberBottomGridPanel = new JPanel(new GridLayout(1, 4, 2, 2));
		leftPanelGrid.add(firstNumberBottomGridPanel);
		for (int i = 4; i <= 6; i++) {
			firstNumberBottomGridPanel.add(arrOfButtons[i]);
		}
		firstNumberBottomGridPanel.add(buttMultiply);
		
		JPanel secondBottomGridPanel = new JPanel(new GridLayout(1, 4, 2, 2));
		leftPanelGrid.add(secondBottomGridPanel);
		for (int i = 1; i <= 3; i++) {
			secondBottomGridPanel.add(arrOfButtons[i]);
		}
		secondBottomGridPanel.add(buttMinus);
				
		//Здесь чтобы ноль был растянутый, сделаем BorderLayout
		JPanel thirdBottomGridPanel = new JPanel(new BorderLayout(2,0));
		leftPanelGrid.add(thirdBottomGridPanel);
		thirdBottomGridPanel.add(arrOfButtons[0], BorderLayout.CENTER);
		
		//Две остальные кнопки загоним в Grid и на Восток последней панели.
		JPanel panelCommaPlus = new JPanel(new GridLayout(1, 2, 2, 2));
		thirdBottomGridPanel.add(panelCommaPlus, BorderLayout.EAST);
		
		panelCommaPlus.add(buttComa);
		buttComa.setPreferredSize(new Dimension(54, 0));
		panelCommaPlus.add(buttPlus);
		buttPlus.setPreferredSize(new Dimension(54, 0));
		
		setResizable(false);
		setVisible(true);
		
	}

}
