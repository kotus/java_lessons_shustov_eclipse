package lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;


public class ListOperations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1. �� �������� � ����������
		//int [ ] arrInt = {1,2,3,4,5,6,7}; 
		String [ ]   arrStr = {"���", "���", "���", "�"};
		
		//1.1. ������� ����� ������ Arrays -
		//��� ����������� �����, ������� ����� ������ ����������� ������
		//Math.
		List           <String>   collList2 = Arrays.asList( arrStr );
		
		//3. �������� �������� �� �������� - 
				//������ - ��� ��������� ���� Liist � ��� ���������� ArrayList, LinkedList � Vector
				List<String>     coll4FromAsList = new LinkedList(collList2); 
				//coll4FromAsList.    //����� �������
				//3.1.   ���������� ������ 
				//List<Integer>     listColl = null;  // � ����� ��������� �� ����� ���� �������
				List<Integer>     listColl = new ArrayList<Integer>();
				listColl.add( 1 );   //��������� � ������  � ����� ������  ����� 1
				
				
				ArrayList<Object> collObj = new ArrayList<Object>();
				LinkedList<Object> collObj2 = new LinkedList<Object>();
				Vector<Object> collObj3 = new Vector<Object>();
				
				
				
				
				Collection<Integer> collection = new ArrayList<Integer>();
				List<Integer> list = new ArrayList<Integer>();
				for (int i = 0; i < 10; i++) {
					list.add(i);
				}
				System.out.println(list);
				Integer last = list.get(list.size()-1);
				
				
				
				
				
				
				
				List<Object> listObj = new ArrayList<Object>();
				//listObj.
				
				
				//0�   1�       2�
				//1      null    null
				
				
				listColl.add(  listColl.size(), 17);   //����� ���������� ��������� �����
				
				//listColl.add(3, 17);   //��������� � ������ � 3� �������  ����� 17, ��� ������� � 3-�� �������� ������������ ������
													//IndexOutOfBoundsException: - �.�. ������� �������� ����� ������� ������ ����������, � �� ����� ����������
				
				//3.2. �������� ���������� � �����
//			for (int i = 0; i < listColl.size()  ; i++)   //!!!* � ������� ����������� ������ ���������� size() 
																				//���� ������ ����� ���� add() � ��� �� ���������
//					listColl.add( i ); 					//������� OutOfMemory
			
				//���� ���������� ������ ��������� �� ����� � ����������, � � ����� ����� ��� ����������
				int sizeColl =  listColl.size() ;
				for (int i = 0; i < sizeColl ; i++)   //!!!* � ������� ����������� ������ ���������� size() 
					//���� ������ ����� ���� add() � ��� �� ���������
							listColl.add( i ); 	
				
				//3.3. ����������  � ������  ���������
				for (int i = 0; i < sizeColl /*����������� ����������*/ ; i++)   
							listColl.add( 0, i );   //0 - ���������� ��������� � ������ ���������, ���� ������� ����� ���������
															//��� ArrayList � Vector, � � ���������� ��� �� �������������, 
															//�.�. � ���� ������������� ������ �������� �������
				
				//3.4. ����������  � �������� ���������
						for (int i = 0; i < sizeColl ; i++)   
									listColl.add( 3, i );   //������ 3 - ������ ��������� �� �������, ��� �� �����-��������� �������
																	//��� ArrayList � Vector, � � ���������� ��� �� �������������, 

			//3.5. ����-�������� ����������  (���� - ���������, ��� �����)
						listColl.addAll(listColl); 
						
			//3.6.  ������ � ����� �� ����������� ������
//			for (Integer elem : listColl)   //���� foreach()  ������ ����� ��� ���������� ���������, ������ ������ �� ��������� , ��� ���������
															//�.�. � ����� �����() ������ ���� ���� ��������, � �� ����������� � ������� ���()
			//	listColl.add( 7 );
					
			//3.7
			//���������� ��������� �� ��������� �����, �� �������� �� ����� ���������.
			for (Iterator iterator = listColl.iterator(); iterator.hasNext();) {
				Integer elem = (Integer) iterator.next();
								
			//	listColl.add( 7 );   		//ConcurrentModificationException - �.�. ������ ����������, � ���������� - �������()
							//���� ��������� �������� � ��������� - �� ������ �� ���������� ���� ������ ������� ������
				
			}
			
		
				System.out.println( listColl);
				
				//4. ���������� � ��������� ��������
		String [ ]   arrFIO = {"������", "������", "�������"};
		//4.1. ������� ������ ���������
		List <Client> collClient = new ArrayList<>();
		
		//4.2. � ����� 
		for (int i = 0; i < arrFIO.length; i++) {
			collClient.add ( /*������*/   new Client()  );
				//�������� � ������ ���� ������ ���� �� ������� � i-� ������� 
			collClient. get ( i ).name = arrFIO [ i ];        //���� ������� ������ ��� ������� ����� ����� ������ � ��� �� ������
			collClient. get ( i ).setName(  arrFIO [ i ] );   //���� ������� �������� �������� � 
																				//����� ������� ������ ���� � ������ ������
		}
		
		//������� � ������ ����� � ��� ������� (������ ���������� ������ � �������, � ��. ����. ��� ���)
		int ii=0;
		//for (Client elem : collClient) {  //!!!  ��� ������� �� ��������� ���������� �������� ����������� ����
														//!!! � ����� ��������� ������ ������ - �.�. ���� ������ � ���� �� �����
		for (String elem : arrFIO) {
			//����� ����������� � ���������� ����  � ������
			collClient.add ( /*������*/   new Client( arrFIO [  ii++ ]  )  );  //���� ������� ��������� ��� ��������� ��� �������� 
		}
		
		//������� � �� ��������� �������� �������
		 ii=0;
		 for (String elem : arrFIO) {
			//����� ����������� � ���������� ����  � ������
			Client  cl = new Client( arrFIO [  ii++ ]  ) ;  //������ ��� �������� � ����� �������
			//������� ��������� ������� �������
			//... 100500 ����� ����
			
			collClient.add ( /*������*/ cl    );  //���� ������� ��������� ��� ��������� ��� �������� 
		}
		 
		 //5. ������� �� �����
		 System.out.println( collClient );  //���� ������� ������ , �.�. �������� ��� �����
		 											//�� ����� toString() ������ ����, � ���� ����� ����� ������ �����, �� �������� ����� ����
		 
		 //6.   ������ �� ��������� ������, � �.�. ����� ����������
		//� ������� �� ������� �  ������ ������� ������ for
		 for (int j = 0; j < collClient.size(); j++) {
			System.out.println("ID = " +   collClient.get(j).ID  + 
					                        ", name=" + collClient.get(j).name + 
					                        ", sum=" + collClient.get(j).sum + "\n");
		}
		 //������ ������ ����� ��� ��������
		 for ( Client  elem : collClient) {
				System.out.println("ID = "       + elem.ID  + 
						                        ", name=" + elem.name + 
						                        ", sum="   + elem.sum + "\n");
			}
		
		 //������ ����������
		 for (Iterator iterator = collClient.iterator(); iterator.hasNext();) {
			Client elem = (Client) iterator.next();
			
			//...  ��������� elem-�
		}
		 
		 //������ ����� ����������, ������� ���� � �������  - ListIterator
		 for (ListIterator listIterator = collClient.listIterator(); listIterator.hasNext();) {
			Client elem  = (Client) listIterator.next();
			
			// ...  ��������� elem-�
		}
		 
		 System.out.println("-------------------------------------�������� ������� ������� � 5-�� ��������--------------------------");
		 //������ ����� ����������, � �������� �������, � �������� ���������
		 //for (ListIterator listIterator = collClient.listIterator( collClient.size() /*������� � ����� ���������*/ ); listIterator.hasPrevious();) 
		 for (ListIterator listIterator = collClient.listIterator( 5 ); listIterator.hasPrevious();) {  //������ ������� � 5-�� �������� ������
			Client elem  = (Client) listIterator.previous();   //������ � �������� �������
			// ...  ��������� elem-�
			//������: ������ �������� �������� �������  ����������� (����� ���� �������� ������)
			if ( listIterator.hasPrevious()  ) {
					listIterator.set(   //��������� ������-�� �������� � ������� ��������  
							                collClient.get ( listIterator.previousIndex()   ) );   //���� ������()  ���������� ����������� �������
					
					System.out.println("ID = "       + elem.ID  + 
								                        ", name=" + elem.name + 
								                        ", sum="   + elem.sum );
			} //if
		}	
		 
		 
		 //7.  ����� (� �������� �� ��������� �����)  
		 // ������: � ������ listColl ����� ����� 17 � �������� ��� �� 100
		 // ������� ������  - ����� ������� ����
		 Integer etalon = 17;
		 Integer newElem = 100;
		 //������� � ������ �����
		 for (Integer elem : listColl) {
			if (  elem == etalon )  {
				//������ ������ ������ - � elem  �� ��������� �� ��������� ������� � ���������
				// ��� �� ��� � � ����������� � ������� �����    met1 (Client  par)     met1(Integer par)
				elem =  newElem ;
				System.out.println("17  �������,  � elem = " + elem);
			}  //if	
		} //for
		 
		 System.out.println("����� ����� �����:" + listColl );
		 
		 //������� � ������ ���()
		 for (int j = 0; j < listColl.size(); j++) {
			 if (  listColl.get(j) == etalon )  {
				 listColl.set( j,   newElem) ;
			 }   //if
		} // for
		 System.out.println("����� ����� ���() ������ 17-�� �� 100: " + listColl );
	
		 //������: �� �� �����, ������ ���������� � �������� 0-�� �� 7
		 etalon = 0;
		 newElem = 7;
		 ii = 0;
		 for (Iterator iterator = listColl.iterator(); iterator.hasNext();) {
			Integer elem = (Integer) iterator.next();
			
			if ( elem == etalon) {
				// elem = 100;   //�������, �.�. elem - �� ���� ������� ���������
				//�.�,  ������� �������� �� ����� �������, �� ������ �������� ������������� �����
								//����� �������� ����� ������, �.�. ����� ��� ������������
				listColl.set( ii, newElem) ;
				//  listColl.remove( ii );  //d � ����� ���������� ������� ����� ������ ����������, ����� ����������������
			} //if
			ii++;
		} //for
		 System.out.println("����� ����� ���������� ������ 0-��� �� 7: " + listColl );
		 
		 
		 //8. ����� � ��������� ��������
		 //������:  ����� ������� � ��  = 3 , � �������� ������� �� ��������
		 //�������� ������-�������, ������� ����� ������ �������  ��=3
		 Client  etalonCl = new Client( 3, "");
		 //����� ����
		 String  newFam = "��������"; 
		 //����� � ��������
		 for (int i = 0; i < collClient.size(); i++) {
			if ( /*���� ID � i-�� �������� ��������� collClient*/ collClient.get(i) .ID  == etalonCl.ID )  {
				collClient.get(i) . name = newFam ;			//���� ����� ������ � ����� ������
				 //collClient.get(i) .setName( newFam );  //���� ����� ������ � ������ ������
			} //if
			
		} //for
		 System.out.println(collClient );
		 
		 
		 
	} //main

}//class

class Client {
	int ID;  //���, ����������� ���������� ����� �������
	String  name;
	long    sum;
	
	static int countClients=0;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Client(String name) {
		//super();
		this.name = name;
		countClients++;  
		ID = countClients;
		
		//  ID = ++  countClients;
	}
	public Client() {
		//super();
		// TODO Auto-generated constructor stub
	}

	
	public Client(int iD, String name) {
		super();
		ID = iD;
		this.name = name;
	}
	//����� toString()  ���  ������ ��������
		@Override
	public String toString() {
		return "Client [ID=" + ID + ", name=" + name + ", sum=" + sum + "]\n" ;
	}
	
	
}

