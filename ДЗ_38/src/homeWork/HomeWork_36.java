package homeWork;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.TableColumn;

@SuppressWarnings(value = {"serial", "unused"})
public class HomeWork_36 extends JFrame   {

	static Object[][] data = {
		{ "������ ������ �����", 1000, "2014.03.17", "10:17:40" },
		{ "������ �������", new Integer(2000), "2014.03.17", "10:15:60" },
		{ "������ ������ �����", new Integer(3000), "2014.03.17", "13:18:30"},
		{ "����� �������", new Integer(4000), "2014.03.17", "14:12:40" },
		{ "������ ������ �����", new Integer(5000), "2014.03.17", "11:15:60" },
		{ "������ � ����-�����", new Integer(6000), "2014.03.17", "09:11:00" },
		{ "������ � ����-�����", new Integer(7000),"2014.03.17", "15:17:40" }
		};
	
	String [ ]   colNames =  {"��� ��������", "�����", "����", "�����"};
	JTable table = new JTable(data, colNames);
	
	public HomeWork_36(String title) throws HeadlessException { // '������� ������� �������� ����
																	
		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		setLayout(new FlowLayout());
		
		JScrollPane  scrollPan = new JScrollPane(table);
		scrollPan.setPreferredSize( new Dimension(400, 100) );
		
		table.setPreferredScrollableViewportSize(new Dimension(400, 100));
		add(scrollPan);
	
		data[2][1] = 30000;
		
		//���������� ����������� ����� ������ 3� ������ � 2-�� ������� � 2014-10-07. ����� ������ ������� 
		//������� �������
		table.setValueAt("2014-10-07", 3, 2);
		
		TableColumn colForMonth = table.getColumnModel().getColumn(2);
		
		JComboBox  cBoxMonth = new JComboBox<>();
		cBoxMonth.addItem("������");
		cBoxMonth.addItem("�������");
		cBoxMonth.addItem("����");
		cBoxMonth.addItem("������");
		cBoxMonth.addItem("���");
		cBoxMonth.addItem("����");
		cBoxMonth.addItem("����");
		cBoxMonth.addItem("��������");
		cBoxMonth.addItem("�������");
		cBoxMonth.addItem("������");
		cBoxMonth.addItem("�������");
		
		colForMonth.setCellEditor(new DefaultCellEditor(cBoxMonth));
		
		
		
		/*			
		// --------------------------------------��������� �
		// �������------------------------------------------------------------------------------
		// 1. ����������� ��������� - �.�. ��, ������� ������ ��� �����������
		// ������������
		// 1.1 ��������� ����� ��������� ������� ������ �������
		data[0][0] = "�����.���������:������ �������"; // ���� ������ �����
														// ������������ ������,
														// ����� �� �����
														// ��������
														// ����������������
		// �� ������ ��������� ����� ����, ��� ������� ����� ������� - ����
		// ������ �����������
		table.repaint(); // ������������ ���������
		table.revalidate(); // ������� ��������� ��������� (������������)
		// 1.2 � ������� ������() �������
		table.setValueAt(7000, 0, 1); // ������ ������������ �
										// �����������������, �.�. ���� �����
										// ��� ��������� ����� ����������������
		// ��� ������� ��������� - �.�. ��������� � ������� - ���������
		// ��������� � ������� ������ � ��������

		// 2. ���������������� ��������� - ��� ����� ���� � ����� �����,
		// � ���� ������ ����� � �������� - �.�. ����� �������� �����
		// �������� ����� ����� � ������� ���������� - �������� � ��������, ���
		// ������������ ����� ��������� ������
		// ����� �������� ����� ������, �����
		// - ������ �� ����� � ��� ������������ � ����
		// - �.�. ��� ����� ����� ������ ����� - ������ � ������ - ��� ������
		// ������� ��� ����������
		// 1. ������: ������� � 0-� ������� �������� �����������
		// 1.1 ������� ���������
		JComboBox cBox = new JComboBox<>();
		cBox.addItem("������ ������ �����");
		cBox.addItem("������ ������ �����");
		cBox.addItem("����� ������� ");
		cBox.addItem("������ ������ �����");
		cBox.addItem("������ � ����-�����");
		cBox.addItem("������ � ����-����� ");
		cBox.addItem(data[1]);
		cBox.addItem(data[2][0]);
		// 1.2. �������� ������� - ������� ��� ��� ����� ��� ����� ������()
		// table.getColumn(identifier); //���� ������() �� �������� ���
		// ���������� ��������� �������, �.�. ������� �������������� �������
		// table.getColumnModel(). //���������� ������������� �������
		TableColumn colForComboBox = table.getColumnModel().getColumn(0); // �����
																			// ����������
																			// ������
																			// 0-�
																			// ������
																			// ���
																			// �������
		// 1.3 ������� �������������� ������� � �����������
		// ��������� ������ - ��� ��������������
		// �.�. ��� ���� ����� �� �������� , ���������� �� ���������� �
		// ���������� ����� ��� ������� ��������
		// ���������� ������ ������������
		// ��� ����� �������� ������� (� �� ������, �������, �������� ���� �
		// ��.)
		// � ����� ������ ��������� ������� ���� �������� ������ - �.�. ������
		// ��� ������ �������
		// ����� ���� ����� � ��������� ��������������, �� �� ����� ������������
		// ���� � ��� �� �������� ������ ��� ���� �������
		colForComboBox.setCellEditor(new DefaultCellEditor(cBox)); //� ��������� ����������� ��������
																 

		// ������� 2�-������� � ����������� - �������� �����������, �����
		// ������������
		// �.�. ��� ���� �������� ��������� �������� ����������
		// �� ������ ������ ����� �������� ������������� ��������
		// 1. ������� ���������
		JTextField txt = new JTextField("���� ��.��.��");
		// 2. �������� �������
		TableColumn colForTextField = table.getColumnModel().getColumn(2); // ������
																			// (�����)
																			// �������
		// 3. ��������� � ������� ��������� ������� ��� ��������
		colForTextField.setCellEditor(new DefaultCellEditor(txt)); //
		// ��������� ����� �������� ����� �������� ������ � �������������
		// �����������, ��
		// ������������ ������������ ������������� ���������, ������� ����� �
		// ��������� ���

		// ���� ��� 3� ��������� �������� - � ��������� (����� ��������), �� ��
		// �������� ��� ��������������� ������ �������
		// � ����� ��� �����
		*/

		setVisible(true);
		
	
	} // construcror

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		
		 new HomeWork_36("JTable");

	}//main()


}// class
