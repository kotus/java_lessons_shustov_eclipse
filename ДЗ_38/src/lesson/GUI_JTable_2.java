package lesson;


import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Point;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;


public class GUI_JTable_2 extends JFrame   {

	static Object[][] data = {  //��� Object - �.�. � �������� ����� ������� ������ �����
											//�.�. � ����� ��������� ��-������������: "��������� ������ �������� ������������� � ��������"
											//������-�� ���� ������� ������ � ����� Object ������, �.�. ��� �������� �����
											//�� ��������, ���� �������� �����������
		{ "������ ������ �����", 1000,                        "2014.03.17", "10:17:40" },
		{ "������ �������",        new Integer(2000), "2014.03.17", "10:15:60" },
		{ "������ ������ �����",new Integer(3000), "2014.03.17", "13:18:30"},
		{ "����� ������� ",         new Integer(4000), "2014.03.17", "14:12:40" },
		{ "������ ������ �����",new Integer(5000), "2014.03.17", "11:15:60" },
		{ "������ � ����-�����",new Integer(6000), "2014.03.17", "09:11:00" },
		{ "������ � ����-����� ",new Integer(7000),"2014.03.17", "15:17:40" }
		};
	
		//������ ����������
	String [ ]   colNames =  {"��� ��������", "�����", "����", "�����"};
	
			//������� ������ �������
	JTable table = new JTable(data, colNames);
	
	
	
	public GUI_JTable_2(String title) throws HeadlessException { // '������� ������� �������� ����
																	
		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); //������� - ������� ��������� ����� �������� ���������� ����

		// setLayout(null); //�������� ����� ���������, �� �� ��� �� �����
		setLayout(new FlowLayout()); // �������� ����������() - ����������,
                 										// ��������� ���������� ����� �������


		//������� ������ ���������
		JScrollPane  scrollPan = new JScrollPane(table);
		scrollPan.setPreferredSize( new Dimension(400, 100) );
		
		table.setPreferredScrollableViewportSize(new Dimension(400, 100));
		add(scrollPan);
	
//add(table);		//����� ����� ������� ��� ������ ��������� �� ���������� �������� ��������
					
		//--------------------------------------��������� � �������------------------------------------------------------------------------------
		//1. ����������� ��������� - �.�. ��, ������� ������ ��� ����������� ������������
		//1.1 ��������� ����� ��������� ������� ������ �������
		data [ 0 ] [ 0 ] = "�����.���������:������ �������";    //���� ������ ����� ������������ ������, ����� �� ����� �������� ����������������
				//�� ������ ��������� ����� ����, ��� ������� ����� ������� - ���� ������ �����������
		table.repaint();  		//������������ ���������
		table.revalidate();   //������� ��������� ��������� (������������)
		//1.2  � ������� ������() �������
		table.setValueAt(7000, 0, 1);   //������ ������������ � �����������������, �.�. ���� ����� ��� ��������� ����� ����������������
				//��� ������� ��������� - �.�. ��������� � ������� - ��������� ��������� � ������� ������ �  ��������
		
		
			JComboBox  cBox = new JComboBox<>();
			cBox.addItem("������ ������ �����");
			cBox.addItem( "������ ������ �����");
			cBox.addItem( "����� ������� ");
			cBox.addItem( "������ ������ �����");
			cBox.addItem( "������ � ����-�����");
			cBox.addItem( "������ � ����-����� ");
			cBox.addItem(data[1]);
			cBox.addItem(data[2][0]);
		
			TableColumn  colForComboBox =  table.getColumnModel().getColumn( 0 );   //����� ���������� ������ 0-� ������ ��� �������
			colForComboBox.setCellEditor(  new DefaultCellEditor(cBox)  /*� ��������� ����������� �������� */);
			
			
			//1. ������� ���������
			JTextField txt = new JTextField("���� ��.��.��");
			//2. �������� �������
			TableColumn  colForTextField =  table.getColumnModel().getColumn( 2 );  //������ (�����) �������
			//3. ��������� � ������� ��������� ������� ��� ��������
			colForTextField.setCellEditor(  new DefaultCellEditor(txt) );  //
					//��������� ����� �������� ����� �������� ������ � ������������� �����������, �� 
					//������������ ������������ ������������� ���������, ������� ����� � ��������� ���
			
			//���� ��� 3� ��������� �������� - � ��������� (����� ��������), �� �� �������� ��� ��������������� ������ ������� 
			//� ����� ���  �����
			
			//------------------------------------------------------- ���� 37 ----------------------------------------------
			//��������� ������ ���� � �������
			table.addMouseListener( new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent event) {
					// TODO Auto-generated method stub
							//������������� ������ �������
					//������: �������� ������� ����� ��������������
					//� ���������� �� ���������� ����� ������ ��� ���� ����
					//1. ���������� ������ �� ������ ������ ����
					if ( event.getButton() ==  MouseEvent.BUTTON3) {
						System.out.println( "������� ����: �������� ���" );
						//2. ���� ��, �� ����������  ������ � ������� ��������� ������
						
						Point objPoint = event.getPoint();  //��������� ����� �,�
								//�������� JTable ���������� �������� � �������
						int col = table.columnAtPoint(objPoint);
						int  row = table.rowAtPoint(objPoint);
								//��������� ���������� ������
						//���� 2 �������� 
						//1. ����� ������ ��� 2� ����� ������()
						String strCell = data [ row ] [ col ].toString();
								 //  strCell = (String)table.getValueAt(row, col);  
						  			strCell = table.getValueAt(row, col).toString();
						System.out.println("�������� ������: " + row + " , " + col + " :" + strCell);
					}	
						
				}
			}  )  ; 

			
			//------------------------------------------------------------------------------------------------------------------------------------------
			//������� ��������� TabelModelListener, ������� ���� ������ ������� 
			//��� ���� ����������� ��������� ������� setValueAt
			//1. ���� ������� ������������   � ������ �������, � �� �  ����� �������
			//table.add  // �������� ��� ������ ������
			//�������� ������� �������� ������ �������  -  	table.getModel()
			table.getModel().addTableModelListener( new TableModelListener() {
				
				@Override  //���� ���������� - �.�. ��� ������ Actionlistener-� � ����������
										//�������� �������� �� �������� ���� � �������, 
										//� ������� ����� ����� ���������
				//���� ���������� ���������� �����:
				//1. ����� ������� ������� �����
				//2. ����� ����� ������ ���� ���������� ������ - �.�. ��� ���� ����������� 
				//    ����� ���������� � ���������� ������
				//3. ����� ������ ������� �������� � ��������� ��� �������� ������� �����������
				//4. ����� ������ ������������� ������  setValueAt()
				//5. ����� ��������-������� ������ ������� ��� ��������� ������
				//6.  ��  ������� �� ����������, ���� ��������� � ������ ������� ����������� 
				//      � ������� ������ �������  data [ ] [ ]  - ������������, �����  ����� ������� �������� ������, 
				//     � ������� � �� ����� ������������� ��������� �������� ��������� �������
				public void tableChanged(TableModelEvent event) {
					// TODO Auto-generated method stub
					//������: ���������� � �����  ������ ��������� ��������� 
					//  �.�. ������ ����� ������ � ����� ������� �������� ������ 
					
					//����� ����� ���� �� �������, �� ������ � ��� �� ����� ������
					int col = event.getColumn();   //� ������� � ������� ���������
					int row = event.getFirstRow();   //� ������ � �����������
					int rowLast = event.getLastRow();   //� �������� ������ � ����������� 
																				//(���� ��������� � ����� ������ ��� ������ - �� )
																				//row �  rowLast ���������  
					System.out.println("rowFirst  " + row + "rowLast" + rowLast);
					
					//����� ����� �� �� ���� �� ������� ������� - ���� ������ �������������
					System.out.println("���������� ��������� � �������: " + 
					                                  table.getEditingRow()  + "    " +  table.getEditingColumn() );
				
					TableModel objTabModel = (TableModel)  event.getSource() ;  //���������� Object, �� ������ ���������� ������� ���� ��� ������, 
					                                  //� �������� ���-�����  ������� - � ������ ������ �������� TableModel
					 									//��������� ��� �������� ������ getSource() ��� �.�. ����������� �� ����� ���� 
					 									//������ � ������������ ���������� �������� �  TableModel
				
					 
					 			//��� ��������� �������� ������ ����������� ������� -  ��������� �� 
					 		//����� ������� �����-����, � ������ ����� ������ ����� �����������
					 
					 					//������ ������� ��������� �����-���������� � �������
					 					//������:  ��������� ��������� � ������ ����� (�������� ������ ������)
					 		//������� ��������� , ���������� �����������  ��������� ������, � �� ������ �������
					table.setCellSelectionEnabled( true );  //���������� ����� ��������� �� ������, � ������
										 
					 		//��� ������� ��������� �������, ��� ���� ����� ������� ��������� � �� ������, 
					 		//� ������� ���� ������ ������ ��� ��������� ���������
					table.setColumnSelectionInterval(col, col);  //����� �������� ����� � 3� �������
					table.setRowSelectionInterval(row, row);		
					 
					JOptionPane.showMessageDialog(null, "������ � ������ � ������ " + ( row+1) + "� ������� "  + (col+1) );
					 
					 
					//���� ���������� ������ ���, ��� � ��� ���� ��������� ��������� ������
					 //�.�. ��������� ������() �������� � ���������� � ������� - � ��������� ������� ���������.
					 //������� ������� ����������-�����()-����������-�����- ����������   - �.�. ����� ��������.
					 //�������� - �����  ������ ������ ����� �� ������, ������� ������� � ������������  
					 //������������ -  ������� ��������� �� ������, ��-�� ������������� �����
					
							//��������� �������� ������������ � ������������ �����
							//��� ������ ������ ������ ��� ����� ��������� � ������� , ���������� � ������ 
							//����� �� ������ -�����������

					//table.setValueAt(7000, 0, 1);
							//��������� � ������ ���� �� ���� ������� ������ ����� ����������� - 
							//�� ���� ������ ������� ��������� ������� ������
					data [ 1 ] [ 1 ] = 0;  //��� ��������� ����������  ����� ������������ ����� ����������� �������
					table.repaint();  		//������������ ���������
					
				}
			}) ;
			
		setVisible(true);
		
	
	} // construcror

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		// TODO Auto-generated method stub
		
		//������� ������� ��� ��������� ��� � ��
//		UIManager.setLookAndFeel(   UIManager.getSystemLookAndFeelClassName());
		
		
		//��� ������ ���������� ������� - �� ���������, �.�. � ���� ��� �����
							//������� ���������� 1 ��� � �����, ������� ��� �� �� �����
		 new GUI_JTable_2("JTable");

	}//main()


}// class
