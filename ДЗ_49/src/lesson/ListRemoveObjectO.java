package lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//�������� �������� � ����������

//����������� ������� � ���������� (Object o)
//contains(Object), remove(Object), equals(Object), indexOf(Object), lastIndexOf(Object)
//��� ������ ������ ���, ��� ��������� �������� ������, �.�. ���� �������� �� ����������.


public class ListRemoveObjectO {

	static Client  adrSpec=null;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//��������� �����
		Integer [ ] arrInt = {1,2,3,4,5,6,7, 3, 7, 3}; 

		List           <Integer>   collListInt = Arrays.asList( arrInt );  // ���� ������� ������, �� UnsupportedOperationException
		System.out.println(collListInt);
		
		//������1: ������� ����� 3 �� ���������
		int         etalon_Index = 3;  //����������� ��� ��� ����� � ��������() ��������� ��� ������ ���������� ��������
		Integer etalon2 = 3;            //������-������� ������� , ������� ����� ������� �� ���������
				//������� ��������� �������������� ���������
		List <Integer> listInt = new ArrayList(collListInt);
		listInt.remove(3);  //����� - ���� ����������� int - ������� �������������� �� ��� ������, � ��� ������ ���������� �����
		listInt.remove( etalon_Index); //� ��������� ���� ����������� int - ������� �� �������������� �� ��� ������, � ��� ������ ���������� �����
		listInt.remove( etalon2); 
		System.out.println("����� �������� ������ 1: " + listInt);
		
		//������2: ����� ��������� 3-�� � ���������
		etalon_Index =   listInt.lastIndexOf( etalon2 )	;  //���������� ������ ���������� ��������  ���� ��� (��������) 
		listInt.remove( etalon_Index ) ;
		System.out.println("����� �������� ������ 2: " + listInt);
		
		//������3: ����� ������ ����� 7 � ���������
		etalon2 = 7;
		int count = 0;
		// for (Integer elem : listInt) {  //��������� ���� ����� ������� �� ���������
		for (int i = 0; i < listInt.size(); i++) {

			//if ( elem == etalon2) {
			if ( listInt.get(i)   /*i-� ������� ���������*/ == etalon2) {
				count++;
			}//if
			if (count == 2) {  //������ 2-� �� ����� ������� ������
				//������� ��������� �������
				listInt.remove( i );  //���� ����������� ������� ���������, �� ������ ������ ������ �� ����� ����������
				break;
			} //if
			
		} //for
		
		System.out.println("����� �������� ������ 3: " + listInt);
		
		//������ 4: ������� ��� 7-��  ����� (� ������� ��������� ����-��������)
		listInt.add(1, 7);
					//������� 1 - ������� ��������� ��������� � ����� ���������
		//List coll7 = new ArrayList();  coll7.add( 7 );
		//listInt.removeAll(  coll7 );    //� ��������� ��������� ���������
		
				//������2 - � ������� ��������� - �������� - ����. ���������, ��������� �� 1-�� ��������, 
				//����� ��� �������� ������ � ��������, ���������� � ��������� ��������. 
		listInt.removeAll(  Collections.singletonList( etalon2) );  //������������� ��������� ��������� ��� ������� � ����� ������
		
		List  list = new ArrayList(7);  //7 - ��������� ������ ���������
			
		
		System.out.println("����� �������� ������ 4: " + listInt);
		
		//--------------------------------------- �������� ��������� � ��������� ��������-----------------------------------------
		Client [ ]  arrClients = {  new Client("������"),  new Client("������"), new Client("�������")    };
		List<Client> listCl= new ArrayList<Client>(  Arrays.asList( arrClients )   ) ;  //��� ����������� ���������
		System.out.println( listCl  );
		
		//������: � ��������� �������� ������� �������
		
		//������� 1: ������ �������
		String  strNameEtalon = "������" ;
		for (int i = 0; i < listCl.size(); i++) {
			//if ( listCl.get(i).name.equalsIgnoreCase( strNameEtalon)  ) {  //��������� ����
			if ( listCl.get(i).ID == 2)   {   //��������� ID  
			
				//��������� �����-������  ��� ���������� ��������
				adrSpec = listCl.get(i) ;
				
				listCl.remove( i ) ;		//�������� �� �������, � �������� ������-������ ������� �� ���������
			} //if
		} //for
		System.out.println( listCl  );
		
		//�������2 :  ����� ������� ������ remove(Object   o) 
		//listCl.add( new  Client("������") );		//��������� ������ �� �������� ��� ������ � �������� � ���������� ������� (Object  o)
				//�� ��������� ������� �������� ���� ����������� ��������� ������ �� ������, 
				//� ������������ ��� ������ ����� � ��������� ���� (Object o)
		Client  obj1 = new Client("������");
		listCl.add( obj1 );
		
			//�������� ������ �������� � ����� ��� �������
		//Client  etalonCl = new Client("������");
		Client  etalonCl = obj1;		//� ������� ���� �������� ������ �� ������� � ��������� ������
		listCl.remove( etalonCl);		//� ��������� ���� ������ ������, ��
										//��� �� �����  ��������� � ���������� ������� ������ - �.�. � ������� � ����������� ���� (Object �)
										//�����  ���������-��������  �������� �� ������  ������� � ���������
									//����������� �������� ����� �� ����� �������� � ���������� (Object o)
									//��� ������:  contains(Object), remove(Object), equals(Object), indexOf(Object), lastIndexOf(Object)
		
		//listCl.remove( listCl.get(2) );   //���� ������� �� ��������, �.�. �� �������� ����� �� �� ����� ������ �������� ��������
		
		//����� ������ ������ � �������� �������� - ���� �������� � ���������� ������.
		//����� ������ � ����� ��������-���������� ������� ������ ���� � ����� ����������,
		//� � ��������� ��������� ���������� ������ ����� ����� �������
		//��������: 	������ ������ � ����� �������  ������ ���� ����, 
		//							� � ���������� ����������� ������ �� ����� �������,
		//					����� ��������� ����� ���� ����� (��������� ����������, ������������, ���������, ����������)
		//					� ������ ������� - ����, � � ���������� ������ ��� ������
		//�����-������  ����� ����� ���� ��� ����� ������ � �����-�� ��������� � ����� � ������ ��������� 1 (�� ������ 77) 
		
		//� ����� ���� �����-������  ������������ � ������� � ���������� (Object o)  ��� �������� � ��� ������
		
		System.out.println( listCl  );
		
	} //main

}

class Client {
	int ID;  //���, ����������� ���������� ����� �������
	String  name;
	long    sum;
	
	static int countClients=0;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Client(String name) {
		//super();
		this.name = name;
		countClients++;  
		ID = countClients;
		
		//  ID = ++  countClients;
	}
	public Client() {
		//super();
		// TODO Auto-generated constructor stub
	}

	
	public Client(int iD, String name) {
		super();
		ID = iD;
		this.name = name;
	}
	//����� toString()  ���  ������ ��������
		@Override
	public String toString() {
		return "Client [ID=" + ID + ", name=" + name + ", sum=" + sum + "]\n" ;
	}
	
}