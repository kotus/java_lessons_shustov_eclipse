
package package_main.localhost;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CardCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Brend" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShopCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cardCode",
    "brend",
    "shopCode"
})
@XmlRootElement(name = "Add")
public class Add {

    @XmlElement(name = "CardCode", required = true)
    protected String cardCode;
    @XmlElement(name = "Brend", required = true)
    protected String brend;
    @XmlElement(name = "ShopCode", required = true)
    protected String shopCode;

    /**
     * Gets the value of the cardCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardCode() {
        return cardCode;
    }

    /**
     * Sets the value of the cardCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardCode(String value) {
        this.cardCode = value;
    }

    /**
     * Gets the value of the brend property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrend() {
        return brend;
    }

    /**
     * Sets the value of the brend property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrend(String value) {
        this.brend = value;
    }

    /**
     * Gets the value of the shopCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * Sets the value of the shopCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopCode(String value) {
        this.shopCode = value;
    }

}
