package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import arrays.Mass;

public class PayComboBox extends JFrame{
		
	private static final long serialVersionUID = 1L;
	//������� ���� � �������� �����
	private static final String [] arrayOfValutes = {"���", "����", "����"};
	
	JLabel label_platelhik = new JLabel("����������");
	JLabel label_poluchatel = new JLabel("����������");
	JLabel label_rr_1 = new JLabel("��������� ����");
	JLabel label_rr_2 = new JLabel("��������� ����");
	JLabel label_summa = new JLabel("�����");
	
	JTextField textField_rr_platelhik = new JTextField("", 12);
	JTextField textField_rr_poluchatel = new JTextField("", 12);
	JTextField textField_summa = new JTextField("", 10);
	
	JButton button_begin_tranzaction = new JButton("<html>������<br>����������");
	
	JList jlist_platelhik = new JList(getArrayOfPersons());
	JList jlist_poluchatel = new JList(getArrayOfPersons());
	
	// ������� ������ �������������� �� ����� ������� buttonGroup
	// � ����� ������������� ��������
	JRadioButton radioButtons_valutes [] = new JRadioButton [3];
	ButtonGroup buttonGroup = new ButtonGroup();
	{
		for (int i = 0; i < radioButtons_valutes.length; i++) {
			radioButtons_valutes[i] = new JRadioButton(arrayOfValutes[i]);
		}
		radioButtons_valutes[0].setSelected(true);
	}
	
	JCheckBox checkBox_commission = new JCheckBox("��������� ��������", true);
	
	public PayComboBox() throws HeadlessException {
		
		setLocation(300, 300); 
		setSize(600, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE ); 
		
		setLayout(null);
		//setLayout( new FlowLayout());
		
		Font font = new Font("Arial", Font.BOLD, 16);
		
		/*
		label_platelhik.setFont(font);
		label_platelhik.setForeground(Color.RED);
		label_platelhik.setBackground(Color.BLUE);
		label_platelhik.setOpaque(true);
		label_platelhik.setBounds(10, 10, 100, 20);
		add(label_platelhik);
		*/	
						
		add(jlist_platelhik);
		jlist_platelhik.setBackground(Color.BLUE);
		jlist_platelhik.setForeground(Color.blue);
		JScrollPane jScrollPanel_platelhik = new JScrollPane(jlist_platelhik);
		add(jScrollPanel_platelhik);
		
		/*
		Box box1 = Box.createVerticalBox();
		box1.add(label_platelhik);
		box1.add(Box.createVerticalStrut(6));
		box1.add(jScrollPanel_platelhik);
		*/
		/*
		label_poluchatel.setBounds(180, 10, 100,  20);
		label_poluchatel.setFont(font);
		label_poluchatel.setForeground(Color.RED);
		label_poluchatel.setBackground(Color.BLUE);
		label_poluchatel.setOpaque(true);
		add(label_poluchatel);
		*/
						
		add(jlist_poluchatel);
		
		JScrollPane jScrollPanel_poluchatel = new JScrollPane(jlist_poluchatel);
		add(jScrollPanel_poluchatel);
		
		/*
		Box box2 = Box.createVerticalBox();
		box2.add(label_poluchatel);
		box2.add(Box.createVerticalStrut(6));
		box2.add(jScrollPanel_poluchatel);
		*/
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				jScrollPanel_platelhik, jScrollPanel_poluchatel);
		
		splitPane.setPreferredSize(new Dimension(320, 90));
		add(splitPane);
		splitPane.setBounds(10, 10, 380, 160);
		splitPane.setDividerLocation(0.5);
		splitPane.setVisible(true);
		
		jScrollPanel_platelhik.setBorder(BorderFactory.createTitledBorder("����������"));
		jScrollPanel_poluchatel.setBorder(BorderFactory.createTitledBorder("����������"));
		
		label_rr_1.setBounds            (10, 170, 100,  20);
		textField_rr_platelhik.setBounds(10, 190, 150, 20);
		
		add(label_rr_1);
		add(textField_rr_platelhik);
		
		label_rr_2.setBounds             (180, 170, 100,  20);
		textField_rr_poluchatel.setBounds(180, 190, 150, 20);
		
		add(label_rr_2);
		add(textField_rr_poluchatel);
		
		label_summa.setBounds(400, 50, 150, 20);
		textField_summa.setBounds(400, 70, 100, 20);
		
		add(label_summa);
		add(textField_summa);
		
		button_begin_tranzaction.setBounds(220, 220, 100, 40);
		add(button_begin_tranzaction);
		
		//������ ���������� �������������� � �������� �� � ������
		for (int i = 0; i < radioButtons_valutes.length; i++) {
			radioButtons_valutes[i].setBounds(500, 35 + 30 * i, 70, 20);
			add(radioButtons_valutes[i]);
			buttonGroup.add(radioButtons_valutes[i]);
		}
		
		checkBox_commission.setBounds(400, 130, 200, 20);
		add(checkBox_commission);
		
		setVisible(true);
		
	}

	public static void main(String[] args) {
	
		new PayComboBox();
		
	}
	
	/**
	 * ����� ������������ �� ������ Mass ������ � ���������
	 * @return String[] ��������
	 */
	private String[] getArrayOfPersons(){
		
		String [][] matr1 = Mass.AddArr(Mass.arr1, Mass.arr2);
		String [][] matr2 = Mass.AddArr(matr1, Mass.arr3);
		return Mass.splitMatrix(matr2);
		
	}
	
}
