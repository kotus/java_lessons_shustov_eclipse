package gui;

import java.awt.FlowLayout;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FormNewClient extends JFrame{
	
	private static final long serialVersionUID = 2L;
	JLabel label_surename = new JLabel("�������");
	JLabel label_name = new JLabel("���");
	JLabel label_middlename = new JLabel("��������");
	JLabel label_numPassport = new JLabel("����� ���������");
	JLabel label_postNumber = new JLabel("�������� ������");
	JLabel label_identiCode = new JLabel("����������������� ���");
	
	JTextField textField_surename = new JTextField("", 12);
	JTextField textField_name = new JTextField("", 12);
	JTextField textField_middlename = new JTextField("", 12);
	JTextField textField_numPassport = new JTextField("", 12);
	JTextField textField_postNumber = new JTextField("", 12);
	JTextField textField_identiCode = new JTextField("", 12);
	
	public FormNewClient() throws HeadlessException {
		super("���� ������ � ����� ������� �����");
		
		setLocation(300, 300); 
		setSize(600, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE ); 
		
		add(label_surename);
		add(textField_surename);
		add(label_name);
		add(textField_name);
		add(label_middlename);
		add(textField_middlename);
		
		add(label_numPassport);
		add(textField_numPassport);
		add(label_postNumber);
		add(textField_postNumber);
		add(label_identiCode);
		add(textField_identiCode);
		
		setLayout(new FlowLayout());
		setVisible(true);
		
	}

	public static void main(String[] args) {
		
		new FormNewClient();

	}

}
