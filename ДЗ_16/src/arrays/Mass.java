package arrays;


public class Mass {
	
	public static String[][] arr1 = { { "�������", "����", "��������" },
			{ "�������", "����", "��������" },
			{ "�����", "�����", "��������" },
			{ "��������", "�����", "���������" },
			{ "������", "�����", "���������" } };
	public static String[][] arr2 = { { "������", "����", "��������" },
			{ "������", "����", "��������" },
			{ "�������", "�����", "���������" },
			{ "�������", "�����", "���������" },
			{ "������", "�����", "���������" } };
	public static String[][] arr3 = { { "����", "������������", "" }, 
			{ "����", "���", "" },
			{ "���", "����", "�������" } };
	
	public static void main(String[] args) {
	
		/*
		long arr1 [] = new long [50];
		for (int i = 0; i < arr1.length; i++) {
			arr1[i] = 6;
			System.out.print(arr1[i]);
		}
	
		long arr2 [] = new long [7];
		for (int i = arr2.length - 1; i >= 0; i--) {
			arr2[i] = i;
		}
		
		System.out.print("[ ");
		for (int i = 0; i < arr2.length; i++) {
			System.out.print(arr2[i] + " ");
		}
		System.out.print("]");
		*/
			
		/*
		String adr [] = {"��.���������� ������� ���������",
				"��.�������� ������� ������",
				"��.������ ������� ������",
				"��.������� ������� �����",
				"��.��������� ������� �����"};
		*/
		
		//������� 2 ������ ������  ���� ���� � 3 ������ � 4 ������� � ��������� ��� ������  ������� -1. ������� ���������� ������� �� �������.
		/*
		long arr[][] = new long [3][4];
		for (int i = 0; i < arr.length; i++) {
			System.out.print("\n"+ (i+1) +"-� ������ �������: ");
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = -1;
				System.out.print(arr[i][j] + "  ,  ");
			}
		}
		*/
		
		//2.2. ������� 2 ������ ������  ���� ���� � 3 ����� � 7 ��������, � ��������� ��� ������ �� ����������� �� 1 �� 21
		//� ����������� �� ��������� ������ � ������.  ������� ������ ����� ������� �� �����  �� 1� �� ��������� ������.
		/*
		long arr[][] = new long [3][7];
		int counter = 21;
		for (int i = arr.length - 1; i >= 0; i--) {
			for (int j = arr[i].length - 1; j >= 0; j--) {
				arr[i][j] = counter--;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			System.out.print("\n"+ (i+1) +"-� ������ �������: ");
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + "  ,  ");
			}
		}
		
		
		String[][] arr1 = { { "�������", "����", "��������" },
							{ "�������", "����", "��������" },
							{ "�����", "�����", "��������" },
							{ "��������", "�����", "���������" },
							{ "������", "�����", "���������" } };
		String[][] arr2 = { { "������", "����", "��������" },
							{ "������", "����", "��������" },
							{ "�������", "�����", "���������" },
							{ "�������", "�����", "���������" },
							{ "������", "�����", "���������" } };
		String[][] arr3 = { { "����", "������������", "" },
							{ "����", "���", "" }, 
							{ "���", "����", "�������" } };

		String [][] matr1 = Mass.AddArr(arr1, arr2);
		String [][] matr2 = Mass.AddArr(matr1, arr3);
		//Mass.showMatrix(matr2);
		String [] mass = Mass.splitMatrix(matr2);
		Mass.showArray(mass);	
		*/
		
	}

	/**
	 * ����� ��� �������� ���� ��������� ��������.
	 * @param arr1 1-� ��������� ������ ���� String[][].
	 * @param arr2 2-� ��������� ������ ���� String[][]. 
	 * @return ��������� ������ String [][].
	 */
	public static String[][] AddArr(String [][] arr1, String [][] arr2) {
		
		int rows = arr1.length + arr2.length;
		int cols = arr1[0].length;
		String [][] resArr = new String[rows][cols];
		
		int ind = 0;
		for (int i = 0; i < arr1.length; i++) {
			for (int j = 0; j < arr1[i].length; j++) {
				resArr[i][j] = arr1[i][j];
			}
			ind++;
		}
		
		for (int i = 0; i < arr2.length; i++) {
			for (int j = 0; j < arr2[i].length; j++) {
				resArr[ind][j] = arr2[i][j];
			}
			ind++;
		}
		
		return resArr;
		
	}
	
	/**
	 * �����, ������������� ��������� ������ ����� � ���������� ������ �����.
	 * @param matrix ���� <b>String [][]</b>, �������� ��������� ������.  
	 * @return ���������� ������ ���� <b>String []</b>.
	 */
	public static String [] splitMatrix(String [][] matrix){
		
		String result [] = new String[matrix.length];
		
		for (int i = 0; i < matrix.length; i++) {
			String tempString = "";
			for (int j = 0; j < matrix[i].length; j++) {
				tempString = tempString + matrix[i][j] + " ";
			}
			result[i] = tempString;
		}
		
		return result;
		
	}; 
	
	//����� ��� ������ ������� �� �����
	public static void showMatrix(String [][] arr) {
		
		String result = "";
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				result = result + arr[i][j] + " ";
			}
			result = result + "\n";
		}
		System.out.println(result);
		
	}
	
	//����� ��� ������ ������� �� �����
	public static void showArray(String [] arr){
	
		String result = "";
		for (int i = 0; i < arr.length; i++) {
			result = result + arr[i] + " ";
			result = result + "\n";
		}
		System.out.println(result);
		
	}
	
}
