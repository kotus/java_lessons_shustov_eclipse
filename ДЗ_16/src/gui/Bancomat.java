package gui;

public class Bancomat {

	private String numInvent;
	private String address;
	private Summ summ;
	private Integer errorCode;
	private static int bmatCount;
	private static int bmatErrNum;
	
	static{
		Bancomat.bmatCount = 0;
		Bancomat.bmatErrNum = 0;
	}
	
	public static int getBmatErrNum() {
		return bmatErrNum;
	}
	public static void setBmatErrNum(int bmatErrNum) {
		Bancomat.bmatErrNum = bmatErrNum;
	}
	public static int getBmatCount() {
		return bmatCount;
	}
	public static void setBmatCount(int bmatCount) {
		Bancomat.bmatCount = bmatCount;
	}
	public String getNumInvent() {
		return numInvent;
	}
	public void setNumInvent(String numInvent) {
		this.numInvent = numInvent;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public static void main(String args[]){
		
		Bancomat bancomat = new Bancomat("1", "���. ���������� ����������� �����", 1000L, "UAH");
		
		System.out.println("�� �������� � ���������:" + bancomat.getSumm().getSumm() + " " +
									bancomat.getSumm().getValuta());
		
		boolean result = bancomat.sumOut(new Summ(500L, "UAH"), 1000000000000004L);
		System.out.println(result);
		
		System.out.println("����� �������� � ���������:" + bancomat.getSumm().getSumm() + " " +
				bancomat.getSumm().getValuta());
		
	}
		
	public Bancomat(String numInvent, String address) {
		super();
		this.numInvent = numInvent;
		this.address = address;
		Bancomat.bmatCount++;
	}
	
	public Bancomat(String numInvent, String address, Long summ, String valuta) {
		super();
		this.numInvent = numInvent;
		this.address = address;
		this.summ = new Summ(summ, valuta);
		Bancomat.bmatCount++;
	} 
	
	/**
	 * ����� ������ ������� �� ���������
	 * @param sumOutPar ���� Summ - ����� ������.
	 * @param cardNumber ���� Long - ����� ���������� �����.
	 *
	 * @return Boolean - ��������� ���������� ��������
	 */
	public Boolean sumOut(Summ sumOutPar, Long cardNumber){
		
		BankCard[] arrOfCards = BankCard.makeMassCard(BankCard.makeMassNums(10),
														"UAH", 500L);
		for (int i = 0; i < arrOfCards.length; i++) {
			if (arrOfCards[i].getCardNumber().longValue() == cardNumber
					.longValue()) {

				if (arrOfCards[i].getSumm().getSumm() - sumOutPar.getSumm() >= 0) {
					// ��������� � �����
					arrOfCards[i].getSumm().setSumm(
							arrOfCards[i].getSumm().getSumm()
									- sumOutPar.getSumm());
					// ��������� � ���������
					this.summ
							.setSumm(this.summ.getSumm() - sumOutPar.getSumm());
					return true;
				} else {
					System.out.println("������������ ����� �� �����!");
					return false;
				}

			}
		}
		System.out.println("����� �� �������!");
		return false;
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "����� Bancomat:[����� = "+numInvent+"; ����� = "+address+";"+
				summ.toString()+"]";
	}
	

	
}
