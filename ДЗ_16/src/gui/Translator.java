package gui;

import java.awt.HeadlessException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

public class Translator extends JFrame{

	private static final long serialVersionUID = 1L;
	
	JLabel lable_english_word = new JLabel("���������� �����");
	JLabel lable_russian_word = new JLabel("������� �����");
	JLabel lable_way_of_tramslate = new JLabel("����������� ��������");
	JLabel lable_choise = new JLabel("(������� �� ���������� ���������)");
	
	JComboBox combobox_english_word = new JComboBox();
	JComboBox combobox_russian_word = new JComboBox();
	
	JButton button_check_translate = new JButton("<html><p align = center>���������<br>�������</html>");
	
	JRadioButton radioButton_English_Russian = new JRadioButton("���������� -> �������");
	JRadioButton radioButton_Russian_English = new JRadioButton("������� -> ����������");
	ButtonGroup buttonGroup = new ButtonGroup();
	
	JCheckBox checkBox_test_mode = new JCheckBox("����� �����");
	
	public Translator(String title) throws HeadlessException {
		super(title);
		
		setSize(500, 250);
		setLocation(300, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		setLayout(null);
		
		lable_english_word.setBounds(10, 20, 150, 20);
		add(lable_english_word);
		
		combobox_english_word.setBounds(10, 40, 200, 20);
		add(combobox_english_word);
		
		lable_russian_word.setBounds(10, 70, 150, 20);
		add(lable_russian_word);
		
		combobox_russian_word.setBounds(10, 90, 200, 20);
		add(combobox_russian_word);
		
		button_check_translate.setBounds(40, 140, 100, 40);
		add(button_check_translate);
		
		lable_way_of_tramslate.setBounds(300, 20, 150, 20);
		add(lable_way_of_tramslate);
		
		radioButton_English_Russian.setBounds(300, 50, 200, 20);
		radioButton_Russian_English.setBounds(300, 70, 200, 20);
		radioButton_English_Russian.setSelected(true);
		buttonGroup.add(radioButton_English_Russian);
		buttonGroup.add(radioButton_Russian_English);
		
		add(radioButton_English_Russian);
		add(radioButton_Russian_English);
		
		checkBox_test_mode.setBounds(300, 100, 200, 20);
		add(checkBox_test_mode);
		
		lable_choise.setBounds(250, 130, 220, 20);
		add(lable_choise);
		
		setVisible(true);
		
	}



	public static void main(String[] args) {
	
		new Translator("����������");

	}

}
