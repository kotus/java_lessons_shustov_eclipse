package gui;

import arrays.Mass;

public class Client extends Person{
	
	private BankCard bankCard;
	private Account account = new Account();
	
	public BankCard getBankCard() {
		return bankCard;
	}
	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Client(String surname, String name, String middleName) {
		super(surname, name, middleName);
		this.account.setAccCod((byte)0);
		this.account.setSumm(new Summ(0L, "USD"));
	}
		
	public Client(String surname, String name, String middleName, BankCard bankCard) {
		super(surname, name, middleName);
		this.bankCard = bankCard;
	}
	
	public Client(String surname, String name, String middleName,
			BankCard bankCard, Account account) {
		super(surname, name, middleName);
		this.bankCard = bankCard;
		this.account = account;
	}
	
	//������� � ������� �����, �� ����� ������� �������
	public String payBCard(Summ summa, Client client){
		
		Long summaThisClient = this.bankCard.getSumm().getSumm();		
		Long summaComingClient = client.bankCard.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();
		
		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0){
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0){
			return "������������ ������� ��� ������";
		}
		
		this.bankCard.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.bankCard.getSumm().setSumm(summaComingClient + summaOfPayment);
				
		return "OK";
		
	}
	
	public static void main(String[] args) {
		
		String [][] matrixOfClients = Mass.AddArr(Mass.AddArr(Mass.arr1, Mass.arr2),
													Mass.arr3);
		Client [] arrOfClients =  Client.makeMassClient(matrixOfClients);
		
		Client.testMassClient(arrOfClients);
		
	}
	
	/*
	3.1.	� ������ ������ �������� ����� makeMassClient(� ��������� ��������� 2-������ ������ � ���),
	 � ���������� ������ ��������. ����� ������ ������ ���� �������� ��� ���������� ���� �����������
	  ������� � ��� � ���������.
	3.1.1.	������� ���� ����� � �����() ������ ������ �������� ������ �������� �� ������� ��� .
	 ������ � ��� ������� ��� � ��_14  � ������� ������ ����������� ��������.
	{"�������",    "����", "��������"},
					 {"�������",    "����", "��������"},
					 {"�����",     "�����", "��������"},
					 {"��������", "�����", "���������"},
					 {"������", "  �����", "���������"},

	{"������", "����", "��������"},
		                 {"������", "����", "��������"},
		                 {"�������", "�����", "���������"},
		                 {"�������", "�����", "���������"},
		                 {"������", "�����", "���������"}

	"����", "������������",""},
		                 {"����", "���",""},
		                {"���", "����", "�������"},
	3.1.2.	�������� ����� testMassClient() ��������� �� ������� ��� ��������, � ����� ������ ��� ������ �������. ������ ��� ����� ������ �� ��������, � �� �� ������� �����.
	3.1.2.1.	������� ���� ����� � �����() ������ ������ � ���������, ������������� �� � ������� �������� ��� �������� ��������� � ��������  ����� � ��� .
	*/
	
	/**
	 * ����������� �����, ��������� ������ �������� Client[] �������� �� ����������� � ����
	 * ���������� �������.
	 * 
	 * @param fio - ��� String[][], ��������� ������ ��� �������: { { "������", "����", "��������" } , ...} 
	 * 
	 * @return arrClients - ��� Client [], ���������� ������ �������� ���� Client. 
	 */
	public static Client[] makeMassClient(String[][] fio) {
		
		Client [] arrClients = new Client[fio.length];
		for (int i = 0; i < arrClients.length; i++) {
			arrClients[i] = new Client(fio[i][0], fio[i][1], fio[i][2]);
		}
		
		return arrClients;
		
	}
	
	/**
	 * ����� ����������� ������ ������ makeMassClient, �� �������� ������� �������� ���� Client
	 * 
	 * @param arrOfClients - ��� Client [], ���������� ������ �������� ���� Client.  
	 */
	public static void testMassClient(Client [] arrOfClients) {
		
		for (int i = 0; i < arrOfClients.length; i++) {
			System.out.println(arrOfClients[i]);
		}		
		
	}
	
	@Override
	public String toString() {
		return "����� Client:[�������=" + getSurename() + "; ���=" + getName()
				+ "; ��������=" + getMiddleName() + "]";
	}
			
}
