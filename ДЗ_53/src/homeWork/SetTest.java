package homeWork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeSet;

public class SetTest {

	public static void main(String[] args) {


		ArrayList<ClientInner> innerClients = new ArrayList<ClientInner>();
		innerClients.add(new ClientInner(1, "������ ���� ��������"));
		innerClients.add(new ClientInner(2, "������ ���� ��������"));
		innerClients.add(new ClientInner(3, "������� ����� ��������"));
		innerClients.add(new ClientInner(4, "������� ����� ���������"));
		innerClients.add(new ClientInner(5, "������ ����� ���������"));
		innerClients.add(new ClientInner(5, "������ ����� ���������"));
		innerClients.add(new ClientInner(7, "������ ����� ���������"));
		
		TreeSet<ClientInner> treeSetClients = new TreeSet<ClientInner>(innerClients);
		
		HashSet<ClientInner> hashSetClients = new HashSet<ClientInner>(innerClients);
		System.out.println(hashSetClients);
		
		
	}

}


class ClientInner implements Comparable<ClientInner>{
	
	Integer iD;
	String fio;
	
	public ClientInner(Integer iD, String fio) {
		super();
		this.iD = iD;
		this.fio = fio;
	}

	@Override
	public String toString() {
		return "ClientInner [iD=" + iD + ", ���=" + fio + "]";
	}

	@Override
	public int compareTo(ClientInner Client_2) {
		return this.iD.compareTo(Client_2.iD);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fio == null) ? 0 : fio.hashCode());
		result = prime * result + ((iD == null) ? 0 : iD.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientInner other = (ClientInner) obj;
		if (fio == null) {
			if (other.fio != null)
				return false;
		} else if (!fio.equals(other.fio))
			return false;
		if (iD == null) {
			if (other.iD != null)
				return false;
		} else if (!iD.equals(other.iD))
			return false;
		return true;
	}
	
}