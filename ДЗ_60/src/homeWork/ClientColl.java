package homeWork;

import java.util.TreeSet;

import bankSystem.Client;

public class ClientColl extends TreeSet<Client>{
	
	private static final long serialVersionUID = 1L;
	private TreeSet<Client> treeSetOfClients;
			
	public ClientColl(TreeSet<Client> treeSetOfClients) {
		super(treeSetOfClients);
		this.treeSetOfClients = treeSetOfClients;
	}

	public Long myGetSummFromAllClients() {
		
		Long summ = 0L;
		for (Client client : treeSetOfClients) {
			summ = summ + client.getAccount().getSumm().getSumm();
		}
		return summ;
		
	}
	
}
