package pack1;

public class Client {

	private String surname;
	private String name;
	private String patronymic;
	private BankCard bankCard;
	private Account account = new Account();
		
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	public BankCard getBankCard() {
		return bankCard;
	}
	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Client(){
		
	};
	
	public Client(String surname, String name, String patronymic) {
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.account.setAccCod((byte)0);
		this.account.setSumm(new Summ(0L, "USD"));
	}
		
	public Client(String surname, String name, String patronymic, BankCard bankCard) {
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.bankCard = bankCard;
	}
	
	public Client(String surname, String name, String patronymic,
			BankCard bankCard, Account account) {
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.bankCard = bankCard;
		this.account = account;
	}
	
	//������� � ������� �����, �� ����� ������� �������
	public String payBCard(Summ summa, Client client){
		
		Long summaThisClient = this.bankCard.getSumm().getSumm();		
		Long summaComingClient = client.bankCard.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();
		
		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0){
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0){
			return "������������ ������� ��� ������";
		}
		
		this.bankCard.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.bankCard.getSumm().setSumm(summaComingClient + summaOfPayment);
				
		return "OK";
		
	}
	
	public static void main(String[] args) {
		
		/*Client client1 = new Client("", "client1", "", new BankCard(1234567732144563L, new Summ(1000L, "978")));
		Client client2 = new Client("", "client2", "", new BankCard(3215369874521254L, new Summ(2000L, "978")));
		
		System.out.println("������ 1 ����� = " + client1.getBankCard().getSumm().getSumm());
		System.out.println("������ 2 ����� = " + client2.getBankCard().getSumm().getSumm());
		
		String result = client1.payBCard(new Summ(1000L, "978"), client2);
		if (result != "OK") {
			System.out.println(result);
			return;};
		
		result = client2.payBCard(new Summ(2000L, "978"), client1);
		if (result != "OK") {
			System.out.println(result);
			return;};
		
		System.out.println("����� ����� ��������:");	
		System.out.println("������ 1 ����� = " + client1.getBankCard().getSumm().getSumm());
		System.out.println("������ 2 ����� = " + client2.getBankCard().getSumm().getSumm());*/
		
		String arrOfNames [][] = {{"�������", "����", " ��������"},
		                         {"�������", "����", " ��������"},
		                         {"�����", "�����", " ��������"},
		                         {"��������", "�����", " ���������"},
		                         {"������", "�����", " ���������"}};
	
		//Bancomat [] arrOfBancomats = new Bancomat [25];
		Client [] arrOfClients = new Client [5];
		
		for (int i = 0; i < arrOfClients.length; i++) {
			arrOfClients[i] = new Client(arrOfNames[i][0], arrOfNames[i][1], arrOfNames[i][2]);
		}
		
		/*
		for (int i = 0; i < arrOfClients.length; i++) {
			System.out.println(arrOfClients[i]);
		}
		*/
		
		Client client = new Client();
		client.massClientOut(arrOfClients);
		
		
	}
	
	void massClientOut(Client[] massClientPar){
		
		for (int i = 0; i < massClientPar.length; i++) {
			System.out.println(
					"����� Client:[���=" + massClientPar[i].surname + 
					"; ���=" + massClientPar[i].name + 
					"; ��������=" + massClientPar[i].patronymic + "]");
		}
		
	}
	
	@Override
	public String toString() {
		return "����� Client:[���=" + surname + "; ���=" + name
				+ "; ��������=" + patronymic + "]";
	}
	
		
}
