package gui;

public class BankCard {
	
	private Long cardNumber;
	private String expDate;
	private Short cvCode;
	private Summ summ;
	private Client client;
	
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public Short getCvCode() {
		return cvCode;
	}
	public void setCvCode(Short cvCode) {
		this.cvCode = cvCode;
	}
	public Summ getSumm() {
		return summ;
	}
	public void setSumm(Summ summ) {
		this.summ = summ;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	public BankCard(Long cardNumber) {
		this.cardNumber = cardNumber;
		this.cvCode = 0;
		this.expDate = "";
		this.summ = new Summ((long)0, "UAH");
	}
	
	public BankCard(Long cardNumber, String expDate, Short cvCode, Long summ, String valuta) {
		super();
		this.cardNumber = cardNumber;
		this.expDate = expDate;
		this.cvCode = cvCode;
		this.summ = new Summ(summ, valuta);
	}
	
    public BankCard(Long cardNumber, Summ summ) {
		super();
		this.cardNumber = cardNumber;
		this.summ = summ;
	}

	public BankCard(Long cardNumber, Summ summ, Client client) {
		this.cardNumber = cardNumber;
		this.summ = summ;
		this.client = client;
	}
    
    public static void main(String args[]){
	
    	Long arrOfNumbers[] = BankCard.makeMassNums(5);
    	BankCard bankCards[] = BankCard.makeMassCard(arrOfNumbers, "UAH", 500L);
    
    	BankCard.testMassBankCard(bankCards);
        
	}
    
    /**
     * ����������� �����, ��������� ������ ������� ���� � ������� ������ � 16 ����, 
     * �� 1000 0000 0000 0000 � ����������� + 1.
     * 
     * @param massCount - ��� Integer, ���������� ������������ �����.
     *  
     * @return arrOfNumbers[] - ��� Long, ������ ������� ����.  
     */
    public static Long[] makeMassNums(Integer massCount){
		
    	Long arrOfNumbers[] = new Long[massCount];
		Long variable = new Long(1000000000000001L);
		for (int i = 0; i < arrOfNumbers.length; i++) {
			arrOfNumbers[i] = variable;
			variable++;
		}
		return arrOfNumbers;
    	
	}
    
    /**
     * ����������� ����� ������������ �� ������� ������� ����,
     * ������ �������� ���� BankCard.
     * 
     * @param cardNums - ��� Long[], ������ ������� ����.
     * @param currency - ��� String, ������������� ������, �������� "UAH".
     * @param startMoney - ��� Long, ��������� ����� �� ������.
     * 
     * @return BankCard [] - ������ �������� ���� BankCard
     */
    public static BankCard [] makeMassCard(Long[] cardNums, String currency, Long startMoney) {
		    	
		BankCard[] arrOfCards = new BankCard[cardNums.length];
		
		for (int i = 0; i < arrOfCards.length; i++) {
			arrOfCards[i] = new BankCard(cardNums[i], new Summ(startMoney, currency));
		}
    	    	
    	return arrOfCards;
		
	}
    
    /**
     * ����� �����������, ����� �� �������� ������ �������� ����� ������ �� ����� ������� ��������,
     * �� ������� ��������.
     * @param bankCards [] - ������ �������� ���� BankCard.
     */
    public static void testMassBankCard(BankCard [] bankCards) {
    	
    	for (int i = 0; i < bankCards.length; i++) {
			System.out.println(bankCards[i]);
		}
    	
	}
    
	@Override
	public String toString() {
		return "����� BankCard:[cardNumber=" + cardNumber + "; expDate=" + expDate
				+ "; cvCode=" + cvCode + ";" + summ + "]";
	}
	
}
