package gui;

import java.io.IOException;

import parser.ParserStaff;
import parser.ParserUniversal;

public class Staff extends Person{
	
	private String post;
	private String department;
	private Summ salary;
	
	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Summ getSalary() {
		return salary;
	}

	public void setSalary(Summ salary) {
		this.salary = salary;
	}

	public Staff(String surename, String name, String middleName,
			String post, String department, Summ salary) {
	
		super(surename, name, middleName);
		this.post = post;
		this.department = department;
		this.salary = salary;
		
	}
	
	public Staff() {
		
		super();
					
	}

	public static void main(String[] args) throws IOException{
		
		/*
		String [][] staffArray = {{"������", "�����", "������", "����������� ������", "����� - ����������", "0"},
								{"��������", "�����", "�����������", "������", "����� - �����", "0"},
								{"�����������", "������", "��������������", "��������������", "����� ��������", "0"},
								{"�������", "������", "��������", "���.������������", "�����-����������", "0"}};
		
		Staff [] staffs = Staff.makeMassStaff(staffArray);
		Mass.showArrayOfObjects(staffs);
		*/
		
//		Staff [] staffs = Staff.makeArrStaff("src/staff.in");
//		testArrStaff(staffs);
		
		
//		Staff staffs[] = null;
//		try {
//			staffs = ParserStaff.makeMassStaffs("src/staff.in");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		for (int i = 0; i < staffs.length; i++) {
//			System.out.println(staffs[i]);
//		}
		
		Staff [] staffs = Staff.makeArrStaff("src/staff.in");
		testArrStaff(staffs);
		
		
	}
			
	/**
	 * �����, ��� �������� ������� ���� <b>Staff[]</b>
	 * @param arrOfWorkers ���� <b>String[][]</b>, ��������� ������.
	 * @return arrOfStaffs ���� <b>Staff[]</b>, ���������� ������ �������� ���� <b>Staff</b>.
	 */
	public static Staff[] makeMassStaff(String[][] arrOfWorkers) {
		
		Staff[] arrOfStaffs = new Staff[arrOfWorkers.length];
		
		for (int i = 0; i < arrOfWorkers.length; i++) {
			Staff staff = new Staff(arrOfWorkers[i][0], arrOfWorkers[i][1], arrOfWorkers[i][2], 
					"", arrOfWorkers[i][4], new Summ(Long.parseLong(arrOfWorkers[i][5]), "UAH"));
			arrOfStaffs[i] = staff;
		}
		
		return arrOfStaffs;
		
	}
	
	public static Staff[] makeArrStaff(String fileName) throws IOException{
		
		ParserUniversal.parserManagerClient("src/staff.in");
		
		return ParserUniversal.arrStaff;
		
	}
	
	private static void testArrStaff(Staff[] staffs) {
		
		for (int i = 0; i < staffs.length; i++) {
			System.out.println(staffs[i]);
		}
		
	}
	
	@Override
	public String toString() {
		return "����� Staff:[�������=" + getSurename() + "; ���=" + getName() + "; ��������=" + getMiddleName() + 
				" ���������=" + post + "; �����=" + department + "; ���.�����=" + salary + "]";
	}
	
}
