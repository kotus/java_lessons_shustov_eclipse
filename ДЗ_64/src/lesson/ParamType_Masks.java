package lesson;

import java.util.Collections;
import java.util.List;

import javax.swing.JButton;

//����� ���������� ������������
// - � ���������� �������
//� ���������� ���������
//� ������������ ���� ������ ��� �������� ������
//� ���������� ���������� ��� �������� ������


//����� ����������
//�������, ����� � ��������� ����� ��������� �������� ������ � ��� ���-�������
//����� �������� �� ����� ����������� � ����� ��������� ����� ������� �� ������, � ���������� �����
	//������ ������ � ���������� ����� ������� �����   ��������
	//��� ����� � ����� ����� ����������
//�������� ������ Person  � ��� ���-������� �������� ��������� � ��������� 
//� Client-��  �  staff-��

//��������� ������ ��-��������� - �.�. �� ��������� ������� ��������������, 
//���� ���������� � �����-�����

//��������� � ��������� - �� ��������, �.�. ��� ��������� ������������ ����� ��� 
//� ���������, �� ������ ���� ��� � ������� ����� � ��� �� ���������

//������� ��� ����������, ������ ������� - ��������� ������� ����� ��������� ���������, 
//�� ��������� ���������� �������� �����

//����� ���������� ���� 2� �����:
	//1. ����� ���� extends - ������������, ����� ������ � ���� ������ ����������� � ������ ������ �� ����� =  
	//2. ����� ���� super -     ������������, ����� ������ � ���� ������ ����������� � ������ �����   �� ����� =
	//3. ���� ������ ������������ � ������ �� = � ����� �� = -  �� ����� ����� �� ����� ������
	//4. <?>   -  �������� ����� ���, �� �������������� ������������ - ������������ �����  �������� � �� �����.
	//5. ������  �� NEW  ����� ����������
	
		
	//��� ���� ��� �������� ������������� ����� ����� ��������� �� �������� ������ � ����� �� = 
	//1. � ��������� return  coll ;     // ��� �� ���� ������ ������ �� = 
	//int met(int par) {return par;}      int  x = met(2);   met(2) = x;  - ������
	//2. ��� ����������� - ���� �������� �������� (��� ��������), � ���� �������� ���������� - (���� ����������)
	//��� ��������� (��������� ������ �������� ��������) - ������ ������ �� =   (�������� ������ ����� ���� extends)
	//��� ���������� (��������� ���� ������� ��������) -  ������ ����� �� = (�������� ������ ����� ���� super)
	//
	
//       ���� �������� � ��������� � ������� � ������������ - �� ����� �� ����� ������, 
//     � ����� ���������� �������� �1 � ��������� ���� ��� ����� ��� �������� ����� <?> 




public class ParamType_Masks  <T extends Point>  /*<T super Cube>*/ {  //? � super   � �������� ������ ����������
//<T extends Point>  - ����� ����� �����������, �� �������� ����� ������ ������ Point
	
	//������� ���������� ���������� � ����������� ����
	void met(List par){};  //��������� � Object-����������. ����� � ��� ������� ������ ����, �� 
										//�������� �� ������� ���� ��������� ����������. - � ������������ ����������� ���������
	void met1(List<Integer> par){};  //��������� - �.�. �������� ����� �� ����� ���������� - ��
														//��������� ��-��������� - �.�. �� �������������
	<T> void met2(List<T> par){};      //������������� - ��������� ����� ��� ������ �, 
													//�� ������ ��������� ����� ���� ������ �������� ����������� ����
	
	
	//������� �����:
	//1. ����� ������������� ����� - ������ � ������������ ���������� ���
	void  metMask(List < ?  extends Point>  par ) {};  //� ��������� ���� ����� ����� Point ��� ��� ������ ���������� 
	
	//2. ����� ������������� ����� - � ����� � 
<T>	void  metMask2(List < ?  extends T>  par ) {};  //� �������� ����� ���� ������ ����,  � � ��� �����������
<T>	void  metMask2_1(List <T>  par ) {};   //����� ��� ����� ���� � ��������� �� ����������


//3. ����� ���� ����� ����� �����������, ��� �
void  metMask3(List < ?  super Cube>  par ) {};  

//4.  ����� ���� ����� , �����  �����������, � �
<E>void  metMask4(List < ?  super E>  par ) {};

//5. ������ � ������������ ������
<T>   List<? super T>  metMask5(List <? super T> par1,  T par2) {
	//���������� 2� �������� ������������, �.�. ��� ����� �� ����� ���������� ��� ������ �� ������
	//� ���� �� ����� ���� ��������, �.�. � ���� ��������, � ��������� � ������ �� ��������
	return par1;
}

//������������ ����� extends � super � ��������� � � �������� ����������
//�.�. �������� ������������ ��������� ������ ��� ��� � .
//<T>   List<? extends T>  metMask6(List <? super T> par1,  T par2) {

<T>   List<? extends T>  metMask6(List <? extends T> par1,  T par2) {
	//���������� 2� �������� ������������, �.�. ��� ����� �� ����� ���������� ��� ������ �� ������
	//� ���� �� ����� ���� ��������, �.�. � ���� ��������, � ��������� � ������ �� ��������
	return par1;
}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	//Arrays.
	//	Collections. //����� �������� ������� � �������
	//
		
	} //main

}

//��������� ������� ����� �������� ��� ��������� ������������� ����� ����������
class Point { int x,y; void  met1 (int x, int y) { this.x = x; this.y = y ;}  };
class Square extends Point    {  int  s; } //������� ������
class Cube   extends Square {  int  v; } //����� ������

