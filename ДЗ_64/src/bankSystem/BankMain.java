package bankSystem;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Deque;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import operations.Operation;
import arrays.Mass;

public class BankMain {

	public static ArrayList<Bankomat> arrOfBancomats;
	public static ArrayList<Staff> arrOfStaffs;
	public static ArrayList<Client> arrayListOfClients;
	public static ArrayList<BankCard> arrayOfBankCards;
	public static Deque<Operation> queueOfOperations;
	
	public BankMain() throws IOException {
		
		arrOfBancomats = Bankomat.makeCollBankomat("src/bancomats.in");
		arrOfStaffs = Staff.makeCollStaff("src/staff.in");
		arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
		arrayOfBankCards = BankCard.makeCollOfBankCards("src/master-visa-2014-05-14---01.in");
		
		makeMassBCardGiveOut();
		
//		testMassBCardGiveout();
		
//		TreeSet<BankCard> setOfCards = Client.createSetOfBankCards(arrayListOfClients);
//		System.out.println("Set of BankCards:");
//		System.out.println(setOfCards);
			
	}
	
	private void makeMassBCardGiveOut() throws IOException {
		
		ArrayList<String> listFromString = getDataFromFileBankCardsToClient("src/BCards_to_clients.out");
		for (String stringFromFile : listFromString) {
			
			String[] tempArrayOfString = stringFromFile.split("[\t]+");
			
			BankCard bankCard = findStringAboveCardNumber(tempArrayOfString[4]);
			if (bankCard != null) {
				Client client = arrayListOfClients.get(Integer.parseInt(tempArrayOfString[3]));
				setBankCardToClient(client, bankCard);
				setClientToBankCard(bankCard, client);
			} 
			
		}	
		
	}

	public static void main(String[] args) throws IOException {
		
		BankMain mainBank= new BankMain();
		testMassAll();
	
	}//main

	public static void testMassAll() {
		
		Bankomat.testCollBankomats(arrOfBancomats);
		Staff.testCollStaff(arrOfStaffs);
		Client.testCollClient(arrayListOfClients);
		BankCard.testCollBankCards(arrayOfBankCards);
		
	}
	
	public static void testMassBCardGiveout(){
				
		int i = 1;
		for (Client client : arrayListOfClients) {
			
			System.out.println(i + ". ������:" + client.toString() + " ���� ����� �� �������:" + client.getBankCard().toString() + "\t" +
			" ������ �� ���� �����: " + client.getBankCard().getClient().toString());
			i++;
		}
			
	}
	
	@SuppressWarnings("resource")
	private ArrayList<String> getDataFromFileBankCardsToClient(String path) throws IOException {
		
		ArrayList<String> list = new ArrayList<String>();
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
		
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			list.add(line);
		}
		
		return list;
			
	}
		
	private BankCard findStringAboveCardNumber(String cardNumber) {
		
		for (BankCard bankCard : arrayOfBankCards) {
			String currCardNumber = Long.toString(bankCard.getCardNumber());
			if(currCardNumber.equalsIgnoreCase(cardNumber)){
				return bankCard;
			}
		}
		
		System.out.println("������ � ������� ����� "+ cardNumber + " �� �������!!!");
		
		return null;
		
	}
	
	private void setBankCardToClient(Client client, BankCard bankCard) {
		client.setBankCard(bankCard);
	}
	
	private void setClientToBankCard(BankCard bankCard, Client client) {
		bankCard.setClient(client);
	}
	
	
}
