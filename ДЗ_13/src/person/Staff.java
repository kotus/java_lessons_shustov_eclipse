package person;

public class Staff extends Person{
	
	private String post;
	private String department;
	private Summ salary;
	
	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Summ getSalary() {
		return salary;
	}

	public void setSalary(Summ salary) {
		this.salary = salary;
	}

	public Staff(String surename, String name, String middleName,
			String post, String department, Summ salary) {
	
		super(surename, name, middleName);
		this.post = post;
		this.department = department;
		this.salary = salary;
		
	}

	public static void main(String[] args) {
		
		
		
	}
	
	@Override
	public String toString() {
		return "����� Staff:[�������=" + getSurename() + "; ���=" + getName() + "; ��������=" + getMiddleName() + 
				"���������=" + post + "; �����=" + department + "; ���.�����=" + salary + "]";
	}

}
