package person;

public class Client extends Person{
	
	private BankCard bankCard;
	private Account account = new Account();
	
	public BankCard getBankCard() {
		return bankCard;
	}
	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Client(String surname, String name, String middleName) {
		super(surname, name, middleName);
		this.account.setAccCod((byte)0);
		this.account.setSumm(new Summ(0L, "USD"));
	}
		
	public Client(String surname, String name, String middleName, BankCard bankCard) {
		super(surname, name, middleName);
		this.bankCard = bankCard;
	}
	
	public Client(String surname, String name, String middleName,
			BankCard bankCard, Account account) {
		super(surname, name, middleName);
		this.bankCard = bankCard;
		this.account = account;
	}
	
	//������� � ������� �����, �� ����� ������� �������
	public String payBCard(Summ summa, Client client){
		
		Long summaThisClient = this.bankCard.getSumm().getSumm();		
		Long summaComingClient = client.bankCard.getSumm().getSumm();
		Long summaOfPayment = summa.getSumm();
		
		if (summaOfPayment == 0) {
			return "����� � �������� = 0";
		} else if (summaThisClient == 0){
			return "� ����������� 0 �� �����";
		} else if ((summaThisClient - summaOfPayment) < 0){
			return "������������ ������� ��� ������";
		}
		
		this.bankCard.getSumm().setSumm(summaThisClient - summaOfPayment);
		client.bankCard.getSumm().setSumm(summaComingClient + summaOfPayment);
				
		return "OK";
		
	}
	
	public static void main(String[] args) {
		
		Client objClient1 = new Client("������", "����", "��������");
		Client objClient2 = new Client("������", "����", "��������");
		Client objClient3 = new Client("�������", "����", "���������");
		
		System.out.println(objClient1);
		System.out.println(objClient2);
		System.out.println(objClient3);
		
	}
	
	@Override
	public String toString() {
		return "����� Client:[�������=" + getSurename() + "; ���=" + getName()
				+ "; ��������=" + getMiddleName() + "]";
	}
			
}
