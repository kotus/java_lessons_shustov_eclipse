package calculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class CalculatorProcess implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent event) {
		
		String[] arrOfNumbers = {"1","2","3","4","5","6","7","8","9","0"};
		String[] arrOfOperands = {"+","-","*","/"};
		String eventString = event.getActionCommand();
		
		if (stringInArray(arrOfNumbers, eventString)) {
			
			System.out.println("���� ������ ����� "+eventString);
			
			processClickNumberButton(eventString);			
			renewMainTextfield();
			
		} else if (stringInArray(arrOfOperands, eventString)){
			
			System.out.println("��� ����� ������� "+eventString);
			
			processClickOfOperand(eventString);
			renewMainTextfield();
			
		} else if (event.getActionCommand() == "="){
			
			System.out.println("���� ������ = ");
			
			calculate();
			
		} else if (event.getActionCommand() == "C"){
			
			System.out.println("���� ������ � ");
			resetMainTextfield();
			
		} else if (event.getActionCommand() == ",") {
			
			System.out.println("���� ������ ,");
			processClickButtonComa(eventString);
			
		}
		
	}
	
    private void processClickButtonComa(String eventString) {
		
		if (Calculator.operation == "") {
			if ( (Calculator.operand1 != "") && (Calculator.operand1.indexOf(".") == -1) ) {
				Calculator.operand1 = Calculator.operand1 + ".";
			}
		} else {
			if ( (Calculator.operand2 != "") && (Calculator.operand2.indexOf(".") == -1) ) {
				Calculator.operand2 = Calculator.operand2 + ".";
			}
		}
		
		renewMainTextfield();
				
	}

	private void processClickOfOperand(String eventString) {

		if (Calculator.operation == "") {
			if (Calculator.operand1 != "") {
				Calculator.operation = eventString;
			}
		} else if (Calculator.operation != "") {
			if (Calculator.operand2 == "") {
				Calculator.operation = eventString;
			} else {
				calculate();
				Calculator.operation = eventString;
			}
		} 
		
		renewMainTextfield();
		
	}

	private void processClickNumberButton(String buttonString) {

		if (Calculator.operation == "") {
			Calculator.operand1 = Calculator.operand1 + buttonString;
		} else if (Calculator.operation != ""){
			Calculator.operand2 = Calculator.operand2 + buttonString;
		} 
		renewMainTextfield();
		
	}

	private boolean stringInArray(String[] inputArray, String findingString) {
		
		for (int i = 0; i < inputArray.length; i++) {
			if (inputArray[i].equalsIgnoreCase(findingString)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	private void renewMainTextfield() {
		
		Calculator.textField.setText(Calculator.operand1+Calculator.operation+Calculator.operand2);
		
	}

	private void resetMainTextfield() {
		
		Calculator.operand1 = "";
		Calculator.operand2 = "";
		Calculator.operation = "";
		Calculator.textField.setText("");
		
	}
	
	private void setResultToTextField(Double result) {
		
		if (Double.compare(result, Math.round(result)) == 0 ) { //�����
			String resultString = String.valueOf(result);
			Calculator.textField.setText(resultString.substring(0, resultString.indexOf(".")));
		} else { //�� �����
			Calculator.textField.setText(String.format("%.2f", result));
		}
		
	}
	
	private void calculate() {
		
		StringBuffer javascript = null;
		ScriptEngine runtime = null;

		try {
			
			runtime = new ScriptEngineManager().getEngineByName("javascript");
			javascript = new StringBuffer();
			javascript.append(Calculator.operand1+Calculator.operation+Calculator.operand2);
			
			double result = (Double) runtime.eval(javascript.toString());
			setResultToTextField(result);
			clearCerviceFields();
			Calculator.operand1 = String.valueOf(result);
			
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
			resetMainTextfield();
			
		}
		
	}
	
	private void clearCerviceFields() {

		Calculator.operand1 = "";
		Calculator.operand2 = "";
		Calculator.operation = "";
		
	}

	

}
