package forms;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.xml.crypto.Data;

import bankSystem.Client;
import bankSystem.Summ;

public class PayComboBox extends JFrame{
		
	private static final long serialVersionUID = 1L;
	//������� ���� � �������� �����
	private static final String [] arrayOfValutes = {"���", "����", "����"};
	
	static JLabel label_platelhik = new JLabel("����������");
	static JLabel label_poluchatel = new JLabel("����������");
	static JLabel label_rr_1 = new JLabel("��������� ����");
	static JLabel label_rr_2 = new JLabel("��������� ����");
	static JLabel label_summa = new JLabel("�����");
	
	static JTextField textField_rr_platelhik = new JTextField("", 12);
	static JTextField textField_rr_poluchatel = new JTextField("", 12);
	static JTextField textField_summa = new JTextField("", 10);
	
	static JButton button_begin_tranzaction = new JButton("<html> &nbsp&nbsp ������ <br> ����������");
	
	static JList list_platelhik;
	static JList list_poluchatel;
	{
		try {
			list_platelhik = new JList(getArrayOfPersons());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			list_poluchatel = new JList(getArrayOfPersons());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// ������� ������ �������������� �� ����� ������� buttonGroup
	// � ����� ������������� ��������
	static JRadioButton radioButtons_valutes [] = new JRadioButton [3];
	static ButtonGroup buttonGroup = new ButtonGroup();
	{
		for (int i = 0; i < radioButtons_valutes.length; i++) {
			radioButtons_valutes[i] = new JRadioButton(arrayOfValutes[i]);
		}
		radioButtons_valutes[0].setSelected(true);
	}
	
	static JCheckBox checkBox_commission = new JCheckBox("��������� ��������", true);
	
	static PayComboBoxProcess payComboBoxProcess = new PayComboBoxProcess();
	
	public PayComboBox(String title) throws HeadlessException {
		super(title);
		
		setLocation(300, 300); 
		setSize(600, 250);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE ); 
		
		JPanel mainPanel = new JPanel(new BorderLayout(2, 0));
		add(mainPanel);
		
		JPanel centralPanel = new JPanel(new GridLayout(1, 3, 4, 2));
		mainPanel.add(centralPanel, BorderLayout.CENTER);
		
		JPanel panelOfNamesOfPayers = new JPanel(new GridLayout(4, 1, 1, 1));
		centralPanel.add(panelOfNamesOfPayers);
		panelOfNamesOfPayers.add(label_platelhik);
		list_platelhik.setLayoutOrientation(JList.VERTICAL);
		JScrollPane scrollPanePlatlhik = new JScrollPane(list_platelhik);
		panelOfNamesOfPayers.add(scrollPanePlatlhik);
		
		panelOfNamesOfPayers.add(label_poluchatel);
		panelOfNamesOfPayers.add(list_poluchatel);
		list_poluchatel.setLayoutOrientation(JList.VERTICAL);
		JScrollPane scrollPanePoluchatel = new JScrollPane(list_poluchatel);
		panelOfNamesOfPayers.add(scrollPanePoluchatel);
		
		label_platelhik.setPreferredSize(new Dimension(700, 10));
		
		JPanel panelOfAccountsOfPayers = new JPanel(new GridLayout(4, 1, 1, 1));
		centralPanel.add(panelOfAccountsOfPayers);
		panelOfAccountsOfPayers.add(label_rr_1);
		panelOfAccountsOfPayers.add(textField_rr_platelhik);
		panelOfAccountsOfPayers.add(label_rr_2);
		panelOfAccountsOfPayers.add(textField_rr_poluchatel);
		
		JPanel panelOfSumm = new JPanel(new GridLayout(4, 1, 1, 1));
		centralPanel.add(panelOfSumm);
		panelOfSumm.add(new JLabel("   "));
		panelOfSumm.add(label_summa);
		panelOfSumm.add(textField_summa);
		panelOfSumm.add(new JLabel("   "));
		
		JPanel panelOfComboBox = new JPanel(new GridLayout(4, 1, 1, 1));
		mainPanel.add(panelOfComboBox, BorderLayout.EAST);
		panelOfComboBox.add(new JLabel("   "));
		for (int i = 0; i < radioButtons_valutes.length; i++) {
			panelOfComboBox.add(radioButtons_valutes[i]);
			buttonGroup.add(radioButtons_valutes[i]);
		}
				
		JPanel southPanel = new JPanel();
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		southPanel.add(button_begin_tranzaction);
		southPanel.add(checkBox_commission);
		
		
        //����������� ������������ �������
		list_platelhik.addListSelectionListener(payComboBoxProcess);
		list_poluchatel.addListSelectionListener(payComboBoxProcess);
		button_begin_tranzaction.addActionListener(payComboBoxProcess);
		
		setFocusable(true);
		addKeyListener(payComboBoxProcess);
		
		textField_summa.addKeyListener(payComboBoxProcess);
		PayComboBox.textField_summa.setEditable(false);
		PayComboBox.textField_summa.setBackground(Color.WHITE);
		
		setResizable(false);
		setVisible(true);
		
	}

	public static void main(String[] args) {
	
		new PayComboBox("����� ������");
		
	}
	
	
	private Client[] getArrayOfPersons() throws FileNotFoundException, IOException{
		
		return Client.makeMassClient("src/fio_clients.in");
		
	}
	
	
	static void winPayInside(Client payer, Client dest, Long sum) {

		if (payComboBoxProcess.checkSum(textField_summa.getText())) {
		
			payer.getBankCard().setSumm(new Summ(1000L, "UAH"));
			
			System.out.println("��");
			System.out.println(payer.getBankCard().getSumm().getSumm());
			
			String result = payer.payBCard(new Summ(sum, buttonGroup.getSelection().toString()), dest);
			
			System.out.println("�����");
			System.out.println(payer.getBankCard().getSumm().getSumm());
			
		}
		
	}
	
	
	
	
}
