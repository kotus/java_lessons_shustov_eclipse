package lesson;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

//��������� � ���������� ����� ������ ������������ - �������.

//������� ��� - ���������� - ��, � �������� ���������, �  ��������� ��������� � ����� ������ this

//��������� ������� Action - ��� ���������� �������� ������� ��� �������� ��������� - 
//������, ���������,  ���������

public class GUI_EventProcess4 extends JFrame implements ActionListener,
		MouseListener, MouseMotionListener {
	// ���� �������������� �������� ������ ����� �����������������- �� �������
	// 1-� ��������� � ������������� -
	// ����� ����� ������������� ��� ������-����������� ������� ������� ��������

	JButton butt1 = new JButton("Ok");

	JButton butt2 = new JButton("Cancel");

	JTextField tFld1 = new JTextField("���� ������� ���");

	String[] massStr = { "������", "������", "�������" };
	JComboBox cBox = new JComboBox<>(massStr);

	static int count = 0;
	Color oldBackground1 = null;
	Color oldBackground2 = null;
	

	public GUI_EventProcess4(String title) throws HeadlessException { // '�������
																		// ���
																		// �������
																		// ��
																		// ��������
																		// ����
																		// (�������������
																		// ������
																		// �������)
		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); // ������� ����� ���
													// �������� ���� ����
													// ���������

		// setLayout(null); //�������� ����� ���������, �� �� ��� �� �����
		setLayout(new FlowLayout()); // �������� ����������() - ����������,
										// ��������� ���������� ����� �������

		// this.add(butt1);
		add(butt1); // ��� ����� ������ �� ������ ����� ���������� ������ ��
					// ����� ������� ������ ����, �� ������� ����������� ��
					// �����
					// ���� �� ������ - ��� �������� ������� ������ - ������
					// ����� ������� ���� - �.�. Action-�������

		// 1. �������� ������� ������� add<...>Listener(), �������� ����� ������
		// - �� ����������, ������� ������� ����� ����������,
		// �.�. �������� ������ �� ����������, � ������� �� ������������ ����
		// �������
		// ActionListener - ��������, ����� ������� � ����� ������������ �
		// ����������
		// 2. � ������� ������� � ��������� ������� �����, � ������� �����
		// ���������� (��� this ��� ��� ����� �����)
		// 3. ���� �������������� �������� ������ addActionListener (��� �����
		// ������ ��� 1� ������������� ����� ������)
		// - ����� �������� ��������� ��������� � ������������� -
		// ���������������� ActionListener -
		// - ����� ����� ������ ����� ����� ������ ��������� implements
		// ActionListener
		// 4. ���� �������������� ��� ������ ������ (��� ����� ������ ��� ������
		// ������������� ����� ������ addActionListener), ��
		// -����� �������� ������ ��������� ����������� - ��������
		// �������������������� ������
		// 5. ����� ������������ �������� (��������) ������ �����������
		butt1.addActionListener(this); // � ��������� ���� ������� ������� -
										// �.�. �����, ������� ������� �����
										// ������������
										// � � ������������ �������
		// this - ��������, ��� ���������� ������� ����� � ���� ������

		// ������� ��������� ������� ����� ������ � ���������
		add(cBox);
		cBox.addActionListener(this);

		add(tFld1);
		// ��� ��������� ������� � ���������� ������������ ����� ���� ������
		// ���������� ������ ������
		// ��� �������� ������� ���� ������� �����. ���������� ������ ���� ���
		// ���������� ������� ���
		tFld1.addActionListener(this);

		// --------------------------------------- ���� 31
		// -------------------------------------------------
		// ��������� ������� ���� MouseListener (MouseEvent) - ������� �������
		// ���� � ���� ���������
		// ������: ���������� ���� ���� �� ������ � ������� MouseListener
		butt1.addMouseListener(this);
		cBox.addMouseListener(this);
		tFld1.addMouseListener(this);

		// -----------------------------------���� 32
		// -------------------------------------------------------
		add(butt2);
		butt2.addMouseMotionListener(this); // ����� ����������� ������� ���� ��
											// ����� �������� ������� ��
											// ����������
		butt2.addMouseListener(this); // ����� ������� ������ ���� ��� ������
										// ������� � ���������� ����������

		setVisible(true);
	} // construcror

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GUI_EventProcess4("��������� ������� � �����");
	}

	@Override
	// ���������� ������������ ����� - �������� ������� ����������,
	// ���� ������ � ������� ����� ������������ ���������
	// ����� ����� ������ ��������: ��� � ��������
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub

		// ���������� ������� ����� ����
		// ��� ������� ����� ������� ��� �����, ������� ��������� ������
		// �������� ����� ����� �����

		// ������: ����� ����� ����� �� butt1 ������� �� �������
		// "��� ���� �� ������ butt1"
		// System.out.println("��� ���� �� ������ butt1.");

		// ������: ���������� ����� ���������� ����� ���������� �������,
		// � ������� ������ ��������� �� �������
		System.out.println(event.paramString());
		// if (event.getActionCommand().equals("Ok") ) //������ ������ ���
		// ������� ������ �� �������, �.�. ���������� �������� ������

		if (event.getSource() == butt1) // �������� ����� ��������� ������� �
										// ������� ������ butt1
			System.out.println("��� ���� �� ������ butt1.");
		else if (event.getSource() == cBox) { // ���� ��� ���� �� ����������
			// �������� ������, �� ������� ���� ������ �����
			String userItemSel = (String) cBox.getSelectedItem();
			System.out.println("��� ���� ��� ������ � ����������. �������: "
					+ userItemSel);

		} else if (event.getSource() == tFld1) { // ���� ��� ����� ����� ���
													// ������� � ����������
			String userTextTfld = (String) tFld1.getText();
			System.out
					.println("���� ������� ������  � ����������. ��� ���� � ���������� ���� ��������: "
							+ userTextTfld);
		}

		// �������� ��������� � ��� �� �����
		// �� �������, ��� ���� ������� � ��() ������������ , �� ��� �������
		// ���������� ifelse

	} // ����� ������-����������� �������

	// ------------------------------------------------------�����������
	// ������������--------------------------------------------------
	@Override
	// ���������� ����� ���� - ���� - ��� ������� � ���������� (����� ������ �
	// ����� ����������)
	public void mouseClicked(MouseEvent event) {
		// ���� ���� ���������� ���������� � ����� ���������� -
		// �� ��� ���� if �� ����� ��� ����������� ��������� �������
		// ���� ������� �������� � ���������� �����������, �� ����� ��(
		// event.getsource() )
		// TODO Auto-generated method stub
		if (event.getSource() == butt1) {
			System.out.println("�����������: ��� ����  �� ������ Butt1: ");
		} else if (event.getSource() == cBox) {
			System.out.println("�����������: ��� ����  �� ����������: ");
		} else if (event.getSource() == tFld1) {
			System.out.println("�����������: ��� ����  �� ����������: ");
		} // if tFld1
	}// mouseClicked

	@Override
	// ��������� ������� ������ ����, �� �� ����������
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	// ��������� ���������� ������ ����, �� �� �������
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	// �������� ���� ���������� ��� ��������� ������� ���� �� ���
	// ��� - 2� ������� - ��������� ������� ����� � ������ ������.

	// ��� ��������� ������� �� ������ 1 (��) ������� ������ �����
	// � ��� �������� ������� - ������� �� �������
	// 1. ���������� ��� ������� �������� - mouseListener
	// ������ ����� �������� ���� ��� ������ - ������� butt1. � � ������
	// ������������� ����� ��� ������ add...Listener
	// butt1.add
	// 2. �������� ���� ������� � ����������
	// 3. ������� ������ ������� ����� �������� - mouseEntered
	// 4. �������� ������� �����������.

	@Override
	// ���������� ������ ������� ���� �� ���������� ����������
	public void mouseEntered(MouseEvent event) {
		// TODO Auto-generated method stub
		System.out.println("�����������. ������ �� ����������." + ++count);
		if (event.getSource() != butt2) {
			oldBackground1 = butt1.getBackground(); // ��������� ������ ����
			butt1.setBackground(Color.BLUE); // ���������� ����� ���� ����
		}
		else 
			oldBackground2 = butt2.getBackground(); // ��������� ������ ����
		
	}

	@Override
	// ���������� ������ ������� ���� � ���������� ����������
	public void mouseExited(MouseEvent event) {
		// TODO Auto-generated method stub
		if (event.getSource() != butt2) {
			System.out.println("�����������. ������ ���� �� ������ ���������.");
			butt1.setBackground(oldBackground1); // ������ ������ ����
		}// if
		else {  //���� ���������� ������� ���� ������ butt2
			System.out.println("�����������. ������ ���� � ���������� butt2.");
			butt2.setBackground(oldBackground2); // ������ ������ ����
		}//else
	}

	// --------------------------------------------- ����������� ����� 32 ---------------------------------------
	@Override
	// ���������� ������� ������������� ���� � ������� ����� ������� ��
	// ��������� ����������
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	// ���������� ������� �������� ����������� ���� �� ���������� ����������
	// ���� ���������� ����������� ����� ����� - ��� ����� ������ ������� ����, ������� �������� ������� �����,
	//� ������������ ��� ������� ������ � ������� �������
	public void mouseMoved(MouseEvent event) {
		// TODO Auto-generated method stub
		System.out.println("���������������. ������ �� ����������." + ++count);
		butt2.setBackground(Color.GREEN); // ���������� ����� ���� ����
		//event.
	}
	
	//������� ��������� � ������������
	//1. ������� ����� �������� ������ ������ ����������
	//2. ��������� ����� ������� �� �����, ����� ���� �������� ��������� � ����� ��������
	//3. ������� ������� ������� ������������ - �������� �� ������ ����������, ��� �������-������ ������ �� ���������� ����������, ��� ������� ������ � �.�.
	//�.�. ������� �� ��������, ������� ������������ ��� ����������, ������ �������� �� ��������� ����� ��������

}// class
