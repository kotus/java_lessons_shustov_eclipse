package lesson;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

//��������� ������� ����������


public class GUI_EventKey extends JFrame implements KeyListener  {
	// ���� �������������� �������� ������ ����� �����������������- �� �������
	// 1-� ��������� � ������������� -
	// ����� ����� ������������� ��� ������-����������� ������� ������� ��������

	JButton butt1 = new JButton("Ok");
	JButton butt2 = new JButton("Cancel");

	JTextField tFld1 = new JTextField("���� ������� ���");
	String oldTxt="";  //c����� ���������� ����������
	

	String[] massStr = { "������", "������", "�������" };
	JComboBox cBox = new JComboBox<>(massStr);

	static int count = 0;
	
	public GUI_EventKey(String title) throws HeadlessException { // '������� ������� �������� ����
																	
		super(title);

		setLocation(300, 300);
		setSize(500, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE); 

		// setLayout(null); //�������� ����� ���������, �� �� ��� �� �����
		setLayout(new FlowLayout()); // �������� ����������() - ����������,
										// ��������� ���������� ����� �������

		// this.add(butt1);
		add(butt1); 
		add(cBox);
		add(tFld1);
		add(butt2);
		
		// ��� ��������� ������� � ���������� ������������ ����� ���� ������
				// ���������� ������ ������
				// ��� �������� ������� ���� ������� �����. ���������� ������ ���� ���
				// ���������� ������� ���
		
		//����� ������ ��� ��� ������� �� ������� ������ � ����������, ���� ��� �������� ������
		//��� ���� ���� � �����-�� ��������� � ���� ��������� ������ ���������� , �� 
		//����� ���������� �����  �� ��������� ��������� - ����� �� ��������� ���������� ������ ������ � ���.
		//1. ���������� , ������� ����� ������������� - ������ ���� ��������� (������ ���� �������� ����������� �������� ������ �� ����)
		//2. ������ ��� ��������� ������ �������� 100% ������ ����� ������ ���������� �� ������ 
		// (�.�. ����� ���� ��� ����������  �����  ������� )
		
		tFld1.addKeyListener(this);
		
		setVisible(true);
		
				//��� 2 ������ ��� ������ ������������ ����� ����, ��� ���������� ��������� �� ������
		tFld1.setFocusable(true);  //�������� ����������� ������ , �.�. ������� ���������� ���������
		tFld1.requestFocus();
		
		
	} // construcror

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GUI_EventKey("��������� ������� � �����");
	}

	@Override   //������� ������ �������� ������� 
	//���������, ���������� ������� � ���� ������-����������� �� ��������������
	//Extended ���� � �������() �� ��������������
	public void keyTyped(KeyEvent event) {
	
	
		System.out.println("������ �������: " + event.getKeyChar() );  //���� ���������� �������� ������� �����, �����, �����, ������� � ����� ����� ���������
		System.out.println("Alt ����?  :" + event.isAltDown() );   //����� ������������� �������, ���, ���� �������� � ���� �����������
		System.out.println("event.getModifiersExText:  "+event.getModifiersExText(  event.getModifiersEx() )  );   //�����, ��� ��������� ����� �.�. ������������ ��� ������ ����
		System.out.println("event.getKeyModifiersText" + event.getKeyModifiersText( event.getModifiers()  ) );
		
		System.out.println(event.paramString());
		
		//��� ������ � ���� ����������� ������ �� ����
	    System.out.println("��� �������: " + event.getExtendedKeyCode() );  //�� ��������, �.�. Ex
		System.out.println(event.getKeyCode() );   // //�������� ������ � ����������� ��������()
		System.out.println(event.getKeyText( event.getKeyCode() ) );    //�������� ������ � ����������� ��������() 
		event.getKeyLocation();		//����� �� ��������, ������ unknown_location? �������� � keyPressed()
	
		//������ �������������� ����� "������� ����� ����"
		String str = tFld1.getText();
		if (str.indexOf("����")!= -1)  //���� ��������� ����� ���� � ����������
			tFld1.setText("");
		
					//������: ����������������� ���� ������ ����� � ��������� , ��� � ���� �����
		//���� ������� ��� �� ��������, �.�. �� ����������� ������ ������� ������� ����� ����� ����� ����� ������ � ���������
		//�������� ������������ ���������� � �������� ����������   �������(), �.�. �������� �������� ��������
		if ( event.getKeyChar() >= '0'  &&  event.getKeyChar() <= '9') {  //���� ������ �����
			System.out.println("������� �����");
			//������ ����� �������� ��������� ����������, �.�. �� ��������� ��������� ���������� �� ���������
			tFld1.setText( tFld1.getText() + event.getKeyChar()  );  //������ ���������� ���������� + ��������� ������� ������
			//����� ���������� ���������� ��������� � ������ ���������
			oldTxt = tFld1.getText();
		}
		else {
					//������ ��������� ������ ��������� ����������, � �������� ��� ������ ���������� ����������
			tFld1.setText(  oldTxt );
			System.out.println("������� �� �����!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}

		//��������� ���������� ������ �����������:
		//������� ������� ���������� � ����������  �����  ������ �����������
		//�������  ������ ����������� ���������� ������ ���������� ���������� �� �������,
		//�.�. ��� ����� ����� ������� ����������� ������
		//�����: ���� ��������� ����� ��������� ������� ����� �����������.
		//- ������� ��������  �� ������������� - ����� ��������� ��� �� ��������� ������������
		tFld1.setEditable( false);  //��������� ���� �������� � ��������� ����� �����������
		tFld1.setBackground(Color.WHITE); //�� ������ ���� ���� ���� ����� ����������� ������ ������ �����, �� ������� ��� � �����

	}

	@Override  //������� ����� �������, �������� � ����������
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override  //���������� ����� �������, �������� � ����������
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
	//������� ��������� � ������������
	//1. ������� ����� �������� ������ ������ ����������
	//2. ��������� ����� ������� �� �����, ����� ���� �������� ��������� � ����� ��������
	//3. ������� ������� ������� ������������ - �������� �� ������ ����������, ��� �������-������ ������ �� ���������� ����������, ��� ������� ������ � �.�.
	//�.�. ������� �� ��������, ������� ������������ ��� ����������, ������ �������� �� ��������� ����� ��������

}// class
