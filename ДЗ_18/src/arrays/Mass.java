package arrays;

public class Mass {
	
	public static String[][] arr1 = { { "�������", "����", "��������" },
			{ "�������", "����", "��������" },
			{ "�����", "�����", "��������" },
			{ "��������", "�����", "���������" },
			{ "������", "�����", "���������" } };
	public static String[][] arr2 = { { "������", "����", "��������" },
			{ "������", "����", "��������" },
			{ "�������", "�����", "���������" },
			{ "�������", "�����", "���������" },
			{ "������", "�����", "���������" } };
	public static String[][] arr3 = { { "����", "������������", "" }, 
			{ "����", "���", "" },
			{ "���", "����", "�������" } };
	
	public static void main(String[] args) {
	
		/*
		long arr1 [] = new long [50];
		for (int i = 0; i < arr1.length; i++) {
			arr1[i] = 6;
			System.out.print(arr1[i]);
		}
	
		long arr2 [] = new long [7];
		for (int i = arr2.length - 1; i >= 0; i--) {
			arr2[i] = i;
		}
		
		System.out.print("[ ");
		for (int i = 0; i < arr2.length; i++) {
			System.out.print(arr2[i] + " ");
		}
		System.out.print("]");
		*/
			
		/*
		String adr [] = {"��.���������� ������� ���������",
				"��.�������� ������� ������",
				"��.������ ������� ������",
				"��.������� ������� �����",
				"��.��������� ������� �����"};
		*/
		
		//������� 2 ������ ������  ���� ���� � 3 ������ � 4 ������� � ��������� ��� ������  ������� -1. ������� ���������� ������� �� �������.
		/*
		long arr[][] = new long [3][4];
		for (int i = 0; i < arr.length; i++) {
			System.out.print("\n"+ (i+1) +"-� ������ �������: ");
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = -1;
				System.out.print(arr[i][j] + "  ,  ");
			}
		}
		*/
		
		//2.2. ������� 2 ������ ������  ���� ���� � 3 ����� � 7 ��������, � ��������� ��� ������ �� ����������� �� 1 �� 21
		//� ����������� �� ��������� ������ � ������.  ������� ������ ����� ������� �� �����  �� 1� �� ��������� ������.
		/*
		long arr[][] = new long [3][7];
		int counter = 21;
		for (int i = arr.length - 1; i >= 0; i--) {
			for (int j = arr[i].length - 1; j >= 0; j--) {
				arr[i][j] = counter--;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			System.out.print("\n"+ (i+1) +"-� ������ �������: ");
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + "  ,  ");
			}
		}
		
		
		String[][] arr1 = { { "�������", "����", "��������" },
							{ "�������", "����", "��������" },
							{ "�����", "�����", "��������" },
							{ "��������", "�����", "���������" },
							{ "������", "�����", "���������" } };
		String[][] arr2 = { { "������", "����", "��������" },
							{ "������", "����", "��������" },
							{ "�������", "�����", "���������" },
							{ "�������", "�����", "���������" },
							{ "������", "�����", "���������" } };
		String[][] arr3 = { { "����", "������������", "" },
							{ "����", "���", "" }, 
							{ "���", "����", "�������" } };

		String [][] matr1 = Mass.AddArr(arr1, arr2);
		String [][] matr2 = Mass.AddArr(matr1, arr3);
		//Mass.showMatrix(matr2);
		String [] mass = Mass.splitMatrix(matr2);
		Mass.showArray(mass);	
		*/
		
		/*
		Double[][] matr = {{0D, 10D, 54D},
						  {54D, 10D, 0D},
						  {0D, 54D, 10D},
						  {10D, 0D, 54D}};
		
		Double[] res = Mass.findMaxInLine(matr);
		*/
		
		/*
		Integer[][] array = new Integer[5][7];
		Integer[][] resArray = Mass.fillInArray(array);
		Mass.showIntegerMatrix(resArray);
		*/
		
		/*
		Integer[] arr = {1,2,3,4,5};
		arr = Mass.reverseArrayOfInteger(arr);
		Mass.showIntegerArray(arr);
		*/
		
		Integer[][] arr = Mass.dz_18_task_3_2(5, 7);
		Mass.showIntegerMatrix(arr);
		
	}

	/**
	 * ����� ��� �������� ���� ��������� ��������.
	 * @param arr1 1-� ��������� ������ ���� String[][].
	 * @param arr2 2-� ��������� ������ ���� String[][]. 
	 * @return ��������� ������ String [][].
	 */
	public static String[][] AddArr(String [][] arr1, String [][] arr2) {
		
		int rows = arr1.length + arr2.length;
		int cols = arr1[0].length;
		String [][] resArr = new String[rows][cols];
		
		int ind = 0;
		for (int i = 0; i < arr1.length; i++) {
			for (int j = 0; j < arr1[i].length; j++) {
				resArr[i][j] = arr1[i][j];
			}
			ind++;
		}
		
		for (int i = 0; i < arr2.length; i++) {
			for (int j = 0; j < arr2[i].length; j++) {
				resArr[ind][j] = arr2[i][j];
			}
			ind++;
		}
		
		return resArr;
		
	}
	
	/**
	 * �����, ������������� ��������� ������ ����� � ���������� ������ �����.
	 * @param matrix ���� <b>String [][]</b>, �������� ��������� ������.  
	 * @return ���������� ������ ���� <b>String []</b>.
	 */
	public static String [] splitMatrix(String [][] matrix){
		
		String result [] = new String[matrix.length];
		
		for (int i = 0; i < matrix.length; i++) {
			String tempString = "";
			for (int j = 0; j < matrix[i].length; j++) {
				tempString = tempString + matrix[i][j] + " ";
			}
			result[i] = tempString;
		}
		
		return result;
		
	}; 
	
	/**
	 * �����, ��������� � ������ ������ 2-� ������� ������� ������������ ����� � �����������
	 * ��� � ������ ����������.
	 * @param matrix ���� <b>Double[][]</b>, �������� ��������� ������. 
	 * @return ���������� ������ ���� <b>Double []</b>.
	 */
	public static Double[] findMaxInLine(Double[][] matrix) {
		
		Double[] arrayResult = new Double[matrix.length];
		
		for (int i = 0; i < matrix.length; i++) {
			Double maxLine = null;
			for (int j = 0; j < matrix[i].length; j++) {
				if (j == 0) {
					maxLine = matrix[i][j];
				} else {
					if (matrix[i][j] > maxLine) {
						maxLine = matrix[i][j];
					}
				}   
			}
			arrayResult[i] = maxLine;
		}
		
		return arrayResult;
		
	}
	
	/**
	 * �����, ������� �����������  ���������� ������ ���� <b>Integer[]</b> 
	 * @param arr ���� <b>Integer[]</b>, ������� ���������� ������. 
	 * @return arr ���� <b>Integer[]</b>, �������������� ���������� ������.
	 */
	public static Integer[] reverseArrayOfInteger(Integer[] arr){
		
		for (int i = 0; i < arr.length/2; i++) {
			Integer tmp = arr[i];
			arr[i] = arr[arr.length-1-i];
			arr[arr.length-1-i] = tmp;
		}
		
		return arr;
		
	}
	
	
	//����� ��� ������ ������� ����� �� �����
	public static void showStringMatrix(String [][] arr) {
		
		String result = "";
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				result = result + arr[i][j] + " ";
			}
			result = result + "\n";
		}
		System.out.println(result);
		
	}
	
	//����� ��� ������ ������� ����� �� �����
	public static void showStringArray(String [] arr){
	
		String result = "";
		for (int i = 0; i < arr.length; i++) {
			result = result + arr[i] + " ";
			result = result + "\n";
		}
		System.out.println(result);
		
	}
	
	//����� ��� ������ ������� �������� ������ ���� �� �����
	public static void showArrayOfObjects(Object[] objArray){
		
		for (int i = 0; i < objArray.length; i++) {
			System.out.println(objArray[i]);
		}
			
	}
	
	//����� ��� ������ ������� ����� �� �����
	public static void showDoubleArray(Double[] arr){
	
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		
	}
	
	//����� ��� ������ ������� ����� �� �����
	public static void showDoubleMatrix(Double[][] arr){
	
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	
	//����� ��� ������ ������� ����� �� �����
	public static void showIntegerArray(Integer[] arr){
		
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
			
	}
		
	//����� ��� ������ ������� ����� �� �����
	public static void showIntegerMatrix(Integer[][] arr){
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
			
	}
			
	/**
	 * �����, ���������� ������ �������: 1� ������ � �� �������, ������� �� 1
	 * 1� ������� � �� �������, ������� �� 1. � ���������  ������� ����� � 2 ���� �������,
	 * ��� � �������� ������ ����� ������.
	 * @param rows ���� <b>int</b>, ����� ����� �������.
	 * @param colls ���� <b>int</b>, ����� ������� �������.
	 * @return <b>Integer[][]</b>, ���������.
	 */
	public static Integer[][] dz_18_task_3_2(int rows, int colls){
		
		Integer[][] arr = new Integer[rows][colls];
		int counter = 1;
		
		for (int j = 0; j < arr[0].length; j++) {
			arr[0][j] = counter;
			counter++;
		}
		
		counter = 1;
		for (int i = 0; i < arr.length; i++) {
			arr[i][0] = counter;
			counter++;
		}
		
		for (int i = 1; i < arr.length; i++) {
			for (int j = 1; j < arr[i].length; j++) {
				arr[i][j] = arr[i-1][j-1]*2;
			}
		}
		
		return arr;
		
	} 
	
	/**
	 * �����, ���������� ������ ������� �� �������, ������ �������� � ���������. 
	 * @param array ���� <b>Integer[][]</b>, �������� ��������� ������.
	 * 
	 * @return resultArray ���� <b>Integer[][]</b>, �������������� ��������� ������.
	 */
	public static Integer[][] fillInArray(Integer[][] array) {
		
		Integer[][] resultArray = array;
		Integer counter = 1;
		for (int i = 0; i < resultArray.length; i++) {
			for (int j = 0; j < resultArray[i].length; j++) {
				resultArray[i][j] = counter;
				counter++;
			}
		}
		
		return resultArray;
		
	}
	
}
