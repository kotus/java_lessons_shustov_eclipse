package pack1;

import java.io.IOException;
import java.io.RandomAccessFile;

public class FileIO {

	
	public static void main(String[] args) throws IOException {

		//1.1
		/*
		RandomAccessFile reader = new RandomAccessFile("src/forum.log", "rw");
		RandomAccessFile writer = new RandomAccessFile("src/forum_10_15.txt", "rw");
		
		String str = "";
		
		for (int i = 0; i < 15; i++) {
			
			str = reader.readLine();
			
			if(i < 9) continue;
			
			//writer.write((str + "\n").getBytes("cp1251"));
			writer.write((str + "\n").getBytes("ISO-8859-1"));
				
		*/
		
//		1.2.	� �����() � ������� ������� ������ RAF �������� � 3 ����� �� 2 ������: �latin letters� � �������� ������:
		
//		1.2.1.	� ����  DZ_24bytes  �������� ��� ������ � 1-������� ���� ������ �������� � ������� ������ writeBytes. ���������, ��� ������� 
//		����� ��������� �������� ����� ������������ ����������.
		
		/*
		RandomAccessFile writer = new RandomAccessFile("src/DZ_24bytes.txt", "rw");
		
		String strRUS = "������� �����";
		String strENG = "latin letters";
		
		writer.writeBytes(new String(strRUS.getBytes(), "ISO-8859-1") + "\n");
		writer.writeBytes(strENG + "\n");
		*/		
		
		
//		1.2.2.	� ����  DZ_24bytes  �������� ��� ������ � 1-������� ���� ������ �������� � ������� ������ write( bytes [ ]   arr). ���������, 
//		��� ������� ����� ��������� �������� ����� ������������ ����������. 
		
		/*
		RandomAccessFile writer = new RandomAccessFile("src/DZ_24bytes_2.txt", "rw");
		
		String strRUS = "������� �����";
		String strENG = "latin letters";
		
		writer.write(strRUS.getBytes("cp1251"));
		writer.write(strENG.getBytes("cp1251"));
		*/	
		
		
//		1.2.3.	� ����  DZ_24_ISOchar  �������� ��� ������ � 2-������� ����. ���������, ��� ������� ����� ��������� �������� ����� ������������ 
//		���������� ���� NotePad2.
		
		
		RandomAccessFile writer = new RandomAccessFile("src/DZ_24_ISOchar.txt", "rw");
		
		String strRUS = "������� �����";
		String strENG = "latin letters";
		
		writer.writeChars(strRUS + "\n");
		writer.writeChars(strENG + "\n");
		
		
//		1.2.4.	������ � ������� ������������ ��������� ���� NotePad   2� ������� ������ �� ������������ ��������� ? 
//		1.2.5.	� ����  DZ_24bad  �������� ��� ������ � ������ �����. �  ��� � 1.3.1 � 1.3.3
//		��������� ,��� ��������� �������� (���� ����� � ������� �������2)  ����� �� �� ����� � ����� DZ_24bad   ���������� ������������.  
//		������ �������� �� ���� ������������� ���� ���� ? 

	}

}
