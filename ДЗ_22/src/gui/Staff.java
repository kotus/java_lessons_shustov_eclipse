package gui;

public class Staff extends Person{
	
	private String post;
	private String department;
	private Summ salary;
	
	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Summ getSalary() {
		return salary;
	}

	public void setSalary(Summ salary) {
		this.salary = salary;
	}

	public Staff(String surename, String name, String middleName,
			String post, String department, Summ salary) {
	
		super(surename, name, middleName);
		this.post = post;
		this.department = department;
		this.salary = salary;
		
	}

	public static void main(String[] args) {
		
		/*
		String [][] staffArray = {{"������", "�����", "������", "����������� ������", "����� - ����������", "0"},
								{"��������", "�����", "�����������", "������", "����� - �����", "0"},
								{"�����������", "������", "��������������", "��������������", "����� ��������", "0"},
								{"�������", "������", "��������", "���.������������", "�����-����������", "0"}};
		
		Staff [] staffs = Staff.makeMassStaff(staffArray);
		Mass.showArrayOfObjects(staffs);
		*/
		
	}
			
	/**
	 * �����, ��� �������� ������� ���� <b>Staff[]</b>
	 * @param arrOfWorkers ���� <b>String[][]</b>, ��������� ������.
	 * @return arrOfStaffs ���� <b>Staff[]</b>, ���������� ������ �������� ���� <b>Staff</b>.
	 */
	public static Staff[] makeMassStaff(String[][] arrOfWorkers) {
		
		Staff[] arrOfStaffs = new Staff[arrOfWorkers.length];
		
		for (int i = 0; i < arrOfWorkers.length; i++) {
			Staff staff = new Staff(arrOfWorkers[i][0], arrOfWorkers[i][1], arrOfWorkers[i][2], 
					"", arrOfWorkers[i][4], new Summ(Long.parseLong(arrOfWorkers[i][5]), "UAH"));
			arrOfStaffs[i] = staff;
		}
		
		return arrOfStaffs;
		
	}
	
	@Override
	public String toString() {
		return "����� Staff:[�������=" + getSurename() + "; ���=" + getName() + "; ��������=" + getMiddleName() + 
				" ���������=" + post + "; �����=" + department + "; ���.�����=" + salary + "]";
	}

}
