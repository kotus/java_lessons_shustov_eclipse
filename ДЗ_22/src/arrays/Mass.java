package arrays;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Mass {
	
	public static String[][] arr1 = { { "�������", "����", "��������" },
			{ "�������", "����", "��������" },
			{ "�����", "�����", "��������" },
			{ "��������", "�����", "���������" },
			{ "������", "�����", "���������" } };
	public static String[][] arr2 = { { "������", "����", "��������" },
			{ "������", "����", "��������" },
			{ "�������", "�����", "���������" },
			{ "�������", "�����", "���������" },
			{ "������", "�����", "���������" } };
	public static String[][] arr3 = { { "����", "������������", "" }, 
			{ "����", "���", "" },
			{ "���", "����", "�������" } };
	
	public static void main(String[] args) {
	
		/*
		long arr1 [] = new long [50];
		for (int i = 0; i < arr1.length; i++) {
			arr1[i] = 6;
			System.out.print(arr1[i]);
		}
	
		long arr2 [] = new long [7];
		for (int i = arr2.length - 1; i >= 0; i--) {
			arr2[i] = i;
		}
		
		System.out.print("[ ");
		for (int i = 0; i < arr2.length; i++) {
			System.out.print(arr2[i] + " ");
		}
		System.out.print("]");
		*/
			
		/*
		String adr [] = {"��.���������� ������� ���������",
				"��.�������� ������� ������",
				"��.������ ������� ������",
				"��.������� ������� �����",
				"��.��������� ������� �����"};
		*/
		
		//������� 2 ������ ������  ���� ���� � 3 ������ � 4 ������� � ��������� ��� ������  ������� -1. ������� ���������� ������� �� �������.
		/*
		long arr[][] = new long [3][4];
		for (int i = 0; i < arr.length; i++) {
			System.out.print("\n"+ (i+1) +"-� ������ �������: ");
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = -1;
				System.out.print(arr[i][j] + "  ,  ");
			}
		}
		*/
		
		//2.2. ������� 2 ������ ������  ���� ���� � 3 ����� � 7 ��������, � ��������� ��� ������ �� ����������� �� 1 �� 21
		//� ����������� �� ��������� ������ � ������.  ������� ������ ����� ������� �� �����  �� 1� �� ��������� ������.
		/*
		long arr[][] = new long [3][7];
		int counter = 21;
		for (int i = arr.length - 1; i >= 0; i--) {
			for (int j = arr[i].length - 1; j >= 0; j--) {
				arr[i][j] = counter--;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			System.out.print("\n"+ (i+1) +"-� ������ �������: ");
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + "  ,  ");
			}
		}
		
		
		String[][] arr1 = { { "�������", "����", "��������" },
							{ "�������", "����", "��������" },
							{ "�����", "�����", "��������" },
							{ "��������", "�����", "���������" },
							{ "������", "�����", "���������" } };
		String[][] arr2 = { { "������", "����", "��������" },
							{ "������", "����", "��������" },
							{ "�������", "�����", "���������" },
							{ "�������", "�����", "���������" },
							{ "������", "�����", "���������" } };
		String[][] arr3 = { { "����", "������������", "" },
							{ "����", "���", "" }, 
							{ "���", "����", "�������" } };

		String [][] matr1 = Mass.AddArr(arr1, arr2);
		String [][] matr2 = Mass.AddArr(matr1, arr3);
		//Mass.showMatrix(matr2);
		String [] mass = Mass.splitMatrix(matr2);
		Mass.showArray(mass);	
		*/
		
		/*
		Double[][] matr = {{0D, 10D, 54D},
						  {54D, 10D, 0D},
						  {0D, 54D, 10D},
						  {10D, 0D, 54D}};
		
		Double[] res = Mass.findMaxInLine(matr);
		*/
		
		/*
		Integer[][] array = new Integer[5][7];
		Integer[][] resArray = Mass.fillInArray(array);
		Mass.showIntegerMatrix(resArray);
		*/
		
		/*
		Integer[] arr = {1,2,3,4,5};
		arr = Mass.reverseArrayOfInteger(arr);
		Mass.showIntegerArray(arr);
		*/
		
		Integer[][] arr = Mass.dz_18_task_3_2(5, 7);
		Mass.showIntegerMatrix(arr);
		
	}

	/**
	 * ����� ��� �������� ���� ��������� ��������.
	 * @param arr1 1-� ��������� ������ ���� String[][].
	 * @param arr2 2-� ��������� ������ ���� String[][]. 
	 * @return ��������� ������ String [][].
	 */
	public static String[][] AddArr(String [][] arr1, String [][] arr2) {
		
		int rows = arr1.length + arr2.length;
		int cols = arr1[0].length;
		String [][] resArr = new String[rows][cols];
		
		int ind = 0;
		for (int i = 0; i < arr1.length; i++) {
			for (int j = 0; j < arr1[i].length; j++) {
				resArr[i][j] = arr1[i][j];
			}
			ind++;
		}
		
		for (int i = 0; i < arr2.length; i++) {
			for (int j = 0; j < arr2[i].length; j++) {
				resArr[ind][j] = arr2[i][j];
			}
			ind++;
		}
		
		return resArr;
		
	}
	
	/**
	 * �����, ������������� ��������� ������ ����� � ���������� ������ �����.
	 * @param matrix ���� <b>String [][]</b>, �������� ��������� ������.  
	 * @return ���������� ������ ���� <b>String []</b>.
	 */
	public static String [] splitMatrix(String [][] matrix){
		
		String result [] = new String[matrix.length];
		
		for (int i = 0; i < matrix.length; i++) {
			String tempString = "";
			for (int j = 0; j < matrix[i].length; j++) {
				tempString = tempString + matrix[i][j] + " ";
			}
			result[i] = tempString;
		}
		
		return result;
		
	}; 
	
	/**
	 * �����, ��������� � ������ ������ 2-� ������� ������� ������������ ����� � �����������
	 * ��� � ������ ����������.
	 * @param matrix ���� <b>Double[][]</b>, �������� ��������� ������. 
	 * @return ���������� ������ ���� <b>Double []</b>.
	 */
	public static Double[] findMaxInLine(Double[][] matrix) {
		
		Double[] arrayResult = new Double[matrix.length];
		
		for (int i = 0; i < matrix.length; i++) {
			Double maxLine = null;
			for (int j = 0; j < matrix[i].length; j++) {
				if (j == 0) {
					maxLine = matrix[i][j];
				} else {
					if (matrix[i][j] > maxLine) {
						maxLine = matrix[i][j];
					}
				}   
			}
			arrayResult[i] = maxLine;
		}
		
		return arrayResult;
		
	}
	
	/**
	 * �����, ��������� � ������ ������ 2-� ������� ������� ������������ ����� � �����������
	 * ��� � ������ ����������.
	 * @param matrix ���� <b>int[][]</b>, �������� ��������� ������. 
	 * @return ���������� ������ ���� <b>int []</b>.
	 */
	public static int[] findMaxInLine(int[][] matrix) {
		
		int[] arrayResult = new int[matrix.length];
		
		for (int i = 0; i < matrix.length; i++) {
			int maxLine = 0;
			for (int j = 0; j < matrix[i].length; j++) {
				if (j == 0) {
					maxLine = matrix[i][j];
				} else {
					if (matrix[i][j] > maxLine) {
						maxLine = matrix[i][j];
					}
				}   
			}
			arrayResult[i] = maxLine;
		}
		
		return arrayResult;
		
	}
	
	/**
	 * �����, ������� �����������  ���������� ������ ���� <b>Integer[]</b> 
	 * @param arr ���� <b>Integer[]</b>, ������� ���������� ������. 
	 * @return arr ���� <b>Integer[]</b>, �������������� ���������� ������.
	 */
	public static Integer[] reverseArrayOfInteger(Integer[] arr){
		
		for (int i = 0; i < arr.length/2; i++) {
			Integer tmp = arr[i];
			arr[i] = arr[arr.length-1-i];
			arr[arr.length-1-i] = tmp;
		}
		
		return arr;
		
	}
	
	/**
	 * �����, ������� �����������  ���������� ������ ���� <b>String[]</b> 
	 * @param arr ���� <b>String[]</b>, ������� ���������� ������. 
	 * @return arr ���� <b>String[]</b>, �������������� ���������� ������.
	 */
	public static String[] reverseArrayOfString(String[] arr){
		
		for (int i = 0; i < arr.length/2; i++) {
			String tmp = arr[i];
			arr[i] = arr[arr.length-1-i];
			arr[arr.length-1-i] = tmp;
		}
		
		return arr;
		
	}
	
	
	//����� ��� ������ ������� ����� �� �����
	public static void showStringMatrix(String [][] arr) {
		
		String result = "";
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				result = result + arr[i][j] + " ";
			}
			result = result + "\n";
		}
		System.out.println(result);
		
	}
	
	//����� ��� ������ ������� ����� �� �����
	public static void showStringArray(String [] arr){
	
		String result = "";
		for (int i = 0; i < arr.length; i++) {
			result = result + arr[i] + " ";
			result = result + "\n";
		}
		System.out.println(result);
		
	}
	
	//����� ��� ������ ������� �������� ������ ���� �� �����
	public static void showArrayOfObjects(Object[] objArray){
		
		for (int i = 0; i < objArray.length; i++) {
			System.out.println(objArray[i]);
		}
			
	}
	
	//����� ��� ������ ������� ����� �� �����
	public static void showDoubleArray(Double[] arr){
	
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		
	}
	
	//����� ��� ������ ������� ����� �� �����
	public static void showDoubleMatrix(Double[][] arr){
	
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	
	//����� ��� ������ ������� ����� �� �����
	public static void showIntegerArray(Integer[] arr){
		
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
			
	}
		
	//����� ��� ������ ������� ����� �� �����
	public static void showIntegerMatrix(Integer[][] arr){
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
			
	}
	
	// ����� ��� ������ ������� ����� ����� �� �����
	public static <T extends Number> void showMatixOfNumbers(T[][] arr) {

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}

	}
	
	/**
	 * �����, ���������� ������ �������: 1� ������ � �� �������, ������� �� 1
	 * 1� ������� � �� �������, ������� �� 1. � ���������  ������� ����� � 2 ���� �������,
	 * ��� � �������� ������ ����� ������.
	 * @param rows ���� <b>int</b>, ����� ����� �������.
	 * @param colls ���� <b>int</b>, ����� ������� �������.
	 * @return <b>Integer[][]</b>, ���������.
	 */
	public static Integer[][] dz_18_task_3_2(int rows, int colls){
		
		Integer[][] arr = new Integer[rows][colls];
		int counter = 1;
		
		for (int j = 0; j < arr[0].length; j++) {
			arr[0][j] = counter;
			counter++;
		}
		
		counter = 1;
		for (int i = 0; i < arr.length; i++) {
			arr[i][0] = counter;
			counter++;
		}
		
		for (int i = 1; i < arr.length; i++) {
			for (int j = 1; j < arr[i].length; j++) {
				arr[i][j] = arr[i-1][j-1]*2;
			}
		}
		
		return arr;
		
	} 
	
	/**
	 * �����, ���������� ������ ������� �� �������, ������ �������� � ���������. 
	 * @param array ���� <b>Integer[][]</b>, �������� ��������� ������.
	 * 
	 * @return resultArray ���� <b>Integer[][]</b>, �������������� ��������� ������.
	 */
	public static Integer[][] fillInArray(Integer[][] array) {
		
		Integer[][] resultArray = array;
		Integer counter = 1;
		for (int i = 0; i < resultArray.length; i++) {
			for (int j = 0; j < resultArray[i].length; j++) {
				resultArray[i][j] = counter;
				counter++;
			}
		}
		
		return resultArray;
		
	}

	/**
	 * ����� ������������� ������ � ������ �������� �� ���������� �����������.
	 * @param stringToDecompose ���� <b>String</b>, �������� ������.
	 * @param decomposeSymbol ���� <b>String</b>, ����������.
	 * @return ������ ���� <b>String[]</b>.
	 */
	public static String[] decomposeStringIntoArrayOfSubstrings(String stringToDecompose, String decomposeSymbol){
		
		return stringToDecompose.split(decomposeSymbol);
				
	}
	
	/**
	 * �����, �������� ���� �� ����� � ������������ ������ ����� ����� �����.
	 * @param fileName ���� String, ��� ����� �� �����.
	 * @return <b>String[]</b>, ������ ��������� ���� String. 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static String[] readFileFromDisk(String fileName) throws IOException, FileNotFoundException{
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
		
		Integer stringCount = 0;
		String strBuffer = "";
		//������� ���������� ����� � �����:
		while (bufferedReader.readLine() != null) {
			stringCount++;
		}
		bufferedReader.close();
			
		//������� �������������� ������
		bufferedReader = new BufferedReader(new FileReader(fileName));
		String[] resArray = new String[stringCount];
		
		//� ��������� ���:
		for (int i = 0; i < resArray.length; i++) {
			if ((strBuffer = bufferedReader.readLine()) != null) {
				resArray[i] = strBuffer;
			} else {
				break;
			}
			
		}
		
		bufferedReader.close();
		
		return resArray;
				
	}
	
	/**
	 * �����, �������� ���� �� ����� � ������������ ��� ������ ����� �������.
	 * @param fileName ���� String, ��� ����� �� �����.
	 * @return <b>String</b>, ������ ��������� ���� String. 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static String readStringFromDisk(String fileName) throws IOException, FileNotFoundException{
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
		
		String strBuffer = "";
		String resultString = "";
		while ((strBuffer = bufferedReader.readLine()) != null) {
			resultString = resultString + strBuffer;
		}
		bufferedReader.close();
		
		return resultString;
				
	}
	
	/**
	 * �����, ������������ ������ ����� � ���� �� �����. 
	 * @param fileName ���� String, ��� ����� �� �����.
	 * @param arrayToWrite ���� String[], ������ ��� ������ � ����.
	 * @param inOneString ���� Boolean, ���� ������ ������ ������ ����� � ����, ����������, ��� ������ ����� �������.
	 * ���� �������� True, ������ ��������� ����� �������, ���� False - � ����� �������. 
	 * @return <b>boolean</b>, ��������� ��������. 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static boolean writeFileToDisk(String fileName, String[] arrayToWrite, Boolean inOneString) throws IOException, FileNotFoundException{
		
		BufferedWriter bufferedWriter = null;
		bufferedWriter = new BufferedWriter(new FileWriter(fileName));
		
		for (int i = 0; i < arrayToWrite.length; i++) {

			try {
				bufferedWriter.append(arrayToWrite[i] + " ");
				if(!inOneString)bufferedWriter.newLine();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}

		}
		
		bufferedWriter.close();
		
		return true;
				
	}
	
	/**
	 * ����� ������� ��� ����� ������ lowerValue � ������� upperValue � 2-������ ������� � ���������� ��
	 * � 1-������ ������ ����������.
	 * @param int[][] inputMatrix - ������� ������� ���� int[][]
	 * @param int lowerValue - ����� ������ ����� ��������
	 * @param int upperValue - ����� ������ ����� �������� 
	 * @return int[]
	 */
	public static int[] dz_20_task_1_5(int[][] inputMatrix, int lowerValue, int upperValue){
		
		int[] result = null;
		int elementsCount = 0;
		
		for (int i = 0; i < inputMatrix.length; i++) {
			for (int j = 0; j < inputMatrix[i].length; j++) {
				if ((inputMatrix[i][j] > upperValue) || (inputMatrix[i][j] < lowerValue)) {
					elementsCount++;
				}
				
			}
		}
		
		if (elementsCount > 0) {
			
			result = new int[elementsCount];
			int ind = 0;
			
			for (int i = 0; i < inputMatrix.length; i++) {
				for (int j = 0; j < inputMatrix[i].length; j++) {
					if ((inputMatrix[i][j] > upperValue) || (inputMatrix[i][j] < lowerValue)) {
						result[ind] = inputMatrix[i][j];
						ind++;
					}
					
				}
			}
			
		} else {
			System.out.println("������ �������� �� �������!");
		}
				
		return result;
		
	} 
	
}
