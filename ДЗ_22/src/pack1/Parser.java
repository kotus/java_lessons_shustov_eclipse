package pack1;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import gui.*;

public class Parser {

	static public Client[] arrClient;
	
	public static void main(String[] args) throws IOException {

		
		
//		System.out
//				.println("------------------------------------------���������------------------------------------");
//		for (int i = 0; i < parser.arrClient.length; i++) {
//			// System.out.println( arrCl[ i ].fam + "  "+ arrCl [ i ].name +
//			// "  "+ arrCl [ i ].surName /*...*/ );
//
//			// ������ �������������� ����� toString() � ����� ������ Client
//			// ��� �������� ������������� � sysout
//			System.out.println(parser.arrClient[i]);
//		}
		

	}

	public static Client[] makeMassClient(String fileName) throws IOException {
			
		BufferedReader strBuf = new BufferedReader(new FileReader(fileName));
		String str;
		int count = 0;
		while ((str = strBuf.readLine())!=null) {
			if (str.trim().equals("")) continue;
			count++;
		}
		
		Parser.arrClient = new Client[count];
		
		parseManagerSimple(fileName);
		
		return Parser.arrClient;
		
	}
	
	private static void parseManagerSimple(String fileName)
			throws IOException {
		BufferedReader strBuf = new BufferedReader(new FileReader(fileName));
		String str;
		int i = -1;
		for (; (str = strBuf.readLine()) != null;) {

			// ���� ������ ������ - �� �� ������������ ��
			if (str.trim().equals(""))
				continue;

			// ������� ������ �������� �������
			i++;
			//Parser.arrClient[i] = new Client();

			// �������� �������� - �������� ���������� ������ ���-��, ������ //
			// -- #
			if (str.indexOf("//") != -1) {
				str = str.substring(0, str.indexOf("//")); // indexOf()
															// ���������� ���
															// -1, ��� ������
															// ���������
															// ������-���������
															// �
															// ������-���������
			}

			System.out.println(str); // ��� �������

			// ������� ������ �����������
			StringTokenizer tokzer = new StringTokenizer(str, " :\t");

			// � ����� ������ �� ���� ������ ������, ���� �� ����� ������
			// �������� ����� - nextToken
			// ������ �� ������, ������ �����
			String word;
			String[] arrOfFio = null;
			
			while (tokzer.countTokens() > 0) {

				word = tokzer.nextToken(); // ������ ��������� �����
				System.out.println("������� �����: " + word);
				
				// �������� ������� ����� �� ����� ��������� �������
				if (word.equalsIgnoreCase("�������")) { // ���� �����������
														// �������� �����
														// �������
					// 4. ������� ������ �����-��������,
					String passpNum = readPassport(tokzer);
				} else if (word.equalsIgnoreCase("���")) { // ���� �����������
															// �������� �����
															// ���
					// 4. ������� ������ �����-��������,
					Long idCod = readCod(tokzer);
				} else if (word.equalsIgnoreCase("ZIP")) { // ���� �����������
															// �������� �����
															// ���
					// 4. ������� ������ �����-��������,
					Integer zip = readZIP(tokzer);
				} else if (word.equalsIgnoreCase("���")) { // ���� �����������
															// �������� �����
															// ���
					// 4. ������� ������ �����-��������,
					arrOfFio = readFIO(tokzer);
				}
				
			}// while - ������ ����� � ������

			Parser.arrClient[i] = new Client(arrOfFio[0], arrOfFio[1], arrOfFio[2]);
						
		}

	} // parseManagerSimple

	// -----------------------------------------------------------------------------------------------------
	// ������-�������� ���� �������� ����
	private static String readPassport(StringTokenizer tokzerPar) {
		// System.out.println("----�������� ������ �������� ������----");

		// ��������� ����� ��������
		// ����� �������� ���������� � ������ ��������� str �� ��� ����� ���
		// ���� � ���,
		// ��� (� ����� ������� ������ str) ��������� ����� ��������, ����� ���
		// �������
		// ����� ������������ ����� str.indexOf("�������")

		String passpNum = tokzerPar.nextToken();
		System.out.println("����� ��������: " + passpNum);
		return passpNum;
	}

	private static Long readCod(StringTokenizer tokzerPar) {
		// System.out.println("----�������� ������ ����  ������----");

		// ��������� ����� ��.����
		Long idCode = Long.parseLong(tokzerPar.nextToken());
		System.out.println("�� ���: " + idCode);
		return idCode;
	}

	private static Integer readZIP(StringTokenizer tokzerPar) {
		// System.out.println("----�������� ������ ZIP  ������----");

		// ��������� ����� ��.����
		Integer ZIP = Integer.parseInt(tokzerPar.nextToken());
		System.out.println("ZIP: " + ZIP);
		return ZIP;
	}

	private static String[] readFIO(StringTokenizer tokzerPar) {
		
		String[] arrOfFio = new String[3];
		arrOfFio[0] = tokzerPar.nextToken();
		arrOfFio[1] = tokzerPar.nextToken();
		arrOfFio[2] = tokzerPar.nextToken();
		
		return arrOfFio;
		
	}

	
	
}

//class Client {
//	String fam, name, surName; // ���
//	String passpNum;
//	int zip;
//	long idCod;
//
//	public String toString() {
//		return this.fam + "  " + this.name + "  " + this.surName + "  "
//				+ this.passpNum + "  " + this.zip + "  " + this.idCod;
//	}
//
//}
