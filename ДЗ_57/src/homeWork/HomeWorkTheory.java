package homeWork;

import java.util.Arrays;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;


public class HomeWorkTheory {

	public static void main(String[] args) {

		Student[] massSt = { new Student("������", "����"),
				new Student("������", "����"), 
				new Student("�������", "����"),
				new Student("������", "����") };
		
		HashSet<Student> objHashSet = new HashSet<Student>(Arrays.asList(massSt));

		Student etalon = new Student("������", "����");
		//System.out.println(objHashSet.contains(etalon));
		
//		for (Student student : objHashSet) {
//			if(student.equals(etalon)) System.out.println("������! " + student);
//		}
		
		TreeSet<Student> objTreeSet = new TreeSet<Student>(Arrays.asList(massSt));
		SortedSet<Student> setOfStudents = objTreeSet.subSet(etalon, true, etalon, true);
		if (setOfStudents.size() > 0 ) System.out.println("������! "+setOfStudents.first());
		
	}

}

class Student implements Comparable<Student> {
	
	String lastName;
	String name;
	int ID;
	long summ;

	static int count = 0;

	//
	public Student(String lastNamePar, String namePar) {

		lastName = lastNamePar;
		name = namePar;

		count++;
		ID = count;
		summ = ID * 1000;
	}

	public Student(String lastNamePar, String namePar, int ID_Par, long sumPar) {
		// super();
		/* this. */lastName = lastNamePar;
		/* this. */name = namePar;

		count++;
		ID = ID_Par;
		summ = sumPar;
	}

	public Student() {
		// TODO Auto-generated constructor stub
	}

	// @Override //����� ��������� comparable
	public int compareTo(Student oPar) {// �������� ����
	
		Student tmp = oPar;

		int res = lastName.compareTo(oPar.lastName);
		return (res == 0 ? -name.compareTo(oPar.name) : -res);
	}// compareTo

	// ��� ������ ��������� �� �����
	public String toString() {
		return ID + " " + lastName + " " + name + " " + summ;
	}

	// ----------------------------------------------------------------------
	// ��� ����, ����� ����� � ������������� ���������� �� �� �������-������, �
	// �� ��������� ����� -
	// ����� �������������� ������ ������() � ������()

	// ������ �������� ������� ������() � ������() �� ����� ��������������
	// ��������
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());

		// ����, ������� ����� ���� �� ��������� � ������� �� ������ ���������
		// �������
		// � ������������ �������
		// �� ���� ������� ������ ������������ �� ������ ��������������� ����
		// ����� ��������
		// result = prime * result + ID;
		// result = prime * result + (int) (summ ^ (summ >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		// ���� �������� �� ����, ��� ������-�������� ������ contains() - �.�.
		// ������
		// ���� �������� ������ equals()

		Student other = (Student) obj;
		if (lastName == null) { // ���� � �������� ������ ������� Null - �� ��
								// ���������� �������
			// � ���������� ����� ������
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) { // ���� � �������� ������ ����� Null - �� ��
							// ���������� �����
		// � ���������� ����� ������
		} else if (!name.equals(other.name))
			return false;
		if (ID == 0) { // ���� � �������� ������ ID ���� - �� �� ����������
						// �����
		// � ���������� ����� ������

			
		} 
		/*
		else if (ID != other.ID)
			return false;
		if (summ == 0) { // ���� � �������� ������ summ ���� - �� �� ����������
							// �����
		// � ���������� ����� ������

		} else if (summ != other.summ)
			return false;*/
		return true; // ���� ��� �������� - �� ������� ������
	}

}// class