package homeWork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import bankSystem.Client;
import bankSystem.Summ;

public class HomeWork {

	public static void main(String[] args) throws IOException {
		
		ArrayList<Client> arrayListOfClients = Client.makeCollClients("src/ClientsAllAcc.in");
		HashMap<Integer, Client> hashMap = new HashMap<Integer, Client>();
		for (Client client : arrayListOfClients) {
			client.getBankCard().setSumm(new Summ(1000L, "UAH"));
			client.getAccount().setSumm(new Summ(2000L, "UAH"));
			hashMap.put(client.getID(), client);
		}
		
		TreeMap<Integer, Client> treeMap = new TreeMap<Integer, Client>(new Comparator<Integer>() {
			@Override
			public int compare(Integer key_1, Integer key_2) {
				int res = key_1.compareTo(key_2);
				return ((res == 0) ? -1: res);
			}
		});
		
		treeMap.putAll(hashMap);
				
		NavigableMap<Integer,Client> finalTreeMap = treeMap.headMap(7, true);
		
		System.out.println(finalTreeMap);
			
		
	}

}
