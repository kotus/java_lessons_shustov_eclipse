package lesson;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

//���� - ������������ ��� ����-��������
//����� - ���������, � �������� - ����� �����������
//� ������ ������ ����� �������, ���� ����� ��� ������. � � ��������� - ������ �������
//� ����� ���  ����������, 


public class MapMy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1. ����������
		Map map1 = new TreeMap<Integer, String>();
		TreeMap<Integer, String>  treeMap = new TreeMap();   //����� �����, � ���� ��������
		HashMap<String, String> hashMap = new HashMap();  //����� �������, � ������������ ��������������� ������ 
																								//������() � ������() ��� ������ � ��������� �� ������� �����
																							//������ ��� ������ � ����� �� �������� ����
		
				//�������� ��������
		//1�. ���������� ��� ���������� � ���, ������ ������ add() -  ����� put()
		 //for (String elem : map1) {		}   //� ����� ���� ����������, �.�. ��� �� ��������� ��������� ��������
		
		map1.put(1, new Student("������", "����")) ;
		map1.put(2, new Student("�������", "�����")) ;
		map1.put(3, new Student("������", "����")) ;
		map1.put(4, new Student("������", "����")) ;
		
		map1.put(4, new Student("���������", "����")) ;  //��������� ��������� ������ ������� �����, 
																								//�.�. ���� ��� ����� ��
		map1.put(5, 7);    //�.�. ��� ���������� ���� ����� ��� ����������, �� ��� �������� �����,
										//�.�. ��� ���������� ����� � �������� ���� ������� �������� - � � ����� 
										//����� ������  ���� ��������� �������� - ������� ���� ���� � ���� ������������
		
		System.out.println( map1 ); //�� ������������� �� ���, �.�. ������� ��������� � 2� ����� ���, � ��������� 
													//� ���������� �� ������������ � ����� ��� �������� 
		
		
		//2� �����, �������
		//������: �������� ��������� � ������ 2 � �����
			//1. �������, ��� ����� ����, �� ���� �� ����� ����������� �� ������, �.�. �� ����� �� �������� �� ���������
			//2. ��� ������� ������ ��� ���� ������������� � ���, 
			//    ������ ����� ���, ������� ��� ������� ������ ����
		Set setFromMap = map1.entrySet() ;  //����� ���������� ���, � ������� ���������� �������� ���� ����-��������
			//����� ������� ��� ��� ���
		Set  setEntry = setFromMap ;
			//3. ����� ���� �� ����
				//��� ���������
		for (Object elem : setEntry) {  //���� ���������� ENTRY (����)
			//����������� elem  � �����-����, �.�. ��� ��������� �� �������� � ����� elem ���� Object - 
			//� � ���� ��� ��������� �������
			//elem.
			Map.Entry entry = (Map.Entry) elem;
			Integer key1 = (Integer) entry.getKey();
			
			if (key1 >= 2)
				System.out.println( entry.getValue());
	/*		Student val1 = new Student();
			val1 = (Student) entry.getValue();
			entry.setValue( new Student( "���������", "���")) ;
			*/
		}
		//System.out.println( map1);

		//�� �� ������ � ������� ����-�������� - ������������ �� ������ - �.�. �� ������
		treeMap.putAll(map1);  //�������� ������, � �������� ����������� ����� 
											//�������� �� �����������, �.�. � ��������� ��� ����������
		
		System.out.println("�������� � ������ 2 � �����: " + treeMap.tailMap(2, true));
		
		
		
		
		

	} //main

}

//����� � ����������� Comparable  
//��� ������� ����� ������ �������, ����� ����� ����������� � ������ ������ ������ ��������� 
//��������� ������� � ����������� �������
		class Student  implements Comparable<Student> {//�������� ��� �������� �������������� ���������
													//����� ����� ����� ������� ������� � ������� � � Student 
			String lastName;
			String name;
			int ID;
			long summ;
			
			static int count = 0;
					//
			public Student(String lastNamePar, String namePar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = count;
				summ = ID * 1000;
			}
			
			public Student(String lastNamePar, String namePar, int  ID_Par, long sumPar) {
				//super();
				/*this.*/lastName = lastNamePar;
				/*this.*/name = namePar;
				
				count++;
				ID = ID_Par;
				summ = sumPar;
			}
			
			public Student() {
						// TODO Auto-generated constructor stub
					}
			//@Override	//����� ��������� comparable
			public int compareTo(Student oPar) {//�������� ����
				// TODO Auto-generated method stub
				Student tmp = oPar;
					
				int res = lastName.compareTo(oPar.lastName);
				return (res == 0  ? -name.compareTo(oPar.name): -res);
			}//compareTo
			
					//��� ������ ��������� �� �����
			public String toString (){
				return ID + " "+lastName + " "+name + " " + summ;
			}
			
			//----------------------------------------------------------------------
			//��� ����, ����� ����� � ������������� ���������� �� �� �������-������, � �� ��������� ����� - 
			//����� �������������� ������ ������() � ������()
			
			//������ �������� ������� ������() � ������() �� �����  �������������� ��������
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result
						+ ((lastName == null) ? 0 : lastName.hashCode());
				result = prime * result
						+ ((name == null) ? 0 : name.hashCode());
				
			//����, ������� ����� ���� �� ��������� � ������� �� ������ ��������� ������� 
				//� ������������ �������
			//�� ���� ������� ������ ������������ �� ������ ��������������� ���� ����� ��������
	//				result = prime * result + ID;
	//				result = prime * result + (int) (summ ^ (summ >>> 32));
				return result;
			}
			
			@Override
			public boolean equals(Object obj) {  
				//���� �������� �� ����, ��� ������-�������� ������ contains()  - �.�. ������
				//���� ��������  ������ equals()
	
				Student other = (Student) obj;
				if (lastName == null) {   //���� � �������� ������ ������� Null - �� �� ���������� ������� 
											         //� ���������� ����� ������
				} else if (!lastName.equals(other.lastName))
					return false;
				if (name == null) { //���� � �������� ������ ����� Null - �� �� ���������� ����� 
												//� ���������� ����� ������
				} else if (!name.equals(other.name))
					return false;
				if ( ID == 0 ) {		//���� � �������� ������ ID ���� - �� �� ���������� ����� 
											//� ���������� ����� ������
					
				}
				else if (ID != other.ID)  
					return false;
				if (summ == 0) {		//���� � �������� ������ summ ���� - �� �� ���������� ����� 
												//� ���������� ����� ������
					
				}
				else if (summ != other.summ)
					return false;
				return true;		//���� ��� �������� - �� ������� ������
			}
			

			
		}//class
		

